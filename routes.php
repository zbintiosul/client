<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function()
{
    //return View::make('hello');
    return Redirect::to('/');
});

Route::group(array('before' => 'auth'), function()
{
    Route::group(array('before' => 'authUserManagement'), function()
    {
        Route::controller('users', 'UserManagementController');
    });
    Route::group(array('before' => 'authSettings'), function()
    {
        Route::controller('settings', 'SettingController');
    });
    Route::group(array('before' => 'Broker'), function()
    {
        Route::controller('broker', 'BrokerController');
    });

    Route::controller('notification', 'NotificationController');
    Route::controller('client', 'ClientController');

    Route::controller('profile', 'ProfileController');
    Route::get('/', 'HomeController@getIndex');
    Route::get('profile', 'ProfileController@getIndex');
    Route::get('/account/logout', 'AccountController@getLogout');

});

//Route::get('/account/is-logged', 'AccountController@getIsLogged');
//Route::get('/account/email-change-confirm/{email}/{crypt}', 'AccountController@getEmailChangeConfirm');


Route::get('/get-policy/{token}', 'HomeController@getPolicy');

Route::group(array('before' => 'guest'), function()
{
    Route::controller('account', 'AccountController');
    //Route::controller('reminder', 'RemindersController');
});


//Route::controller('common', 'CommonController');



