<!-- Modal -->
<div class="modal fade" id="emailToClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Отправить предложения
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('OfferController@postSendEmailToClient')}}" id="email-to-client" class="smart-form" method="post" onsubmit="return false;">
                    <input type="hidden" name="offer_id" value="{{$offer->id}}">
                    <fieldset>
                        <section>
                            <label class="label">Электронная почта</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="email" placeholder="Электронная почта">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> Электронная почта</b>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Отправить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $emailToClient = $('#email-to-client').validate({
            // Rules for form validation
            rules : {
                'email' : {
                    required : true,
                    email :true
                }
            },

            // Messages for form validation
            messages : {
                'email' : {
                    required : 'Пожалуйста, заполните это поле',
                    email : 'Пожалуйста, введите действующий адрес электронной почты'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#emailToClient').modal('hide');
                        //loadURL("{{action('ClientController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }

</script>