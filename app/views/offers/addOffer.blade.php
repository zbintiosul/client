<!-- Modal -->
<div class="modal fade" id="addOffer" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Новая предложения
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('OfferController@postAddOffer')}}" id="add-offer" class="smart-form" method="post">
                    <input type="hidden" name="policy_id" value="{{$policy->id}}">
                    <fieldset>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma" placeholder="Страховая сумма">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia" placeholder="Страховая премия" value="{{$policy->calcTotalSumInt()}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label">Франшиза</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="fransiza" placeholder="Франшиза">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                        <section>
                            <label class="label">АКЦИЯ</label>
                            <textarea rows="6" id="mymarkdown61" class="custom-scroll mark-down-text-area" name="baner">
                             <strong>АКЦИЯ<br> НЕДЕЛИ!</strong>
                            </textarea>
                        </section>
                        <section>
                            <label class="label mandatory">Фирма *</label>
                            <label class="select">
                               <select name="company_id">
                                   <option value="0"  selected disabled>Выберите фирму</option>
                                   @foreach (Company::orderBy('name','asc')->get() as $company)
                                   <option value="{{$company->id}}">{{$company->name}}</option>
                                   @endforeach
                               </select>
                               <i></i> </label>
                        </section>
                        @if ($policy->type->id==2)
                        <section>
                            <label class="label">Условия страхования по основным рискам</label>
                            <textarea rows="8" id="mymarkdown1" class="custom-scroll mark-down-text-area" name="terms">
                                <p>
                                    "По рискам «УЩЕРБ» и «АВТОКАСКО» обращение Страхователя (Выгодоприобретателя, лиц, допущенных к управлению ТС) в компетентные органы и предоставление Страховщику соответствующих документов компетентных органов не является обязательным в случае:
                                    <br>
                                    ?&nbsp; Повреждения следующих стеклянных элементов: стекла кузова (ветрового, заднего, бокового опускного, бокового подвижного/ неподвижного двери/кузова), зеркального элемента наружного зеркала заднего вида, рассеивателя фары головного освещения (блок фары), рассеивателя заднего фонаря, рассеивателя повторителя указателя поворота. Если иное не оговорено в договоре страхования, данное положение распространяется на неограниченное количество страховых случаев за весь срок действия договора страхования.
                                    <br>
                                    При повреждении любых иных стеклянных элементов - обращение Страхователя (Выгодоприобретателя, лиц, допущенных к управлению ТС) в компетентные органы и предоставление Страховщику документов компетентных органов является обязательным.
                                    <br>
                                    При незначительных повреждениях стеклянных элементов допускается&nbsp; восстановление стеклянного элемента осуществлять путем проведения ремонта на специализированных СТОА.
                                    <br>
                                    ?&nbsp; Если сумма ущерба не превышает 2% от страховой суммы по риску «УЩЕРБ» или по риску «АВТОКАСКО», но не более 30 000 рублей (применяется наименьшее из указанных значений), что является лимитом возмещения при страховании на условиях, когда обращение Страхователя (Выгодоприобретателя, лиц, допущенных к управлению ТС) в компетентные органы и предоставление Страховщику соответствующих документов компетентных органов не является обязательным, если иное не оговорено в договоре страхования. Данное положение распространяется на один страховой случай за весь срок действия договора страхования, если в договоре страхования не оговорено иное.
                                    <br>
                                    При этом Страховщик не возмещает ущерб по устранению обнаруженных скрытых повреждений.
                                    <br>
                                    Выплата страхового возмещения без обращения в компетентные органы и предоставления документов из государственных компетентных органов осуществляется только путем оплаты восстановительного ремонта по направлению Страховщика, если договором страхования не предусмотрено иное (п. 14.12 Правил от 03.04.2013)."&nbsp;&nbsp;
                                    <br>&nbsp;
                                 </p>
                            </textarea>
                        </section>
                        <section>
                            <label class="label">Дополнительные услуги</label>
                            <textarea rows="6" id="mymarkdown21" class="custom-scroll mark-down-text-area" name="additional_services">
                                <p>
                                    Эвакуатор: "Предоставляется бесплатно до места ремонта или парковки при страховом случае, в результате которого автомобиль получил повреждения и не способен самостоятельно передвигаться:
                                    <br>
                                    - один раз в течение действия договора страхования. При этом Страховщик вправе выдать второй талон на эвакуацию в период действия одного Договора страхования в случае, если первая эвакуация была произведена в результате страхового случая с наличием регрессного требования. Для вторичного получения талона на эвакуацию Страхователю необходимо обратиться к Страховщику (к специалисту, ведущему его выплатное дело) с письменным заявлением."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <br>
                                    "В компетентные органы - незамедлительно с момента, как Страхователь узнал или должен был узнать о наступлении события имеющего признаки страхового случая:
                                    <br>
                                    - при угоне (хищении) – в органы МВД;
                                    <br>
                                    - при ДТП – в органы ГИБДД;
                                    <br>
                                    - при пожаре – в МЧС РФ;
                                    <br>
                                    - при повреждении (уничтожении) упавшим предметом, в том числе при падении посторонних предметов, деревьев, снега, льда – в органы МВД;
                                    <br>
                                    - при повреждении (уничтожении) отскочившим предметом, в том числе выброса гравия, камней из-под колес транспорта – в органы ГИБДД;
                                    <br>
                                    - при повреждении (уничтожении) в результате противоправных действиях третьих лиц – в органы МВД, а в результате террористического акта - также в органы ФСБ;
                                    <br>
                                    - при повреждении (уничтожении) животными - в органы МВД;
                                    <br>
                                    - при повреждении (уничтожении) в результате опасных природных явления – в органы по гидрометеорологии и мониторингу окружающей среды, МЧС и МВД.
                                    <br>
                                    Страховщику - письменное Заявление о выплате (по установленной форме) - в течение 3 рабочих дней, а при угоне (хищении) ТС в течение 2 рабочих дней с момента наступления страхового случая (п. 12.4.11 Правил от 03.04.2013)."
                                </p>
                            </textarea>
                        </section>
                        @endif
                    </fieldset>
                    @if ($policy->type->id==2)
                    <fieldset>
                        <legend>ГРАЖДАНСКАЯ ОТВЕТСТВЕННОСТЬ</legend>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma_grajdanscaia" placeholder="Страховая сумма">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia_grajdanscaia" placeholder="Страховая премия">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>ДОБРОВОЛЬНОЕ СТРАХОВАНИЕ</legend>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma_dobrovolina" placeholder="Страховая сумма">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia_dobrovolina" placeholder="Страховая премия">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    @endif
                    <fieldset>
                        @if ($policy->type->id==2)
                        <section>
                            <label class="label">Станции технического обслуживания</label>
                            <textarea rows="6" id="mymarkdown51" class="custom-scroll mark-down-text-area" name="stantii">
                             <p>
                                <strong>
                                    <ul style="padding-left:0px;">
                                        <li>
                                            //text here
                                        </li>
                                        <li>
                                            //text here
                                        </li>
                                    </ul>
                                </strong>
                            </p>
                            </textarea>
                        </section>
                        @endif
                        <section>
                            <label class="label">Комментарий</label>
                            <textarea rows="6" id="mymarkdown31" class="custom-scroll mark-down-text-area" name="comments"></textarea>
                        </section>
                        <section>
                            <label class="label">Signature <button type="button" class="btn btn-default btn-xs" onclick="resetSignature()">reset</button></label>
                            <textarea rows="6" id="mymarkdown41" class="custom-scroll mark-down-text-area" name="signature">
                            <table cellpadding="0" cellspacing="0" style="width: 100%; padding:10px;">
                                        <tbody>
                                            <tr>
                                                 <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px;text-align:center; font-size: 14px; vertical-align: middle; width:20%;">

                                                     <p>
                                                        <strong>С уважением,<br>
                                                        {{$policy->client->agent()->firstname}} {{$policy->client->agent()->lastname}}</strong>
                                                    </p>
                                                </td>
                                                 <td style="background-color:#dadada; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;margin-top:15px;">
                                                    <p>
                                                        <img width="256px" border="0" src="http://ins24.ru/images/logo.png">
                                                    </p>
                                                </td>
                                                 <td style="background-color:#0c54a0; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;">
                                                    <p>
                                                      <strong>
                                                        129110
                                                        Проспект Мира, д.68, <br>стр.1А, оф.307<br>
                                                         тел:</strong>
                                                         <span style="color:white;">+{{$policy->client->agent()->work_phone}}</span>.
                                                    </p>
                                                </td>
                                                <td style="background-color:#0c54a0; color:#ffffff; padding-right:50px; text-align:center; font-size: 14px; vertical-align: middle;">
                                                    <p>
                                                        <strong>E-mail:</strong>
                                                        <br>
                                                        <strong>
                                                            <a style="color:#ffffff" href="mailto:{{$policy->client->agent()->email}}">{{$policy->client->agent()->email}}</a>
                                                        </strong>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                            </table>
                            </textarea>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    function resetSignature(){
        //CKEDITOR.instances.mymarkdown41.setData('');
        CKEDITOR.instances.mymarkdown41.setData('<table cellpadding="0" cellspacing="0" style="width: 100%; padding:10px;">'+
                                                 '<tbody>'+
                                                     '<tr>'+
                                                         '<td style="background-color:#0c54a0; color:#ffffff; padding-left:50px;text-align:center; font-size: 14px; vertical-align: middle; width:20%;">'+

                                                              '<p>'+
                                                                 '<strong>С уважением,<br>'+
                                                                 '{{$policy->client->agent()->firstname}} {{$policy->client->agent()->lastname}}</strong>'+
                                                             '</p>'+
                                                         '</td>'+
                                                          '<td style="background-color:#dadada; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;margin-top:15px;">'+
                                                             '<p>'+
                                                                 '<img width="256px" border="0" src="http://ins24.ru/images/logo.png">'+
                                                             '</p>'+
                                                         '</td>'+
                                                          '<td style="background-color:#0c54a0; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;">'+
                                                             '<p>'+
                                                               '<strong>'+
                                                                '129110'+
                                                                'Проспект Мира, д.68, <br>стр.1А, оф.307<br>'+
                                                                'тел:</strong>'+
                                                                '<span style="color:white;">+{{$policy->client->agent()->work_phone}}</span>.'+
                                                             '</p>'+
                                                         '</td>'+
                                                         '<td style="background-color:#0c54a0; color:#ffffff; padding-right:50px; text-align:center; font-size: 14px; vertical-align: middle;">'+
                                                             '<p>'+
                                                                 '<strong>E-mail:</strong>'+
                                                                 '<br>'+
                                                                 '<strong>'+
                                                                     '<a style="color:#ffffff" href="mailto:{{$policy->client->agent()->email}}">{{$policy->client->agent()->email}}</a>'+
                                                                 '</strong>'+
                                                             '</p>'+
                                                         '</td>'+
                                                     '</tr>'+
                                                 '</tbody>'+
                                     '</table>');
    }

    	   	// PAGE RELATED SCRIPTS

    	// pagefunction

	var pagefunction = function() {

		CKEDITOR.replace( 'mymarkdown41', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
		CKEDITOR.replace( 'mymarkdown31', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
		CKEDITOR.replace( 'mymarkdown61', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
@if ($policy->type->id==2)
		CKEDITOR.replace( 'mymarkdown51', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );

		CKEDITOR.replace( 'mymarkdown21', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
		CKEDITOR.replace( 'mymarkdown1', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
@endif
	};

	// end pagefunction

	// run pagefunction on load
	loadScript("js/plugin/ckeditor/ckeditor.js", pagefunction);
    function CKupdate(){
        for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();

    }
    
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addoffer = $('#add-offer').validate({
            // Rules for form validation
            rules : {
                strahavaia_suma : {
                    required : true,
                    number : true
                },
                strahavaia_premia : {
                    required : true,
                    number : true
                },
                fransiza : {
                    number : true
                },
                strahavaia_suma_grajdanscaia : {
                    required : true,
                    number : true
                },
                strahavaia_premia_grajdanscaia : {
                    required : true,
                    number : true
                },
                strahavaia_suma_dobrovolina : {
                    required : true,
                    number : true
                },
                strahavaia_premia_dobrovolina : {
                    required : true,
                    number : true
                },
                company_id  : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                strahavaia_suma : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                fransiza : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_suma_grajdanscaia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia_grajdanscaia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_suma_dobrovolina : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia_dobrovolina : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                company_id : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                CKupdate();
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addOffer').modal('hide');
                        loadURL("{{action('OfferController@getOffers')}}/{{$policy->id}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

</script>