<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Предложении
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Новые </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <button class="btn btn-success" onclick="getModal('{{action('OfferController@getModalAddOffer')}}/{{$policy->id}}','#addOffer')">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новая предложения</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{$offers->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th>Фирма</th>
                                <th>Страховая премия</th>
                                <th>Sent times</th>
                                <th>Обновлена</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($offers as $offer)
                            <tr id="tr_offer_{{$offer->id}}" idOffer="{{$offer->id}}">
                                <td class="custom-table-number hidden-xs">{{$offer->id}}</td>
                                <td  class="custom-data-column">
                                    <div class="txt-color-darken">{{$offer->company->name}}</div>
                                </td>
                                <td  class="custom-data-column">
                                    <div>{{Locale::number($offer->strahavaia_premia)}}</div>
                                </td>
                                <td class="custom-data-action">
                                        {{$offer->sent_times}}
                                </td>
                                <td class="custom-data-action">
                                    {{$offer->updated_at->format('d-m-Y H:i:s')}}
                                </td>
                                <td class="custom-data-action">
                                    <div class="columncontrols">
                                        <div class="btn-group" style="width: 140px!important;">
                                        <a href="{{action('OfferController@getViewOffer')}}/{{$offer->id}}" class="btn btn-sm btn-default ajaxa" title="Просмотр"><i class="fa fa-eye"></i></a>
                                            <button class="btn btn-sm btn-success" onclick="getModal('{{action('OfferController@getModalSendEmailToClient')}}/{{$offer->id}}','#emailToClient')" title="Отправить предложения">
                                                <i class="fa fa-envelope-o"></i>
                                            </button>
                                            <button class="btn btn-sm btn-default" onclick="getModal('{{action('OfferController@getModalEditOffer')}}/{{$offer->id}}','#editOffer')" id="editOffer_{{$offer->id}}" title="Редактировать">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-sm btn-danger" onclick="deleteOffer({{$offer->id}})" id="deleteCompany_{{$offer->id}}" title="Удалить">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$offers->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function deleteOffer(id)
        {
            $.SmartMessageBox({
                title: "Удалить  Предложениe",
                content: "Вы уверены?",
                buttons: '[Нет][Да]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Да") {
                    loading('show');
                    $.ajax({
                        type: "POST",
                        url: "{{action('OfferController@postDeleteOffer')}}/" + id
                    }).done(function (msg) {
                        $('#tr_offer_' + id).remove();
                        $('#tr_edit_offer').remove();
                        $.smallBox({
                            title: "{{Lang::get('client.success')}}",
                            content: "{{Lang::get('client.successText')}}",
                            color: "rgb(115, 158, 115)",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    })
                        .fail(function () {
                            $.smallBox({
                                title: "{{Lang::get('client.error')}}",
                                content: "{{Lang::get('client.errorText')}}",
                                color: "#c26565",
                                iconSmall: "fa fa-times bounce animated",
                                timeout: 4000
                            });
                            loading('hide');
                        });
                }
                if (ButtonPressed === "Нет") {
                    //nothing
                }
            });
        }
</script>