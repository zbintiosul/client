<!DOCTYPE html>
<html lang="en">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div style="width: 970px; margin: auto; padding: 5px; background-color:#ffffff; font-size:14px; line-height:1.428; font-family:Tahoma">

        <p style="text-align: left; padding-left:50px;">
            <img src="{{$offer->company->logo}}" style="height: 130px;">
        </p>

        <table cellpadding="0" cellspacing="10" style="width: 100%; ">
            <tbody>
                <tr>
                    <td style="background-color:#dadada; color:#000000; padding-left:50px; width:50px; text-align:left; font-size: 16px; -webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px;" colspan="2">
                        <p>
                            Добрый день <b>{{$offer->policy->client->firstname}} {{$offer->policy->client->secondname}}</b>,
                        </p>
                        <p>
                            Согласно нашей телефонной договоренности, высылаю Вам информацию, <br>касательно предложения страхования {{$offer->policy->type->name}} от <strong>{{$offer->company->name}}.</strong>
                        </p>
                    </td>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; width:133px; padding-right:50px; text-align:center; font-size: 24px;-webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; position:absolute">
                        <p>
                            {{$offer->baner}}
                        </p>
                    </td>
                </tr>
                <tr>
                    @if ($offer->policy->type->id==2)
                    <td style="width:50%; vertical-align: top;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="background-color:#c33823; color:#ffffff; padding-left:50px; -webkit-border-top-right-radius: 20px; -moz-border-radius-topright: 20px; border-top-right-radius: 20px;">
                                        <p>
                                            <strong style="font-size: 19px;">
                                                СТРАХОВАЯ СУММА:
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px;">
                                        <p>
                                            <strong style="font-size: 29px;">
                                                <span>{{Locale::number($offer->strahavaia_suma)}} руб</span>
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#dadada; color:#000000; padding-left:50px; padding-right:20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; text-align:justify">
                                        <em style="font-size: 10px;">Страховой суммой является определенная договором страхования денежная сумма, в пределах которой страховая компания обязуется при наступлении страхового случая (страховых случаев) выплатить страховое возмещение</em>.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    @endif

                    <td style="vertical-align: top; text-align:right;" colspan="2">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="background-color:#c33823; color:#ffffff; padding-right:50px; -webkit-border-top-left-radius: 20px; -moz-border-radius-topleft: 20px; border-top-left-radius: 20px;">
                                        <p>
                                            <strong style="font-size: 19px;">
                                                СТРАХОВАЯ ПРЕМИЯ:
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#0c54a0; color:#ffffff; padding-right:50px;">
                                        <p>
                                            <strong style="font-size: 29px;">
                                                <span>{{Locale::number($offer->strahavaia_premia)}} руб</span>
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                @if ($offer->fransiza!="0.00")
                                <tr>
                                    <td style="background-color:#dadada; font-size: 21px;line-height: 18px; padding-right:50px;-webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px;">
                                        <p>
                                            <strong>
                                                Франшиза:<span style="color:#0c54a0;"> {{Locale::number($offer->fransiza)}} руб</span>
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                @else
                                 <!---------------------------------------------CIND ESTE FRANCHIZA ACEST TR TREBUIE SA DISPARA  -->
                                        <tr>
                                            <td style="background-color:#dadada; color:#000000; padding-right:50px;-webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px;">
                                                  <br>
                                                  <em style="font-size: 10px;">Стоимость страховой защиты</em>.
                                                  <br>
                                                  <br>
                                            </td>
                                        </tr>
                                    <!--------------------------------------------END Dispare -->
                                    @endif
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        <table cellpadding="0" cellspacing="10" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; padding-right:50px; text-align:center; font-size: 16px; -webkit-border-top-left-radius: 20px;-webkit-border-top-right-radius: 20px;-moz-border-radius-topleft: 20px;-moz-border-radius-topright: 20px; border-top-left-radius: 20px;border-top-right-radius: 20px;" colspan="2">
                        <p>
                            <strong>
                                Объектом страхования, по основным рискам {{$offer->policy->type->name}}, является:
                            </strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#dadada; color:#0c54a0; padding-left:50px; padding-right:30px; text-align:center; font-size: 22px; -webkit-border-bottom-right-radius: 20px;-moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; width:50%; vertical-align: top;">
                        <p>
                           <strong>{{$offer->policy->car->modelCar->brand->name}} {{$offer->policy->car->modelCar->name}} @if ($offer->policy->car->year_of_construction)/ {{$offer->policy->car->year_of_construction}} г.@endif @if ($offer->policy->car->motor_ls)/ {{$offer->policy->car->motor_ls}} л.с.@endif</strong>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <table cellpadding="0" cellspacing="10" style="width: 100%;">
            <tbody>
               <tr>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; padding-right:50px; text-align:center; font-size: 16px; -webkit-border-top-left-radius: 20px;-webkit-border-top-right-radius: 20px;-moz-border-radius-topleft: 20px;-moz-border-radius-topright: 20px; border-top-left-radius: 20px;border-top-right-radius: 20px; -webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px;">
                        <p>
                            <strong>
                                Условия страхования по основным рискам:
                            </strong>
                        </p>
                    </td>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; padding-right:50px; text-align:center; font-size: 16px; -webkit-border-top-left-radius: 20px;-webkit-border-top-right-radius: 20px;-moz-border-radius-topleft: 20px;-moz-border-radius-topright: 20px; border-top-left-radius: 20px;border-top-right-radius: 20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px;">
                        <p>
                            <strong>
                                Дополнительные услуги:
                            </strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#dadada; color:#000000; padding-left:50px; padding-right:30px; text-align:justify; font-size: 13.5px; -webkit-border-top-right-radius: 20px;-moz-border-radius-topright: 20px; border-top-right-radius: 20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; width:50%; vertical-align: top;">
                        {{$offer->terms}}
                    </td>
                    <td style="background-color:#dadada; color:#000000; padding-left:30px; padding-right:50px; text-align:justify; font-size: 14px; -webkit-border-top-left-radius: 20px;-moz-border-radius-topleft: 20px; border-top-left-radius: 20px; -webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px; vertical-align: top;">
                        {{$offer->additional_services}}
                    </td>
                </tr>
            </tbody>
        </table>
         @if ($offer->policy->type->id==2)
         <table cellpadding="0" cellspacing="10" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; padding-right:50px; text-align:center; font-size: 16px; -webkit-border-top-left-radius: 20px;-webkit-border-top-right-radius: 20px;-moz-border-radius-topleft: 20px;-moz-border-radius-topright: 20px; border-top-left-radius: 20px;border-top-right-radius: 20px;" colspan="2">
                        <p>
                         <strong>По дополнительным рискам:</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; vertical-align: top;">
                         <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="background-color:#c33823; color:#ffffff; padding-left:50px; -webkit-border-top-right-radius: 20px; -moz-border-radius-topright: 20px; border-top-right-radius: 20px;">
                                        <p>
                                            <strong style="font-size: 15px;">
                                                Гражданская ответственность:
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#dadada; color:#0c54a0; padding-left:50px;">
                                      <p style="font-size: 15px;">
                                             Страховая сумма:
                                            <strong><span style="font-size: 19px;">{{Locale::number($offer->strahavaia_suma_grajdanscaia)}} руб</span></strong>
                                            <br>
                                            Страховая премия:
                                            <strong><span style="font-size: 19px;">{{Locale::number($offer->strahavaia_premia_grajdanscaia)}} руб</span></strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#dadada; color:#000000; padding-left:50px; padding-right:20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; text-align:justify">
                                        <em style="font-size: 10px;">Страховым случаем является наступление гражданской ответственности Страхователя за причинение вреда жизни, здоровью или имуществу Потерпевших в результате ДТП, совершенного с участием ТС</em>.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top; text-align:right">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="background-color:#c33823; color:#ffffff; padding-right:50px; -webkit-border-top-left-radius: 20px; -moz-border-radius-topleft: 20px; border-top-left-radius: 20px;">
                                        <p>
                                            <strong style="font-size: 15px;">
                                                Добровольнoe страхование от несчастных случаев:
                                            </strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#dadada; color:#0c54a0; padding-right:50px;">
                                        <p style="font-size: 15px;">
                                             Страховая сумма:
                                            <strong><span style="font-size: 19px;">{{Locale::number($offer->strahavaia_suma_dobrovolina)}} руб</span></strong>
                                            <br>
                                            Страховая премия:
                                            <strong><span style="font-size: 19px;">{{Locale::number($offer->strahavaia_premia_dobrovolina)}} руб</span></strong>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                     <td style="background-color:#dadada; color:#000000; padding-right:50px;-webkit-border-bottom-right-radius: 20px; -moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px;">
                                        <em style="font-size: 10px;">Страхование на случай смерти, телесных и тяжких телесных в результате несчастного случая.<br></em>.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table cellpadding="0" cellspacing="10" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="background-color:#0c54a0; color:#ffffff; padding-left:50px; padding-right:50px; text-align:center; font-size: 16px; -webkit-border-top-left-radius: 20px;-webkit-border-top-right-radius: 20px;-moz-border-radius-topleft: 20px;-moz-border-radius-topright: 20px; border-top-left-radius: 20px;border-top-right-radius: 20px;" colspan="2">
                          <p style="font-weight: bold;">
                                Станции технического обслуживания автомобилей марки {{$offer->policy->car->modelCar->brand->name}}:
                          </p>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#dadada; color:#000000; padding-left:50px; padding-right:50px; text-align:justify; font-size: 13px; -webkit-border-bottom-right-radius: 20px;-moz-border-radius-bottomright: 20px; border-bottom-right-radius: 20px; -webkit-border-bottom-left-radius: 20px; -moz-border-radius-bottomleft: 20px; border-bottom-left-radius: 20px; width:50%; vertical-align: top;" colspan="2">
                        {{$offer->stantii}}
                    </td>
                </tr>
            </tbody>
        </table>
        @endif
        @if ($offer->comments)
        <table cellpadding="0" cellspacing="10" style="width: 100%;">
            <tbody>
                 <tr>
                    <td style="background-color:#dadada; color:#000000; padding-left:50px; padding-right:50px; text-align:justify; font-size: 14px; -webkit-border-bottom-right-radius: 20px;
                    -moz-border-radius-bottomright:20px; border-bottom-right-radius: 20px; -webkit-border-top-left-radius: 20px; -moz-border-radius-topleft: 20px; border-top-left-radius: 20px; width:50%; vertical-align: top; margin-bottom:10px;" colspan="4" >
                        {{$offer->comments}}
                    </td>
                </tr>
            </tbody>
        </table>
        @endif
        @if ($offer->signature)
        {{$offer->signature}}
        @endif
    </div>
</html>