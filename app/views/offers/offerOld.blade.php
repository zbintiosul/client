<html dir="ltr">
<head>
    <title>Коммерческое Предложение</title>
</head>
<body>
<div style="width: 600px; margin: auto;border: 1px solid #ccc; padding: 5px;">
    <p style="text-align: center;"><img src="{{$offer->company->logo}}" style="width:499px;"></p>
    <table cellpadding="0" cellspacing="0" style="width: 100%;background-color: #ecf8ff;padding: 5px;">
        <tbody>
        <tr>
            <td style="padding: 5xp!important">
                <p>Добрый день, <b></b>,</p>

                <p>Согласно нашей телефонной договоренности, высылаю Вам информацию, касательно предложения страхования  от&nbsp;<strong>{{$offer->company->name}}.</strong></p>

                <p>&nbsp;</p>
            </td>
        </tr>
        @if ($offer->policy->type->id==2)
        <tr>
            <td style="padding: 5xp">
                <p><strong style="font-size: 19px;">СТРАХОВАЯ СУММА – <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_suma)}} руб</span></strong><br>
                    <em style="font-size: 10px;">Страховой суммой является определенная договором страхования денежная сумма, в пределах которой страховая компания обязуется при наступлении страхового случая (страховых случаев) выплатить страховое возмещение</em>.</p>
            </td>
        </tr>
        @endif
        <tr>
            <td style="padding: 5xp">
                <br>
                <p><strong style="font-size: 19px;">СТРАХОВАЯ ПРЕМИЯ – <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_premia)}} руб</span> </strong> <br>
                    <em style="font-size: 10px;">Стоимость страховой защиты</em><em> </em></p>
                @if ($offer->fransiza!="0.00")
                <p><strong>Франшиза – <span style="color: #2ba6cb">{{Locale::number($offer->fransiza)}} руб </span></strong></p>
                @endif
            </td>
        </tr>
        </tbody>
    </table>

    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>

    <p style="margin: 0px; background-color: #ecf8ff; padding: 5px"><u>Объектом страхования является</u>: <strong style="color: #3276b1">{{$offer->policy->car->modelCar->brand->name}} {{$offer->policy->car->modelCar->name}}  /  {{$offer->policy->car->year_of_construction}} г. /  {{$offer->policy->car->motor_ls}} л.с. </strong>, по основным риском {{$offer->policy->type->name}}
    <ul style="margin: 0px; padding: 0px; padding-left: 30px;background-color: #ecf8ff;">
        <li>
            <strong style="color: #3276b1">Ущерб</strong>
        </li>
        <li>
            <strong style="color: #3276b1">Хищение</strong>
        </li>
    </ul>
    </p>
    @if ($offer->policy->type->id==2)
    <table cellpadding="5" cellspacing="5" style="width:100%; ">
        <tbody>
        <tr>
            <td style="text-align: center; background-color: #ecf8ff;padding: 5px;border-right: 1px solid #fff">
                <p> <strong>Условия страхования по основным рискам</strong></p>
            </td>
            <td style="text-align: center; background-color: #ecf8ff; padding: 5px">
                <p><strong>Дополнительные услуги </strong></p>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td style="vertical-align: top; border-right: 1px solid #CCC ">
                {{$offer->terms}}
            </td>
            <td style="vertical-align: top; padding-left: 10px;">
                {{$offer->additional_services}}
            </td>
        </tr>
        </tbody>
    </table>

    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>

    <p style="margin-left:0.25in"><u>&nbsp;<strong>по дополнительным рискам:</strong></u></p>

    <table cellpadding="0" cellspacing="0" style="width:100%; ">
        <tbody>
        <tr>
            <td style="text-align: center; background-color: #ecf8ff;padding: 5px; width: 50%;border-right: 1px solid #fff">
                <p><strong>Гражданская ответсвенность</strong></p>
            </td>
            <td style="text-align: center; background-color: #ecf8ff;padding: 5px">
                <p><strong>Добровольние страхование от несчастных случаях</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; vertical-align: top; width: 50%; border-right: 1px solid #ccc">
                <p>Страховая сумма –  <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_suma_grajdanscaia)}} руб</span></p>
                <p>Страховая премия –   <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_premia_grajdanscaia)}} руб</span></p>
                <p><em style="font-size: 10px;">Страховым случаем является наступление гражданской ответственности Страхователя за причинение вреда жизни, здоровью или имуществу Потерпевших в результате ДТП, совершенного с участием ТС</em>.</p>
            </td>
            <td style="padding: 5px; vertical-align: top;">
                <p>Страховая сумма – <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_suma_dobrovolina)}} руб</span></p>

                <p>Страховая премия – <span style="color: #2ba6cb">{{Locale::number($offer->strahavaia_premia_dobrovolina)}} руб</span></p>
                <p><em style="font-size: 10px;">Страхование на случай смерти, телесных и тяжких телесных в результате несчастного случая.</em></p>

                <p>&nbsp;</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>
    <p style="font-weight: bold;">Станции технического обслуживания автомобилей марки {{$offer->policy->car->modelCar->brand->name}}</p>
    {{$offer->stantii}}
    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>
    <br>
    @endif
    @if ($offer->comments)
    {{$offer->comments}}
    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>
    @endif
    @if ($offer->signature)
    {{$offer->signature}}
    <p style="width: 100%; height: 10px;background-color: #2ba6cb; margin-top: 5px;margin-bottom: 5px"></p>
    @endif
</div>
</body>
</html>