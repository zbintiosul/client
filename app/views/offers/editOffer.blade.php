<!-- Modal -->
<div class="modal fade" id="editOffer" tabindex="-1"  data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Редактировать
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('OfferController@postEditOffer')}}" id="edit-offer" class="smart-form" method="post">
                    <input type="hidden" name="offer_id" value="{{$offer->id}}">
                    <fieldset>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma" placeholder="Страховая сумма" value="{{$offer->strahavaia_suma}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia" placeholder="Страховая премия" value="{{$offer->strahavaia_premia}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label">Франшиза</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="fransiza" placeholder="Франшиза" value="{{$offer->fransiza}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                        <section>
                            <label class="label">АКЦИЯ</label>
                            <textarea rows="6" id="mymarkdown6" class="custom-scroll mark-down-text-area" name="baner">
                             {{$offer->baner}}
                            </textarea>
                        </section>
                        <section>
                            <label class="label mandatory">Фирма *</label>
                            <label class="select">
                               <select name="company_id">
                                   <option value="0"  selected disabled>Выберите фирму</option>
                                   @foreach (Company::orderBy('name','asc')->get() as $company)
                                   @if ($company->id == $offer->company->id)
                                   <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                   @else
                                   <option value="{{$company->id}}">{{$company->name}}</option>
                                   @endif
                                   @endforeach
                               </select>
                               <i></i> </label>
                        </section>
                        @if ($offer->policy->type->id==2)
                        <section>
                            <label class="label">Условия страхования по основным рискам</label>
                            <textarea rows="8" id="mymarkdown" class="custom-scroll mark-down-text-area" name="terms">{{$offer->terms}}</textarea>
                        </section>
                        <section>
                            <label class="label">Дополнительные услуги</label>
                            <textarea rows="6" id="mymarkdown2" class="custom-scroll mark-down-text-area" name="additional_services">{{$offer->additional_services}}</textarea>
                        </section>
                        @endif
                    </fieldset>
                    @if ($offer->policy->type->id==2)
                    <fieldset>
                        <legend>ГРАЖДАНСКАЯ ОТВЕТСТВЕННОСТЬ</legend>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma_grajdanscaia" placeholder="Страховая сумма" value="{{$offer->strahavaia_suma_grajdanscaia}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia_grajdanscaia" placeholder="Страховая премия" value="{{$offer->strahavaia_premia_grajdanscaia}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>ДОБРОВОЛЬНОЕ СТРАХОВАНИЕ</legend>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label mandatory">Страховая сумма *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_suma_dobrovolina" placeholder="Страховая сумма" value="{{$offer->strahavaia_suma_dobrovolina}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label mandatory">Страховая премия *</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="strahavaia_premia_dobrovolina" placeholder="Страховая премия" value="{{$offer->strahavaia_premia_dobrovolina}}">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>[0-9]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    @endif
                    <fieldset>
                        @if ($offer->policy->type->id==2)
                        <section>
                            <label class="label">Станции технического обслуживания</label>
                            <textarea rows="6" id="mymarkdown5" class="custom-scroll mark-down-text-area" name="stantii">{{$offer->stantii}}</textarea>
                       </section>
                       @endif
                       <section>
                            <label class="label">Комментарий</label>
                            <textarea rows="6" id="mymarkdown3" class="custom-scroll mark-down-text-area" name="comments">{{$offer->comments}}</textarea>
                       </section>
                       <section>
                            <label class="label">Signature <button type="button" class="btn btn-default btn-xs" onclick="resetSignature()">reset</button></label>
                            <textarea rows="6" id="mymarkdown4" class="custom-scroll mark-down-text-area" name="signature">{{$offer->signature}}</textarea>
                       </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    function resetSignature(){
            //CKEDITOR.instances.mymarkdown41.setData('');
            CKEDITOR.instances.mymarkdown41.setData('<table cellpadding="0" cellspacing="0" style="width: 100%; padding:10px;">'+
                                                     '<tbody>'+
                                                         '<tr>'+
                                                             '<td style="background-color:#0c54a0; color:#ffffff; padding-left:50px;text-align:center; font-size: 14px; vertical-align: middle; width:20%;">'+

                                                                  '<p>'+
                                                                     '<strong>С уважением,<br>'+
                                                                     '{{$offer->policy->client->agent()->firstname}} {{$offer->policy->client->agent()->lastname}}</strong>'+
                                                                 '</p>'+
                                                             '</td>'+
                                                              '<td style="background-color:#dadada; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;margin-top:15px;">'+
                                                                 '<p>'+
                                                                     '<img width="256px" border="0" src="http://ins24.ru/images/logo.png">'+
                                                                 '</p>'+
                                                             '</td>'+
                                                              '<td style="background-color:#0c54a0; color:#ffffff; text-align:center; font-size: 14px; vertical-align: middle;">'+
                                                                 '<p>'+
                                                                   '<strong>'+
                                                                    '129110'+
                                                                    'Проспект Мира, д.68, <br>стр.1А, оф.307<br>'+
                                                                    'тел:</strong>'+
                                                                    '<span style="color:white;">+{{$offer->policy->client->agent()->work_phone}}</span>.'+
                                                                 '</p>'+
                                                             '</td>'+
                                                             '<td style="background-color:#0c54a0; color:#ffffff; padding-right:50px; text-align:center; font-size: 14px; vertical-align: middle;">'+
                                                                 '<p>'+
                                                                     '<strong>E-mail:</strong>'+
                                                                     '<br>'+
                                                                     '<strong>'+
                                                                         '<a style="color:#ffffff" href="mailto:{{$offer->policy->client->agent()->email}}">{{$offer->policy->client->agent()->email}}</a>'+
                                                                     '</strong>'+
                                                                 '</p>'+
                                                             '</td>'+
                                                         '</tr>'+
                                                     '</tbody>'+
                                         '</table>');
    }
    	// PAGE RELATED SCRIPTS

    	// pagefunction

	var pagefunction = function() {

		CKEDITOR.replace( 'mymarkdown4', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
		CKEDITOR.replace( 'mymarkdown3', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );

		CKEDITOR.replace( 'mymarkdown6', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
        @if ($offer->policy->type->id==2)
		CKEDITOR.replace( 'mymarkdown5', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );

		CKEDITOR.replace( 'mymarkdown2', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           			{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );
		CKEDITOR.replace( 'mymarkdown', { height: '200px', toolbarGroups: [
                                                          		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
                                                           		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
                                                           		'/',																// Line break - next group will be placed in new line.
                                                           		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },

                                                           		{ name: 'links' },
                                                           		{ name: 'paragraph', groups: [ 'list', 'align' ]},
                                                           		{ name: 'insert', groups: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
                                                          	]
} );@endif
	};

	// end pagefunction

	// run pagefunction on load
	loadScript("js/plugin/ckeditor/ckeditor.js", pagefunction);
    function CKupdate(){
        for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();
    }
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addoffer = $('#edit-offer').validate({
            // Rules for form validation
            rules : {
                strahavaia_suma : {
                    required : true,
                    number : true
                },
                strahavaia_premia : {
                    required : true,
                    number : true
                },
                fransiza : {
                    number : true
                },
                strahavaia_suma_grajdanscaia : {
                    required : true,
                    number : true
                },
                strahavaia_premia_grajdanscaia : {
                    required : true,
                    number : true
                },
                strahavaia_suma_dobrovolina : {
                    required : true,
                    number : true
                },
                strahavaia_premia_dobrovolina : {
                    required : true,
                    number : true
                },
                company_id  : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                strahavaia_suma : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                fransiza : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_suma_grajdanscaia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia_grajdanscaia : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_suma_dobrovolina : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                strahavaia_premia_dobrovolina : {
                    required : 'Пожалуйста, заполните это поле',
                     number: 'Пожалуйста, проверьте ваш ввод'
                },
                company_id : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                CKupdate();
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#editOffer').modal('hide');
                        loadURL("{{action('OfferController@getOffers')}}/{{$offer->policy->id}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

</script>