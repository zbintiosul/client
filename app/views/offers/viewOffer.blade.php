<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>{{$offer->company->name}} {{$offer->id}}  </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <div class="btn-group">
                                          <button class="btn btn-sm btn-success" onclick="getModal('{{action('OfferController@getModalSendEmailToClient')}}/{{$offer->id}}','#emailToClient')" title="Отправить предложения">
                                                        <i class="fa fa-envelope-o"></i>
                                           </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <iframe id="offer" width="100%" height="800" src="/offer/offer/{{$offer->id}}">
                           @include('offers.offer')
                        </iframe>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    //pageSetUp();


</script>