<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> Топ Агентов </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/font-awesome.min.css">

    <!-- Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-production.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-skins.css">

    <!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-rtl.css"> -->

    <!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.-->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/client_style.css">


    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/demo.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>
<body
    <?php
    if ($page_body_prop) {
        foreach ($page_body_prop as $prop_name => $value) {
            echo $prop_name.'="'.$value.'" ';
        }
    }

    ?>
>

<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
    <header id="header">
        <!--<span id="logo"></span>-->

        <div id="logo-group">
            <span id="logo"> <img src="/img/client_logo.png" alt="Client"> </span>

            <!-- END AJAX-DROPDOWN -->
        </div>
    </header>

    <div id="main" role="main">

        <!-- MAIN CONTENT -->
        <div id="content" class="container">
        <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">
        <article class="col-sm-12">
        <!-- new widget -->
        <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
        <!-- widget options:
        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

        data-widget-colorbutton="false"
        data-widget-editbutton="false"
        data-widget-togglebutton="false"
        data-widget-deletebutton="false"
        data-widget-fullscreenbutton="false"
        data-widget-custombutton="false"
        data-widget-collapsed="true"
        data-widget-sortable="false"

        -->
        <header>
            <span class="widget-icon"> <i class="fa fa-sort-numeric-asc"></i> </span>
            <h2>Топ Агентов </h2>

            <ul class="nav nav-tabs pull-right in" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#s1"> <span">Месяц</span></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#s2"> <span>Неделя</span></a>
                </li>

                <li>
                    <a data-toggle="tab" href="#s3"> <span>День</span></a>
                </li>
            </ul>

        </header>

        <!-- widget div-->
        <div class="no-padding">
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">

                test
            </div>
            <!-- end widget edit box -->

            <div class="widget-body">
                <!-- content -->
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="s1">
                        <?php $i=1;?>
                        <div id="chat-body" class="chat-body top-body">
                        <ul>
                        @foreach($month as $m)
                        <div class="row no-space">
                            <div class="col-sm-1 col-md-1 col-lg-1 top-number">
                                <span class="top-number-text">{{$i++}}</span>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 top-img">
                                @if (!$m->user->hasImage())
                                <img alt="140x140" src="/img/avatars/avatar_user.jpg" width="100%">
                                @else
                                <img  alt="140x140" src="data:image/png;base64,{{ base64_encode(Image::make($m->user->imagePath())->grab(186,186))}}" width="100%">
                                @endif
                            </div>
                            <div class="col-sm-7 col-md-7 col-lg-7">
                                <li class="message">

									<span class="message-text top-message-text">
										<a href="javascript:void(0);" class="username top-username">{{$m->user->lastname}} {{$m->user->firstname}} {{$m->user->secondname}}</a>
                                        <div class="invoice-sum-total">
                                            <h3><strong>Продана: <span class="text-success">{{Locale::number($m->total)}} руб.</span></strong></h3>
                                        </div>
                                         <div class="invoice-sum-total top-sales">
                                             <h3><strong>Заказы: <span class="text-success">{{$m->nr}}</span></strong></h3>
                                         </div>
                                    </span>
                                </li>

                            </div>
                        </div>
                        @endforeach
                        </ul>
                        </div>
                    </div>
                    <!-- end s1 tab pane -->

                    <div class="tab-pane fade" id="s2">
                        <?php $i=1;?>
                        <div id="chat-body" class="chat-body top-body">
                            <ul>
                                @foreach($week as $m)
                                <div class="row no-space">
                                    <div class="col-sm-1 col-md-1 col-lg-1 top-number">
                                        <span class="top-number-text">{{$i++}}</span>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2 top-img">
                                        @if (!$m->user->hasImage())
                                        <img alt="140x140" src="/img/avatars/avatar_user.jpg" width="100%">
                                        @else
                                        <img  alt="140x140" src="data:image/png;base64,{{ base64_encode(Image::make($m->user->imagePath())->grab(186,186))}}" width="100%">
                                        @endif
                                    </div>
                                    <div class="col-sm-7 col-md-7 col-lg-7">
                                        <li class="message">

									<span class="message-text top-message-text">
										<a href="javascript:void(0);" class="username top-username">{{$m->user->lastname}} {{$m->user->firstname}} {{$m->user->secondname}}</a>
                                        <div class="invoice-sum-total">
                                            <h3><strong>Продана: <span class="text-success">{{Locale::number($m->total)}} руб.</span></strong></h3>
                                        </div>
                                         <div class="invoice-sum-total top-sales">
                                             <h3><strong>Заказы: <span class="text-success">{{$m->nr}}</span></strong></h3>
                                         </div>
                                    </span>
                                        </li>

                                    </div>
                                </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- end s2 tab pane -->

                    <div class="tab-pane fade" id="s3">
                        <?php $i=1;?>
                        <div id="chat-body" class="top-body chat-body">
                            <ul>
                                @foreach($day as $m)
                                <div class="row no-space">
                                    <div class="col-sm-1 col-md-1 col-lg-1 top-number">
                                        <span class="top-number-text">{{$i++}}</span>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2 top-img">
                                        @if (!$m->user->hasImage())
                                        <img alt="140x140" src="/img/avatars/avatar_user.jpg" width="100%">
                                        @else
                                        <img  alt="140x140" src="data:image/png;base64,{{ base64_encode(Image::make($m->user->imagePath())->grab(186,186))}}">
                                        @endif
                                    </div>
                                    <div class="col-sm-7 col-md-7 col-lg-7">
                                        <li class="message">

									<span class="message-text top-message-text">
										<a href="javascript:void(0);" class="username top-username">{{$m->user->lastname}} {{$m->user->firstname}} {{$m->user->secondname}}</a>
                                        <div class="invoice-sum-total">
                                            <h3><strong>Продана: <span class="text-success">{{Locale::number($m->total)}} руб.</span></strong></h3>
                                        </div>
                                         <div class="invoice-sum-total top-sales">
                                             <h3><strong>Заказы: <span class="text-success">{{$m->nr}}</span></strong></h3>
                                         </div>
                                    </span>
                                        </li>

                                    </div>
                                </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- end s3 tab pane -->
                </div>

                <!-- end content -->
            </div>

        </div>
        <!-- end widget div -->
        </div>
        <!-- end widget -->

        </article>
        </div>

        <!-- end row -->

        <!-- row -->

        </section>
        </div>

    </div>
    <!-- END MAIN PANEL -->
    <!-- ==========================CONTENT ENDS HERE ========================== -->

@include('layouts.scripts')


@include('layouts.footer')

<script type="text/javascript">// <![CDATA[

    var socket = io.connect('{{Config::get("app.url")}}:3000');

    socket.emit('want_top');

    socket.on('topAgents.update', function (data) {

        //alert('s');
        ajaxLoadPart('s1',getCurentUrl(),null,null);
        console.log('Notifications updated: 1');

        ajaxLoadPart('s2',getCurentUrl(),null,null);
        console.log('Notifications updated: 2');

        ajaxLoadPart('s3',getCurentUrl(),null,null);
        console.log('Notifications updated: 3');
    });
</script>