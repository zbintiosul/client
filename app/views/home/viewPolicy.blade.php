<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> Заявка на страхование </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="/css/smartadmin-production_unminified.css" type="text/css" />
    <link rel="stylesheet" href="/css/smartadmin-skins.css" type="text/css" />
    <link rel="stylesheet" href="/css/client_style.css" type="text/css" />

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon/favicon.ico" type="image/x-icon">


    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
<body style="padding: 0px!important; margin: 0px!important; font-size: 12px" id="printBody">
    @if ($policy->type->id == 1)
        @if ($is_broker)
            @include('clients.policy.OSAGO')
        @else
            @include('home.OSAGO')
        @endif
    @else
        @if ($policy->type->id == 2)
            @if ($is_broker)
                @include('clients.policy.KASKO')
            @else
                @include('home.KASKO')
            @endif
        @else
            @if ($policy->type->id == 3)
                @include('home.OTHER')
            @endif
        @endif
    @endif
</body>
</html>