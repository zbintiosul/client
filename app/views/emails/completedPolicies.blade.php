<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Страховки
			<span>>
                Завершенные
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Завершенные </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        {{$salaries->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">Прадажа №</th>
                                <th>Тип</th>
                                <th class="hidden-xs smart-form">
                                    <label class="select">
                                        <select name="user" onchange="filterUser(this.value)">
                                            <option value="" selected>Username</option>
                                            @foreach ($usedUsers as $user)
                                            <option value="{{$user->user->id}}" @if (Input::get('user')==$user->user->id) selected @endif>{{$user->user->lastname}} {{$user->user->firstname}}</option>
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th>Страховая премия</th>
                                <th>Комиссион</th>
                                <th>Прибыль агента</th>
                                <th>Invoice ID</th>
                                <th>Инфо</th>
                                <th>Дата продажы</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($salaries as $salary)
                            <tr>
                                <td class="custom-table-number hidden-xs">{{$salary->id}}</td>
                                <td class="custom-data-name">{{$salary->policy->type->name}}</td>
                                <td class="custom-data-name">{{$salary->user->lastname}} {{$salary->user->firstname}}</td>
                                <td class="custom-data-name">{{$salary->policy->calcTotalSum()}} руб</td>
                                <td class="custom-data-name">{{$salary->salaryTable->percentage}} %</td>
                                <td class="custom-data-name">{{Locale::number($salary->calcVenit())}} руб</td>
                                <td class="custom-data-name">
                                    <a href="{{action('ClientController@getViewPolicy')}}/{{$salary->policy->id}}" class="ajaxa">{{$salary->policy->id}}</a>
                                </td>
                                <td class="custom-data-name">
                                    Сума всех продаж: {{Locale::number($salary->user->calcMonthTotalSales($salary->created_at))}} руб <br>
                                    Стаж агента: {{$salary->user->calcTimeRegister()}} мес. <br>
                                    Уровень: {{$salary->salaryTable->rank}} <br>
                                    ЕЖЕМЕСЯЧНЫЙ ДОХОД: <span style="white-space: nowrap">{{Locale::number($salary->user->calcMonthTotalVenitSales($salary->created_at))}}</span> руб<br>
                                </td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">{{$salary->created_at->format('d-m-Y H:i:s')}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$salaries->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function filterUser(value)
    {
        window.location.hash = "{{action('ClientController@getCompletedPolicies')}}?user="+value;
    }
</script>