<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
        <title>Новая продажа</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-production_unminified.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-skins.css" type="text/css" />
        <link rel="stylesheet" href="/css/client_style.css" type="text/css" />
	</head>
	<body>
		<h2>Добрый день</h2>
        <p>{{$policy->client->agent()->firstname}}, ProAssist, стаж: -/продал: Р ---</p><br>

        <p>1. Транспортное средство (ТС) - {{$policy->car->modelCar->brand->name}} {{$policy->car->modelCar->brand->name}} / $policy->car->year_of_construction}</p>
        <p>2. Страховая Сумма: {{$policy->typeSumInsured->name}} руб</p>
        <p>3. Страховая Премия: {{$policy->calcTotalSum()}} руб</p>
        <p>4. Страховая Компания - {{$policy->insurance_company}}</p><br>

        <p>{{$policy->client->agent()->firstname}}, всего на сегодня:  Р ----</p>
        @include('emails.signature')
	</body>
</html>