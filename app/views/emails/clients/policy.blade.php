<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
        <title>Заявка на страхование</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-production_unminified.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-skins.css" type="text/css" />
        <link rel="stylesheet" href="/css/client_style.css" type="text/css" />
	</head>
	<body style="font-size: 18px">
		<h2 style="font-size: 18px">Добрый день, Дорогой Клиент!</h2>
        <p style="font-size: 18px">Мы предоставляем Вам заявку <b>на страхование</b>. </p>
        <p style="font-size: 18px">Пожалуйста нажмите <a href="{{action('HomeController@getPolicy',array('id' => $token))}}" >здесь</a> чтоб ознакомиться и проверить детали.</p>
        <p style="font-size: 18px"><u>Когда мы позвоним:</u> Мы всегда стремимся перезвонить вам сразу после отправления заявки. Тем не менее, если заявка была отправлена &nbsp;после 19:00 (Мск) в будний день, то мы свяжемся с вами на следующий
            рабочий день.</p><br>
        <p style="font-size: 18px">Спасибо за сотрудничество.</p>
        @include('emails.signature')
	</body>
</html>