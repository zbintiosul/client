<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
        <title>Заявка на страхование</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-production_unminified.css" type="text/css" />
        <link rel="stylesheet" href="/css/smartadmin-skins.css" type="text/css" />
        <link rel="stylesheet" href="/css/client_style.css" type="text/css" />
	</head>
	<body style="font-size: 18px">
		<h2 style="font-size: 18px">Добрый день, Дорогой Партнер!</h2>
        <p style="font-size: 18px">Мы предоставляем Вам новую заявку на утверждение. Ознакомитесь с деталями и проверьте расчеты.</p>
        <p style="font-size: 18px">Пожалуйста нажмите <a href="{{action('HomeController@getPolicyBroker',array('id' => $token))}}" >здесь</a> чтоб ознакомиться и проверить детали.</p>

        <p  style="font-size: 18px">Если Вы убедились в правильности деталей заявки, пожалуйста, утвердите и  известите нас когда будет вручен Договор клиенту.</p><br>
        <p  style="font-size: 18px>Спасибо за сотрудничество.</p>
        @include('emails.signature')
	</body>
</html>