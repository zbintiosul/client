@if (Authority::can('add_notice'))
<form method="post" action="{{action('NotificationController@postAddNotification')}}" id="add-note" class="well padding-bottom-10 smart-form note-form" onsubmit="return false;">
    <input type="hidden" name="note_type" id="note_type" value="0">
    <fieldset>
        <section>
                <label class="input"> <i class="icon-append fa fa-tag"></i>
                    <input class="form-control" placeholder="Название" name="name" maxlength="50" />
                    <b class="tooltip tooltip-top-right"><i class="fa fa-tag txt-color-teal"></i> Мах. 50</b> </label>
                </label>
            </section>
            <section>
                <label class="textarea"> <i class="icon-append fa fa-bell"></i>
                    <textarea rows="2" class="form-control resizeTextaria" name="text" placeholder="Пишите сюда уведомления"></textarea>
                </label>
            </section>
            <section class="user-note" style="display: none;">
                <label class="select"> <i class="icon-append fa fa-user"></i>
                    <div class="form-group">
                        <select multiple style="width:100%" class="select2Custom" id="select2CustomUsers" placeholder="Выберите пользователей" name="users">
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->username}}</option>
                            @endforeach
                        </select>
                    </div>
                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Введите пароль</b> </label>
                </label>
            </section>
            <div class="row" id="rowDatetimeNote" style="display: none;">
                <section class="col col-6">
                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="noteDate" placeholder="Выберите дату" class="form-control datepicker" data-mask="9999-99-99" data-dateformat="yy-mm-dd" value="{{Carbon\Carbon::now()->toDateString()}}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </label>
                </section>
                <section class="col col-6">
                    <label class="input"> <i class="icon-append fa fa-clock-o"></i>
                        <div class="form-group">
                             <div class="input-group">
                                <input class="form-control" name="noteTime" id="timepicker" type="text" placeholder="Выберите время">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </label>
                </section>
            </div>
    </fieldset>
    <fieldset style="margin: 0; padding:0;">
    <div class="margin-top-10">
        <button type="submit" class="btn btn-sm btn-primary pull-right">
            Послать
        </button>
        <div class="btn-group btn-group-smart">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default" title="Add Time">
                    <input type="checkbox" name="timeNote" value="1">
                    <i class="fa fa-clock-o"></i> </label>
            </div>
            <a href="javascript:void(0);" class="btn btn-default hide" title="Add File"><i class="fa fa-file"></i></a>
            <div class="btn-group">
                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" id="dropdown-note" rel="tooltip" data-placement="bottom" title="Тип">
                    По умолчанию <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="note-type">
                    @foreach (NotificationType::all() as $noteType)
                    <li id="{{$noteType->id}}" class="{{$noteType->class}}">
                        <a href="javascript:void(0);" id="hrefNote_{{$noteType->id}}" onclick="changeTypeNote({{$noteType->id}},'{{$noteType->class}}')">{{$noteType->name}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <a href="javascript:void(0);" class="btn btn-default" id="who-note"  title="Users"><i class="fa fa-group"></i> Всем</a>
        </div>
    </div>
    </fieldset>
</form>


<script>


    /*
     * TIMEPICKER
     */

    //Load time picker script

    loadScript("/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js", runTimePicker);

    function runTimePicker() {
        $('#timepicker').timepicker({
            minuteStep: 10,
            showMeridian:false
        });
    }

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addNote = $('#add-note').validate({
            // Rules for form validation
            rules : {
                name : {
                    required : true
                },
                text : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                name : {
                    required : 'Пожалуйста, заполните это поле'
                },
                text : {
                    required : 'Пожалуйста, заполните это поле'
                },
                users : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loadingLocal('show','#add-note');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        //$(form).reset();
                        loadingLocal('hide','#add-note');
                        //$('#addRole').modal('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loadingLocal('hide','#add-note');
                    }
                });
            }
        });

    }

    $( document ).ready(function() {
        $('#who-note').click(function() {
            $('.user-note').toggle("slow", function() {
                if ($(".user-note").is(":visible")) {
                    $("#select2CustomUsers").rules( "add", {
                        required: true
                    });
                    $('#who-note').html('<i class="fa fa-arrow-up"></i> Верхние');
                } else {
                    $('#who-note').html('<i class="fa fa-group"></i> Всем');
                    $(".select2Custom").select2('val', 'All');
                    //$(".select2Custom option:selected").removeAttr("selected");
                }
            });

        });

        $('#select2CustomUsers').select2();

        $('input[name="timeNote"]').change(function() {
            if($(this).is(':checked')){
                $('#rowDatetimeNote').show('slow');
            }else{
                $('#rowDatetimeNote').hide('slow');
            }
        });
    });

    function changeTypeNote(id,$class)
    {
        var val = $('#hrefNote_'+id).html();
        $('#dropdown-note').html(val);
        $('#dropdown-note').removeClass('alert-default');
        $('#dropdown-note').removeClass('alert-warning');
        $('#dropdown-note').removeClass('alert-success');
        $('#dropdown-note').removeClass('alert-info');
        $('#dropdown-note').removeClass('alert-danger');
        $('#dropdown-note').addClass($class);
        $('#note_type').val(id);
    }

</script>
@endif