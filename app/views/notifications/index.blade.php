<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Уведомлении
			<span>>
				Все уведомления
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"
                 data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-user"></i> </span>

                    <h2> Все уведомления</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group input-groupSearch">
                                        <input class="form-control usersSearch" id="prepend" placeholder="Искать в названии" type="text" value="{{Input::get('search')}}">
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    @if (Input::get('search'))
                                    <a type="button" href="{{action('NotificationController@getNewNote')}}"  class="btn btn-default ajaxa" title="Очищать">
                                        <i class="fa fa-fw fa-search fa-times"></i>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <a class="btn btn-success ajaxa" href="{{action('NotificationController@getNewNote')}}" >
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новое уведомление </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {{$notifications->links()}}
                        <table id="custom-table" class="table dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">От</th>
                                <th></th>
                                <th></th>
                                <th  class="hidden-xs">Дата</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($notifications as $note)
                            <tr id="tr_{{$note->id}}" class="{{$note->type->class}}">
                                <td class="custom-table-number hidden-xs" >
                                    @if ($note->notification_type_id==0)
                                    <em class="badge no-border-radius pull-left margin-right-5" style="padding: 0!important; background: none!important" rel="tooltip" data-placement="right" data-original-title="{{$note->user->username}}">
                                        @if (!$note->user->hasImage())
                                        {{User::getDefaultImage(40,40)}}
                                        @else
                                        <img data-src="holder.js/30x30" class="img-rounded"  id="user_img_{{$note->user->id}}" alt="{{$note->user->username}}" src="data:image/png;base64,{{base64_encode(Image::make($note->user->imagePath())->resize(40, null, true))}}" />
                                        @endif
                                    </em>
                                    @else
                                    <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5" rel="tooltip" data-placement="right" data-original-title="{{$note->type->name}}">
                                        <i class="fa {{$note->getIcon()}} fa-fw fa-2x"></i>
                                    </em>
                                    @endif
                                </td>
                                <td style="cursor:pointer" onclick="showNote({{$note->id}})">
                                    <div class="pop" rel="popover" data-placement="top"  data-original-title="{{$note->name}}" data-content="{{$note->text}}" >
                                    <b>{{str_limit($note->name, $limit = 50, $end = '...')}}</b><br>
                                    {{str_limit($note->text, $limit = 150, $end = '...')}}
                                </div>
                                </td>
                                <td class="custom-data-action">
                                     @if ($note->pivot->seen==0)
                                     <span class="badge bg-color-red">новая</span>
                                     @endif
                                </td>
                                <td class="custom-data-action hidden-xs width100px">
                                    {{$note->getTimeAgo()}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$notifications->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function showNote(id)
    {
        $(".pop").popover({html : true});
        $.get( '{{action('NotificationController@getReadNote')}}/'+id, function(data) {
        $('#noteBody_'+id).removeClass('unread');
        updateNrNotification();
    });
    }
    loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);


    function runXEditDemo() {

        // constructs the suggestion engine
        var repos = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            prefetch: '{{action('NotificationController@getJsonGetNote')}}'
    });

    // kicks off the loading/processing of `local` and `prefetch`
    repos.initialize();

    $('.usersSearch').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'roles',
            displayKey: 'name',
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: repos.ttAdapter()
        });

    $(".usersSearch").keyup(function (e) {
        if (e.keyCode == 13) {
            window.location.hash = "{{action('NotificationController@getIndex')}}?search="+$(this).val();
        }
    });
    }

</script>