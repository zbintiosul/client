@if (Auth::user()->nrNewNote()>0)
    @foreach (Auth::user()->notifications()->where('appear_time','<=',Carbon\Carbon::now())->wherePivot('seen','=','0')->orderBy('appear_time','desc')->get() as $noty)
        <ul class="notification-body">
            <li>
                <span class="padding-10 unread" style="cursor: pointer" id="noteBody_{{$noty->id}}" onclick="showNote({{$noty->id}})"   rel="popover" data-placement="right" data-original-title="{{$noty->name}}" data-content="{{$noty->text}}">
                        @if ($noty->type->id==0)
                            <em class="badge no-border-radius pull-left margin-right-5" style="padding: 0!important; background: none!important">
                                @if (!$noty->user->hasImage())
                                {{User::getDefaultImage(40,40)}}
                                @else
                                <img data-src="holder.js/30x30" class="img-rounded"  id="user_img_{{$noty->user->id}}" alt="{{$noty->user->username}}" src="data:image/png;base64,{{base64_encode(Image::make($noty->user->imagePath())->resize(40, null, true))}}" />
                                @endif
                            </em>
                        @else
                            <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5" >
                                <i class="fa {{$noty->getIcon()}} fa-fw fa-2x"></i>
                            </em>
                        @endif
                    <span style="text-overflow: ellipsis;">
                         <b>{{str_limit($noty->name, $limit = 50, $end = '...')}}</b>
                         <br>
                         {{$noty->text}}
                         <span class="pull-right font-xs text-muted"><i>{{$noty->getTimeAgo()}}</i></span>
                    </span>
                </span>
            </li>
        </ul>
    @endforeach
@else
<!-- notification content -->
<ul class="notification-body">
    <li>
        <span class="padding-10 unread">
            <h4 style="text-align:center;">Нет новых уведомлении</h4>
        </span>
    </li>
</ul>

@endif
<script>
    $(document).ready(function() {
        $("[rel=popover]").popover({html : true});
    });

    function showNote(id)
    {
        $("[rel=popover]").popover({html : true});
        $.get( '{{action('NotificationController@getReadNote')}}/'+id, function(data) {
            $('#noteBody_'+id).removeClass('unread');
            updateNrNotification();
        });
    }
</script>
