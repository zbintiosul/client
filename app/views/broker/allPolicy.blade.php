<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Брокер
			<span>>
				Все мои страховки
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2> Все мои страховки </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        {{$policies->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th>Клиент</th>
                                <th>Автомобиль</th>
                                <th class="hidden-xs hidden-sm">Риск</th>
                                <th class="hidden-xs hidden-sm">Расчет</th>
                                <th class="hidden-xs hidden-sm">Дата</th>
                                <th class="hidden-xs">Статус</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($policies as $policy)
                            <tr id="tr_{{$policy->id}}" idPolicy="{{$policy->id}}">
                                <td class="custom-table-number hidden-xs">{{$policy->id}}</td>
                                <td  class="custom-data-name">
                                    <div>{{$policy->client->firstname}} {{$policy->client->lastname}}</div>
                                </td>
                                <td  class="custom-data-name">
                                    <div>{{$policy->car->modelCar->brand->name}} {{$policy->car->modelCar->name}}</div>
                                </td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">
                                    {{$policy->type->name}}
                                </td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">
                                    {{$policy->calcTotalSum()}} руб.
                                </td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">
                                    {{$policy->updated_at->format('d-m-Y H:i:s')}}
                                </td>
                                <td class="custom-data-action hidden-xs">
                                    {{$policy->status->name}}
                                </td>
                                <td class="custom-data-action">
                                    <a href="{{action('BrokerController@getViewPolicy')}}/{{$policy->id}}" class="btn btn-xs btn-default ajaxa"><i class="fa fa-eye"></i> Просмотр</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$policies->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

</script>