<!-- Bread crumb is created dynamically -->
<!-- row -->
<div class="row">
    <!-- col -->
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <!-- PAGE HEADER --><i class="fa-fw fa fa-file-o"></i>Мой профиль <span></span></h1>
    </div>
    <!-- end col -->
    @if (!Authority::canOnly('is_broker'))
    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info">
                <h5> Топ<span class="txt-color-blue">
                        <a href="{{action('TopController@getIndex')}}" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-sort-numeric-asc"></i></a>
                </span></h5>

            </li>
            <li class="sparks-info">
                <h5> Ежемесячный доход <span class="txt-color-blue">{{Locale::number(Auth::user()->calcMonthTotalVenitSales(Carbon\Carbon::now()))}} руб</span></h5>
                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    {{Auth::user()->calcMonthTotalSales(Carbon\Carbon::now())}}
                </div>
            </li>
            <li class="sparks-info">
                <h5> Мои заказы <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;{{Auth::user()->calcMonthCountSales(Carbon\Carbon::now())}}</span></h5>
                <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                    {{Auth::user()->salaries()->count()}}
                </div>
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->
    @endif
</div>
<!-- end row -->

<!-- row -->

<div class="row">

<div class="col-sm-12">


<div class="well well-sm">

<div class="row">

<div class="col-sm-12 col-md-12 col-lg-6">
<div class="well well-light well-sm no-margin no-padding">

<div class="row">

    <div class="col-sm-12">
        <div id="myCarousel" class="carousel fade profile-carousel">
            <div class="air air-bottom-right padding-10">

            </div>
            <div class="air air-top-left padding-10">
                <h4 class="txt-color-white font-md">{{Locale::date(Carbon\Carbon::now())}}</h4>
            </div>
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner">
                <!-- Slide 1 -->
                <div class="item active">
                    <img src="/img/demo/s1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">

        <div class="row">

            <div class="col-sm-3 profile-pic">
                @if (!Auth::user()->hasImage())
                {{User::getDefaultImage(100,100)}}
                @else
                <img data-src="holder.js/30x30" class="img-rounded"  alt="{{Auth::user()->username}}" title="{{Auth::user()->username}}" src="data:image/png;base64,{{base64_encode(Image::make(Auth::user()->imagePath())->resize(null, 100, true))}}" />
                @endif
                <div class="padding-10">
                    <h4 class="font-md"><strong>{{Locale::number(Auth::user()->calcDayTotalSales(Carbon\Carbon::now()))}} руб</strong>
                        <br>
                        <small>Ежедневный  доход</small></h4>
                    <br>
                    <h4 class="font-md"><strong>{{Auth::user()->calcDayTotalVenitSales(Carbon\Carbon::now())}}</strong>
                        <br>
                        <small>Заказы</small></h4>
                </div>
            </div>
            <div class="col-sm-6">
                <h1>{{Auth::user()->lastname}} <span class="semi-bold">{{Auth::user()->firstname}}</span>
                    <br>
                    <small>
                    @foreach (Auth::user()->roles as $role)
                       {{$role->name}},
                    @endforeach
                    </small></h1>

                <ul class="list-unstyled">
                    <li>
                        <p class="text-muted">
                            <i class="fa fa-phone"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{Auth::user()->home_phone}}</span></span>
                        </p>
                    </li>
                    <li>
                        <p class="text-muted">
                            <i class="fa fa-phone-square"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{Auth::user()->work_phone}}</span>
                        </p>
                    </li>
                    <li>
                        <p class="text-muted">
                            <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:{{Auth::user()->email}}">{{Auth::user()->email}}</a>
                        </p>
                    </li>
                </ul>
                <br>
                <br>

            </div>
            <div class="col-sm-3">

            </div>

        </div>

    </div>

</div>
@if (Authority::can('view_in_process_clients'))
<div class="row">

    <div class="col-sm-12">

        <hr>

        <div class="padding-10">

            <ul class="nav nav-tabs tabs-pull-right">
                <li class="active">
                    <a href="#a1" data-toggle="tab">Клиенты в процессе</a>
                </li>
            </ul>

            <div class="tab-content padding-top-10">
                <div class="tab-pane fade in active" id="a1">

                    @foreach(Auth::user()->clients()->where('status_client_id','=','2')->orderBy('updated_at','desc')->get() as $client)
                    <div class="user">
                        <img src="/img/avatars/male.png"><a href="{{action('ClientController@getInProgress')}}/{{$client->id}}" class="ajaxa">{{$client->phone}}</a>
                        <div class="email">
                            {{$client->firstname}} {{$client->lastname}} {{$client->secondname}}
                        </div>
                    </div>
                    @endforeach
                </div><!-- end tab -->
            </div>

        </div>

    </div>

</div>
<!-- end row -->
@endif
</div>

</div>


<div class="col-sm-12 col-md-12 col-lg-6">

    @include('notifications.blocks.addNotification')

</div>
</div>

</div>


</div>

</div>

<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp()

    // PAGE RELATED SCRIPTS

</script>
