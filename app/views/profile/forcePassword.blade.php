<!-- Modal -->
<div class="modal fade" id="forcePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Принудительное изменение пароля при первом входе
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ProfileController@postForcePassword')}}" id="force-password" class="smart-form" method="post">
                    <fieldset>
                        <section>
                            <label class="label">Новый пароль</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password" placeholder="Новый пароль" id="password">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-lock txt-color-teal"></i> Не забывайте пароль. Мин. 4 Мах. 40</b>
                            </label>
                        </section>
                        <section>
                            <label class="label">Повторите пароль</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password_confirmation" placeholder="Повторите пароль">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-lock txt-color-teal"></i> Не забывайте пароль. Мин. 4 Мах. 40</b>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <a href="{{action('AccountController@getLogout')}}" class="btn btn-danger" title="Выход" data-logout-msg="Убедитесь, что вы не оставили активность."><i class="fa fa-sign-out"></i> Выход</a>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $forcePassword = $('#force-password').validate({

            rules : {
                password : {
                    required : true,
                    minlength : 4,
                    maxlength : 40
                },
                password_confirmation : {
                    required : true,
                    minlength : 4,
                    maxlength : 40,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
            messages : {
                password : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40'
                },
                password_confirmation : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40',
                    equalTo : 'Пароль не совпадают'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#forcePassword').modal('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }
    $( document ).ready(function() {


    });
</script>