<!-- Modal -->
<div class="modal fade" id="editCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Редактировать
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('CompanyController@postEditCompany')}}" id="edit-company" class="smart-form" method="post">
                    <input type="hidden" name="company_id" value="{{$company->id}}">
                    <fieldset>
                        <section>
                            <label class="label">Название</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="name" placeholder="Название" value="{{$company->name}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                            </label>
                        </section>
                        <section>
                            <label class="label">Сайт</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="url" placeholder="Сайт" value="{{$company->url}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i></b>
                            </label>
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label">Логотип адрес</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="logo" placeholder="http://adm.proassist.ru/img/client_logo.png" value="{{$company->logo}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i></b>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addcompany = $('#edit-company').validate({
            // Rules for form validation
            rules : {
                name : {
                    required : true,
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                name : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#editCompany').modal('hide');
                        loadURL("{{action('CompanyController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

</script>