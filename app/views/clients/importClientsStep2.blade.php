<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-paperclip"></i> </span>
                    <h2> Импорт </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        @if (is_array($excel))
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-success" onclick="saveImport('{{$importName}}')">
                                            <i class="fa fa-save"></i> <span class="hidden-mobile hidden-xs">Сохранить импорт</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-block alert-success">
                            <h4 class="alert-heading"><i class="fa fa-check"></i> Успех в чтения данных!</h4>
                            <p>
                                Cтрок: {{count($excel)}}
                            </p>
                            <p>
                                Данный импорт будет сохраняется 15 минут в память если не сохраните он будет удаляется.
                            </p>
                        </div>
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Номер</th>
                                <th>Срок</th>
                                <th>Риск</th>
                                <th>Банк</th>
                                <th>Страховая  компания</th>
                                <th>Страховая премия</th>
                                <th>Авто</th>
                                <th>Модель</th>
                                <th>г.в.</th>
                                <th>Серия</th>
                                <th>№</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($excel as $data)
                            <tr>
                                <td>{{$data['1']}}</td>
                                <td>{{$data['2']}}</td>
                                <td>{{$data['3']}}</td>
                                <td>{{$data['4']}}</td>
                                <td>{{$data['5']}}</td>
                                <td>{{$data['6']}}</td>
                                <td>{{$data['7']}}</td>
                                <td>{{$data['8']}}</td>
                                <td>{{$data['9']}}</td>
                                <td>{{$data['10']}}</td>
                                <td>{{$data['11']}}</td>
                                <td>{{$data['12']}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                            <div class="alert alert-block alert-danger">
                                <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Ошибка, файл не найден!</h4>
                                <p>

                                </p>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

   function saveImport(name)
   {
       $.SmartMessageBox({
           title : "Сохранить импорт",
           content : "Вы уверены?",
           buttons : '[Нет][Да]'
       }, function(ButtonPressed) {
           if (ButtonPressed === "Да") {
               loading('show');
               $.ajax({
                   type: "POST",
                   url: "{{action('ClientController@postSaveImport')}}/"+name
               }).done(function(msg) {
                   if (msg=='false')
                   {
                       $.smallBox({
                           title : "{{Lang::get('client.error')}}",
                           content : "{{Lang::get('client.errorText')}}",
                           color : "#c26565",
                           iconSmall : "fa fa-times bounce animated",
                           timeout : 4000
                       });
                       window.location.hash = "{{action('ClientController@getImportClientsStep2')}}/"+name;
                   }else
                   {
                       $.smallBox({
                           title : "{{Lang::get('client.success')}}",
                           content : "{{Lang::get('client.successText')}}",
                           color : "rgb(115, 158, 115)",
                           iconSmall : "fa fa-check bounce animated",
                           timeout : 4000
                       });
                       window.location.hash = "{{action('ClientController@getIndex')}}?status=0&import="+msg.id;
                   }

                   loading('hide');
               })
                   .fail(function(msg) {
                       $.smallBox({
                           title : "{{Lang::get('client.error')}}",
                           content : "{{Lang::get('client.errorText')}}",
                           color : "#c26565",
                           iconSmall : "fa fa-times bounce animated",
                           timeout : 4000
                       });
                       window.location.hash = "{{action('ClientController@getImportClientsStep2')}}/"+name;
                       loading('hide');
                   });
           }
       });
   }



</script>