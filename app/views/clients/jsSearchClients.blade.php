loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);

function runXEditDemo() {

// constructs the suggestion engine
var repos = new Bloodhound({
datumTokenizer: Bloodhound.tokenizers.obj.whitespace('phone'),
queryTokenizer: Bloodhound.tokenizers.whitespace,
// `states` is an array of state names defined in "The Basics"
prefetch: '{{action('ClientController@getJsonClients')}}',
            dupDetector: function(remoteMatch, localMatch) {
                    return remoteMatch.id === localMatch.id;
            }
});

// kicks off the loading/processing of `local` and `prefetch`
repos.initialize();

$('.usersSearch').typeahead({
hint: true,
highlight: true,
minLength: 1
},
{
name: 'users',
displayKey: 'phone',
// `ttAdapter` wraps the suggestion engine in an adapter that
// is compatible with the typeahead jQuery plugin
source: repos.ttAdapter()
});

$(".usersSearch").keyup(function (e) {
if (e.keyCode == 13) {
window.location.hash = "{{action('ClientController@getIndex')}}?search="+$(this).val();
}
});
}