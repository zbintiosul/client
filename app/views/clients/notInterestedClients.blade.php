<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Клиенты
			<span>>
				  Не заинтересованные
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Новые </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group input-groupSearch">
                                        <input class="form-control notInterestedSearch" id="prepend" placeholder="Поиск клиента" type="text" value="{{Input::get('search')}}">
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    @if (Input::get('search'))
                                    <a type="button" href="{{action('ClientController@getNotInterestedClients')}}"  class="btn btn-default ajaxa" title="Очищать">
                                        <i class="fa fa-fw fa-search fa-times"></i>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">

                                </div>
                            </div>
                        </div>
                        {{$clients->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th>Телефон</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th class="hidden-xs hidden-sm">Отчество</th>
                                <th class="hidden-xs">Комментарий</th>
                                <th class="hidden-xs">Обновлен</th>
                                <th class="hidden-xs">Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($clients as $client)
                            <tr id="tr_{{$client->id}}" idClient="{{$client->id}}" @if ($client->trashed()) class="trashedRecord" @endif>
                            <td class="custom-table-number hidden-xs">{{$client->id}}</td>
                            <td  class="custom-data-column">
                                <div class="txt-color-darken">{{$client->phone}}</div>
                            </td>
                            <td  class="custom-data-column">
                                <div>{{$client->firstname}}</div>
                            </td>
                            <td class="custom-data-column"><div>{{$client->lastname}}</div></td>
                            <td class="custom-data-column hidden-xs"><div>{{$client->secondname}}</div></td>

                            <td  class="custom-data-column">
                                <div style="height: 50px; white-space: normal;">
                                    {{$client->comments}}
                                </div>
                            </td>
                            <td class="custom-data-action hidden-xs hidden-sm width100px">
                                {{$client->updated_at->format('d-m-Y H:i:s')}}
                            </td>
                            <td class="custom-data-action hidden-xs hidden-sm width100px">
                                {{$client->status->name}}
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$clients->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();


    $( document ).ready(function() {
        $("#custom-table .custom-data-column").click(function() {
            $this = $(this);
            showNotInterested($this);
        });
        $("#custom-table .custom-data-name").click(function() {
            $this = $(this);
            showNotInterested($this);
        });
    });

    function showNotInterested($this)
    {
        window.location.hash = "{{action('ClientController@getNotInterested')}}/"+$this.closest("tr").attr("idClient");
    }

    loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);

    function runXEditDemo() {

        // constructs the suggestion engine
        var repos = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('phone'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            prefetch: '{{action('ClientController@getJsonClients')}}/3',
            dupDetector: function(remoteMatch, localMatch) {
                    return remoteMatch.id === localMatch.id;
            }
    });

    // kicks off the loading/processing of `local` and `prefetch`
    repos.initialize();

    $('.notInterestedSearch').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'users',
            displayKey: 'phone',
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: repos.ttAdapter()
        });

    $(".notInterestedSearch").keyup(function (e) {
        if (e.keyCode == 13) {
            window.location.hash = "{{action('ClientController@getNotInterestedClients')}}?search="+$(this).val();
        }
    });
    }



</script>