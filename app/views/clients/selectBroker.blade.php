<!-- Modal -->
<div class="modal fade" id="approveBroker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Выберите Брокер
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postApproveManager')}}" id="approve-policy" class="smart-form" method="post">
                    <input type="hidden" name="policy_id" value="{{$policy->id}}">
                    <fieldset>
                        <section>
                            <label class="label">Отправить Брокеру</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <select style="width:100%" class="select2Custom" id="select2Brokers" name="user">
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </section>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label"></label>
                                <label class="checkbox">
                                    <input type="checkbox" name="send_email" value="1">
                                    <i></i>Отправить и письмо
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="label"></label>
                                <label class="checkbox">
                                    <input type="checkbox" name="send_note" value="1">
                                    <i></i>Отправить и  уведомление
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Отправить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    $( document ).ready(function() {
        $('#select2Brokers').select2({
            formatNoMatches: function() {
                return '';
            },
            formatMatches: function() {
                return '';
            },
            formatInputTooShort: function() {
                return '';
            },
            formatInputTooLong: function() {
                return '';
            }
        });
    });

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $approvepolicy = $('#approve-policy').validate({
            // Rules for form validation
            rules : {
                user : {
                    required : true

                }
            },

            // Messages for form validation
            messages : {
                user : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#approveBroker').modal('hide');
                        $('#sendApprove_{{$policy->id}}').remove();
                        $('#sendDecline_{{$policy->id}}').remove();
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }

</script>