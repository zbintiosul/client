<!-- Modal -->
<div class="modal fade" id="distributeAllClients" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Распределить всех клиентов
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postDistributeClientsAll')}}" id="distribute-clients" class="smart-form" method="post" onsubmit="return false;">
                    <fieldset>
                        <section class="user-note">
                            <label class="select"> <i class="icon-append fa fa-user"></i>
                                <div class="form-group">
                                    <select multiple style="width:100%" id="select2CustomUsers" placeholder="Выберите пользователей" name="users[]">
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->username}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Распределить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    $( document ).ready(function() {
        $('.select2Custom').select2();
    });

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $distributeClients = $('#distribute-clients').validate({
            // Rules for form validation
            rules : {
                'users[]' : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                'users[]' : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                    $(form).ajaxSubmit({
                        success : function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.success')}}",
                                content : "{{Lang::get('client.successText')}}",
                                color : "rgb(115, 158, 115)",
                                iconSmall : "fa fa-check bounce animated",
                                timeout : 4000
                            });
                            $('#distributeClients').modal('hide');
                            window.location.hash = "{{action('ClientController@getIndex')}}?status=0";
                            loading('hide');
                        },
                        fail :  function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.error')}}",
                                content : "{{Lang::get('client.errorText')}}",
                                color : "#c26565",
                                iconSmall : "fa fa-times bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                        },
                        error :  function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.error')}}",
                                content : "{{Lang::get('client.errorText')}}",
                                color : "#c26565",
                                iconSmall : "fa fa-times bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                        }
                    });
            }
        });

    }

</script>