<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Клиенты
			<span>>
				Все
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2> Все </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group input-groupSearch">
                                        <input class="form-control allClientsSearch" id="prepend" placeholder="Поиск клиента" type="text" value="{{Input::get('search')}}">
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    @if (Input::get('search'))
                                    <a type="button" href="{{action('ClientController@getIndex')}}"  class="btn btn-default ajaxa" title="Очищать">
                                        <i class="fa fa-fw fa-search fa-times"></i>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-warning" onclick="getModal('{{action('ClientController@getModalDistributeClient')}}','#distributeAllClients')">
                                            <i class="fa fa-random"></i> <span class="hidden-mobile hidden-xs">Распределить все</span>
                                        </button>
                                        <button class="btn btn-success" onclick="getModal('{{action('ClientController@getModalAddClient')}}','#addClient')">
                                            <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новый клиент</span>
                                        </button>
                                        <button class="btn btn-success" onclick="getModal('{{action('ClientController@getModalImportClients')}}','#importClients')">
                                            <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Импортировать</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (Session::has('import'))
                        <div class="alert alert-block alert-success">
                            <h4 class="alert-heading"><i class="fa fa-check"></i> Успех в сохранение данных!</h4>
                            <p>
                                {{Session::get('import')->log}}
                            </p>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-8 col-sm-5 col-md-5 col-lg-5">
                                {{$clients->links()}}
                            </div>
                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                <div class="row">
                                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                        <div class="btn-group showOnCheck" style="margin: 18px 0; display: none;">
                                            <button class="btn btn-warning" onclick="getModal('{{action('ClientController@getModalDistributeSelectedClient')}}','#distributeSelectedClients')">
                                                <i class="fa fa-random"></i> <span class="hidden-mobile hidden-xs">Распределить</span>
                                            </button>
                                            <button class="btn btn-danger" onclick="deleteSelected()">
                                                <i class="glyphicon glyphicon-trash"></i> <span class="hidden-mobile hidden-xs">Удалить</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <span class="pull-right countDetails"><strong>{{$clients->getFrom()}}-{{$clients->getTo()}}</strong> of <strong>{{$clients->getTotal()}}</strong></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="smart-form">
                                    <label class="checkbox">
                                        <input type="checkbox" id="checkAll" name="checkAll" onchange="checkAll('.client-checkbox')">
                                        <i></i></label>
                                </th>
                                <th></th>
                                <th class="hidden-xs">ID</th>
                                <th>Телефон</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th class="hidden-xs hidden-sm">Отчество</th>
                                <th class="hidden-xs">Комментарий</th>
                                <th class="hidden-xs">Премии</th>
                                <th class="hidden-xs">Срок</th>
                                <th class="hidden-xs smart-form">
                                    <label class="select">
                                        <select name="user" onchange="filterUser(this.value)">
                                            <option value="" selected>Username</option>
                                            @foreach ($usedUsers as $user)
                                            @if (is_object($user->user))
                                            <option value="{{$user->user->id}}" @if (Input::get('user')==$user->user->id) selected @endif>{{$user->user->username}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th class="hidden-xs">Обновлен</th>
                                <th class="hidden-xs smart-form">
                                    <label class="select">
                                        <select name="status" onchange="filterClient(this.value)">
                                            <option value="" selected>Статус</option>
                                            @foreach (StatusClient::all() as $status)
                                            <option value="{{$status->id}}" @if (Input::get('status')==$status->id) selected @endif>{{$status->name}}</option>
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th class="smart-form" style="min-width: 50px">
                                    <label class="select">
                                        <select name="status" onchange="filterPesrpectivnii(this.value)">
                                            <option value="" selected>Проспект</option>
                                            @foreach (PotentialClientType::all() as $type)

                                            <option value="{{$type->id}}" @if (Input::get('type_pot')==$type->id) selected @endif title="{{$type->name}}">
                                             @if ($type->id==1)
                                            <i class="fa fa-thumbs-up" style="color:green"></i>
                                            @endif
                                            @if ($type->id==2)
                                            <i class="fa fa-thumbs-down" style="color:red"></i>
                                            @endif

                                            </option>
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th class="hidden-xs smart-form" colspan="3">
                                    <label class="select">
                                        <select name="importid" onchange="filterImport(this.value)">
                                            <option value="" selected>импорт</option>
                                            @foreach (Import::orderBy('created_at', 'desc')->get() as $imp)
                                            <option value="{{$imp->id}}" @if (Input::get('importid')==$imp->id) selected @endif>{{$imp->id}} - {{explode("<br>", $imp->log)[0]}}</option>
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($clients as $client)
                            <tr id="tr_{{$client->id}}" idClient="{{$client->id}}" @if ($client->trashed()) class="trashedRecord" @endif >
                            <td class="custom-table-number smart-form">
                                <label class="checkbox">
                                    <input type="checkbox" class="client-checkbox" id="id_client_check_{{$client->id}}" name="client_check[]" value="{{$client->id}}">
                                    <i></i></label>
                            </td>
                            <td style="width:5px; padding:0px; margin: 0px;"  class="colorClient_{{$client->id}} @if ($client->color!=null) {{$client->color}} @endif"></td>
                            <td class="custom-table-number hidden-xs">{{$client->id}}</td>
                            <td  class="custom-data-column">
                                <div class="txt-color-darken">{{$client->phone}}</div>
                            </td>
                            <td  class="custom-data-column">
                                <div>{{$client->lastname}}</div>
                            </td>
                            <td class="custom-data-column"><div>{{$client->firstname}}</div></td>
                            <td class="custom-data-column hidden-xs"><div>{{$client->secondname}}</div></td>
                            <td  class="custom-data-column">
                                <div style="height: 50px; white-space: normal;">
                                    {{$client->comments}}
                                </div>
                            </td>
                            <td  class="custom-data-column">
                                <div style="height: 50px; white-space: normal;">
                                    @if (count($client->imports))
                                        @foreach($client->imports as $import)
                                        {{$import->pivot->sum}}
                                        @endforeach
                                    @endif
                                </div>
                            </td>
                            <td  class="custom-data-column">
                                <div style="height: 50px; white-space: normal;">
                                    @foreach($client->imports as $import)
                                    {{$import->pivot->srok}}
                                    @endforeach
                                </div>
                            </td>

                            <td  class="custom-data-column">
                                <div>
                                    @foreach ($client->users as $user)
                                    {{$user->username}}
                                    @endforeach
                                </div>
                            </td>
                            <td class="custom-data-action hidden-xs hidden-sm width100px">
                                {{$client->updated_at->format('d-m-Y H:i:s')}}
                            </td>
                            <td class="custom-data-action hidden-xs hidden-sm width100px">
                                {{$client->status->name}}
                            </td>
                            <td class="custom-data-column">
                                @if ($client->potential_client_type_id==1)
                                <i class="fa fa-thumbs-up" style="color:green"></i>
                                @endif
                                @if ($client->potential_client_type_id==2)
                                <i class="fa fa-thumbs-down" style="color:red"></i>
                                @endif
                            </td>
                            <td><div class="widget-toolbar" role="menu">
                                    <a id="" data-toggle="dropdown" class="dropdown-toggle color-box colorClient_{{$client->id}} @if ($client->color!=null) {{$client->color}} @endif  selector" href="javascript:void(0);"></a>
                                    <div class="dropdown-backdrop"></div>
                                    <ul class="dropdown-menu arrow-box-up-right color-select pull-right">
                                        <li><span class="bg-color-green" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li>
                                        <li><span class="bg-color-greenDark" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li>
                                        <li><span class="bg-color-greenLight"  onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li>
                                        <li><span class="bg-color-purple"  onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li>
                                        <li><span class="bg-color-magenta"  onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li>
                                        <li><span class="bg-color-pink"  onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li>
                                        <li><span class="bg-color-pinkDark" onclick="changeColor({{$client->id}},this)"  rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li>
                                        <li><span class="bg-color-blueLight" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li>
                                        <li><span class="bg-color-teal" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li>
                                        <li><span class="bg-color-blue"  onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li>
                                        <li><span class="bg-color-blueDark" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li>
                                        <li><span class="bg-color-darken" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="right" data-original-title="Night"></span></li>
                                        <li><span class="bg-color-yellow" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li>
                                        <li><span class="bg-color-orange" onclick="changeColor({{$client->id}},this)"  rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li>
                                        <li><span class="bg-color-orangeDark" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li>
                                        <li><span class="bg-color-red" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li>
                                        <li><span class="bg-color-redLight" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li>
                                        <li><span class="bg-color-white" onclick="changeColor({{$client->id}},this)" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li>
                                        <li><a href="javascript:void(0);" class="jarviswidget-remove-colors" onclick="changeColor({{$client->id}},this)"  rel="tooltip" data-placement="bottom" data-original-title="Reset color">Remove</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td class="custom-data-action">
                                <a href="{{action('ClientController@getViewClient')}}/{{$client->id}}" class="btn btn-xs btn-default ajaxa">Редактировать</a>
                            </td>
                            <td class="custom-data-action">
                                <button type="button" class="btn btn-xs btn-danger" onclick="deleteClient({{$client->id}})" title="Удалить">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button>
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$clients->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function changeColor(id,element)
    {
        $class = $(element).attr("class");
        $.ajax({
            type: "post",
            url: "{{action('ClientController@postChangeColor')}}",
            data: {client:id, color:$class}
        }).done(function (data) {
            $('.colorClient_'+id)[0].className = $('.colorClient_'+id)[0].className.replace(/\bbg-color.*?\b/g, '');
            $('.colorClient_'+id)[1].className = $('.colorClient_'+id)[1].className.replace(/\bbg-color.*?\b/g, '');
            //$('#colorClient_'+id).addClass($class);
            $('.colorClient_'+id).addClass($class);
            $.smallBox({
                title: "{{Lang::get('client.success')}}",
                content: "{{Lang::get('client.successText')}}",
                color: "rgb(115, 158, 115)",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
        })
            .fail(function () {
                $.smallBox({
                    title: "{{Lang::get('client.error')}}",
                    content: "{{Lang::get('client.errorText')}}",
                    color: "#c26565",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 4000
                });
            });
    }

    $( document ).ready(function() {

        $('.client-checkbox, #checkAll').on('change',function(){
            var check = 0;
            $(".client-checkbox").each(function( index ) {
                if ($(this).prop('checked'))
                {
                    check = 1;
                    return false;
                }
            });




            if (check)
            {
                $('.showOnCheck').show();
                $('.hideOnCheck').hide();
            }
            else
            {
                $('.showOnCheck').hide();
                $('.hideOnCheck').show();
            }
        });
    });

    function filterClient(value)
    {

        window.location.href = URLInjectionHelper(window.location.href,'status',value);
    }

        function filterPesrpectivnii(value)
        {

            window.location.href = URLInjectionHelper(window.location.href,'type_pot',value);
        }

    function filterUser(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'user',value);
        //window.location.hash = "{{action('ClientController@getIndex')}}?user="+value;
    }

    function filterImport(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'importid',value);
        //window.location.hash = "{{action('ClientController@getIndex')}}?user="+value;
    }



    loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);

    function runXEditDemo() {

        // constructs the suggestion engine
        var repos = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('phone'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            prefetch: '{{action('ClientController@getJsonClients')}}/-1',
            dupDetector: function(remoteMatch, localMatch) {
                    return remoteMatch.id === localMatch.id;
            }
    });

    // kicks off the loading/processing of `local` and `prefetch`
    repos.initialize();

    $('.allClientsSearch').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'users',
            displayKey: 'phone',
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: repos.ttAdapter()
        });

    $(".allClientsSearch").keyup(function (e) {
        if (e.keyCode == 13) {
            window.location.hash = "{{action('ClientController@getIndex')}}?search="+$(this).val();
        }
    });
   }

   function deleteSelected()
   {
       var data = $('.client-checkbox, #distribute-selected-clients').serializeArray();
       $.SmartMessageBox({
           title: "Удалить клиентов",
           content: "Вы уверены?",
           buttons: '[Нет][Да]'
       }, function (ButtonPressed) {
           if (ButtonPressed === "Да") {
               loading('show');
               $.ajax({
                   type: "POST",
                   url: "{{action('ClientController@postDeleteSelectedClient')}}",
                   data: data
               }).done(function (msg) {
                   //alert('{{Request::url()}}/?page={{$clients->getCurrentPage()}}');
                   loadURL("{{Request::url()}}/?page={{$clients->getCurrentPage()}}", $('#content'));
                   $.smallBox({
                       title: "{{Lang::get('client.success')}}",
                       content: "{{Lang::get('client.successText')}}",
                       color: "rgb(115, 158, 115)",
                       iconSmall: "fa fa-check bounce animated",
                       timeout: 4000
                   });
                   loading('hide');
               })
                   .fail(function () {
                       $.smallBox({
                           title: "{{Lang::get('client.error')}}",
                           content: "{{Lang::get('client.errorText')}}",
                           color: "#c26565",
                           iconSmall: "fa fa-times bounce animated",
                           timeout: 4000
                       });
                       loading('hide');
                   });
           }
           if (ButtonPressed === "Нет") {
               //nothing
           }
       });
   }

    function deleteClient(id)
    {
        $.SmartMessageBox({
            title: "Удалить клиента",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeleteClient')}}/" + id
                }).done(function (msg) {
                    $('#tr_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }

</script>