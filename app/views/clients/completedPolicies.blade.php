<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Страховки
			<span>>
                Завершенные
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Завершенные </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                           <b class="pull-right"> Всего: {{Locale::number($kaplate)}} руб </b>
                        </div>
                        <div class="row">
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                {{$salaries->links()}}
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                                <div class="btn-group pull-right" style="margin: 18px 0;">
                                    <a class="btn btn-success" href="{{action('ClientController@getExportCompletedPolicies')}}?user={{Input::get('user')}}&date={{Input::get('date')}}" target="_blank">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Экспортировать</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">№</th>
                                <th>Риск</th>
                                <th>ФИО клиента</th>
                                <th>Страховая премия</th>
                                <th>Скидка</th>
                                <th>Страховая премия со скидкой</th>
                                <th>Комиссион</th>
                                <th>Комиссион к оплате</th>
                                <th class="hidden-xs smart-form">
                                    <label class="select">
                                        <select name="user" onchange="filterUser(this.value)">
                                            <option value="" selected>Username</option>
                                            @foreach ($usedUsers as $user)
                                                @if (is_object($user->user))
                                                <option value="{{$user->user->id}}" @if (Input::get('user')==$user->user->id) selected @endif>{{$user->user->lastname}} {{$user->user->firstname}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th>Invoice ID</th>
                                <th style="min-width: 100px;">
                                    <input class="form-control" id="month" type="month" placeholder="To" value="{{Input::get('date')}}" max="{{Carbon\Carbon::now()->format("Y-m")}}" onblur="filterDate(this.value)">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($salaries as $salary)
                            <tr>
                                <td class="custom-table-number hidden-xs">{{$salary->id}}</td>
                                <td class="custom-data-name">{{$salary->policy->type->name}}</td>
                                <td class="custom-data-name">
                                    @if (!empty($salary->policy->client->id))
                                         @if (empty($salary->policy->client->lastname) && empty($salary->policy->client->firstname))
                                            {{$salary->policy->client->id}}
                                         @else
                                            {{$salary->policy->client->lastname}} {{$salary->policy->client->firstname}}
                                        @endif
                                    @endif
                                </td>
                                <td class="custom-data-name">{{$salary->policy->calcTotalSum()}}</td>
                                <td class="custom-data-name">{{Locale::number($salary->sum_diff)}}</td>
                                <td class="custom-data-name">
                                    <div style="white-space: nowrap;">
                                        {{Locale::number($salary->calcNew())}} руб
                                     </div>
                                </td>
                                <td class="custom-data-name">{{$salary->salaryTable->percentage}} %</td>
                                <td class="custom-data-name">{{Locale::number($salary->calcVenit())}} руб</td>
                                <td class="custom-data-name">
                                    @if (is_object($salary->user))
                                        <a href="{{action('UserManagementController@getUser')}}/{{$salary->user->id}}" class="ajaxa">{{$salary->user->lastname}} {{$salary->user->firstname}} </a>@if($salary->user->trashed()) (уд.) @endif
                                    @else
                                        Физически удален
                                    @endif
                                </td>
                                <td class="custom-data-name">
                                    <a href="{{action('ClientController@getViewPolicy')}}/{{$salary->policy->id}}" class="ajaxa">{{$salary->policy->id}}</a>
                                </td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">{{$salary->created_at->format('d-m-Y H:i:s')}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$salaries->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function filterUser(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'user',value);
    }

    function filterDate(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'date',value);
    }

</script>