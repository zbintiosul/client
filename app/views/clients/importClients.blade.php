<!-- Modal -->
<div class="modal fade" id="importClients" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Импорт клиентов
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postImportClientsStep1')}}" id="import-clients" class="smart-form" method="post">
                    <fieldset>
                        <section id="importSection">
                            <div class="input input-file">
                                <span class="button"><input id="fileImport" type="file" name="fileImport" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Выберите файл для импорта" readonly="" >
                            </div>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Импорт
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();


    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addclient = $('#import-clients').validate({
            // Rules for form validation
            rules : {
                fileImport : {
                    required : true,
                    extension: "xls|xlsx"
                }
            },

            // Messages for form validation
            messages : {
                fileImport : {
                    required : 'Пожалуйста, заполните это поле',
                    extension: 'Файлы принимаются: .xls, .xlsx'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {

                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#importClients').modal('hide');
                        window.location.hash = "{{action('ClientController@getImportClientsStep2')}}/"+msg;
                        //loadURL("{{action('ClientController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });


        $.validator.addMethod("checkPhone",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('ClientController@postCheckClient')}}", // script to validate in server side
                    data: {phone: value},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Этот телефон уже занят! Попробуйте другой."
        );

    }

</script>