<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Страховки
			<span>>
                Завершенные
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Завершенные </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                           <b class="pull-right"> Всего: {{Locale::number($pribali)}} руб </b>
                        </div>
                        <div class="row">
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                {{$salaries->links()}}
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                                <div class="btn-group pull-right" style="margin: 18px 0;">
                                    <a class="btn btn-success" href="{{action('ClientController@getExportIncome')}}?user={{Input::get('user')}}&date={{Input::get('date')}}" target="_blank">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Экспортировать</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">№</th>
                                <th>ФИО клиента</th>
                                <th>Марка Модели</th>
                                <th>Страховая премия</th>
                                <th>Риск</th>
                                <th>Комиссион</th>
                                <th>Скидка</th>
                                <th>Комиссион с учетом скидки</th>
                                <th style="min-width: 100px;">
                                    <input class="form-control" id="month" type="month" placeholder="To" value="{{Input::get('date')}}" max="{{Carbon\Carbon::now()->format("Y-m")}}" onblur="filterDate(this.value)">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($salaries as $salary)
                            <tr>
                                <td class="custom-table-number hidden-xs">{{$salary->id}}</td>
                                <td class="custom-data-name">
                                    @if (!empty($salary->policy->client->id))
                                         @if (empty($salary->policy->client->lastname) && empty($salary->policy->client->firstname))
                                            {{$salary->policy->client->id}}
                                         @else
                                            {{$salary->policy->client->lastname}} {{$salary->policy->client->firstname}}
                                        @endif
                                    @endif
                                </td>
                                <td class="custom-data-name">{{$salary->policy->car->modelCar->brand->name}} {{$salary->policy->car->modelCar->name}}</td>
                                <td class="custom-data-name"><div style="white-space: nowrap;"><span id="policySum_{{$salary->id}}" >{{$salary->policy->calcTotalSum()}}</span> руб</div></td>
                                <td class="custom-data-name">{{$salary->policy->type->name}}</td>

                                <td class="custom-data-name" ><div style="white-space: nowrap;">
                                        <a href="#" id="company_per_{{$salary->id}}" data-id="{{$salary->id}}" class="company_per_Class" data-type="text" data-pk="1" data-original-title="Комиссион">{{Locale::number($salary->company_per)}}</a> %
                                </div></td>
                                <td class="custom-data-name" >
                                    <div style="white-space: nowrap;">
                                            <a href="#" id="sum_diff_{{$salary->id}}" data-id="{{$salary->id}}" class="sum_diff_Class" data-type="text" data-pk="1" data-original-title="Cкидка">{{$salary->sum_diff}}</a>
                                    </div>
                                </td>
                                <td class="custom-data-name" id="companySum_{{$salary->id}}">{{Locale::number($salary->calcCompanyTotVenit())}} руб</td>
                                <td class="custom-data-action hidden-xs hidden-sm width100px">{{$salary->created_at->format('d-m-Y H:i:s')}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$salaries->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function calAll()
    {
        loadURL(getCurentUrl(), $('#content'));

        /*$('.company_per_Class').each(function(){
            var idD = $(this).attr('data-id');
            //alert(idD);
            str = $('#policySum_'+idD).text();
            str = str.replace(/\s+/g, '');;
            $('#companySum_'+idD).text(parseFloat(str)* parseFloat($(this).text())/100);
            //alert($(this).text());
        });*/
    }




    /*
     * X-Ediable
     */
    /*
     * X-Ediable
     */

    loadScript("/js/plugin/x-editable/moment.min.js", loadMockJax);

    function loadMockJax() {
        loadScript("/js/plugin/x-editable/jquery.mockjax.min.js", loadXeditable);
    }

    function loadXeditable() {
        loadScript("/js/plugin/x-editable/x-editable.min.js", runXEditDemo);
    }

    function runXEditDemo() {

        (function (e) {
            "use strict";
            var t = function (e) {
                this.init("address", e, t.defaults)
            };
            e.fn.editableutils.inherit(t, e.fn.editabletypes.abstractinput);
            e.extend(t.prototype, {
                render: function () {
                    this.$input = this.$tpl.find("input")
                },
                value2html: function (t, n) {
                    if (!t) {
                        e(n).empty();
                        return
                    }
                    var r = e("<div>").text(t.city).html() + ", " + e("<div>").text(t.street).html() +
                        " st., bld. " + e("<div>").text(t.building).html();
                    e(n).html(r)
                },
                html2value: function (e) {
                    return null
                },
                value2str: function (e) {
                    var t = "";
                    if (e)
                        for (var n in e)
                            t = t + n + ":" + e[n] + ";";
                    return t
                },
                str2value: function (e) {
                    return e
                },
                value2input: function (e) {
                    if (!e)
                        return;
                    this.$input.filter('[name="city"]').val(e.city);
                    this.$input.filter('[name="street"]').val(e.street);
                    this.$input.filter('[name="building"]').val(e.building)
                },
                input2value: function () {
                    return {
                        city: this.$input.filter('[name="city"]').val(),
                        street: this.$input.filter('[name="street"]').val(),
                        building: this.$input.filter('[name="building"]').val()
                    }
                },
                activate: function () {
                    this.$input.filter('[name="city"]').focus()
                },
                autosubmit: function () {
                    this.$input.keydown(function (t) {
                        t.which === 13 && e(this).closest("form").submit()
                    })
                }
            });
            t.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
                tpl: '<div class="editable-address"><label><span>City: </span><input type="text" name="city" class="input-small"></label></div><div class="editable-address"><label><span>Street: </span><input type="text" name="street" class="input-small"></label></div><div class="editable-address"><label><span>Building: </span><input type="text" name="building" class="input-mini"></label></div>',
                inputclass: ""
            });
            e.fn.editabletypes.address = t
        })(window.jQuery);

        //ajax mocks
        $.mockjaxSettings.responseTime = 500;

        $.mockjax({
            url: '/post',
            response: function (settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function (settings) {
                this.responseText = 'Please input correct value';postSaveSumDiff
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function (settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        /*
         * X-EDITABLES
         */

        $('#inline').on('change', function (e) {
            if ($(this).prop('checked')) {
                window.location.href = '?mode=inline#ajax/plugins.html';
            } else {
                window.location.href = '?#ajax/plugins.html';
            }
        });

        if (window.location.href.indexOf("?mode=inline") > -1) {
            $('#inline').prop('checked', true);
            $.fn.editable.defaults.mode = 'inline';
        } else {
            $('#inline').prop('checked', false);
            $.fn.editable.defaults.mode = 'popup';
        }

        //defaults
        $.fn.editable.defaults.url = '/post';
        //$.fn.editable.defaults.mode = 'inline'; use this to edit inline

        $( document ).ready(function() {
            $('.company_per_Class').each(function(e){
                //alert('s');
                //editables
                $(this).editable({
                    url: '{{action('ClientController@postSaveCompanyPer')}}/'+$(this).attr('data-id'),
                    type: 'text',
                    name: 'company_per',
                    title: 'Комиссион',
                    success: function(response, newValue) {
                        calAll();
                    }
                });

            });

            $('.sum_diff_Class').each(function(e){
                //alert('s');
                //editables
                $(this).editable({
                    url: '{{action('ClientController@postSaveSumDiff')}}/'+$(this).attr('data-id'),
                    type: 'text',
                    name: 'summ_diff',
                    title: 'Cкидка',
                    success: function(response, newValue) {
                    calAll();
                }
            });

            });

        });



    }

    function filterUser(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'user',value);
    }

    function filterDate(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'date',value);
    }


</script>