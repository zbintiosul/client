<tr id="tr_edit_policy" style="border-top: none;">
<td colspan="8">
<div class="row" id="editPolicyDiv">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{action('ClientController@postEditClientPolicyDo')}}" id="edit-policy" class="smart-form" method="post">
        <input type="hidden" name="policy_id" value="{{$policy->id}}">
            <fieldset>
                <div class="row">
                    <section class="col col-6">
                    <label class="label mandatory">Тип страховки *</label>
                    <label class="select">
                        <select name="type_policy_id" onchange="policyEditChange(this.value)">
                            <option value="0" selected disabled>Выберите тип</option>
                            @foreach ($typesPolicy as $type)
                            <option value="{{$type->id}}"  @if ($policy->type->id == $type->id) selected="selected" @endif>{{$type->name}}</option>
                            @endforeach
                        </select>
                        <i></i> </label>
                    </section>
                    <section  class="col col-6 editOsago editKasko">
                        <label class="label">Страховая компания</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="insurance_company" placeholder="Страховая компания" value="{{$policy->insurance_company}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-6 editOther">
                        <label class="label">Называние</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="name" placeholder="Называние" value="{{$policy->name}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                    <section class="col col-6  editOsago editKasko">
                        <label class="label">Дата окончания предыдущева полиса</label>
                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="date_last_policy" placeholder="Выберите дату" class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy" value="{{AppHelper::conInvDate($policy->date_last_policy)}}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </label>
                    </section>
                    <section  class="col col-6 editOsago editKasko">
                        <label class="label">Называние предыдущей СК</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="last_policy_company" placeholder="Называние предыдущей СК" value="{{$policy->last_policy_company}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4 editOsago editKasko">
                        <label class="label">Фамилия менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="lastname_manager" placeholder="Фамилия менеджера" value="{{$policy->lastname_manager}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                    <section  class="col col-4 editOsago editKasko">
                        <label class="label">Имя менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="firstname_manager" placeholder="Имя менеджера" value="{{$policy->firstname_manager}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                    <section  class="col col-4 editOsago editKasko">
                        <label class="label">Отчество менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="secondname_manager" placeholder="Отчество менеджера" value="{{$policy->secondname_manager}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-2 editOsago" @if ($policy->type->id == 2) style="display:none;" @endif>
                        <label class="label">Кбм</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="kbm" placeholder="Кбм" value="{{$policy->kbm}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section class="col col-6 editOsago editKasko">
                        <label class="label">Период страхавания автомобиля</label>
                        <label class="input"> <i class="icon-append fa">Мес.</i>
                            <input type="text" name="period_policy" placeholder="Период страхавания автомобиля" value="{{$policy->period_policy}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4 editKasko" @if ($policy->type->id == 1) style="display:none;" @endif>
                        <label class="label">Рыночная стоимость автомобиля</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="sum_market_car" placeholder="Рыночьная стоимость автомобиля" value="{{$policy->sum_market_car}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4 editKasko" @if ($policy->type->id == 1) style="display:none;" @endif>
                        <label class="label">ТС приобретено в кредит</label>
                        <label class="toggle">
                            <input type="checkbox" name="in_credit"  @if ($policy->in_credit == 1) checked="checked" @endif value="1" />в кредит?
                            <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                    </section>
                    <section  class="col col-4 editKasko" @if ($policy->type->id == 1) style="display:none;" @endif>
                        <label class="label">Банк кредит</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="bank_credit" placeholder="Банк"  value="{{$policy->bank_credit}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <section class="editOsago editKasko" id="selectCarSec">
                        <label class="label mandatory">Автомобиль *</label>
                        <label class="select">
                            <select name="car_id">
                                <option value="0" selected disabled>Выберите автомобиль</option>
                                @foreach ($cars as $car)
                                <option value="{{$car->id}}" @if ($policy->car->id == $car->id) selected="selected" @endif>{{$car->modelCar->brand->name}} {{$car->modelCar->name}} {{$car->year_of_construction}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                </section>
                <div class="row">
                    <section  class="col col-4 editOsago editKasko">
                        <label class="label">Каличество лиц допущенных к управлению</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="hidden" id="real_nr_drivers" name="real_nr_drivers" value="0" />
                            <input type="text" class="number_drivers_edit" name="number_drivers" placeholder="Каличество лиц допущенных к управлению" value="{{$policy->number_drivers}}" disabled>
                            <input type="hidden" class="number_drivers_edit" name="number_drivers" value="{{$policy->number_drivers}}" />
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-2 editOsago editKasko">
                        <label class="label" style="height: 19px"></label>
                        <a class="btn btn-sm btn-success" onclick="addDriver()" title="Добавить">
                            <i class="fa fa-plus"></i> 1
                        </a>
                    </section>
                    <section  class="col col-3 editOsago editKasko editOther">
                        <label class="label">Страховая премия</label>
                        <label class="input"> <i class="icon-append fa">руб</i>
                            <input type="text" name="totalSum" placeholder="Общая сумма" onchange="calEditTotalSum()" value="{{$policy->totalSum}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9 .]</b>
                        </label>
                    </section>
                    <section  class="col col-3 editOsago editKasko editOther">
                        <label class="label">Коэффициент</label>
                        <label class="input"> <i class="icon-append fa">%</i>
                            <input type="text" name="coefficient" placeholder="Коэффициент" onchange="calEditTotalSum()" value="{{$policy->coefficient}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9 .]</b>
                        </label>
                    </section>
                </div>
            </fieldset>
            <fieldset class="editOsago editKasko">
                <div id="contentDriversEdit">
                    @foreach($policy->drivers as $driver)
                    <div class="row driversDiv driversDiv_{{$driver->id}}">
                        <section  class="col col-4">
                            <label class="label">Фамилия</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="lastname_driver_edit_{{$driver->id}}" placeholder="Фамилия" value="{{$driver->lastname}}">
                                <input type="hidden" name="edit_driver_{{$driver->id}}" value="0" />
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                            </label>
                        </section>
                        <section  class="col col-4">
                            <label class="label">Имя</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="firstname_driver_edit_{{$driver->id}}" placeholder="Имя" value="{{$driver->firstname}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                            </label>
                        </section>
                        <section  class="col col-3">
                            <label class="label">Отчество</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="secondname_driver_edit_{{$driver->id}}" placeholder="Отчество" value="{{$driver->secondname}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                            </label>
                        </section>
                        <section  class="col col-1">
                            <a class="btn btn-sm btn-danger btn-driver-delete" onclick="deleteDriver({{$driver->id}})" title="Удалить">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </section>
                    </div>
                    <div class="row driversDiv driversDiv_{{$driver->id}} editOsago editKasko">
                        <section  class="col col-4">
                            <label class="label">Пол</label>
                            <label class="select">
                                <select name="gender_id_driver_edit_{{$driver->id}}">
                                    <option value="0" selected disabled>Выберите пол</option>
                                    @foreach ($genders as $gender)
                                    <option value="{{$gender->id}}" @if ($driver->gender->id == $gender->id) selected="selected" @endif>{{$gender->name}}</option>
                                    @endforeach
                                </select>
                                <i></i> </label>
                        </section>
                        <section  class="col col-2">
                            <label class="label">Возраст</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="age_driver_edit_{{$driver->id}}" placeholder="Возраст" value="{{$driver->age}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                            </label>
                        </section>
                        <section  class="col col-2">
                            <label class="label">Стаж</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="experience_driver_edit_{{$driver->id}}" placeholder="Стаж" value="{{$driver->experience}}">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                            </label>
                        </section>
                        <section  class="col col-3">
                            <label class="label">Семейное положение</label>
                            <label class="select">
                                <select name="marriage_status_driver_id_edit_{{$driver->id}}">
                                    <option value="0" selected disabled>Выберите положение</option>
                                    @foreach ($mStatuses as $status)
                                    <option value="{{$status->id}}" @if ($driver->mariage_status_id == $status->id) selected="selected" @endif>{{$status->name}}</option>
                                    @endforeach
                                </select>
                                <i></i> </label>
                        </section>
                        <section  class="col col-1">

                        </section>
                    </div>
                    @endforeach
                </div>
            </fieldset>
            <fieldset>
            <div class="row">
                <section  class="col col-8">

                </section>
                <section  class="col col-4 editOsago editKasko editOther">
                    <div class="invoice-sum-total pull-right">
                        <h3><strong>Страховая премия: <span class="text-success"><span id="editTotalSum">{{$policy->calcTotalSum()}}</span> руб.</span></strong></h3>
                    </div>
                </section>
            </div>
            </fieldset>
            <footer class="client_footer">
                <div class="btn-group btn-group-justified">
                    <a href="javascript:void(0);" onclick="$('#tr_edit_policy').toggle('slow')" class="btn btn-default"> Отменить</a>
                    <a href="javascript:void(0);" onclick="$('#edit-policy').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</a>
                </div>
            </footer>
        </form>
    </div>
</div>
</td>
</tr>
<script>
    pageSetUp();

    $( document ).ready(function() {
        policyEditChange($("#edit-policy").find("select[name=type_policy_id]").val());
       // console.log($("#edit-policy").find("select[name=type_policy_id]").val());
    });


    function addDriver()
    {
        var nr = $('#real_nr_drivers').val();
        //$('#number_drivers_edit').val();
        nr = nr*1+1;
        $.ajax({
            type: "POST",
            url: "{{action('ClientController@postGetDriver')}}/editNew_"+nr
        }).done(function(msg) {
            //$('#contentDriversEdit').html();
            $('#contentDriversEdit').append(msg);
            $('.number_drivers_edit').val($('.number_drivers_edit').val()*1+1);
            $('#real_nr_drivers').val($('#real_nr_drivers').val()*1+1);
        })
            .fail(function() {
                $.smallBox({
                    title : "{{Lang::get('client.error')}}",
                    content : "{{Lang::get('client.errorText')}}",
                    color : "#c26565",
                    iconSmall : "fa fa-times bounce animated",
                    timeout : 4000
                });
            });
    }



    function deleteEditHtmlDriver(element)
    {
        $(element).remove();
        $('.number_drivers_edit').val($('.number_drivers_edit').val()*1-1);
    }

    function policyEditChange(nr)
    {
        switch(nr) {
            case '1':
                $('.editKasko').hide('slow');
                $('.editOther').hide('slow');
                $('.editOsago').show('slow');
                break;
            case '2':
                $('.editOther').hide('slow');
                $('.editOsago').hide('slow');
                $('.editKasko').show('slow');
                break;
            case '3':
                $('.editKasko').hide('slow');
                $('.editOsago').hide('slow');
                $('.editOther').show('slow');
                break;
        }
    }

    function calEditTotalSum()
    {
        coef = $('#edit-policy input[name="coefficient"]').val();
        sum = $('#edit-policy input[name="totalSum"]').val();
        var num = coef*sum;
        $('#editTotalSum').text(num.toFixed(2));
    }

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $editpolicy = $('#edit-policy').validate({
            // Rules for form validation
            rules : {
                type_policy_id : {
                    required: true
                },

                kbm: {
                    number: true
                },
                sum_now_car: {
                    number: true
                },
                sum_market_car: {
                    number: true
                },
                totalSum: {
                    number: true
                },
                coefficient: {
                    number: true
                },
                number_drivers: {
                    number: true
                },
                car_id : {
                     required: true
                }
            },

            // Messages for form validation
            messages : {
                type_policy_id : {
                    required : 'Пожалуйста, выберите элемент'
                },
                kbm:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                sum_now_car:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                sum_market_car:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                totalSum:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                coefficient:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                number_drivers:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                car_id : {
                    required : 'Пожалуйста, выберите элемент'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addPolicyDiv').hide();
                        ajaxLoadPart('s3',getCurentUrl(),null,s3TabShow);
                        //ajaxLoadPart('selectCarSec','{{action('ClientController@getInProgress')}}/{{$client->id}}',null,s4TabShow);
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s3TabShow()
    {
        $('#s3').removeAttr('style');
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    }


    function deleteDriver(id)
    {
        $.SmartMessageBox({
            title: "Удалить водителя",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeleteDriver')}}/" + id
                }).done(function (msg) {
                    $('.driversDiv_' + id).remove();
                    $('.number_drivers_edit').val($('.number_drivers_edit').val()*1-1);
                    //$('#tr_edit_car').remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }

</script>