<div id="bodyPolicy" style="font-size: 130%; width: 50%; margin: 0px auto; max-width: 750px; min-width: 600px; padding: 5px;">
<form class="smart-form paper-form">
<fieldset>
    <section class="align-center">
        <h3>Заявка на страхование {{$policy->type->name}}</h3>
    </section>
</fieldset>
<fieldset>
    <legend>1. Страхователь</legend>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->client->lastname}} {{$policy->client->firstname}} {{$policy->client->secondname}}">
        </label>
        <div class="note align-center">
            ФИО страхователя
        </div>
    </section>
    <div class="row">
        <section class="col col-5">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->client->phone}}">
            </label>
            <div class="note align-center">
                Контактный телефон
            </div>
        </section>
        <section class="col col-3">
            <label class="input text-input">
                Дата рождения
            </label>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                день
            </div>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                месяц
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                год
            </div>
        </section>
    </div>
    <div class="row">
        <section class="col col-3">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->client->serial_bulletin}}">
            </label>
            <div class="note align-center">
                Серия № паспорта
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->client->serial_driver_licence}}">
            </label>
            <div class="note align-center">
                Серия, № в/у
            </div>
        </section>
        <section class="col col-3">
            <label class="input text-input">
                Дата выдачи в/у
            </label>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->client->date_driver_licence)}}">
            </label>
            <div class="note align-center">
                день
            </div>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->client->date_driver_licence)}}">
            </label>
            <div class="note align-center">
                месяц
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->client->date_driver_licence)}}">
            </label>
            <div class="note align-center">
                год
            </div>
        </section>
    </div>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->client->address}}">
        </label>
        <div class="note align-center">
            Адрес клиента
        </div>
    </section>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->insurance_company}}">
        </label>
        <div class="note align-center">
            Страховая компания
        </div>
    </section>
    <div class="row">
        <section class="col col-6">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->last_policy_company}}">
            </label>
            <div class="note align-center">
                Название предыдущей СК
            </div>
        </section>
        <section class="col col-6">
            <label class="input">
                <div class="input-group">
                    <input type="text" class="paper-input" disabled value="{{$policy->calcTotalSum()}}">
                    <div class="input-group-btn">
                        <input type="text" class="paper-input" value="руб." disabled style="width: 30px;padding: 0px;">руб.
                    </div>
                </div>
            </label>
            <div class="note align-center">
                Расчёт
            </div>
        </section>
    </div>
    <div class="row">
        <section class="col col-8">
            <label class="input text-input" style="text-align: right;">
                Дата окончания предыдущего полиса
            </label>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->date_last_policy)}}">
            </label>
            <div class="note align-center">
                день
            </div>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->date_last_policy)}}">
            </label>
            <div class="note align-center">
                месяц
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->date_last_policy)}}">
            </label>
            <div class="note align-center">
                год
            </div>
        </section>
    </div>
    <div class="row">
        <section class="col col-4">
            <label class="input">
                <div class="input-group">
                    <input type="text" class="paper-input" disabled value="{{$policy->period_policy}}">
                    <div class="input-group-btn">
                        <input type="text" class="paper-input" value="мес." disabled style="width: 30px;padding: 0px;">
                    </div>
                </div>
            </label>
            <div class="note align-center">
                Период страхования автомобиля
            </div>
        </section>
        <section class="col col-4">
            <label class="input text-input">
                Дата оформления заявки
            </label>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->date_created)}}">
            </label>
            <div class="note align-center">
                день
            </div>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->date_created)}}">
            </label>
            <div class="note align-center">
                месяц
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->date_created)}}">
            </label>
            <div class="note align-center">
                год
            </div>
        </section>
    </div>
</fieldset>
<fieldset>
<legend>2. Транспортное средство (ТС)</legend>
<div class="row">
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->modelCar->brand->name}}">
        </label>
        <div class="note align-center">
            Марка
        </div>
    </section>
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->modelCar->name}}">
        </label>
        <div class="note align-center">
            Модель
        </div>
    </section>
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->year_of_construction}}">
        </label>
        <div class="note align-center">
            Год выпуска
        </div>
    </section>
</div>
<div class="row">
    <section class="col col-6">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->register_number}}">
        </label>
        <div class="note align-center">
            Гос. № авто
        </div>
    </section>
    <section class="col col-6">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->VIN}}">
        </label>
        <div class="note align-center">
            VIN
        </div>
    </section>
</div>
<div class="row">
    <section class="col col-6">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->region->name}} {{$policy->car->region->value}}">
        </label>
        <div class="note align-center">
            Место регистраций автомобиля (Кт)
        </div>
    </section>
    <section class="col col-6">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->typeCar->name}}">
        </label>
        <div class="note align-center">
            Тип ТС
        </div>
    </section>
</div>
</fieldset>
</form>
<form class="smart-form paper-form" style="margin-top: 20px;">
<fieldset>
<div class="row">
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->typeConsum->name}}">
        </label>
        <div class="note align-center">
            Тип топлива
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <div class="input-group">
                <input type="text" class="paper-input" disabled value="{{number_format($policy->car->motor_volum,0,'.',' ')}}">
                <div class="input-group-btn">
                    <input type="text" class="paper-input" value="л." disabled style="width: 30px;padding: 0px;">л.
                </div>
            </div>
        </label>
        <div class="note align-center">
            Рабочий объём двигателя
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->motor_ls}}">
        </label>
        <div class="note align-center">
            Л/силы
        </div>
    </section>
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->frame_number}}">
        </label>
        <div class="note align-center">
            Номер рамы
        </div>
    </section>
</div>

<div class="row">
    <section class="col col-4">
        <label class="input">
            <div class="input-group">
                <input type="text" class="paper-input" disabled value="{{number_format($policy->car->mileage,0,'.',' ')}}">
                <div class="input-group-btn">
                    <input type="text" class="paper-input" value="км" disabled style="width: 30px;padding: 0px;">км
                </div>
            </div>
        </label>
        <div class="note align-center">
            Пробег ТС
        </div>
    </section>
    <section class="col col-4">
        <label class="input">
            <div class="input-group">
                <input type="text" class="paper-input" disabled value="{{$policy->car->pure_mass}}">
                <div class="input-group-btn">
                    <input type="text" class="paper-input" value="кг." disabled style="width: 30px;padding: 0px;">кг.
                </div>
            </div>
        </label>
        <div class="note align-center">
            Масса без нагрузки
        </div>
    </section>
    <section class="col col-4">
        <label class="input">
            <div class="input-group">
                <input type="text" class="paper-input" disabled value="{{$policy->car->allow_max_mass}}">
                <div class="input-group-btn">
                    <input type="text" class="paper-input" value="кг." disabled style="width: 30px;padding: 0px;">кг.
                </div>
            </div>
        </label>
        <div class="note align-center">
            Разрешенная максимальная масса
        </div>
    </section>
</div>
<div class="row">
    <section class="col col-6">
        <label class="input text-input">
            Номер кузова, совпадает с VIN?
        </label>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input" disabled value="@if ($policy->car->stand_number==1) Да @else Нет @endif">
        </label>
    </section>
    @if ($policy->car->stand_number!=1)
    <section class="col col-4">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->car->stand_number}}">
        </label>
        <div class="note align-center">
            Номер кузова
        </div>
    </section>
    @endif
</div>
<div class="row">
    <section class="col col-3">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$policy->car->doc->name}}">
        </label>
        <div class="note align-center">
            Документы
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$policy->car->doc_serial}}">
        </label>
        <div class="note align-center">
            Серия
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$policy->car->doc_number}}">
        </label>
        <div class="note align-center">
            Номер
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$policy->car->doc_who}}">
        </label>
        <div class="note align-center">
            Кем выдан
        </div>
    </section>
    <section class="col col-3">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$policy->car->doc_when}}">
        </label>
        <div class="note align-center">
            Когда
        </div>
    </section>
</div>
<section>
    <label class="input text-input">
        Количество лиц допущенных к управлению
    </label>
</section>
@if (count($policy->drivers)==0)
@for($i=1;$i<4;$i++)
<div class="row">
    <section class="col col-5">
        <label class="input">
            <input type="text" class="paper-input" disabled value="">
        </label>
        <div class="note align-center">
            ФИО
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input" disabled value="">
        </label>
        <div class="note align-center">
            Пол
        </div>
    </section>
    <section class="col col-1">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="">
        </label>
        <div class="note align-center">
            Возвраст
        </div>
    </section>
    <section class="col col-1">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="">
        </label>
        <div class="note align-center">
            Стаж
        </div>
    </section>
    <section class="col col-3">
        <label class="input">
            <input type="text" class="paper-input" disabled value="">
        </label>
        <div class="note align-center">
            Семейное положение
        </div>
    </section>
</div>
@endfor
@endif
@foreach ($policy->drivers as  $driver)
<div class="row">
    <section class="col col-5">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$driver->firstname}} {{$driver->lastname}} {{$driver->secondname}}">
        </label>
        <div class="note align-center">
            ФИО
        </div>
    </section>
    <section class="col col-2">
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$driver->gender->name}}">
        </label>
        <div class="note align-center">
            Пол
        </div>
    </section>
    <section class="col col-1">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$driver->age}}">
        </label>
        <div class="note align-center">
            Возвраст
        </div>
    </section>
    <section class="col col-1">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$driver->experience}}">
        </label>
        <div class="note align-center">
            Стаж
        </div>
    </section>
    <section class="col col-3">
        <label class="input">
            <input type="text" class="paper-input no-padding" disabled value="{{$driver->mariageStatus->name}}">
        </label>
        <div class="note align-center">
            Семейное положение
        </div>
    </section>
</div>
@endforeach

</fieldset>
<fieldset>
    <legend>3. Дополнительное</legend>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->client->time_contact}}">
        </label>
        <div class="note align-center">
            Удобное время контакта
        </div>
    </section>
</fieldset>
</form>
</div>