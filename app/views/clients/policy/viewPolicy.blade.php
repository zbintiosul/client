<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>{{$policy->type->name}} {{$policy->id}}  </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" onclick="printPolicy()">
                                            <i class="fa fa-print"></i> <span class="hidden-mobile hidden-xs">Печать</span>
                                        </button>
                                        <button class="btn btn-default" onclick="getModal('{{action('ClientController@getModalSendEmailToClient')}}/{{$policy->id}}','#emailToClient')">
                                            <i class="fa fa-envelope-o"></i> <span class="hidden-mobile hidden-xs">Отправить</span>
                                        </button>
                                        @if ($policy->status->id == 2)
                                            @if (Authority::can('approve_clients'))
                                                <a href="{{action('ClientController@getViewClient')}}/{{$policy->client->id}}"  class="btn btn-default ajaxa">
                                                    <i class="fa fa-pencil-square-o"></i> <span class="hidden-mobile hidden-xs">Редактировать</span>
                                                </a>
                                                <button class="btn btn-success" id="sendApprove_{{$policy->id}}"  onclick="getModal('{{action('ClientController@getModalSelectBroker')}}/{{$policy->id}}','#approveBroker')">
                                                    <i class="fa fa-thumbs-o-up"></i> <span class="hidden-mobile  ">Утвердить</span>
                                                </button>
                                                <button class="btn btn-warning" id="sendDecline_{{$policy->id}}" onclick="declinePolicy({{$policy->id}})">
                                                    <i class="fa fa-thumbs-o-down"></i> <span class="hidden-mobile  ">Отклонить</span>
                                                </button>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (Authority::can('download_client_files'))
                        <div>
                            <table id="custom-table" class="table table-striped table-hover dataTable">
                                @foreach($policy->client->files()->orderBy('created_at','desc')->get() as $file)
                                <tr>
                                    <td>
                                        {{$file->desc}}
                                    </td>
                                    <td>
                                        <a href="{{action('ClientController@getDownload')}}/{{$file->id}}" target="_blank">{{$file->original_name}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        @endif
                        @if ($policy->type->id == 1)
                            @include('clients.policy.OSAGO')
                        @else
                            @if ($policy->type->id == 2)
                                @include('clients.policy.KASKO')
                            @else
                                @if ($policy->type->id == 3)
                                @include('clients.policy.OTHER')
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();
    function printPolicy()
    {
        Popup($('#bodyPolicy').html());
    }

    function approvePolicy(id)
    {
        $.SmartMessageBox({
            title: "Утверждение",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('BrokerController@postApproveBroker')}}",
                    data: {policy:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $('.sendResponce').remove();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }

    function declinePolicy(id)
    {
        $.SmartMessageBox({
            title: "Утверждение",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('BrokerController@postDeclineBroker')}}",
                    data: {policy:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $(".sendResponce").remove();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }



</script>