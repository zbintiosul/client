<div id="bodyPolicy" style="font-size: 130%;  margin: 0px auto; max-width: 750px; min-width: 600px; padding: 5px;">
    <form class="smart-form paper-form">
        <fieldset>
            <section class="align-center">
                Заявка на страхование {{$policy->type->name}}
            </section>
        </fieldset>
        <fieldset>
            <legend>1. Страхователь</legend>
            <section>
                <label class="input">
                    <input type="text" class="paper-input" disabled value="{{$policy->client->lastname}} {{$policy->client->firstname}} {{$policy->client->secondname}}">
                </label>
                <div class="note align-center">
                    ФИО страхователя
                </div>
            </section>
            <div class="row">
                <section class="col col-5">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->client->phone}}">
                    </label>
                    <div class="note align-center">
                        Контактный телефон
                    </div>
                </section>
                <section class="col col-3">
                    <label class="input text-input">
                        Дата рождения
                    </label>
                </section>
                <section class="col col-1">
                    <label class="input">
                            <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->client->birth_date)}}">
                    </label>
                    <div class="note align-center">
                        день
                    </div>
                </section>
                <section class="col col-1">
                    <label class="input">
                        <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->client->birth_date)}}">
                    </label>
                    <div class="note align-center">
                        месяц
                    </div>
                </section>
                <section class="col col-2">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->client->birth_date)}}">
                    </label>
                    <div class="note align-center">
                        год
                    </div>
                </section>
            </div>
            <div class="row">
                <section  class="col col-6">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->insurance_company}}">
                    </label>
                    <div class="note align-center">
                        Страховая компания
                    </div>
                </section>
                <section class="col col-3">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->typeSumInsured->name}}">
                    </label>
                    <div class="note align-center">
                        Страховая сума
                    </div>
                </section>
                <section class="col col-3">
                    <label class="input">
                        <div class="input-group">
                            <input type="text" class="paper-input" disabled value="{{$policy->calcTotalSum()}}">
                            <div class="input-group-btn">
                                <input type="text" class="paper-input" value="руб." disabled style="width: 30px;padding: 0px;">руб.
                            </div>
                        </div>
                    </label>
                    <div class="note align-center">
                        Страховая премия
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->insuranceRisk->name}}">
                    </label>
                    <div class="note align-center">
                        Страховые риски
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input text-input">
                        Дата оформления заявки
                    </label>
                </section>
                <section class="col col-1">
                    <label class="input">
                        <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->date_created)}}">
                    </label>
                    <div class="note align-center">
                        день
                    </div>
                </section>
                <section class="col col-1">
                    <label class="input">
                        <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->date_created)}}">
                    </label>
                    <div class="note align-center">
                        месяц
                    </div>
                </section>
                <section class="col col-2">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->date_created)}}">
                    </label>
                    <div class="note align-center">
                        год
                    </div>
                </section>
            </div>
        </fieldset>
        <fieldset>
            <legend>2. Транспортное средство (ТС)</legend>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->modelCar->brand->name}}">
                    </label>
                    <div class="note align-center">
                        Марка
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->modelCar->brand->name}}">
                    </label>
                    <div class="note align-center">
                        Модель
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->year_of_construction}}">
                    </label>
                    <div class="note align-center">
                        Год выпуска
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col col-8">
                    <label class="input text-input" style="font-size: 12px; padding-top: 10px;">
                        В какую сумму Вы оцениваете свой автомобиль на данный момент времени?
                    </label>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <div class="input-group">
                            <input type="text" class="paper-input" disabled value="{{Locale::number($policy->sum_market_car)}}">
                            <div class="input-group-btn">
                                <input type="text" class="paper-input" value="руб." disabled style="width: 30px;padding: 0px;">руб.
                            </div>
                        </div>
                    </label>
                </section>
            </div>
            <div class="row">
                <section class="col col-8">
                    <label class="input text-input" style="font-size: 12px; padding-top: 10px; ">
                        Рыночная стоимость данного автомобиля
                    </label>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <div class="input-group">
                            <input type="text" class="paper-input" disabled value="{{Locale::number($policy->sum_now_car)}}">
                            <div class="input-group-btn">
                                <input type="text" class="paper-input" value="руб." disabled style="width: 30px;padding: 0px;">руб.
                            </div>
                        </div>
                    </label>
                </section>
            </div>
            <section>
                <label class="input text-input">
                    Количество лиц допущенных к управлению
                </label>
            </section>
            @if (count($policy->drivers)==0)
                @for($i=1;$i<4;$i++)
                <div class="row">
                    <section class="col col-5">
                        <label class="input">
                            <input type="text" class="paper-input" disabled value="">
                        </label>
                        <div class="note align-center">
                            ФИО
                        </div>
                    </section>
                    <section class="col col-2">
                        <label class="input">
                            <input type="text" class="paper-input" disabled value="">
                        </label>
                        <div class="note align-center">
                            Пол
                        </div>
                    </section>
                    <section class="col col-1">
                        <label class="input">
                            <input type="text" class="paper-input no-padding" disabled value="">
                        </label>
                        <div class="note align-center">
                            Возвраст
                        </div>
                    </section>
                    <section class="col col-1">
                        <label class="input">
                            <input type="text" class="paper-input no-padding" disabled value="">
                        </label>
                        <div class="note align-center">
                            Стаж
                        </div>
                    </section>
                    <section class="col col-3">
                        <label class="input">
                            <input type="text" class="paper-input" disabled value="">
                        </label>
                        <div class="note align-center">
                            Семейное положение
                        </div>
                    </section>
                </div>
                @endfor
                @endif
            @foreach ($policy->drivers as  $driver)
            <div class="row">
                <section class="col col-5">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$driver->lastname}} {{$driver->firstname}} {{$driver->secondname}}">
                    </label>
                    <div class="note align-center">
                        ФИО
                    </div>
                </section>

                <section class="col col-2">
                    <label class="input">
                            <input type="text" class="paper-input" disabled value="{{$driver->gender->name}}">
                    </label>
                    <div class="note align-center">
                        Пол
                    </div>
                </section>
                <section class="col col-1">
                    <label class="input">
                        <input type="text" class="paper-input no-padding" disabled value="{{$driver->age}}">
                    </label>
                    <div class="note align-center">
                        Возвраст
                    </div>
                </section>
                <section class="col col-1">
                    <label class="input">
                        <input type="text" class="paper-input no-paddingf" disabled value="{{$driver->experience}}">
                    </label>
                    <div class="note align-center">
                        Стаж
                    </div>
                </section>
                <section class="col col-3">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$driver->mariageStatus->name}}">
                    </label>
                    <div class="note align-center">
                        Семейное положение
                    </div>
                </section>
            </div>
            @endforeach

            <div class="row">
                <section class="col col-4">
                    <label class="input text-input">
                        ТС приобретено в кредит?
                    </label>
                </section>
                    <section class="col col-2">
                        <label class="input">
                            <input type="text" class="paper-input" disabled value="@if ($policy->in_credit) Да @else Нет @endif">
                        </label>
                    </section>
                <section class="col col-6">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->bank_credit}}">
                    </label>
                    <div class="note align-center">
                        Банк
                    </div>
                </section>
            </div>
        </fieldset>
    </form>
    <form class="smart-form paper-form" style="margin-top: 20px;">
        <fieldset>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->region->name}}">
                    </label>
                    <div class="note align-center">
                        Место регистраций автомобиля
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->typeSignaling->name}}">
                    </label>
                    <div class="note align-center">
                        Какая сигнализация установлена?
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->typeGearbox->name}}">
                    </label>
                    <div class="note align-center">
                        Какая установлена КПП?
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->typeConsum->name}}">
                    </label>
                    <div class="note align-center">
                        Тип топлива
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <div class="input-group">
                            <input type="text" class="paper-input" disabled value="{{number_format($policy->car->motor_volum,0,'.',' ')}}">
                            <div class="input-group-btn">
                                <input type="text" class="paper-input" value="л." disabled style="width: 30px;padding: 0px;">л.
                            </div>
                        </div>
                    </label>
                    <div class="note align-center">
                        Рабочий объём двигателя
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input">
                         <input type="text" class="paper-input" disabled value="{{$policy->car->motor_ls}}">
                    </label>
                    <div class="note align-center">
                        Л/силы
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <div class="input-group">
                            <input type="text" class="paper-input" disabled value="{{number_format($policy->car->mileage,0,'.',' ')}}">
                            <div class="input-group-btn">
                                <input type="text" class="paper-input" value="км" disabled style="width: 30px;padding: 0px;">км
                            </div>
                        </div>
                    </label>
                    <div class="note align-center">
                        Пробег ТС
                    </div>
                </section>
                <section class="col col-4">
                    <label class="input text-input">
                        Начала эксплуатации ТС
                    </label>
                </section>
                <section class="col col-4">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->car->start_explotation}}">
                    </label>
                    <div class="note align-center">
                        Ч/М/Г
                    </div>
                </section>
            </div>
        </fieldset>
        <fieldset>
            <legend>3. Дополнительное</legend>
            <div class="row">
                <section class="col col-6">
                    <label class="input text-input" style="font-size: 12px; padding-top: 10px; ">
                        У Вас были обращения по прошлому полису КАСКО?
                    </label>
                </section>
                <section class="col col-2">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="@if ($policy->was_kasko==0) Нет @else Да @endif">
                    </label>
                </section>
                @if ($policy->was_kasko!=0)
                <section class="col col-2">
                    <label class="input text-input">
                        сколько
                    </label>
                </section>
                <section class="col col-2">
                    <label class="input">
                        <input type="text" class="paper-input" disabled value="{{$policy->was_kasko}}">
                    </label>
                </section>
                @endif
            </div>
            <section>
                <label class="input">
                    <input type="text" class="paper-input" disabled value="{{$policy->client->time_contact}}">
                </label>
                <div class="note align-center">
                    Удобное время контакта
                </div>
            </section>
        </fieldset>
    </form>
</div>