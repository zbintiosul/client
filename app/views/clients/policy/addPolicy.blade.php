<div class="row" id="addPolicyDiv" style="display: none;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{action('ClientController@postAddClientPolicy')}}" id="save-policy" class="smart-form" method="post">
        <input type="hidden" name="client_id" value="{{$client->id}}">
            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <label class="label mandatory">Тип страховки *</label>
                        <label class="select">
                            <select name="type_policy_id" onchange="policyChange(this.value)">
                                <option value="0"  selected disabled>Выберите тип</option>
                                @foreach ($typesPolicy as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-6 osago kasko">
                        <label class="label">Страховая компания</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="insurance_company" placeholder="Страховая компания">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-6 other">
                        <label class="label">Называние</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="name" placeholder="Называние">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                    <section class="col col-6  osago kasko">
                        <label class="label">Дата окончания предыдущева полиса</label>
                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="date_last_policy" placeholder="Выберите дату" class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </label>
                    </section>
                    <section  class="col col-6 osago kasko">
                        <label class="label">Называние предыдущей СК</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="last_policy_company" placeholder="Называние предыдущей СК">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4 osago kasko">
                        <label class="label">Фамилия менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="lastname_manager" placeholder="Фамилия менеджера">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                    <section  class="col col-4 osago kasko">
                        <label class="label">Имя менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="firstname_manager" placeholder="Имя менеджера">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                    <section  class="col col-4 osago kasko">
                        <label class="label">Отчество менеджера</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="secondname_manager" placeholder="Отчество менеджера">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-2 osago">
                        <label class="label">Кбм</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="kbm" placeholder="Кбм" >
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section class="col col-6 osago kasko">
                        <label class="label">Период страхавания автомобиля</label>
                        <label class="input"> <i class="icon-append fa">Мес.</i>
                            <input type="text" name="period_policy" placeholder="Период страхавания автомобиля">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4 kasko">
                        <label class="label">Рыночная стоимость автомобиля</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="sum_market_car" placeholder="Рыночьная стоимость автомобиля">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4 kasko">
                        <label class="label">ТС приобретено в кредит</label>
                        <label class="toggle">
                            <input type="checkbox" name="in_credit" value="1">в кредит?
                            <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                    </section>
                    <section  class="col col-4 kasko">
                        <label class="label">Банк кредит</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="bank_credit" placeholder="Банк">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <section class="osago kasko" id="selectCarSec">
                        <label class="label mandatory">Автомобиль *</label>
                        <label class="select">
                            <select name="car_id">
                                <option value="0"  selected disabled>Выберите автомобиль</option>
                                @foreach ($cars as $car)
                                <option value="{{$car->id}}">{{$car->modelCar->brand->name}} {{$car->modelCar->name}} {{$car->year_of_construction}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                </section>
                <div class="row">
                    <section  class="col col-4 osago kasko">
                        <label class="label">Каличество лиц допущенных к управлению</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="number_drivers" placeholder="Каличество лиц допущенных к управлению" >
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-2 osago kasko">
                        <label class="label" style="height: 19px"></label>
                        <button class="btn btn-default" type="button" onclick="createDrivers()"> Создать</button>
                    </section>
                    <section  class="col col-3 osago kasko other">
                        <label class="label">Страховая премия</label>
                        <label class="input"> <i class="icon-append fa">руб</i>
                            <input type="text" name="totalSum" placeholder="Общая сумма" onchange="calTotalSum()" value="0.00">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9 .]</b>
                        </label>
                    </section>
                    <section  class="col col-3 osago kasko other">
                        <label class="label">Коэффициент</label>
                        <label class="input"> <i class="icon-append fa">%</i>
                            <input type="text" name="coefficient" placeholder="Коэффициент" onchange="calTotalSum()" value="1.00">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9 .]</b>
                        </label>
                    </section>
                </div>
            </fieldset>
            <fieldset class="osago kasko">
                <div id="contentDrivers">

                </div>
            </fieldset>
            <fieldset>
            <div class="row osago kasko other">
                <section  class="col col-8">

                </section>
                <section  class="col col-4">
                    <div class="invoice-sum-total pull-right">
                        <h3><strong>Страховая премия: <span class="text-success"><span id="totalSum">0</span> руб.</span></strong></h3>
                    </div>
                </section>
            </div>
            </fieldset>
            <footer class="client_footer">
                <div class="btn-group btn-group-justified">
                    <a href="javascript:void(0);" onclick="$('#addPolicyDiv').toggle('slow')" class="btn btn-default"> Отменить</a>
                    <a href="javascript:void(0);" onclick="$('#save-policy').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</a>
                </div>
            </footer>
        </form>
    </div>
</div>
<script>
    $( document ).ready(function() {

        //runAllForms();
    });

    function createDrivers()
    {
        var nr = $('input[name="number_drivers"]').val();
        $.ajax({
            type: "POST",
            url: "{{action('ClientController@postGetDrivers')}}/"+nr
        }).done(function(msg) {
            $('#contentDrivers').html();
            $('#contentDrivers').html(msg);
        })
            .fail(function() {
                $.smallBox({
                    title : "{{Lang::get('client.error')}}",
                    content : "{{Lang::get('client.errorText')}}",
                    color : "#c26565",
                    iconSmall : "fa fa-times bounce animated",
                    timeout : 4000
                });
            });
    }

    function policyChange(nr)
    {
        switch(nr) {
            case '1':
                $('.kasko').hide('slow');
                $('.other').hide('slow');
                $('.osago').show('slow');
                break;
            case '2':
                $('.other').hide('slow');
                $('.osago').hide('slow');
                $('.kasko').show('slow');
                break;
            case '3':
                $('.kasko').hide('slow');
                $('.osago').hide('slow');
                $('.other').show('slow');
                break;
        }
    }

    function calTotalSum()
    {
        coef = $('input[name="coefficient"]').val();
        sum = $('input[name="totalSum"]').val();
        var num = coef*sum;
        $('#totalSum').text(num.toFixed(2));
    }

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addpolicy = $('#save-policy').validate({
            // Rules for form validation
            rules : {
                type_policy_id : {
                    required: true
                },
                name: {
                    required: true
                },
                kbm: {
                    number: true
                },
                sum_now_car: {
                    number: true
                },
                sum_market_car: {
                    number: true
                },
                totalSum: {
                    number: true
                },
                coefficient: {
                    number: true
                },
                number_drivers: {
                    number: true
                },
                car_id : {
                     required: true
                }
            },

            // Messages for form validation
            messages : {
                type_policy_id : {
                    required : 'Пожалуйста, выберите элемент'
                },
                name : {
                    required : 'Пожалуйста, выберите элемент'
                },
                kbm:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                sum_now_car:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                sum_market_car:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                totalSum:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                coefficient:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                number_drivers:{
                    number: 'Пожалуйста, проверьте ваш ввод'
                },
                car_id : {
                    required : 'Пожалуйста, выберите элемент'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addPolicyDiv').hide();
                        ajaxLoadPart('s3',getCurentUrl(),null,s4TabShow);
                        //ajaxLoadPart('selectCarSec','{{action('ClientController@getInProgress')}}/{{$client->id}}',null,s4TabShow);
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s4TabShow()
    {
        $('#s3').removeAttr('style');
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
        runAllForms();
    }
</script>