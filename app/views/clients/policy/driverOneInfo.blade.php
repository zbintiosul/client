<div class="row driversDiv driversDiv_{{$i}}">
    <section  class="col col-4">
        <label class="label">Фамилия</label>
        <label class="input"> <i class="icon-append fa fa-tag"></i>
            <input type="text" name="lastname_driver_{{$i}}" placeholder="Фамилия">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
        </label>
    </section>
    <section  class="col col-4">
        <label class="label">Имя</label>
        <label class="input"> <i class="icon-append fa fa-tag"></i>
            <input type="text" name="firstname_driver_{{$i}}" placeholder="Имя">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
        </label>
    </section>
    <section  class="col @if ($new) col-4 @else col-3 @endif">
        <label class="label">Отчество</label>
        <label class="input"> <i class="icon-append fa fa-tag"></i>
            <input type="text" name="secondname_driver_{{$i}}" placeholder="Отчество">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
        </label>
    </section>
    @if (!$new)
    <section  class="col col-1">
        <button class="btn btn-sm btn-danger btn-driver-delete" onclick="deleteEditHtmlDriver('.driversDiv_{{$i}}');" title="Удалить">
            <i class="fa fa-trash-o"></i>
        </button>
    </section>
    @endif
</div>
<div class="row driversDiv driversDiv_{{$i}}">
    <section  class="col col-4">
        <label class="label">Пол</label>
        <label class="select">
            <select name="gender_id_driver_{{$i}}">
                <option value="0" selected disabled>Выберите пол</option>
                @foreach ($genders as $gender)
                <option value="{{$gender->id}}">{{$gender->name}}</option>
                @endforeach
            </select>
            <i></i> </label>
    </section>
    <section  class="col col-2">
        <label class="label">Возраст</label>
        <label class="input"> <i class="icon-append fa fa-tag"></i>
            <input type="text" name="age_driver_{{$i}}" placeholder="Возраст">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
        </label>
    </section>
    <section  class="col col-2">
        <label class="label">Стаж</label>
        <label class="input"> <i class="icon-append fa fa-tag"></i>
            <input type="text" name="experience_driver_{{$i}}" placeholder="Стаж">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
        </label>
    </section>
    <section  class="col @if ($new) col-4 @else col-3 @endif">
        <label class="label">Семейное положение</label>
        <label class="select">
            <select name="marriage_status_driver_id_{{$i}}">
                <option value="0" selected disabled>Выберите положение</option>
                @foreach ($mStatuses as $status)
                <option value="{{$status->id}}">{{$status->name}}</option>
                @endforeach
            </select>
            <i></i> </label>
    </section>
    @if (!$new)
    <section  class="col col-1">

    </section>
    @endif
</div>