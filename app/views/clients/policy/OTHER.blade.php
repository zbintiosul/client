<div id="bodyPolicy" style="font-size: 130%; width: 50%; margin: 0px auto; max-width: 750px; min-width: 600px; padding: 5px;">
<form class="smart-form paper-form">
<fieldset>
    <section class="align-center">
        <h3>{{$policy->type->name}}</h3>
    </section>
</fieldset>
<fieldset>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->client->lastname}} {{$policy->client->firstname}} {{$policy->client->secondname}}">
        </label>
        <div class="note align-center">
            ФИО
        </div>
    </section>
    <div class="row">
        <section class="col col-5">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->client->phone}}">
            </label>
            <div class="note align-center">
                Контактный телефон
            </div>
        </section>
        <section class="col col-3">
            <label class="input text-input">
                Дата рождения
            </label>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getDay($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                день
            </div>
        </section>
        <section class="col col-1">
            <label class="input">
                <input type="text" class="paper-input no-padding" disabled value="{{AppHelper::getMonth($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                месяц
            </div>
        </section>
        <section class="col col-2">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{AppHelper::getYear($policy->client->birth_date)}}">
            </label>
            <div class="note align-center">
                год
            </div>
        </section>
    </div>
    <section>
        <label class="input">
            <input type="text" class="paper-input" disabled value="{{$policy->client->address}}">
        </label>
        <div class="note align-center">
            Адрес клиента
        </div>
    </section>
    <div class="row">
        <section class="col col-6">
            <label class="input">
                <input type="text" class="paper-input" disabled value="{{$policy->name}}">
            </label>
            <div class="note align-center">
                Называние
            </div>
        </section>
        <section class="col col-6">
            <label class="input">
                <div class="input-group">
                    <input type="text" class="paper-input" disabled value="{{$policy->calcTotalSum()}}">
                    <div class="input-group-btn">
                        <input type="text" class="paper-input" value="руб." disabled style="width: 30px;padding: 0px;">руб.
                    </div>
                </div>
            </label>
            <div class="note align-center">
                Расчёт
            </div>
        </section>
    </div>
</fieldset>
</form>
</div>