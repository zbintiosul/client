@if (Auth::user()->nrInProgressClients()>0)
    @foreach (Auth::user()->clients()->where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','2')->orderBy('updated_at','desc')->get() as $client)
        <ul class="notification-body">
            <li>
                <a href="{{action('ClientController@getInProgress')}}/{{$client->id}}" class="padding-10 unread ajaxa" style="cursor: pointer" id="inProgressClientBody_{{$client->id}}">
                    <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5" >
                        <i class="fa fa-phone fa-fw fa-2x"></i>
                    </em>
                    <span>
                         <b>{{$client->phone}}</b>
                         <br>
                         {{$client->firstname}}  {{$client->lastname}}  {{$client->secondname}}
                         <span class="pull-right font-xs text-muted"><i>{{$client->getTimeAgoUpdate()}}</i></span>
                    </span>
                </a>
            </li>
        </ul>
    @endforeach
@else
<!-- notification content -->
<ul class="notification-body">
    <li>
        <span class="padding-10 unread">
            <h4 style="text-align:center;">Нет клиентов в процессе</h4>
        </span>
    </li>
</ul>

@endif
