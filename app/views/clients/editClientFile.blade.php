<!-- Modal -->
<div class="modal fade" id="editClientFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Редактировать файл
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postEditClientFile')}}" id="edit-client-file" class="smart-form" method="post">
                    <input type="hidden" name="file_id" value="{{$file->id}}">
                    <fieldset>
                        <section>
                            <label class="label">Название</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="desc" placeholder="Название" value="{{$file->desc}}">
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $editClientFile = $('#edit-client-file').validate({
            // Rules for form validation
            rules : {
                desc:  {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                desc : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#editClientFile').modal('hide');
                        ajaxLoadPart('s5','{{Request::url()}}',null,s4TabShow);
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s4TabShow()
    {
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    }
</script>