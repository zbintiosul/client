<!-- Modal -->
<div class="modal fade" id="addClientFile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Добавить файл
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postAddClientFile')}}" id="add-client-file" class="smart-form" method="post">
                    <input type="hidden" name="client_id" value="{{$client->id}}">
                    <fieldset>
                        <section id="importSection">
                            <label class="label mandatory">Файл *</label>
                            <div class="input input-file">
                                <span class="button"><input id="client_file" type="file" name="client_file" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Выберите файл" readonly="" >
                            </div>
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label mandatory">Название *</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="desc" placeholder="Название">
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addClientFile = $('#add-client-file').validate({
            // Rules for form validation
            rules : {
                client_file : {
                    required : true
                },
                desc:  {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                client_file : {
                    required : 'Пожалуйста, заполните это поле'
                },
                desc : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addClientFile').modal('hide');
                        ajaxLoadPart('s5',getCurentUrl(),null,s4TabShow);
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s4TabShow()
    {
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    }
</script>