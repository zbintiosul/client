<tr id="tr_edit_car" style="border-top: none;">
<td colspan="5">
<div class="row" id="editCarDiv">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{action('ClientController@postEditCarClientDo')}}" id="edit-car" class="smart-form" method="post">
        <input type="hidden" name="car_id" value="{{$car->id}}"
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label mandatory">Марка *</label>
                        <label class="select">
                            <select name="brand_car_id" id="edit_brand_car_id" onchange="getModels(this.value,'#edit_model_car_id')">
                                @foreach ($brands as $brand)
                                <option value="{{$brand->id}}" @if ($car->modelCar->brand->id == $brand->id) selected="selected" @endif>{{$brand->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Модель</label>
                        <label class="select">
                            <select name="model_car_id" id="edit_model_car_id">
                                @foreach ($models as $model)
                                <option value="{{$model->id}}" @if ($car->modelCar->id == $model->id) selected="selected" @endif >{{$model->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label mandatory">Марка *</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="brand" placeholder="Марка" value="{{$car->brand}}" disabled>
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Модель</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="model" placeholder="Модель" value="{{$car->model}}" disabled>
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Год выпуска</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="year_of_construction" placeholder="Год выпуска" data-mask="9999" value="{{$car->year_of_construction}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Рабочий обьем двигателя</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="number" name="motor_volum" placeholder="Рабочий обьем двигателя" value="{{$car->motor_volum}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">л.с</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="number" name="motor_ls" placeholder="л.с" value="{{$car->motor_ls}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section class="col col-4">
                        <label class="label">Гос. № авто</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="register_number" placeholder="Гос. № авто" value="{{$car->register_number}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4">
                        <label class="label">VIN</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="VIN" placeholder="VIN" value="{{$car->VIN}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z А-Я 0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип топлива</label>
                        <label class="select">
                            <select name="type_consum_id">
                                <option value="0" selected disabled>Выберите топлива</option>
                                @foreach ($consums as $consum)
                                <option value="{{$consum->id}}" @if ($car->type_consum_id == $consum->id) selected="selected" @endif>{{$consum->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип ТС</label>
                        <label class="select">
                            <select name="type_car_id">
                                <option value="0" selected disabled>Выберите тип</option>
                                @foreach ($types as $type)
                                <option value="{{$type->id}}" @if ($car->type_car_id == $type->id) selected="selected" @endif>{{$type->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4">
                        <label class="label">Пробег ТС</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="mileage" placeholder="Пробег ТС" value="{{$car->mileage}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип сигнализаций</label>
                        <label class="select">
                            <select name="type_signaling_id">
                                <option value="0" selected disabled>Выберите сигнализацию</option>
                                @foreach ($signalings as $signaling)
                                <option value="{{$signaling->id}}"  @if ($car->type_signaling_id == $signaling->id) selected="selected" @endif>{{$signaling->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип КПП</label>
                        <label class="select">
                            <select name="type_gearbox_id">
                                <option value="0" selected disabled>Выберите КПП</option>
                                @foreach ($gearboxs as $gearbox)
                                <option value="{{$gearbox->id}}" @if ($car->type_gearbox_id == $gearbox->id) selected="selected" @endif>{{$gearbox->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>

                </div>
                <div  class="row">
                    <section  class="col col-2 stand_number_col_edit" @if ($car->stand_number ==1)  style="display: none;" @endif>
                        <label class="label">Номер</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="stand_number_value" placeholder="Номер" value="{{$car->stand_number}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Ч/М/Г начала эксплуатации ТС</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="start_explotation" placeholder="Ч/М/Г начала эксплуатации ТС" value="{{$car->start_explotation}}">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> Ч/М/Г</b>
                        </label>
                    </section>
                </div>

            </fieldset>
            <footer class="client_footer">
                <div class="btn-group btn-group-justified">
                    <a href="javascript:void(0);" onclick="$('#tr_edit_car').remove()" class="btn btn-default"> Отменить</a>
                    <a href="javascript:void(0);" onclick="$('#edit-car').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</a>
                </div>
            </footer>
        </form>
    </div>
</div>
</td>
</tr>
<script>

    pageSetUp();
    // run form elements
   // runAllForms();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $editcar = $('#edit-car').validate({
            // Rules for form validation
            rules : {
                brand_car_id : {
                    required:true
                },
                model_car_id : {
                    required:true
                },
                brand : {
                    required:true,
                    regex: "[A-Za-zА-Яа-я0-9]"
                },
                model : {
                    required:true,
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                brand_car_id : {
                    required : 'Пожалуйста, заполните это поле'
                },
                model_car_id : {
                    required : 'Пожалуйста, заполните это поле'
                },
                brand : {
                    required : 'Пожалуйста, заполните это поле'
                },
                model : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#tr_edit_car').remove();
                        ajaxLoadPart('s2',getCurentUrl(),null,s4TabShow);
                        ajaxLoadPart('selectCarSec',getCurentUrl(),null,s4TabShow);
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s4TabShow()
    {
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    }

</script>