<div class="row" id="addCarDiv" style="display: none;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{action('ClientController@postAddCarClient')}}" id="add-car" class="smart-form" method="post">
        <input type="hidden" name="client_id" value="{{$client->id}}"
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label mandatory">Марка *</label>
                        <label class="select">
                            <select name="brand_car_id" id="add_brand_car_id" onchange="getModels(this.value,'#add_model_car_id')">
                                <option value="0" selected disabled>Выберите марку</option>
                                @foreach ($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label mandatory">Модель *</label>
                        <label class="select">
                            <select name="model_car_id" id="add_model_car_id">
                                <option value="0" selected disabled>Выберите модель</option>
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Год выпуска</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="year_of_construction" placeholder="Год выпуска" data-mask="9999">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Рабочий обьем двигателя</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="number" name="motor_volum" placeholder="Рабочий обьем двигателя">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">л.с</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="number" name="motor_ls" placeholder="л.с">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section class="col col-4">
                        <label class="label">Гос. № авто</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="register_number" placeholder="Гос. № авто">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я 0-9]</b>
                        </label>
                    </section>
                </div>
                <div class="row">

                    <section  class="col col-4">
                        <label class="label">VIN</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="VIN" placeholder="VIN">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z А-Я 0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип топлива</label>
                        <label class="select">
                            <select name="type_consum_id">
                                <option value="0" selected disabled>Выберите топлива</option>
                                @foreach ($consums as $consum)
                                <option value="{{$consum->id}}">{{$consum->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип ТС</label>
                        <label class="select">
                            <select name="type_car_id">
                                <option value="0" selected disabled>Выберите тип</option>
                                @foreach ($types as $consum)
                                <option value="{{$consum->id}}">{{$consum->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                </div>
                <div class="row">
                    <section  class="col col-4">
                        <label class="label">Пробег ТС</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="mileage" placeholder="Пробег ТС">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип сигнализаций</label>
                        <label class="select">
                            <select name="type_signaling_id">
                                <option value="0" selected disabled>Выберите сигнализацию</option>
                                @foreach ($signalings as $signaling)
                                <option value="{{$signaling->id}}">{{$signaling->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Тип КПП</label>
                        <label class="select">
                            <select name="type_gearbox_id">
                                <option value="0" selected disabled>Выберите КПП</option>
                                @foreach ($gearboxs as $gearbox)
                                <option value="{{$gearbox->id}}">{{$gearbox->name}}</option>
                                @endforeach
                            </select>
                            <i></i> </label>
                    </section>

                </div>
                <div class="row">
                    <section  class="col col-2 stand_number_col">
                        <label class="label">Номер</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="stand_number_value" placeholder="Номер">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                    <section  class="col col-4">
                        <label class="label">Ч/М/Г начала эксплуатации ТС</label>
                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                            <input type="text" name="start_explotation" placeholder="Ч/М/Г начала эксплуатации ТС">
                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [0-9]</b>
                        </label>
                    </section>
                </div>
            </fieldset>
            <footer class="client_footer">
                <div class="btn-group btn-group-justified">
                    <a href="javascript:void(0);" onclick="$('#addCarDiv').toggle('slow')" class="btn btn-default"> Отменить</a>
                    <a href="javascript:void(0);" onclick="$('#add-car').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</a>
                </div>
            </footer>
        </form>
    </div>
</div>

<script>

    //pageSetUp();
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addcar = $('#add-car').validate({
            // Rules for form validation
            rules : {
                brand_car_id : {
                    required:true
                },
                model_car_id : {
                    required:true
                },
                brand : {
                    required:true,
                    regex: "[A-Za-zА-Яа-я0-9]"
                },
                model : {
                    required:true,
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                brand_car_id : {
                    required : 'Пожалуйста, заполните это поле'
                },
                model_car_id : {
                    required : 'Пожалуйста, заполните это поле'
                },
                brand : {
                    required : 'Пожалуйста, заполните это поле'
                },
                model : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addCarDiv').hide();
                        ajaxLoadPart('s2',getCurentUrl(),null,s4TabShow);
                        //$('#inProgressContentTab #s2Link').tab('show');
                        ajaxLoadPart('selectCarSec',getCurentUrl(),null,s4TabShow);
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function s4TabShow()
    {
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
        runAllForms();
    }

</script>