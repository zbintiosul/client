<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-progress-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-phone"></i> </span>
                    <h2> Клиент {{$client->phone}} </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <ul id="myTab1" class="nav nav-tabs bordered">
                                <li class="active">
                                    <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Клиент</a>
                                </li>
                                <li>
                                    <a href="#s2" id="s2Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-truck"></i> Автомобили</a>
                                </li>
                                <li>
                                    <a href="#s4" id="s4Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-list-alt"></i> Данные с импорта</a>
                                </li>
                            </ul>
                        </div>
                            <div id="userContentTab" class="tab-content padding-10">
                                <div class="tab-pane fade in active" id="s1">
                                    <form class="smart-form" method="post">
                                        <input type="hidden" name="client_id" value="{{$client->id}}">
                                        <fieldset>
                                            <div class="row">
                                                <section  class="col col-4">
                                                    <label class="label">Фамилия</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="lastname" placeholder="Фамилия" value="{{$client->lastname}}" disabled>
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">Имя</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="firstname" placeholder="Имя" value="{{$client->firstname}}" disabled>
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section  class="col col-4">
                                                    <label class="label">Отчество</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="secondname" placeholder="Отчество" value="{{$client->secondname}}" disabled>
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Дата рождения</label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="birth_date" placeholder="Выберите дату" disabled class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy" value="{{AppHelper::conInvDate($client->birth_date)}}">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </section>
                                                <section  class="col col-6">
                                                    <label class="label">Дата выдачи в/у</label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="date_driver_licence" placeholder="Выберите дату" disabled class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy" value="{{AppHelper::conInvDate($client->date_driver_licence)}}">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Серия, № в/у</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="serial_driver_licence" placeholder="Серия, № в/у" disabled value="{{$client->serial_driver_licence}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section  class="col col-6">
                                                    <label class="label">Серия, № паспорта</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="serial_bulletin" placeholder="Серия, № паспорта" disabled value="{{$client->serial_bulletin}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <section>
                                                <label class="label">Адрес</label>
                                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="address" placeholder="Адрес" disabled value="{{$client->address}}">
                                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Удобное время контакта</label>
                                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="time_contact" placeholder="Удобное время контакта" disabled value="{{$client->time_contact}}">
                                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Комментарий</label>
                                                <label class="textarea"> <i class="icon-append fa fa-tag"></i>
                                                    <textarea rows="4" name="comments" disabled>{{$client->comments}}</textarea>
                                                    <b class="tooltip tooltip-top-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                        </fieldset>

                                        <footer class="client_footer">
                                            <div class="btn-group btn-group-justified">
                                                <a href="javascript:void(0);" onclick="interested()" class="btn btn-primary"><i class="fa fa-eye"></i> Заинтересован</a>
                                            </div>
                                        </footer>
                                    </form>
                                </div>
                                <div class="tab-pane fade in" id="s2">
                                        <table id="custom-table" class="table table-striped table-hover dataTable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Марка</th>
                                                <th>Модель</th>
                                                <th>Год выпуска</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client->cars()->orderBy('created_at','desc')->get() as $car)
                                                    <tr>
                                                        <td>{{$car->id}}</td>
                                                        <td>{{$car->brand}}</td>
                                                        <td>{{$car->model}}</td>
                                                        <td>{{$car->year_of_construction}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane fade in" id="s4">
                                    <table id="custom-table" class="table table-striped table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>ФИО</th>
                                            <th>Номер</th>
                                            <th>Срок</th>
                                            <th>Риск</th>
                                            <th>Банк</th>
                                            <th>Страховая премия</th>
                                            <th>Авто</th>
                                            <th>г.в.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($client->imports as $import)
                                        <tr>
                                            <td>{{$import->pivot->FIO}}</td>
                                            <td>{{$import->pivot->number}}</td>
                                            <td>{{$import->pivot->srok}}</td>
                                            <td>{{$import->pivot->type}}</td>
                                            <td>{{$import->pivot->bank}}</td>
                                            <td>{{$import->pivot->sum}}</td>
                                            <td>{{$import->pivot->brand_model}}</td>
                                            <td>{{$import->pivot->year}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->
    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    function interested(){
        id = '{{$client->id}}';
        $.SmartMessageBox({
            title: "Заинтересованны?",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postAddToMeNotInterested')}}",
                    data: {client:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    window.location.hash = "{{action('ClientController@getInProgress')}}/"+id;
                    updateNrInProgressClients();
                    updateNrNoInterestedClients();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }

</script>