<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Клиенты
			<span>>
				Страховки для утверждение
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2> Страховки для утверждение </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        {{$policies->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th>Клиент</th>
                                <th>Автомобиль</th>
                                <th>Тип</th>
                                <th class="hidden-xs">Дата</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($policies as $policy)
                            <tr id="tr_{{$policy->id}}" idPolicy="{{$policy->id}}">
                            <td class="custom-table-number hidden-xs">{{$policy->id}}</td>
                            <td  class="custom-data-custom">
                                <div>{{$policy->client->lastname}} {{$policy->client->firstname}}</div>
                            </td>
                            <td  class="custom-data-custom">
                                <div>{{$policy->car->modelCar->brand->name}} {{$policy->car->modelCar->name}}</div>
                            </td>
                            <td  class="custom-data-custom">
                                    <div>{{$policy->type->name}}</div>
                            </td>
                            <td class="custom-data-action hidden-xs hidden-sm width100px">
                                {{$policy->updated_at->format('d-m-Y H:i:s')}}
                            </td>
                            <td class="custom-data-action">
                                <a href="{{action('ClientController@getViewPolicy')}}/{{$policy->id}}" class="btn btn-xs btn-default ajaxa"><i class="fa fa-eye"></i> <span class="hidden-xs">Просмотр</span></a>

                            </td>
                                @if ($policy->status->id == 2)
                                @if (Authority::can('approve_clients'))
                                    <td class="custom-data-action">
                                        <a href="{{action('ClientController@getViewClient')}}/{{$policy->client->id}}" class="btn btn-xs btn-default ajaxa"><i class="fa fa-pencil-square-o"></i> <span class="hidden-xs hidden-sm">Редактировать</span></a>

                                    </td>
                                    <td class="custom-data-action">
                                        <button type="button" class="btn btn-xs btn-success" id="sendApprove_{{$policy->id}}" onclick="getModal('{{action('ClientController@getModalSelectBroker')}}/{{$policy->id}}','#approveBroker')" title="Утвердить">
                                            <i class="fa fa-thumbs-o-up"></i> <span class="hidden-xs">Утвердить</span>
                                        </button>
                                    </td>
                                    <td class="custom-data-action">
                                        <button type="button" class="btn btn-xs btn-warning" id="sendDecline_{{$policy->id}}" onclick="declinePolicy({{$policy->id}})" title="Отклонить">
                                            <i class="fa fa-thumbs-o-down"></i> <span class="hidden-xs">Отклонить</span>
                                        </button>
                                    </td>
                                @endif
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$policies->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    /*function approvePolicy(id)
    {
        $.SmartMessageBox({
            title: "Утверждение",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postApproveManager')}}",
                    data: {policy:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $('#sendApprove_'+id).remove();
                    $('#sendDecline_'+id).remove();

                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }*/

    function declinePolicy(id)
    {
        $.SmartMessageBox({
            title: "Утверждение",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeclineManager')}}",
                    data: {policy:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $('#sendApprove_'+id).remove();
                    $('#sendDecline_'+id).remove();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }

</script>