<!-- Modal -->
<div class="modal fade" id="addClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Новый клиент
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postAddClient')}}" id="add-client" class="smart-form" method="post">
                    <fieldset>
                        <section>
                            <label class="label">Телефон</label>
                            <label class="input"> <i class="icon-append fa fa-phone"></i>
                                <input type="tel" name="phone" placeholder="Рабочий телефон" data-mask="(9)(999) 999-9999">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-phone txt-color-teal"></i> [0-9]</b>
                            </label>
                        </section>
                        <div class="row">
                            <section  class="col col-4">
                                <label class="label">Фамилия</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="lastname" placeholder="Фамилия">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="label">Имя</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="firstname" placeholder="Имя">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                            <section  class="col col-4">
                                <label class="label">Отчество</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="secondname" placeholder="Отчество">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label">Отправить пользователю</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <select style="width:100%" class="select2Custom" id="select2CustomAddClientUser" name="user">
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    $( document ).ready(function() {
        $('#select2CustomAddClientUser').select2();
    });

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addclient = $('#add-client').validate({
            // Rules for form validation
            rules : {
                phone : {
                    required : true,
                    checkPhone: true
                },
                firstname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                lastname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                secondname : {
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                phone : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addClient').modal('hide');
                        loadURL("{{action('ClientController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });


        $.validator.addMethod("checkPhone",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('ClientController@postCheckClient')}}", // script to validate in server side
                    data: {phone: value},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Этот телефон уже занят! Попробуйте другой."
        );

    }

</script>