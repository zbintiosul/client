<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-progress-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-phone"></i> </span>
                    <h2> Клиент {{$client->phone}} </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <ul id="myTab1" class="nav nav-tabs bordered">
                                <li class="active">
                                    <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Клиент</a>
                                </li>
                                <li>
                                    <a href="#s2" id="s2Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-truck"></i> Автомобили</a>
                                </li>
                                <li>
                                    <a href="#s3" id="s3Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text"></i> Страховки</a>
                                </li>
                                <li>
                                    <a href="#s4" id="s4Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text-o"></i> Данные импорта</a>
                                </li>
                                <li>
                                    <a href="#s5" id="s5Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-paperclip"></i> Файлы</a>
                                </li>
                                @if (Authority::can('is_agent'))
                                <li>
                                    <a href="#s6" id="s6Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-bell"></i> Напоминания</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                            <div id="inProgressContentTab" class="tab-content padding-10">
                                <div class="tab-pane fade in active" id="s1">
                                    <form action="{{action('ClientController@postSaveClient')}}" id="save-client" class="smart-form" method="post">
                                        <input type="hidden" name="client_id" value="{{$client->id}}">
                                        <fieldset>
                                            <div class="row">
                                                <section  class="col col-4">
                                                    <label class="label">Фамилия</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="lastname" placeholder="Фамилия" value="{{$client->lastname}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">Имя</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="firstname" placeholder="Имя" value="{{$client->firstname}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section  class="col col-4">
                                                    <label class="label">Отчество</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="secondname" placeholder="Отчество" value="{{$client->secondname}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Дата рождения</label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="birth_date" placeholder="Выберите дату" class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy" value="{{AppHelper::conInvDate($client->birth_date)}}">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </section>
                                                <section  class="col col-6">
                                                    <label class="label">Дата выдачи в/у</label>
                                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="date_driver_licence" placeholder="Выберите дату" class="form-control datepicker" data-mask="99-99-9999" data-dateformat="dd-mm-yy" value="{{AppHelper::conInvDate($client->date_driver_licence)}}">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label">Серия, № в/у</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="serial_driver_licence" placeholder="Серия, № в/у" value="{{$client->serial_driver_licence}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                                <section  class="col col-6">
                                                    <label class="label">Серия, № паспорта</label>
                                                    <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                        <input type="text" name="serial_bulletin" placeholder="Серия, № паспорта" value="{{$client->serial_bulletin}}">
                                                        <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <section>
                                                <label class="label">Адрес</label>
                                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="address" placeholder="Адрес" value="{{$client->address}}">
                                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Электронная почта</label>
                                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="email" placeholder="Электронная почта" value="{{$client->email}}">
                                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> Электронная почта</b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Удобное время контакта</label>
                                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="time_contact" placeholder="Удобное время контакта" value="{{$client->time_contact}}">
                                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Комментарий</label>
                                                <label class="textarea" > <i class="icon-append fa fa-tag"></i>
                                                    <textarea rows="8" style="resize:vertical;" name="comments">{{$client->comments}}</textarea>
                                                    <b class="tooltip tooltip-top-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                                </label>
                                            </section>
                                             <section>
                                                <label class="label">Проспект</label>
                                                <label class="select">
                                                    <select name="potential_client_type_id">
                                                        <option value="0" selected disabled>Выберите</option>
                                                        @foreach (PotentialClientType::all() as $pct)
                                                        <option  @if ($pct->id==$client->potential_client_type_id) selected @endif value="{{$pct->id}}">{{$pct->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i></i> </label>
                                            </section>
                                        </fieldset>
                                        <footer class="client_footer">
                                            <div class="btn-group btn-group-justified">
                                                @if ($client->policies()->count()<=0)
                                                    @if (Authority::can('make_not_interested_clients'))
                                                    <a href="javascript:void(0);" onclick="notInterested({{$client->id}})" class="btn btn-warning"><i class="fa fa-exclamation-triangle"></i> Не заинтересованный</a>
                                                    @endif
                                                @endif
                                                @if ($client->status_client_id==2)
                                                    <a href="javascript:void(0);" onclick="callBack({{$client->id}})" class="btn btn-info"><i class="fa fa-phone"></i> Перезвонить</a>
                                                @else
                                                    @if ($client->status_client_id==6)
                                                        <a href="javascript:void(0);" onclick="makeInProgress({{$client->id}})" class="btn btn-success"><i class="fa fa-retweet"></i> В процессе</a>
                                                    @endif
                                                @endif
                                                @if (Authority::can('edit_client'))
                                                    @if ($client->status_client_id==2 || $client->status_client_id==1 || $client->status_client_id==6 ||  Authority::is_super_admin())
                                                    <a href="javascript:void(0);" onclick="$('#save-client').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </footer>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="s2">
                                    <div class="row">
                                        <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            @if (Authority::can('add_client_car'))
                                            <button class="btn btn-success" onclick="$('#addCarDiv').toggle('slow')">
                                                <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новый автомобиль</span>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    @if (Authority::can('add_client_car'))
                                        @include('clients.car.addCar')
                                    @endif
                                        <table id="custom-table" class="table table-striped table-hover dataTable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Марка</th>
                                                <th>Модель</th>
                                                <th>Год выпуска</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client->cars()->orderBy('created_at','desc')->get() as $car)
                                                    <tr id="tr_car_{{$car->id}}">
                                                        <td>{{$car->id}}</td>
                                                        <td>{{$car->modelCar->brand->name}}</td>
                                                        <td>{{$car->modelCar->name}}</td>
                                                        <td>{{$car->year_of_construction}}</td>
                                                        <td class="custom-data-action">
                                                            <div class="columncontrols">
                                                                <div class="btn-group">
                                                                    @if ((Authority::can('is_agent') && $client->status->id==2) || (Authority::can('is_broker') && ($client->status->id==2)) || Authority::is_super_admin())
                                                                        @if (Authority::can('edit_client_car'))
                                                                        <button class="btn btn-sm btn-default" onclick="editClientCar({{$car->id}})" title="Редактировать">
                                                                            <i class="fa fa-edit"></i>
                                                                        </button>
                                                                        @endif
                                                                        @if (Authority::can('delete_client_car'))
                                                                            @if (Policy::where('car_id','=',$car->id)->count()<=0)
                                                                            <button class="btn btn-sm btn-danger" onclick="deleteClientCar({{$car->id}})" title="Удалить">
                                                                                <i class="fa fa-trash-o"></i>
                                                                            </button>
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane fade" id="s3">
                                    <div class="row">
                                        <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            @if (Authority::can('add_client_policy'))
                                            <button class="btn btn-success" onclick="$('#addPolicyDiv').toggle('slow')">
                                                <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новая страховка</span>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    @if (Authority::can('add_client_policy'))
                                        @include('clients.policy.addPolicy')
                                    @endif
                                        <table id="custom-table" class="table table-striped table-hover dataTable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Автомобиль</th>
                                                <th>Тип</th>
                                                <th>Дата</th>
                                                <th>Расчет</th>
                                                <th>Статус</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->policies()->orderBy('created_at','desc')->get() as $policy)
                                            <tr id="tr_policy_{{$policy->id}}">
                                                <td>{{$policy->id}}</td>

                                                <td>{{$policy->car->modelCar->brand->name}} {{$policy->car->modelCar->name}}</td>

                                                <td>{{$policy->type->name}}</td>

                                                <td>{{$policy->created_at->format('d-m-Y H:i:s')}}</td>

                                                <td>{{$policy->calcTotalSum()}} руб.</td>
                                                <td>{{$policy->status->name}}</td>
                                                <td class="custom-data-action">
                                                    <div class="btn-group width100px">
                                                        @if (($policy->status_policy_id==1 || $policy->status_policy_id==6 || $policy->status_policy_id==7) && Authority::can('is_agent'))
                                                        <button class="btn btn-sm btn-success" id="sendToApproval_{{$policy->id}}" onclick="sendPolicy({{$policy->id}})" title="Отправить">
                                                            <i class="fa fa-arrow-right"></i>
                                                        </button>
                                                        @endif

                                                        <a class="btn btn-sm btn-default ajaxa" href="{{action('OfferController@getOffers')}}/{{$policy->id}}" title="Предложении">
                                                            <i class="fa fa-external-link-square"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="custom-data-action">


                                                    <div class="columncontrols">
                                                        <div class="btn-group width100px">
                                                            <a href="{{action('ClientController@getViewPolicy')}}/{{$policy->id}}" class="btn btn-sm btn-default ajaxa" title="Просмотр"><i class="fa fa-eye"></i></a>
                                                            @if ((Authority::can('is_agent') && ($policy->status->id==1 || $policy->status->id==6 || $policy->status->id==7)) || (Authority::canOnly('is_broker') && ($policy->status->id==4)) || (Authority::can('approve_clients') && ($policy->status->id==2)))
                                                                @if (Authority::can('edit_client_policies'))
                                                                <button class="btn btn-sm btn-default" onclick="editClientPolicy({{$policy->id}})" id="editPolicy_{{$policy->id}}" title="Редактировать">
                                                                    <i class="fa fa-edit"></i>
                                                                </button>
                                                                @endif
                                                                @if (Authority::can('delete_client_policy'))
                                                                <button class="btn btn-sm btn-danger" onclick="deleteClientPolicy({{$policy->id}})" id="deletePolicy_{{$policy->id}}" title="Удалить">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </button>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane fade" id="s4">
                                    <table id="custom-table" class="table table-striped table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>ФИО</th>
                                            <th>Номер</th>
                                            <th>Срок</th>
                                            <th>Риск</th>
                                            <th>Банк</th>
                                            <th>Страховая премия</th>
                                            <th>Страховая компания</th>
                                            <th>Авто</th>
                                            <th>Модель</th>
                                            <th>Год в</th>
                                            <th>Серия</th>
                                            <th>№</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($client->imports as $import)
                                            <tr>
                                                <td>{{$import->pivot->FIO}}</td>
                                                <td>{{$import->pivot->number}}</td>
                                                <td>{{$import->pivot->srok}}</td>
                                                <td>{{$import->pivot->type}}</td>
                                                <td>{{$import->pivot->bank}}</td>
                                                <td>{{$import->pivot->sum}}</td>
                                                <td>{{$import->pivot->company}}</td>
                                                <td>{{$import->pivot->brand_model}}</td>
                                                <td>{{$import->pivot->model}}</td>
                                                <td>{{$import->pivot->year}}</td>
                                                <td>{{$import->pivot->serial}}</td>
                                                <td>{{$import->pivot->serial_number}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane fade" id="s5">
                                    <div class="row">
                                        <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            @if (Authority::can('add_client_files'))
                                            <button class="btn btn-success"  onclick="getModal('{{action('ClientController@getModalAddClientFile')}}/{{$client->id}}','#addClientFile')">
                                                <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Добавить файл</span>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    <table id="custom-table" class="table table-striped table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>Файл</th>
                                            <th>Добавлен</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (Authority::can('view_client_files'))
                                        @foreach($client->files()->orderBy('created_at','desc')->get() as $file)
                                        <tr id="fail_{{$file->id}}">
                                            <td>
                                                {{$file->desc}}
                                            </td>
                                            <td>
                                                @if (Authority::can('download_client_files'))
                                                <a href="{{action('ClientController@getDownload')}}/{{$file->id}}" target="_blank">{{$file->original_name}}</a>
                                                @else
                                                {{$file->original_name}}
                                                @endif
                                            </td>
                                            <td>
                                                {{$file->created_at->format('d-m-Y H:i:s')}}
                                            </td>
                                            <td class="custom-data-action" style="width: 120px;">
                                                <div class="columncontrols">
                                                    <div class="btn-group">
                                                        @if (Authority::can('download_client_files'))
                                                        <a href="{{action('ClientController@getDownload')}}/{{$file->id}}" target="_blank" class="btn btn-sm btn-success" title="Скачать">
                                                            <i class="fa fa-download"></i>
                                                        </a>
                                                        @endif
                                                        @if (Authority::can('edit_client_files'))
                                                        <button class="btn btn-sm btn-default" onclick="getModal('{{action('ClientController@getModalEditClientFile')}}/{{$file->id}}','#editClientFile')" title="Редактировать">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        @endif
                                                        @if (Authority::can('delete_client_files'))
                                                        <button class="btn btn-sm btn-danger" onclick="deleteFile({{$file->id}})" title="Удалить">
                                                            <i class="fa fa-trash-o"></i>
                                                        </button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if (Authority::can('is_agent'))
                                <div class="tab-pane fade in" id="s6">
                                    @include('notifications.blocks.agentNotification')
                                    <table id="custom-table" class="table table-striped table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th class="hidden-xs">От</th>
                                            <th></th>
                                            <th></th>
                                            <th  class="hidden-xs">Дата</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($client->notifications()->orderBy('created_at','desc')->get() as $note)
                                        <tr id="note_{{$note->id}}" class="{{$note->type->class}}">
                                            <td class="custom-table-number hidden-xs" >
                                                @if ($note->notification_type_id==0)
                                                <em class="badge no-border-radius pull-left margin-right-5" style="padding: 0!important; background: none!important" rel="tooltip" data-placement="right" data-original-title="{{$note->user->username}}">
                                                    @if (!$note->user->hasImage())
                                                    {{User::getDefaultImage(40,40)}}
                                                    @else
                                                    <img data-src="holder.js/30x30" class="img-rounded"  id="user_img_{{$note->user->id}}" alt="{{$note->user->username}}" src="data:image/png;base64,{{base64_encode(Image::make($note->user->imagePath())->resize(40, null, true))}}" />
                                                    @endif
                                                </em>
                                                @else
                                                <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5" rel="tooltip" data-placement="right" data-original-title="{{$note->type->name}}">
                                                    <i class="fa {{$note->getIcon()}} fa-fw fa-2x"></i>
                                                </em>
                                                @endif
                                            </td>
                                            <td style="cursor:pointer">
                                                <div rel="popover" data-placement="top"  data-original-title="{{$note->name}}" data-content="{{$note->text}}">
                                                    <b>{{str_limit($note->name, $limit = 50, $end = '...')}}</b><br>
                                                    {{str_limit($note->text, $limit = 50, $end = '...')}}
                                                </div>
                                            </td>
                                            <td class="custom-data-action">
                                                @if ($note->sent==0)
                                                <span class="badge bg-color-red">новая</span>
                                                @endif
                                            </td>
                                            <td class="custom-data-action hidden-xs width100px">
                                                 @if ($note->sent==1)
                                                 {{$note->getTimeAgo()}}
                                                 @else
                                                 {{$note->created_at->format('d-m-Y H:i:s')}}
                                                 @endif
                                            </td>
                                            <td class="custom-data-action">
                                                <div class="columncontrols">
                                                    <button class="btn btn-sm btn-danger" onclick="deleteClientNote({{$note->id}})" title="Удалить">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->
    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>

    pageSetUp();

    $(document).ready(function() {
        updateNrNewClients();
        updateNrInProgressClients();
    });

    @if (Authority::can('edit_client'))
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $editclient = $('#save-client').validate({
            // Rules for form validation
            rules : {
                firstname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                lastname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                secondname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                email : {
                    email :true
                }
            },
            // Messages for form validation
            messages : {
                email : {
                    email : 'Пожалуйста, введите действующий адрес электронной почты'
                }
            },
            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        //$('#addClient').modal('hide');
                        //loadURL("{{action('ClientController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }
    @endif

    @if (Authority::can('is_agent'))
    function sendPolicy(id)
    {
        $.SmartMessageBox({
            title: "Отправить на утверждение",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postSendToApproval')}}",
                    data: {policy:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    //$('#sendToApproval_'+id).remove();
                   // $('#editPolicy_'+id).remove();
                   // $('#deletePolicy_'+id).remove();
                    ajaxLoadPart('s3',getCurentUrl(),null,s4TabShow);
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });

    }
    @endif

    function deleteClientNote(id) {
        $.SmartMessageBox({
            title: "Удалить уведомление",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('NotificationController@postDeleteClientNote')}}/" + id
                }).done(function (msg) {
                    $('#note_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }

    @if (Authority::can('make_not_interested_clients'))
    function notInterested(id)
    {
        $.SmartMessageBox({
            title: "Не заинтересованный клиент",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postSendToNotInterested')}}",
                    data: {client:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    window.location.hash = "{{action('ClientController@getNotInterested')}}/"+id;
                    updateNrInProgressClients();
                    updateNrNoInterestedClients();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }
    @endif

    function makeInProgress($id){
        id = '{{$client->id}}';
        $.SmartMessageBox({
            title: "В процессе",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postMakeInProgress')}}",
                    data: {client:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    //window.location.hash = "{{action('ClientController@getInProgress')}}/"+id;
                    loadURL("{{action('ClientController@getInProgress')}}/"+id, $('#content'));
                    updateNrInProgressClients();
                    //updateNrNoInterestedClients();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }

    function callBack(id)
    {
        $.SmartMessageBox({
            title: "Перезвонить",
            content : "Вы уверены?",
            buttons : "[Отменить][Да]"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postCallBackClient')}}",
                    data: {client:id}
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    window.location.hash = "{{action('ClientController@getCallBack')}}";
                    updateNrInProgressClients();
                    //updateNrNoInterestedClients();
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });
    }


    @if (Authority::can('delete_client_files'))
    function deleteFile(id) {
        $.SmartMessageBox({
            title: "Удалить файл",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeleteFile')}}/" + id
                }).done(function (msg) {
                    $('#fail_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
    @endif



    function getModels(id,element)
    {
        $.ajax({
            type: "get",
            url: "{{action('ClientController@getCarModels')}}",
            data: {brand:id}
        }).done(function (data) {
            var options = '';
            $.each(data, function(i, item) {
               if (data[i].id==0)
               {
                   options += '<option value="'+data[i].id+'" disabled selected>'+data[i].name+'</option>';
               }else
               {
                   options += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
               }
            });
            $(element).html(options);
        })
            .fail(function () {
                $.smallBox({
                    title: "{{Lang::get('client.error')}}",
                    content: "{{Lang::get('client.errorText')}}",
                    color: "#c26565",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 4000
                });
            });
    }

    @if (Authority::can('edit_client_car'))
    function editClientCar(id)
    {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('ClientController@postEditCarClient')}}",
            data: {car:id, client:{{$client->id}} }
        }).done(function (data) {
            $('#tr_edit_car').remove();
            $('#tr_car_'+id).after(data);
            loading('hide');
        })
            .fail(function () {
                $.smallBox({
                    title: "{{Lang::get('client.error')}}",
                    content: "{{Lang::get('client.errorText')}}",
                    color: "#c26565",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 4000
                });
                loading('hide');
            });
    }
    @endif

    @if (Authority::can('edit_client_policies'))
    function editClientPolicy(id)
    {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('ClientController@postEditPolicyClient')}}",
            data: {policy:id, client:{{$client->id}} }
    }).done(function (data) {
        $('#tr_edit_policy').remove();
        $('#tr_policy_'+id).after(data);
        loading('hide');
    })
        .fail(function () {
            $.smallBox({
                title: "{{Lang::get('client.error')}}",
                content: "{{Lang::get('client.errorText')}}",
                color: "#c26565",
                iconSmall: "fa fa-times bounce animated",
                timeout: 4000
            });
            loading('hide');
        });
    }
    @endif

    @if (Authority::can('delete_client_car'))
    function deleteClientCar(id)
    {
        $.SmartMessageBox({
            title: "Удалить автомобиль",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeleteCar')}}/" + id
                }).done(function (msg) {
                    $('#tr_car_' + id).remove();
                    $('#tr_edit_car').remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
    @endif

    @if (Authority::can('delete_client_policy'))
    function deleteClientPolicy(id)
    {
        $.SmartMessageBox({
            title: "Удалить  страховку",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('ClientController@postDeletePolicy')}}/" + id
                }).done(function (msg) {
                    $('#tr_policy_' + id).remove();
                    $('#tr_edit_policy').remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
    @endif

</script>