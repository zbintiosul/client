<!-- Modal -->
<div class="modal fade" id="distributeSelectedClients" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Распределить выбранные клиенты
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('ClientController@postDistributeClientsSelected')}}" id="distribute-selected-clients" class="smart-form" method="post" onsubmit="return false;">
                    <fieldset>
                        <section class="user-note">
                            <label class="select"> <i class="icon-append fa fa-user"></i>
                                <div class="form-group">
                                    <select multiple style="width:100%"  id="select2UsersToDistribute" placeholder="Выберите пользователей" name="users[]">
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->username}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Распределить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    $( document ).ready(function() {
        $('#select2UsersToDistribute').select2({
            formatNoMatches: function() {
                return '';
            },
            dropdownCssClass: 'select2-hidden'
        });
    });

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $distributeSelectedClients = $('#distribute-selected-clients').validate({
            // Rules for form validation
            rules : {
                'users[]' : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                'users[]' : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {

                    var data = $('.client-checkbox, #distribute-selected-clients').serializeArray();
                    loading('show');
                    $.ajax({
                        type: "POST",
                        url: "{{action('ClientController@postDistributeClientsSelected')}}",
                        data: data
                    }).done(function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#distributeSelectedClients').modal('hide');
                        loadURL(getCurentUrl(), $('#content'));
                    })
                        .fail(function() {
                            $.smallBox({
                                title : "{{Lang::get('client.error')}}",
                                content : "{{Lang::get('client.errorText')}}",
                                color : "#c26565",
                                iconSmall : "fa fa-times bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                        });
            }
        });

    }

</script>