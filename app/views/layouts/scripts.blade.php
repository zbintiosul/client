<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>


<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
		<script src="/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="/js/notification/SmartNotification.js"></script>

<!-- JARVIS WIDGETS -->
<script src="/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="/js/plugin/jquery-validate/additional-methods.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="/js/plugin/fastclick/fastclick.js"></script>

<!-- Url helper -->
<script src="/js/plugin/purl.js"></script>

<!--[if IE 7]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->

@if (Auth::check())
 <script src="{{Config::get('app.url')}}:3000/socket.io/socket.io.js"></script> 
@endif
<!-- MAIN APP JS FILE -->
<script src="/js/app.js"></script>
<script src="/js/client.js"></script>

@if (Auth::check())
<script type="text/javascript">// <![CDATA[

    // Load form valisation dependency
    $('#search-client').submit(function(e){
        e.preventDefault();
        @if (Authority::can('view_all_clients'))
        window.location.hash = "{{action('ClientController@getIndex')}}?search="+$(this).find('input[name="search"]').val();
        @else
        window.location.hash = "{{action('ProfileController@getSearch')}}?search="+$(this).find('input[name="search"]').val();
        @endif

    });


     var socket = io.connect('{{Config::get("app.url")}}:3000');
     var username = '{{Auth::user()->username}}';

     socket.emit('log_in', { username: username });

     socket.on('notification.update.{{Auth::user()->username}}', function (data) {
         //Do something with data
         console.log('Notifications updated: ', data);
         var json = jQuery.parseJSON(data)
         //var json  = JSON.stringify(eval("(" + data + ")"));
         //alert(json);
         alertNotification(json);
     });

    @if (!Authority::canOnly('is_broker'))
     socket.on('newClient.update.{{Auth::user()->username}}', function (data) {
         //Do something with data
         console.log('New Client updated: ', data);
        //var json = jQuery.parseJSON(data)
         //var json  = JSON.stringify(eval("(" + data + ")"));
         //alert(json);
         $.bigBox({
             title : "Новые клиенты",
             content : "<b>У тебя новыйе клиенты!</b>",
             color : "#496949",
             icon : "fa fa-plus shake animated"
             //number : "1"
             // timeout : 6000
         });
         updateNrNewClients();
     });

     socket.on('notInterested.update.{{Auth::user()->username}}', function (data) {
         //Do something with data
         console.log('not Interested updated: ', data);
         var json = jQuery.parseJSON(data)
         //var json  = JSON.stringify(eval("(" + data + ")"));
         //alert(json);
         $.bigBox({
             title : "Не заинтересованный клиент",
             content : "<b>Новый не заинтересованный клиент!</b>",
             color : "#496949",
             icon : "fa fa-eye-slash shake animated"
             //number : "1"
             // timeout : 6000
         });
         updateNrNoInterestedClients();
     });
    @endif
    function updateNrNewClients()
    {
        $.getJSON("{{action('ProfileController@getNrNewClients')}}", function( data ) {
            $this = $('#newClients > .badge');
            $($this).text(parseInt(data))
            if (parseInt($this.text()) > 0) {
                $this.addClass("bg-color-red bounceIn animated")
            }
            $('#newClientsNrNote').text(parseInt(data));
            $('#newClientsBadge').text(parseInt(data));
        });
    }

    function updateNrInProgressClients()
    {
        $.getJSON("{{action('ProfileController@getNrInProgressClients')}}", function( data ) {
            $this = $('#inProgress > .badge');
            $($this).text(parseInt(data))
            if (parseInt($this.text()) > 0) {
                $this.addClass("bg-color-red bounceIn animated")
            }
            $('#inProgressNrNote').text(parseInt(data));
            $('#inProgressBadge').text(parseInt(data));
        });
    }

    function updateNrNoInterestedClients()
    {
        $.getJSON("{{action('ProfileController@getNrNotInterestedClients')}}", function( data ) {
            $this = $('#notInterested > .badge');
            $($this).text(parseInt(data))
            if (parseInt($this.text()) > 0) {
                $this.addClass("bg-color-red bounceIn animated")
            }
            $('#notInterestedNrNote').text(parseInt(data));
            $('#notInterestedBadge').text(parseInt(data));
        });
    }


    function alertNotification(json)
    {
        if (json.notification_type_id==0)
        {
            $.bigBox({
                title : "Новая уведомления",
                content : "<b>"+json.name+"</b><br>"+json.text,
                color : "#3276b1",
                icon : "fa fa-bell shake animated"
                //number : "1"
               // timeout : 6000
            });
        }else
        if (json.notification_type_id==1)
        {
            $.bigBox({
                title : "Новая уведомления",
                content : "<b>"+json.name+"</b><br>"+json.text,
                color : "#c79121",
                icon : "fa fa-warning shake animated"
                //number : "1"
               // timeout : 6000
            });
        }else
        if (json.notification_type_id==2)
        {
            $.bigBox({
                title : "Новая уведомления",
                content : "<b>"+json.name+"</b><br>"+json.text,
                color : "#739e73",
                icon : "fa fa-check shake animated"
                //number : "1"
                //timeout : 6000
            });
        }else
        if (json.notification_type_id==3)
        {
            $.bigBox({
                title : "Новая уведомления",
                content : "<b>"+json.name+"</b><br>"+json.text,
                color : "#57889c",
                icon : "fa fa-info shake animated"
                //number : "1"
               // timeout : 6000
            });
        }else
        if (json.notification_type_id==4)
        {
            $.bigBox({
                title : "Новая уведомления",
                content : "<b>"+json.name+"</b><br>"+json.text,
                color : "#a90329",
                icon : "fa fa-minus-circle shake animated"
                //number : "1"
                //timeout : 6000
            });
        }
        updateNrNotification();
    }

    function updateNrNotification()
    {
        $.getJSON("{{action('ProfileController@getNrNewNote')}}", function( data ) {
            $this = $('#activity > .badge');
            $($this).text(parseInt(data))
            if (parseInt($this.text()) > 0) {
                $this.addClass("bg-color-red bounceIn animated")
            }
            $('#activeNrNote').text(parseInt(data))
        });
    }

    // ]]></script>
@endif
