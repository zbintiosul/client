<!--include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder-->
@include('layouts.header')

<!--//include left panel (navigation)
//follow the tree in inc/config.ui.php-->
@include('layouts.nav')

   <!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- MAIN PANEL -->
    <div id="main" role="main">
        @include('layouts.ribbon')

        <!-- MAIN CONTENT -->
        <div id="content">

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->
    <!-- ==========================CONTENT ENDS HERE ========================== -->


<!--//include required scripts-->
@include('layouts.scripts')
<!--//include footer-->
@include('layouts.footer')
