<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> ProAssist </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/font-awesome.min.css">

    <!-- Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-production_unminified.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-skins.css">

    <!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="/css/smartadmin-rtl.css"> -->

    <!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.-->
    <link rel="stylesheet" type="text/css" media="screen" href="/css/client_style.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>
<body class="smart-style-3 fixed-header fixed-ribbon fixed-navigation"
    <?php
    if ($page_body_prop) {
        foreach ($page_body_prop as $prop_name => $value) {
            echo $prop_name.'="'.$value.'" ';
        }
    }

    ?>
    >
<!-- POSSIBLE CLASSES: minified, fixed-ribbon, fixed-header, fixed-width
     You can also add different skin classes such as "smart-skin-1", "smart-skin-2" etc...-->

<!-- HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"><a class="ajaxa" href="{{action('ProfileController@getIndex')}}"><img src="/img/client_logo.png" alt="ProAssist"></a> </span>
        <!-- END LOGO PLACEHOLDER -->

        @include('layouts.blocks.activity')
        @if (!Authority::canOnly('is_broker'))
            @include('layouts.blocks.newClients')

            @include('layouts.blocks.inProgress')

            @include('layouts.blocks.notInterested')
        @endif
        <!--<span id="activity" class="activity-dropdown"> <i class="fa fa-retweet"></i> <b class="badge"> 1 </b> </span>
        <span id="activity" class="activity-dropdown hidden-xs"> <i class="fa fa-exclamation-triangle"></i> <b class="badge"> 1 </b> </span>-->

    </div>

    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="{{action('AccountController@getLogout')}}" title="Выход" data-logout-msg="Убедитесь, что вы не оставили активность."><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->

        <!-- input: search field -->
        <form  action="@if (Authority::can('view_all_clients'))  {{action('ClientController@getIndex')}} @else  {{action('ProfileController@getSearch')}} @endif" id="search-client" class="header-search pull-right">
            <input type="text" name="search" placeholder="Поиск номера" id="search-fld">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
            <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
        </form>
        <!-- end input: search field -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" onclick="launchFullscreen(document.documentElement);" title="Full Screen"><i class="fa fa-fullscreen"></i></a> </span>
        </div>
        <!-- end fullscreen button -->

        <!-- time -->
        <div class="header-dropdown-list hidden-xs hidden-sm timeHeader" id="clock">
        </div>
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
Note: These tiles are completely responsive,
you can add as many as you like
-->
<div id="shortcut">
    <ul>
        <li>
            <a href="#" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Мои заказы <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
        </li>
        <li>
            <a href="#" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-bell fa-4x"></i> <span>Мои уведомлений <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->


