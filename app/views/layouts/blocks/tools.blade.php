<!-- Modal -->
<div class="toolsWindow" id="modalTools" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="border: 1px solid #f2f2f2;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showTools()">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <i class="fa fa-wrench hit" style="color: #058dc7;"></i> Инструменты
                </h4>
            </div>
            <div class="modal-body no-padding">
                @foreach (Tools::all() as $tool)
                <ul class="notification-body">
                    <li>
                        <span class="padding-10 unread">
                            @if ($tool->img_path!=null)
                            <em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5" >
                                <img data-src="holder.js/70" class="img-rounded"  id="tool_img_70" alt="70x70" src="data:image/png;base64,{{ base64_encode(Image::make(storage_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'tools'.DIRECTORY_SEPARATOR.$tool->img_path)->resize(70, 70,true))}}">
                            </em>
                            @endif
                    <span>
                         {{$tool->text}}
                    </span>
                        </span>
                    </li>
                </ul>
                @endforeach
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    function showTools()
    {
        $('#modalTools').toggle('slow');
    }
    $( document ).ready(function() {
            $( "#modalTools" ).draggable();
    });

</script>