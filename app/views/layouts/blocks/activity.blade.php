        <!-- Note: The activity badge color changes when clicked and resets the number to 0
        Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
        <span id="activity" class="activity-dropdown"> <i class="fa fa-bell"></i> <b class="badge"> {{Auth::user()->nrNewNote()}} </b> </span>

        <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
        <div class="ajax-dropdown active-down">

            <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
            <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default">
                    <input type="checkbox" name="activity" id="{{action('NotificationController@getAjax')}}">
                    Уведомлений(<span id="activeNrNote">{{Auth::user()->nrNewNote()}}</span>) </label>
            </div>


            <!-- notification content -->
            <div class="ajax-notifications custom-scroll ajax-activ">

                <div class="alert alert-transparent">
                    <h4>Нажмите кнопку, чтобы показывать сообщений</h4>
                    Это пустое сообщение страницы помогает вам защитить вашу частную жизнь.
                </div>

                <i class="fa fa-lock fa-4x fa-border"></i>

            </div>
            <!-- end notification content -->

            <!-- footer: refresh area -->
				<span id="footerActive" style="display: none;">
					<button type="button" id="refreshNotifications" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Загрузка..." class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-refresh"></i>
                </button> </span>
            <!-- end footer -->

        </div>
        <!-- END AJAX-DROPDOWN -->
