<span id="inProgress" class="activity-dropdown"> <i class="fa fa-retweet"></i> <b class="badge"> {{Auth::user()->nrInProgressClients()}} </b> </span>

<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
<div class="ajax-dropdown inProgress-down">

    <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
    <div class="btn-group btn-group-justified" data-toggle="buttons">
        <label class="btn btn-default">
            <input type="checkbox" name="inProgress" id="{{action('ClientController@getInProgressAjax')}}">
            Клиенты в процессе (<span id="inProgressNrNote">{{Auth::user()->nrInProgressClients()}}</span>) </label>
    </div>


    <!-- notification content -->
    <div class="ajax-notifications custom-scroll ajax-in-progress">

        <div class="alert alert-transparent">
            <h4>Нажмите кнопку, чтобы показывать клиентов</h4>
            Это пустое сообщение страницы помогает вам защитить вашу частную жизнь.
        </div>

        <i class="fa fa-lock fa-4x fa-border"></i>

    </div>
    <!-- end notification content -->

    <!-- footer: refresh area -->
							<span id="footerInProgress" style="display: none;">
								<button type="button" id="refreshInProgress" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Загрузка..." class="btn btn-xs btn-default pull-right">
                                    <i class="fa fa-refresh"></i>
                                </button> </span>
    <!-- end footer -->

</div>
<!-- END AJAX-DROPDOWN -->