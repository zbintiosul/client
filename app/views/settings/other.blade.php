<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Настройки
			<span>>
				Другие
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">

        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2> Настройки</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <ul id="myTab1" class="nav nav-tabs bordered">
                            <li class="active">
                                <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text"></i>Другие</a>
                            </li>
                        </ul>

                        <div id="userContentTab" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">

                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $edituserdata = $('#edit-user-data').validate({
            // Rules for form validation
            rules: {
                email: {
                    required: true,
                    email: true,
                    checkEmail: true
                },
                firstname: {
                    regex: "[A-Za-zА-Яа-я]"
                },
                lastname: {
                    regex: "[A-Za-zА-Яа-я]"
                },
                secondname: {
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages: {
                email: {
                    required: 'Пожалуйста, заполните это поле',
                    email: 'Пожалуйста, введите действующий адрес электронной почты'
                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                loading('show');
                $(form).ajaxSubmit({
                    success: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.success')}}",
                            content: "{{Lang::get('client.successText')}}",
                            color: "rgb(115, 158, 115)",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    fail: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    error: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }
</script>