<div class="row" id="level_{{$i}}">
    <section class="col col-1">
        <label class="input"> <i class="icon-append"></i>
            <input type="text" name="rank_{{$i}}" placeholder="Уровень">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Уровень</b>
        </label>
    </section>
    <section class="col col-4">
        <label class="input"> <i class="icon-append">руб</i>
            <input type="text" name="min_{{$i}}" placeholder="Мин">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Мин [0-9]</b>
        </label>
    </section>
    <section class="col col-4">
        <label class="input"> <i class="icon-append">руб</i>
            <input type="text" name="max_{{$i}}" placeholder="Мах">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Мах [0-9]</b>
        </label>
    </section>
    <section class="col col-2">
        <label class="input"> <i class="icon-append">%</i>
            <input type="text" name="percentage_{{$i}}" placeholder="Комиссион">
            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Комиссион [0-9]</b>
        </label>
    </section>
    <section class="col col-1">
        <a class="btn btn-sm btn-danger" onclick="$('#level_{{$i}}').remove();" title="Удалить">
            <i class="fa fa-trash-o"></i>
        </a>
    </section>
</div>