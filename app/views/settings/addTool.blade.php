<!-- Modal -->
<div class="modal fade" id="addTool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Новый инструмент
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('SettingController@postAddTool')}}" id="add-tool" class="smart-form" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <section>
                            <textarea name="content_tool">
                            </textarea>
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <input type="file" name="tool_image" accept=".jpeg, .jpg, .png, .gif" class="btn btn-default" id="tool_image">
                            <p class="help-block">
                                формат .jpeg .jpg .png .gif
                            </p>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary" onclick="CKupdate();">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();



    loadScript("/js/plugin/ckeditor/ckeditor.js", ckeditorStart);

    function ckeditorStart() {
        CKEDITOR.replace( 'content_tool', { toolbarGroups: [
            { name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
            '/',																// Line break - next group will be placed in new line.
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'links' }
        ],height: '100px', startupFocus : true} );

    }

    function CKupdate(){
        for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();
    }

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addtools = $('#add-tool').validate({
            // Rules for form validation
            rules : {
                content_tool : {
                    required : true
                },
                tool_image:  {
                    extension: "png|jpeg|gif|jpg"
                }
            },

            // Messages for form validation
            messages : {
                content_tool : {
                    required : 'Пожалуйста, заполните это поле'
                },
                tool_image : {
                    extension: 'Изображения принимаются: .jpeg, .jpg, .png, .gif'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addTool').modal('hide');
                        loadURL("{{action('SettingController@getTools')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }
</script>