<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Настройки
			<span>>
				 Зарплата
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">

        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2>  Зарплата</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <form action="{{action('SettingController@postSalary')}}" class="smart-form" id="salary-form" method="post">
                            <input type="hidden" name="nr_new_levels" id="nr_new_levels" value="0" />
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                                            <input type="text" name="base" placeholder="База" value="{{SalaryTable::base()->min}}">
                                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>База [0-9]</b>
                                        </label>
                                    </section>
                                </div>
                                @foreach($salary_table as $sal)
                                <div class="row" id="level_{{$sal->id}}">
                                    <section class="col col-1">
                                        <label class="input"> <i class="icon-append fa fa-tag"></i>
                                            <input type="text" name="rank_{{$sal->id}}" placeholder="Уровень" value="{{$sal->rank}}">
                                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Уровень</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input"> <i class="icon-append"> руб</i>
                                            <input type="text" name="min_{{$sal->id}}" placeholder="Мин" value="{{$sal->min}}">
                                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Мин [0-9]</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input"> <i class="icon-append"> руб</i>
                                            <input type="text" name="max_{{$sal->id}}" placeholder="Мах" value="{{$sal->max}}">
                                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Мах [0-9]</b>
                                        </label>
                                    </section>
                                    <section class="col col-2">
                                        <label class="input"> <i class="icon-append"> %</i>
                                            <input type="text" name="percentage_{{$sal->id}}" placeholder="Комиссион" value="{{$sal->percentage}}">
                                            <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i>Комиссион [0-9]</b>
                                        </label>
                                    </section>
                                    <section class="col col-1">
                                        <a class="btn btn-sm btn-danger" onclick="deleteLevel({{$sal->id}})" title="Удалить">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </section>
                                </div>
                                @endforeach
                                <div class="row" id="add-level-button">
                                    <section class="col col-12">
                                        <a class="btn btn-sm btn-success" onclick="addLevel()" title="Добавить">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-default">
                                    Отменить
                                </button>
                            </footer>
                        </form>
                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();

    function addLevel()
    {
        var nr = $('#nr_new_levels').val();
        //$('#number_drivers_edit').val();
        nr = nr*1+1;
        $.ajax({
            type: "POST",
            url: "{{action('SettingController@postGetLevel')}}/new_"+nr
        }).done(function(msg) {
            //$('#contentDriversEdit').html();
            $('#add-level-button').before(msg);
            $('#nr_new_levels').val($('#nr_new_levels').val()*1+1);
        })
            .fail(function() {
                $.smallBox({
                    title : "{{Lang::get('client.error')}}",
                    content : "{{Lang::get('client.errorText')}}",
                    color : "#c26565",
                    iconSmall : "fa fa-times bounce animated",
                    timeout : 4000
                });
            });
    }

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var salaryForm = $('#salary-form').validate({
            // Rules for form validation
            rules: {
             base: {
                    required: true,
                    number: true
                }
            },

            // Messages for form validation
            messages: {
                base: {
                    required: 'Пожалуйста, заполните это поле',
                    number: 'Пожалуйста, проверьте ваш ввод'
                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                loading('show');
                $(form).ajaxSubmit({
                    success: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.success')}}",
                            content: "{{Lang::get('client.successText')}}",
                            color: "rgb(115, 158, 115)",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        loadURL("{{action('SettingController@getSalary')}}", $('#content'));
                        loading('hide');
                    },
                    fail: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    error: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }

    function deleteLevel(id)
    {
        $.SmartMessageBox({
            title: "Удалить уровень",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('SettingController@postDeleteLevel')}}/" + id
                }).done(function (msg) {
                    $('#level_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
</script>