<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Настройки
			<span>>
				Пользователи
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">

        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2> Настройки</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <ul id="myTab1" class="nav nav-tabs bordered">
                            <li class="active">
                                <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text"></i>Пользователи</a>
                            </li>
                            <li>
                                <a href="#s2" id="s2Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-gear"></i> Роли</a>
                            </li>
                        </ul>

                        <div id="userContentTab" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">
                                <form action="{{action('SettingController@postUsers')}}" id="settings-users" class="smart-form" method="post">
                                    <fieldset>
                                        <section>
                                            <label class="label">Пользователи на странице</label>
                                            <label class="input"> <i class="icon-append fa fa-cog"></i>
                                                <input type="text" name="users_per_page" value="{{Setting::getValKey('users_per_page')}}">
                                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-info txt-color-teal"></i> Сколько пользователей будут на одну страницу. Число. Мин. 1 Мах. 999</b>
                                            </label>
                                        </section>
                                    </fieldset>
                                    <div class="form-horizontal form-actions crossButton">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="reset">
                                                    Отменить
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="s2">
                                <form action="{{action('SettingController@postRoles')}}" id="settings-roles" class="smart-form" method="post">
                                    <fieldset>
                                        <section>
                                            <label class="label">Роли на странице</label>
                                            <label class="input"> <i class="icon-append fa fa-cog"></i>
                                                <input type="text"  name="roles_per_page" value="{{Setting::getValKey('roles_per_page')}}">
                                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-info txt-color-teal"></i> Сколько ролей будут на одну страницу. Число. Мин. 1 Мах. 999</b>
                                            </label>
                                        </section>
                                    </fieldset>
                                    <div class="form-horizontal form-actions crossButton">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="reset">
                                                    Отменить
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();
    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $settingsusers = $('#settings-users').validate({
            // Rules for form validation
            rules: {
                users_per_page: {
                    required: true,
                    regex: "^[0-9]{1,3}$"
                }
            },

            // Messages for form validation
            messages: {
                users_per_page: {
                    required: 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                loading('show');
                $(form).ajaxSubmit({
                    success: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.success')}}",
                            content: "{{Lang::get('client.successText')}}",
                            color: "rgb(115, 158, 115)",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    fail: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    error: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        var $settingsroles= $('#settings-roles').validate({
            // Rules for form validation
            rules: {
                roles_per_page: {
                    required: true,
                    regex: "^[0-9]{1,3}$"
                }
            },

            // Messages for form validation
            messages: {
                roles_per_page: {
                    required: 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                loading('show');
                $(form).ajaxSubmit({
                    success: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.success')}}",
                            content: "{{Lang::get('client.successText')}}",
                            color: "rgb(115, 158, 115)",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    fail: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    },
                    error: function (msg) {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }
</script>