<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Настройки
			<span>>
				 Инструменты
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">

        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2>  Инструменты</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                            <div class="widget-body-toolbar">
                                <div class="row">
                                    <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                    </div>
                                    <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                        @if (Authority::can('add_tools'))
                                        <button class="btn btn-success" onclick="getModal('{{action('SettingController@getModalAddTool')}}','#addTool')">
                                            <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Добавить</span>
                                        </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        {{$tools->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Фото</th>
                                <th>Техт</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tools as $tool)
                            <tr id="tr_{{$tool->id}}">
                                <td class="custom-table-number hidden-xs">
                                    @if ($tool->img_path!=null)
                                    <img data-src="holder.js/70" class="img-rounded"  id="tool_img_70" alt="70x70" src="data:image/png;base64,{{ base64_encode(Image::make(storage_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'tools'.DIRECTORY_SEPARATOR.$tool->img_path)->resize(70, 70,true))}}">
                                    @endif
                                </td>
                                <td class="custom-data-column">
                                    <div>{{$tool->text}}</div>
                                </td>
                                <td class="custom-data-action">
                                    @if (Authority::can('delete_tools'))
                                    <button type="button" onclick="deleteTool({{$tool->id}})"
                                            class="btn btn-xs btn-danger" title="Удалить">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$tools->links()}}
                    </div>
                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>
    pageSetUp();

    function deleteTool(id) {
        $.SmartMessageBox({
            title: "Удалить инструмент",
            content: "Вы уверены?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                $.ajax({
                    type: "POST",
                    url: "{{action('SettingController@postDeleteTool')}}/" + id
                }).done(function (msg) {
                    $('#tr_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loadURL("{{action('SettingController@getTools')}}", $('#content'));
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
</script>