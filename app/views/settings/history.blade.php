<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            History
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  Новые </h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">

                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                </div>
                            </div>
                        </div>
                        {{$history->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th>Activity</th>
                                <th> <label class="select">
                                        <select name="user" onchange="filterUser(this.value)">
                                            <option value="" selected>Username</option>
                                            @foreach ($usedUsers as $user)
                                            @if (is_object($user->user))
                                            <option value="{{$user->user->id}}" @if (Input::get('user')==$user->user->id) selected @endif>{{$user->user->lastname}} {{$user->user->firstname}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <i></i> </label>
                                </th>
                                <th>
                                    <div class="row">
                                        <div class="col-sm-6" style="padding: 0">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input class="form-control" id="from" type="text" placeholder="From" value="{{Input::get('from')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" style="padding-left: 0">
                                           <div class="form-group">
                                                <div class="input-group">
                                                    <input class="form-control" id="to" type="text" placeholder="To" value="{{Input::get('too')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($history as $hist)
                            <tr>
                                <td class="custom-table-number hidden-xs">{{$hist->id}}</td>
                                <td  class="custom-data-column">
                                    <div class="txt-color-darken">{{$hist->text}}</div>
                                </td>
                                <td  class="custom-data-column">
                                    <div>{{$hist->user->firstname}} {{$hist->user->lastname}}</div>
                                </td>
                                <td class="custom-data-action" style="width: 200px;">
                                    {{$hist->created_at->format('d-m-Y H:i:s')}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$history->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>
    pageSetUp();

    function filterUser(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'user',value);
    }

    function filterFrom(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'from',value);
    }

    function filterTo(value)
    {
        window.location.href = URLInjectionHelper(window.location.href,'too',value);
    }
    	// pagefunction

    $( document ).ready(function() {

       $("#from").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            maxDate: Date.now(),
            minDate: {{$minDate}},
            dateFormat: "dd-mm-yy",
            numberOfMonths: 3,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            onClose: function (selectedDate) {
               filterFrom(selectedDate);
            }
        });

       $("#to").datepicker({
            defaultDate: "0",
            maxDate: Date.now(),
            minDate: {{$minDate}},
            dateFormat: "dd-mm-yy",
            numberOfMonths: 3,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            onClose: function (selectedDate) {
                filterTo(selectedDate);
                //$("#from").datepicker("option", "minDate", selectedDate);
            }
        });
    });
    		 // Date Range Picker


</script>