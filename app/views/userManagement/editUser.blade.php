<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-user fa-fw "></i>
            Пользователи
			<span>>
				Пользователи
			</span>
            <span>>
				{{$user->username}}
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Общий доход <span class="txt-color-blue"><i class="fa fa-money" data-rel="bootstrap-tooltip"
                                                                 title="Общий доход"></i>&nbsp;0</span></h5>

                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    1, 20, 50, 80, 100, 101, 150, 186, 200, 230, 280, 340, 360
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2> {{$user->username}}</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <ul id="myTab1" class="nav nav-tabs bordered">
                            <li class="active">
                                <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text"></i>Данные</a>
                            </li>
                            <li>
                                <a href="#s2" id="s2Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-gear"></i> Роли</a>
                            </li>
                            <li>
                                <a href="#s3" id="s3Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-key"></i>
                                    Изменить пароль</a>
                            </li>
                            <li>
                                <a href="#s4" id="s4Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-photo"></i>
                                    Фотография</a>
                            </li>
                        </ul>

                        <div id="userContentTab" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">
                                <form class="form-horizontal"
                                      action="{{action('UserManagementController@postEditUserData')}}" method="post"
                                      id="edit-user-data">
                                    <input type="hidden" name="id_user" value="{{$user->id}}">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Имя</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Имя" name="firstname"
                                                       type="text" value="{{$user->firstname}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Фамилия</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Фамилия" name="lastname"
                                                       type="text" value="{{$user->lastname}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Отчество</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Отчество" name="secondname"
                                                       type="text" value="{{$user->secondname}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Домашний
                                                телефон</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Домашний телефон" name="home_phone" type="home_phone"
                                                       value="{{$user->home_phone}}" data-mask="(999) 99-999-999">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Рабочий
                                                телефон</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Рабочий телефон" name="work_phone" type="work_phone"
                                                       value="{{$user->work_phone}}" data-mask="(9)(999) 999-9999 / 99">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Электронная
                                                почта</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Электронная почта" name="email"
                                                       type="email" value="{{$user->email}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="reset">
                                                        Отменить
                                                    </button>
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="fa fa-save"></i>
                                                        Сохранить
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <fieldset>
                                        <div class="form-group">
                                            @if ($user->id != Config::get('app.super_admin'))
                                            <label class="col-md-2 control-label" for="text-field">Активный</label>

                                            <div class="col-md-1 smart-form activeElement">
                                                <label class="toggle">
                                                    <input type="checkbox" class="activeCheckbox" onchange="activateUser({{$user->id}})" name="checkbox-toggle" @if ($user->active) checked="checked" @endif>
                                                    <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                                            </div>
                                            @endif
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="tab-pane fade in" id="s2">
                                <form action="{{action('UserManagementController@postEditUserRoles')}}" id="edit-user-roles" class="smart-form" method="post">
                                    <input type="hidden" name="id_user" value="{{$user->id}}">
                                    <fieldset>
                                        <section>
                                            <div class="row">
                                                <label class="label">Роли</label>
                                                <div class="row">
                                                    @foreach($roles as $role)
                                                    <div class="col col-6">
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="role_{{$role->id}}" class="checkboxRight"
                                                            @if ($user->roles()->where('role_id','=',$role->id)->count()>=1)
                                                            checked="checked"
                                                            @endif
                                                            >
                                                            <i></i>{{$role->name}}</label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </section>
                                    </fieldset>
                                    <div class="form-horizontal form-actions crossButton">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="reset">
                                                    Отменить
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade in" id="s3">
                                <form class="form-horizontal"
                                      action="{{action('UserManagementController@postEditPassword')}}" method="post"
                                      id="edit-user-password">
                                    <input type="hidden" name="id_user" value="{{$user->id}}">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Новый пароль</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Пароль" name="password"
                                                       type="password" id="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Повторите пароль</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Повторите пароль" name="password_confirmation"
                                                       type="password">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="reset">
                                                    Отменить
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade in" id="s4">
                                <form class="form-horizontal" action="{{action('UserManagementController@postUploadImageUser')}}" id="upload_image" method="post">
                                    <input type="hidden" name="user_id" value="{{$user->id}}" />
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Фотография:</label>
                                            <div class="col-md-10">
                                                @if (!$user->hasImage())
                                                {{User::getDefaultImage(140,140)}}
                                                {{User::getDefaultImage(70,70)}}
                                                {{User::getDefaultImage(30,30)}}
                                                @else
                                                <img data-src="holder.js/140" class="img-rounded"  id="user_img_140" alt="140x140" src="data:image/png;base64,{{ base64_encode(Image::make($user->imagePath())->grab(140, 140))}}">
                                                <img data-src="holder.js/70" class="img-rounded"  id="user_img_70" alt="70x70" src="data:image/png;base64,{{ base64_encode(Image::make($user->imagePath())->grab(70, 70))}}">
                                                <img data-src="holder.js/30" class="img-rounded"  id="user_img_30" alt="30x30" src="data:image/png;base64,{{ base64_encode(Image::make($user->imagePath())->grab(40,40))}}">
                                                @endif

                                                <input type="file" name="user_image" accept=".jpeg,.jpg, .png, .gif" class="btn btn-default" id="user_image">
                                                <p class="help-block">
                                                    формат .jpeg .jpg .png .gif
                                                </p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-danger" onclick="removeImageUser({{$user->id}})" type="button">
                                                    <i class="fa fa-trash-o"></i>
                                                    Удалить изображение
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-upload"></i>
                                                    Загрузить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                           </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $edituserdata = $('#edit-user-data').validate({
            // Rules for form validation
            rules : {
                email : {
                    required : true,
                    email : true,
                    checkEmail : true
                },
                firstname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                lastname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                secondname : {
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                email : {
                    required : 'Пожалуйста, заполните это поле',
                    email : 'Пожалуйста, введите действующий адрес электронной почты'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        var $edituserpassword = $('#edit-user-password').validate({
            // Rules for form validation
            rules : {
                password : {
                    required : true,
                    minlength : 4,
                    maxlength : 40
                },
                password_confirmation : {
                    required : true,
                    minlength : 4,
                    maxlength : 40,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
            messages : {
                password : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40'
                },
                password_confirmation : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40',
                    equalTo : 'Пароль не совпадают'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        var $edituserroles = $('#edit-user-roles').validate({
            // Rules for form validation
            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        var $uploadImage = $('#upload_image').validate({
            // Rules for form validation
            rules : {
                user_image:  {
                    required : true,
                    extension: "png|jpeg|gif|jpg"
                }
            },
            //Messages for form validation
            messages : {
                user_image : {
                    required : 'Выберите картинку',
                    extension: 'Изображения принимаются: .jpeg, .png, .gif'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        ajaxLoadPart('s4','{{action('UserManagementController@getEditUser')}}/{{$user->id}}',null,s4TabShow);
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        $.validator.addMethod("checkUserName",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('UserManagementController@postCheckUsername')}}", // script to validate in server side
                    data: {username: value, user:{{$user->id}}},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Этот логин уже занят! Попробуйте другой."
        );

        $.validator.addMethod("checkEmail",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('UserManagementController@postCheckEmail')}}", // script to validate in server side
                    data: {email: value,user:{{$user->id}}},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Это электронная почта уже занята! Попробуйте другая."
        );
    }

    function activateUser(id)
    {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('UserManagementController@postActivateUser')}}/"+id
        }).done(function(msg) {
            $('#activeCheckbox').prop('checked',msg);
            $.smallBox({
                title : "{{Lang::get('client.success')}}",
                content : "{{Lang::get('client.successText')}}",
                color : "rgb(115, 158, 115)",
                iconSmall : "fa fa-check bounce animated",
                timeout : 4000
            });
            loading('hide');
        })
            .fail(function() {
                $.smallBox({
                    title : "{{Lang::get('client.error')}}",
                    content : "{{Lang::get('client.errorText')}}",
                    color : "#c26565",
                    iconSmall : "fa fa-times bounce animated",
                    timeout : 4000
                });
                loading('hide');
            });
    }
    function s4TabShow()
    {
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    }

    function removeImageUser(id)
    {
        $.SmartMessageBox({
            title : "Удалить изображение",
            content : "Вы уверены, что хотите удалить это фото?",
            buttons : '[Нет][Да]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "Да") {
                loading('show');
                $.ajax({
                    type: "POST",
                    url: "{{action('UserManagementController@postRemoveImageUser')}}/"+id
                }).done(function(msg) {
                    ajaxLoadPart('s4','{{action('UserManagementController@getEditUser')}}/{{$user->id}}',null,s4TabShow);
                    $.smallBox({
                        title : "{{Lang::get('client.success')}}",
                        content : "{{Lang::get('client.successText')}}",
                        color : "rgb(115, 158, 115)",
                        iconSmall : "fa fa-check bounce animated",
                        timeout : 4000
                    });
                    loading('hide');
                })
                    .fail(function() {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    });
            }
        });
    }
</script>