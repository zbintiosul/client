<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Новый пользователь
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('UserManagementController@postAddUser')}}" id="add-user" class="smart-form" method="post">
                    <fieldset>
                        <section>
                            <label class="label">Логин</label>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="username" placeholder="Логин">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-user txt-color-teal"></i> Необходимое для входа на сайт. Мин. 4 [A-Z a-z 0-9 _ -] </b>
                            </label>
                        </section>
                        <section>
                            <label class="label">Пароль</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password" placeholder="Пароль" id="password">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-lock txt-color-teal"></i> Не забывайте пароль. Мин. 4 Мах. 40</b>
                            </label>
                        </section>
                        <section>
                            <label class="label">Повторите пароль</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password_confirmation" placeholder="Повторите пароль">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-lock txt-color-teal"></i> Не забывайте пароль. Мин. 4 Мах. 40</b>
                            </label>
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label">Электронная почта</label>
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" name="email" placeholder="Электронная почта">
                                <b class="tooltip tooltip-bottom-right"><i class="fa fa-envelope txt-color-teal"></i> Необходимое для получения электронных писем.</b>
                            </label>
                        </section>
                        <div class="row">
                            <section class="col col-4">
                                <label class="label">Имя</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="firstname" placeholder="Имя">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                            <section  class="col col-4">
                                <label class="label">Фамилия</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="lastname" placeholder="Фамилия">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                            <section  class="col col-4">
                                <label class="label">Отчество</label>
                                <label class="input"> <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="secondname" placeholder="Отчество">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-tag txt-color-teal"></i> [A-Z a-z А-Я а-я]</b>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Домашний телефон</label>
                                <label class="input"> <i class="icon-append fa fa-phone"></i>
                                    <input type="tel" name="home_phone" placeholder="Домашний телефон" data-mask="(9)(999) 999-9999">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-phone txt-color-teal"></i> [0-9]</b>
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="label">Рабочий телефон</label>
                                <label class="input"> <i class="icon-append fa fa-phone"></i>
                                    <input type="tel" name="work_phone" placeholder="Рабочий телефон" data-mask="(9)(999) 999-9999 / 99">
                                    <b class="tooltip tooltip-bottom-right"><i class="fa fa-phone txt-color-teal"></i> [0-9]</b>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label">Роли</label>
                            <div class="row">
                                @foreach($roles as $role)
                                <div class="col col-6">
                                    <label class="checkbox">
                                        <input type="checkbox" name="role_{{$role->id}}" class="checkboxRight">
                                        <i></i>{{$role->name}}</label>
                                </div>
                                @endforeach
                            </div>
                        </section>
                    </fieldset>
                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label"></label>
                                <label class="toggle">
                                    <input type="checkbox" name="active" checked="checked" value="1">
                                    <i data-swchon-text="Да" data-swchoff-text="Нет"></i>Активный</label>
                            </section>
                            <section class="col col-6">
                                <label class="label"></label>
                                <label class="checkbox">
                                    <input type="checkbox" name="force_login" value="1">
                                    <i></i>Принудительное изменение пароля при первом входе
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $adduser = $('#add-user').validate({
            // Rules for form validation
            rules : {
                username : {
                    required : true,
                    minlength : 4,
                    checkUserName: true,
                    regex: "[A-Za-z0-9\_\-]"
                },
                password : {
                    required : true,
                    minlength : 4,
                    maxlength : 40
                },
                password_confirmation : {
                    required : true,
                    minlength : 4,
                    maxlength : 40,
                    equalTo : '#password'
                },
                email : {
                    required : true,
                    email : true,
                    checkEmail : true
                },
                firstname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                lastname : {
                    regex: "[A-Za-zА-Яа-я]"
                },
                secondname : {
                    regex: "[A-Za-zА-Яа-я]"
                }
            },

            // Messages for form validation
            messages : {
                username : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4'
                },
                password : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40'
                },
                password_confirmation : {
                    required : 'Пожалуйста, заполните это поле',
                    minlength : 'Мминимальная длина 4',
                    maxlength : 'Максимальная длина 40',
                    equalTo : 'Пароль не совпадают'
                },
                email : {
                    required : 'Пожалуйста, заполните это поле',
                    email : 'Пожалуйста, введите действующий адрес электронной почты'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addUser').modal('hide');
                        loadURL("{{action('UserManagementController@getIndex')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });


        $.validator.addMethod("checkUserName",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('UserManagementController@postCheckUsername')}}", // script to validate in server side
                    data: {username: value},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Этот логин уже занят! Попробуйте другой."
        );

        $.validator.addMethod("checkEmail",
            function(value, element) {
                var result = false;
                $.ajax({
                    type:"POST",
                    async: false,
                    url: "{{action('UserManagementController@postCheckEmail')}}", // script to validate in server side
                    data: {email: value},
                    success: function(data) {
                        result = (data == 0) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "Это электронная почта уже занята! Попробуйте другая."
        );

    }
</script>