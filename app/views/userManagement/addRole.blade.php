<!-- Modal -->
<div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Новый роль
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form action="{{action('UserManagementController@postAddRole')}}" id="add-role" class="smart-form" method="post">
                    <fieldset>
                        <section>
                            <label class="label">Название</label>
                            <label class="input"> <i class="icon-append fa fa-tag"></i>
                                <input type="text" name="name" placeholder="Название">
                            </label>
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <div class="row">
                            <div class="col col-6">
                                <label class="label">Права</label>
                                </div>
                            <div class="col col-6">
                            <div id="checkMenu" class="pull-right">
                                <button type="button" class="btn btn-default" data-action="check-all">
                                    Вставить все
                                </button>
                                <button type="button" class="btn btn-default" data-action="uncheck-all">
                                    Снять все
                                </button>
                            </div>
                            </div>
                            </div>
                            @foreach ($rights as $rightg)
                            <div class="row">
                                    <label class="label" style="margin-left: 15px">{{$rightg->name}}</label>
                                    @foreach($rightg->rights as $right)
                                    <div class="col col-6">
                                    <label class="checkbox">
                                        <input type="checkbox" name="right_{{$right->id}}" class="checkboxRight">
                                        <i></i>{{$right->name}}</label>
                                    </div>
                                    @endforeach
                            </div>
                            @endforeach
                        </section>
                    </fieldset>
                    <fieldset>
                        <section>
                            <label class="label"></label>
                            <label class="toggle">
                                <input type="checkbox" name="active" checked="checked" value="1">
                                <i data-swchon-text="Да" data-swchoff-text="Нет"></i>Активный</label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Закрыть
                        </button>
                    </footer>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $addrole = $('#add-role').validate({
            // Rules for form validation
            rules : {
                name : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                name : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                        $('#addRole').modal('hide');
                        loadURL("{{action('UserManagementController@getRoles')}}", $('#content'));
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

    }
</script>