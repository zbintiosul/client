<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Пользователи
			<span>>
				Роли
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Роли <span class="txt-color-blue"><i class="fa fa-user" data-rel="bootstrap-tooltip"
                                                          title="Роли"></i>&nbsp;</span></h5>

                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    1, 20, 50, 80, 100, 101, 150, 186, 200, 230, 280, 340, 360
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"
                 data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-user"></i> </span>

                    <h2> Роли</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group input-groupSearch">
                                        <input class="form-control usersSearch" id="prepend" placeholder="Поиск роль" type="text" value="{{Input::get('search')}}">
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    @if (Input::get('search'))
                                    <a type="button" href="{{action('UserManagementController@getRoles')}}"  class="btn btn-default ajaxa" title="Очищать">
                                        <i class="fa fa-fw fa-search fa-times"></i>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    @if (Authority::can('add_role'))
                                    <button class="btn btn-success"
                                            onclick="getModal('{{action('UserManagementController@getModalAddRole')}}','#addRole')">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новый роль</span>
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{$roles->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th >Название</th>
                                <th class="hidden-xs hidden-sm">Права</th>
                                <th  class="hidden-xs">Активный</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($roles as $role)
                            <tr id="tr_{{$role->id}}">
                                <td class="custom-table-number hidden-xs">{{$role->id}}</td>
                                <td class="custom-data-name">
                                    <div>{{$role->name}}</div>
                                </td>
                                <td class="custom-data-column hidden-xs hidden-sm">
                                    <div style="overflow: auto; height: inherit;">
                                        <table>
                                            @foreach($role->rights as $right)
                                            <tr>
                                                <td class="custom-data-name">
                                                    <div>{{$right->name}}</div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </td>
                                <td class="custom-data-action hidden-xs">
                                    @if (Authority::can('edit_role'))
                                    <div class="col-md-1 smart-form activeElement">
                                        <label class="toggle">
                                            <input type="checkbox" class="activeCheckbox_{{$role->id}}"
                                                   onchange="activateRole({{$role->id}})" name="checkbox-toggle" @if
                                            ($role->active) checked="checked" @endif>
                                            <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                                    </div>
                                    @endif
                                </td>
                                <td class="custom-data-action">
                                    @if (Authority::can('edit_role'))
                                    <a href="{{action('UserManagementController@getEditRole')}}/{{$role->id}}" class="btn btn-xs btn-default ajaxa">Редактировать</a>
                                    @endif
                                </td>
                                <td class="custom-data-action">
                                    @if (Authority::can('delete_role'))
                                    <button type="button" onclick="deleteRole({{$role->id}})"
                                            class="btn btn-xs btn-danger" title="Удалить">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$roles->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

    loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);


    function runXEditDemo() {

        // constructs the suggestion engine
        var repos = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            prefetch: '{{action('UserManagementController@getJsonRoles')}}'
    });

    // kicks off the loading/processing of `local` and `prefetch`
    repos.initialize();

    $('.usersSearch').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'roles',
            displayKey: 'name',
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: repos.ttAdapter()
        });

    $(".usersSearch").keyup(function (e) {
        if (e.keyCode == 13) {
            window.location.hash = "{{action('UserManagementController@getRoles')}}?search="+$(this).val();
        }
    });
    }

    //Gets tooltips activated
    $("#custom-table [rel=tooltip]").tooltip();

    $("#custom-table input[type='checkbox']").change(function () {
        $(this).closest('tr').toggleClass("highlight", this.checked);
    });


    @if (Authority::can('edit_role'))
    function activateRole(id) {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('UserManagementController@postActivateRole')}}/" + id
        }).done(function (msg) {
            $('#activeCheckbox_' + id).prop('checked', msg);
            $.smallBox({
                title: "{{Lang::get('client.success')}}",
                content: "{{Lang::get('client.successText')}}",
                color: "rgb(115, 158, 115)",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            loading('hide');
        })
            .fail(function () {
                $.smallBox({
                    title: "{{Lang::get('client.error')}}",
                    content: "{{Lang::get('client.errorText')}}",
                    color: "#c26565",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 4000
                });
                loading('hide');
            });
    }
    @endif

    @if (Authority::can('delete_role'))
    function deleteRole(id) {
        $.SmartMessageBox({
            title: "Удалить роль",
            content: "Вы уверены, что хотите удалить эту роль?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                $.ajax({
                    type: "POST",
                    url: "{{action('UserManagementController@postDeleteRole')}}/" + id
                }).done(function (msg) {
                    $('#tr_' + id).remove();
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                    })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
    @endif
</script>