<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-user fa-fw "></i>
            Пользователи
			<span>>
				Роли
			</span>
            <span>>
				{{$role->name}}
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">

            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2> {{$role->name}}</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <ul id="myTab1" class="nav nav-tabs bordered">
                            <li class="active">
                                <a href="#s1" id="s1Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-file-text"></i>Данные</a>
                            </li>
                            <li>
                                <a href="#s2" id="s2Link" data-toggle="tab"><i class="fa fa-fw fa-lg fa-gear"></i> Права</a>
                            </li>
                        </ul>

                        <div id="userContentTab" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">
                                <form class="form-horizontal"
                                      action="{{action('UserManagementController@postEditRoleData')}}" method="post"
                                      id="edit-role-data">
                                    <input type="hidden" name="id_role" value="{{$role->id}}">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Название</label>

                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Название" name="name"
                                                       type="text" value="{{$role->name}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="reset">
                                                        Отменить
                                                    </button>
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="fa fa-save"></i>
                                                        Сохранить
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Активный</label>
                                            <div class="col-md-1 smart-form activeElement">
                                                <label class="toggle">
                                                    <input type="checkbox" class="activeCheckbox" onchange="activateRole({{$role->id}})" name="checkbox-toggle" @if ($role->active) checked="checked" @endif>
                                                    <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="s2">
                                <form action="{{action('UserManagementController@postEditRoleRights')}}" id="edit-role-rights" class="smart-form" method="post">
                                    <input type="hidden" name="id_role" value="{{$role->id}}">
                                    <fieldset>
                                        <section>
                                            <div class="row">
                                                <div class="col col-6">
                                                    <label class="label">Права</label>
                                                </div>
                                                <div class="col col-6">
                                                    <div id="checkMenu" class="pull-right">
                                                        <button type="button" class="btn btn-default" data-action="check-all">
                                                            Вставить все
                                                        </button>
                                                        <button type="button" class="btn btn-default" data-action="uncheck-all">
                                                            Снять все
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach ($rightsGroups as $rightg)
                                            <div class="row">
                                                <label class="label" style="margin-left: 15px">{{$rightg->name}}</label>
                                                @foreach($rightg->rights as $right)
                                                <div class="col col-6">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="right_{{$right->id}}" class="checkboxRight"
                                                               @if ($role->rights()->where('right_id','=',$right->id)->count()>=1)
                                                               checked="checked"
                                                               @endif
                                                        >
                                                        <i></i>{{$right->name}}</label>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endforeach
                                        </section>
                                    </fieldset>
                                    <div class="form-horizontal form-actions crossButton">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="reset">
                                                    Отменить
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();

    // Load form valisation dependency
    loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
    // Registration validation script
    function runFormValidation() {

        var $edituserdata = $('#edit-role-data').validate({
            // Rules for form validation
            rules : {
                name : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                name : {
                    required : 'Пожалуйста, заполните это поле'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });

        var $editrolerights = $('#edit-role-rights').validate({
            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function(form) {
                loading('show');
                $(form).ajaxSubmit({
                    success : function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.success')}}",
                            content : "{{Lang::get('client.successText')}}",
                            color : "rgb(115, 158, 115)",
                            iconSmall : "fa fa-check bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    fail :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    },
                    error :  function(msg) {
                        $.smallBox({
                            title : "{{Lang::get('client.error')}}",
                            content : "{{Lang::get('client.errorText')}}",
                            color : "#c26565",
                            iconSmall : "fa fa-times bounce animated",
                            timeout : 4000
                        });
                        loading('hide');
                    }
                });
            }
        });
    }

    function activateRole(id) {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('UserManagementController@postActivateRole')}}/" + id
        }).done(function (msg) {
            $('#activeCheckbox').prop('checked', msg);
            $.smallBox({
                title: "{{Lang::get('client.success')}}",
                content: "{{Lang::get('client.successText')}}",
                color: "rgb(115, 158, 115)",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            loading('hide');
        })
            .fail(function () {
                $.smallBox({
                    title: "{{Lang::get('client.error')}}",
                    content: "{{Lang::get('client.errorText')}}",
                    color: "#c26565",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 4000
                });
                loading('hide');
            });
    }
</script>