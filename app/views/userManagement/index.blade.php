<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Пользователи
			<span>>
				Все пользователи
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Пользователи <span class="txt-color-blue"><i class="fa fa-group" data-rel="bootstrap-tooltip" title="Пользователи"></i>&nbsp;{{$totalUsers}}</span></h5>
                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    1, 20, 50, 80, 100, 101, 150, 186, 200, 230, 280, 340, 360
                </div>
            </li>
            <li class="sparks-info">
                <h5> Онлайн <span class="txt-color-greenDark"><i class="fa fa-dot-circle-o"></i>&nbsp;1</span></h5>
                <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                    110,150,300,130,400,240,220,310,220,300, 270, 210
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2> Все пользователи</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-7 col-sm-4 col-md-4 col-lg-4">
                                    <div class="input-group input-groupSearch">
                                        <input class="form-control usersSearch" id="prepend" placeholder="Поиск пользователя" type="text" value="{{Input::get('search')}}">
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    @if (Input::get('search'))
                                    <a type="button" href="{{action('UserManagementController@getIndex')}}"  class="btn btn-default ajaxa" title="Очищать">
                                        <i class="fa fa-fw fa-search fa-times"></i>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                    @if (Authority::can('add_user'))
                                    <button class="btn btn-success" onclick="getModal('{{action('UserManagementController@getModalAddUser')}}','#addUser')">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Новый пользователь</span>
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{$users->links()}}
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="hidden-xs">ID</th>
                                <th></th>
                                <th>Логин</th>
                                <th>Фамилия Имя</th>
                                <th class="hidden-xs">Электронная почта</th>
                                <th class="hidden-xs hidden-sm">Состояние</th>
                                <th class="hidden-xs">Активный</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                            <tr id="tr_{{$user->id}}" idUser="{{$user->id}}" @if ($user->trashed()) class="trashedRecord" @endif>
                                <td class="custom-table-number hidden-xs">{{$user->id}}</td>
                                <td class="smallImage">
                                    @if (!$user->hasImage())
                                    {{User::getDefaultImage(30,30)}}
                                    @else
                                    <img data-src="holder.js/30x30" class="img-rounded"  id="user_img_{{$user->id}}" alt="{{$user->username}}" src="data:image/png;base64,{{base64_encode(Image::make($user->imagePath())->grab(30, 30))}}" />
                                    @endif
                                </td>
                                <td class="custom-data-name"><div>{{$user->username}}</div></td>
                                <td class="custom-data-name"><div>{{$user->lastname}} {{$user->firstname}}</div></td>
                                <td class="custom-data-column hidden-xs"><div>{{$user->email}}</div></td>
                                <td class="custom-data-action hidden-xs hidden-sm">
                                   <?php $status = $user->statusOnline(); ?>
                                   @if ($status->code==0)
                                        <span class="label label-danger">{{$status->text}}</span>
                                   @else
                                        @if ($status->code==1)
                                            <span class="label label-success">{{$status->text}}</span>
                                        @else
                                            <span class="label label-warning">{{$status->text}}</span>
                                        @endif
                                   @endif
                                </td>
                                <td class="custom-data-action hidden-xs">
                                    @if ($user->id != Config::get('app.super_admin'))
                                        @if (Authority::can('edit_user'))
                                            @if (!$user->trashed())
                                            <div class="col-md-1 smart-form activeElement">
                                                <label class="toggle">
                                                    <input type="checkbox" class="activeCheckbox_{{$user->id}}"
                                                           onchange="activateUser({{$user->id}})" name="checkbox-toggle" @if
                                                    ($user->active) checked="checked" @endif>
                                                    <i data-swchon-text="Да" data-swchoff-text="Нет"></i></label>
                                            </div>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td class="custom-data-action">
                                    @if ($user->trashed())
                                        @if (Authority::can('delete_user'))
                                        <button type="button" class="btn btn-xs btn-warning" onclick="RestoreUser({{$user->id}})" title="Восстановить">
                                            <i class="fa fa-share"></i>Восстановить
                                        </button>
                                        @endif
                                    @else
                                        @if (Authority::can('edit_user'))
                                            @if ($user->id == Config::get('app.super_admin'))
                                                @if (Auth::user()->id == Config::get('app.super_admin'))
                                                <a href="{{action('UserManagementController@getEditUser')}}/{{$user->id}}" class="btn btn-xs btn-default ajaxa">Редактировать</a>
                                                @endif
                                            @else
                                                <a href="{{action('UserManagementController@getEditUser')}}/{{$user->id}}" class="btn btn-xs btn-default ajaxa">Редактировать</a>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td class="custom-data-action">
                                    @if ($user->id != Config::get('app.super_admin'))
                                        @if (Authority::can('delete_user'))
                                            @if (!$user->trashed())
                                            <button type="button" class="btn btn-xs btn-danger" onclick="deleteUser({{$user->id}})" title="Удалить">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$users->links()}}
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();


    loadScript("/js/plugin/typeahead/typeahead.bundle.min.js", runXEditDemo);


    function runXEditDemo() {

            // constructs the suggestion engine
            var repos = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                // `states` is an array of state names defined in "The Basics"
                prefetch: '{{action('UserManagementController@getJsonUsers')}}'
            });

            // kicks off the loading/processing of `local` and `prefetch`
            repos.initialize();

            $('.usersSearch').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'users',
                    displayKey: 'username',
                    // `ttAdapter` wraps the suggestion engine in an adapter that
                    // is compatible with the typeahead jQuery plugin
                    source: repos.ttAdapter()
            });

            $(".usersSearch").keyup(function (e) {
                if (e.keyCode == 13) {
                    window.location.hash = "{{action('UserManagementController@getIndex')}}?search="+$(this).val();
                }
            });
    }

    //Gets tooltips activated
    $("#custom-table [rel=tooltip]").tooltip();

    $("#custom-table input[type='checkbox']").change(function() {
        $(this).closest('tr').toggleClass("highlight", this.checked);
    });

    @if (Authority::can('users_money_info'))
        $("#custom-table .custom-data-column").click(function() {
            $this = $(this);
            getUser($this);
        })
        $("#custom-table .custom-data-name").click(function() {
            $this = $(this);
            getUser($this);
        })

        function getUser($this) {
            window.location.hash = "{{action('UserManagementController@getUser')}}/"+$this.closest("tr").attr("idUser");
        }
    @endif

    @if (Authority::can('edit_user'))
    function activateUser(id)
    {
        loading('show');
        $.ajax({
            type: "POST",
            url: "{{action('UserManagementController@postActivateUser')}}/"+id
        }).done(function(msg) {
            $('#activeCheckbox').prop('checked',msg);
            $.smallBox({
                title : "{{Lang::get('client.success')}}",
                content : "{{Lang::get('client.successText')}}",
                color : "rgb(115, 158, 115)",
                iconSmall : "fa fa-check bounce animated",
                timeout : 4000
            });
            loading('hide');
        })
            .fail(function() {
                $.smallBox({
                    title : "{{Lang::get('client.error')}}",
                    content : "{{Lang::get('client.errorText')}}",
                    color : "#c26565",
                    iconSmall : "fa fa-times bounce animated",
                    timeout : 4000
                });
                loading('hide');
            });
    }
    @endif

    @if (Authority::can('delete_user'))
    function deleteUser(id)
    {
        $.SmartMessageBox({
            title: "Удалить пользователя",
            content : "Выберите опцию",
            buttons : "[Отменить][Удалить]",
            input : "checkbox",
            inputValue : "1",
            placeholder : "Физическое удаление"
        }, function (ButtonPressed,Value) {
            if (ButtonPressed === "Удалить") {
                $.ajax({
                    type: "POST",
                    url: "{{action('UserManagementController@postDeleteUser')}}/" + id,
                    data: {physic:Value}
                }).done(function (msg) {
                    if (Value==1)
                    {
                        $('#tr_' + id).remove();
                    }else
                    {
                        loadURL("{{action('UserManagementController@getIndex')}}", $('#content'));
                    }
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Отменить") {
                //alert(Value);
            }
        });

    }
    @endif

    @if (Authority::can('delete_user'))
    function RestoreUser(id)
    {
        $.SmartMessageBox({
            title: "Восстановить роль",
            content: "Вы уверены, что хотите восстановить пользователя?",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Да") {
                $.ajax({
                    type: "POST",
                    url: "{{action('UserManagementController@postRestoreUser')}}/" + id
                }).done(function (msg) {
                    $.smallBox({
                        title: "{{Lang::get('client.success')}}",
                        content: "{{Lang::get('client.successText')}}",
                        color: "rgb(115, 158, 115)",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    loadURL("{{action('UserManagementController@getIndex')}}", $('#content'));
                    loading('hide');
                })
                    .fail(function () {
                        $.smallBox({
                            title: "{{Lang::get('client.error')}}",
                            content: "{{Lang::get('client.errorText')}}",
                            color: "#c26565",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 4000
                        });
                        loading('hide');
                    });
            }
            if (ButtonPressed === "Нет") {
                //nothing
            }
        });
    }
    @endif

</script>