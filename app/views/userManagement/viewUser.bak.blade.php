<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-user fa-fw "></i>
            Пользователи
			<span>>
				Пользователи
			</span>
            <span>>
				{{$user->username}}
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Общий доход <span class="txt-color-blue"><i class="fa fa-money" data-rel="bootstrap-tooltip"
                                                                 title="Общий доход"></i>&nbsp;{{Locale::number($user->calcTotalSalesVenit())}}</span></h5>

                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    {{Locale::number($user->calcTotalSalesVenit())}}
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol"></i> </span>

                    <h2> {{$user->username}}</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div id="chat-body" class="chat-body top-body">
                            <ul>
                                <div class="row no-space">
                                    <div class="col-sm-7 col-md-7 col-lg-7">
                                        <li class="message" style="padding-bottom: 20px;">
                                        <span class="message-text top-message-text">
                                            <a href="javascript:void(0);" class="username top-username">{{Carbon\Carbon::now()->startOfMonth()->format('d-m-Y H:i:s')}} - Сегодня</a>
                                            <div class="invoice-sum-total">
                                                <h3><strong>Продана: <span class="text-success">{{Locale::number($user->calcMonthTotalSales(Carbon\Carbon::now()))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                <h3><strong>Прибыль: <span class="text-success">{{Locale::number($user->calcMonthTotalVenitSales(Carbon\Carbon::now()))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                 <h3><strong>Заказы: <span class="text-success">{{$user->calcMonthCountSales(Carbon\Carbon::now())}}</span></strong></h3>
                                            </div>
                                        </span>
                                        </li>
                                        <li class="message" style="padding-bottom: 20px;">
                                        <span class="message-text top-message-text">
                                            <a href="javascript:void(0);" class="username top-username">{{Carbon\Carbon::now()->subMonth()->startOfMonth()->format('d-m-Y H:i:s')}} - {{Carbon\Carbon::now()->subMonth()->endOfMonth()->format('d-m-Y H:i:s')}}</a>
                                            <div class="invoice-sum-total">
                                                <h3><strong>Продана: <span class="text-success">{{Locale::number($user->calcMonthTotalSales(Carbon\Carbon::now()->subMonth()))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                <h3><strong>Прибыль: <span class="text-success">{{Locale::number($user->calcMonthTotalVenitSales(Carbon\Carbon::now()->subMonth()))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                <h3><strong>Заказы: <span class="text-success">{{$user->calcMonthCountSales(calcMonthCountSales)}}</span></strong></h3>
                                            </div>
                                        </span>
                                        </li>
                                        <li class="message" style="padding-bottom: 20px;">
                                        <span class="message-text top-message-text">
                                            <a href="javascript:void(0);" class="username top-username">{{Carbon\Carbon::now()->subMonths(2)->startOfMonth()->format('d-m-Y H:i:s')}} - {{Carbon\Carbon::now()->subMonths(2)->endOfMonth()->format('d-m-Y H:i:s')}}</a>
                                            <div class="invoice-sum-total">
                                                <h3><strong>Продана: <span class="text-success">{{Locale::number($user->calcMonthTotalSales(Carbon\Carbon::now()->subMonths(2)))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                <h3><strong>Прибыль: <span class="text-success">{{Locale::number($user->calcMonthTotalVenitSales(Carbon\Carbon::now()->subMonths(2)))}} руб.</span></strong></h3>
                                            </div>
                                            <div class="invoice-sum-total top-sales">
                                                <h3><strong>Заказы: <span class="text-success">{{$user->calcMonthCountSales(Carbon\Carbon::now()->subMonths(2))}}</span></strong></h3>
                                            </div>
                                        </span>
                                        </li>
                                    </div>
                                </div>

                            </ul>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();

</script>