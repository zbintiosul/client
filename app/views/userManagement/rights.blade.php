<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa-fw fa fa-group"></i>
            Пользователи
			<span>>
				Права
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Права <span class="txt-color-blue"><i class="fa fa-user" data-rel="bootstrap-tooltip" title="Права"></i>&nbsp;</span></h5>
                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    1, 20, 50, 80, 100, 101, 150, 186, 200, 230, 280, 340, 360
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-user"></i> </span>
                    <h2> Права</h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        <input class="form-control" id="prepend" placeholder="Поиск" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Група</th>
                                <th>Права</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($rights as $rightg)
                            <tr id="tr_{{$rightg->id}}" idRight="{{$rightg->id}}">
                                <td class="custom-data-name">{{$rightg->name}}</td>
                                <td>
                                    <table>
                                        <tr>
                                            <th>Название</th>
                                            <th>Описание</th>
                                        </tr>
                                        @foreach($rightg->rights as $right)
                                            <tr>
                                            <td class="custom-data-name"><div>{{$right->name}}</div></td>
                                            <td class="custom-data-column"><div>{{$right->desc}}</div></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>
    <!-- end widget -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->


<script>
    pageSetUp();

</script>