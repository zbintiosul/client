<div class="row hidden-xs">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-user fa-fw "></i>
            Пользователи
			<span>>
				Пользователи
			</span>
            <span>>
				{{$user->username}}
			</span>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <ul id="sparks" class="">
            <li class="sparks-info">
                <h5> Общий доход <span class="txt-color-blue"><i class="fa fa-money" data-rel="bootstrap-tooltip"
                                                                 title="Общий доход"></i>&nbsp;{{Locale::number($user->calcTotalSalesVenit())}}</span></h5>

                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                    {{Locale::number($user->calcTotalSalesVenit())}}
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-group"></i> </span>
                    <h2>  {{$user->firstname}}  {{$user->lastname}}</h2>
                </header>

                <!-- widget div-->
                <div>



<div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                            @if (Authority::is_super_admin())
                            <form action="{{action('UserManagementController@postAddBonus')}}" id="add-bonus" class="smart-form" method="post">
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label mandatory">Текст</label>
                                                    <label class="input">
                                                            <input type="text" name="text" placeholder="Текст">
                                                     </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label mandatory">Бонус</label>
                                                    <label class="input">
                                                            <input type="text" name="bonus" placeholder="Бонус" value="0">
                                                     </label>
                                                </section>
                                            <div>
                                        </fieldset>
                                         <footer class="client_footer">
                                            <div class="btn-group btn-group-justified">
                                                <a href="javascript:void(0);" onclick="$('#add-bonus').submit()" class="btn btn-primary"><i class="fa fa-save"></i> Отправить</a>
                                            </div>
                                        </footer>
                             </form>
                            @endif
                        <div class="row">
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">

                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">
                                <div class="btn-group pull-right" style="margin: 18px 0;">
                                    <a class="btn btn-success" href="{{action('UserManagementController@getExportUserInfo')}}/{{$user->id}}" target="_blank">
                                        <i class="fa fa-plus"></i> <span class="hidden-mobile hidden-xs">Экспортировать</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <table id="custom-table" class="table table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Всего продано</th>
                                <th>Стаж агента</th>
                                <th>Продажи</th>
                                <th>Клиенты</th>
                                <th>Маржа</th>
                                <th>Средний результат</th>
                                <th>Комиссион</th>
                                <th>Бонусы</th>
                                <th>Всего к оплате</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $totalMonth = $user->calcTimeRegister(); ?>
                            <?php
                                for ($i=0; $i<$totalMonth; $i++) {
                                $nrProdaji = $user->calcMonthCountSales(Carbon\Carbon::now()->subMonths($i));
                                $nrProcededClients = $user->calcNrMonthProcededClients(Carbon\Carbon::now()->subMonths($i));
                                if ($nrProcededClients==0){
                                    $marja = 0;
                                } else
                                $marja = ($nrProdaji/$nrProcededClients) * 100;
                            ?>
                            <tr><td colspan="9" style="text-align:center;">{{Carbon\Carbon::now()->subMonths($i)->format("M Y")}} </td></tr>
                            <tr>
                                <td class="custom-data-name">{{Locale::number($user->calcMonthTotalSales(Carbon\Carbon::now()->subMonths($i)))}} руб</td>
                                <td class="custom-data-name">{{$totalMonth}} мес.</td>
                                <td class="custom-data-name">{{$nrProdaji}}</td>
                                <td class="custom-data-name">{{$nrProcededClients}}</td>
                                <td class="custom-data-name">{{Locale::number($marja)}}%</td>
                                <td class="custom-data-name">{{Locale::number($user->calcMediumResultMonthSales(Carbon\Carbon::now()->subMonths($i)))}} руб</td>
                                <td class="custom-data-name">{{Locale::number($user->calcMonthTotalVenitSales(Carbon\Carbon::now()->subMonths($i)))}} руб</td>
                                <td class="custom-data-name">{{Locale::number($user->calcMonthBonuses(Carbon\Carbon::now()->subMonths($i)))}} руб</td>
                                <td class="custom-data-name">{{Locale::number($user->calcMonthTotalToPay(Carbon\Carbon::now()->subMonths($i)))}} руб</td>
                                <td></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <!-- end widget edit box -->
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </article>
    <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->

<script>

    pageSetUp();

    // Load form valisation dependency
        loadScript("/js/plugin/jquery-form/jquery-form.min.js", runFormValidation);
        // Registration validation script
        function runFormValidation() {

            var $addpolicy = $('#add-bonus').validate({
                // Rules for form validation
                rules : {
                    text : {
                        required: true
                    },
                    bonus: {
                        required: true,
                        number: true
                    }

                },

                // Messages for form validation
                messages : {
                    text : {
                        required : 'Пожалуйста, выберите элемент'
                    },
                    bonus : {
                        required : 'Пожалуйста, выберите элемент',
                        number: 'Пожалуйста, проверьте ваш ввод'
                    }
                },

                // Do not change code below
                errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                },
                submitHandler: function(form) {
                    loading('show');
                    $(form).ajaxSubmit({
                        success : function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.success')}}",
                                content : "{{Lang::get('client.successText')}}",
                                color : "rgb(115, 158, 115)",
                                iconSmall : "fa fa-check bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                            loadURL(getCurentUrl(), $('#content'));
                        },
                        fail :  function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.error')}}",
                                content : "{{Lang::get('client.errorText')}}",
                                color : "#c26565",
                                iconSmall : "fa fa-times bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                        },
                        error :  function(msg) {
                            $.smallBox({
                                title : "{{Lang::get('client.error')}}",
                                content : "{{Lang::get('client.errorText')}}",
                                color : "#c26565",
                                iconSmall : "fa fa-times bounce animated",
                                timeout : 4000
                            });
                            loading('hide');
                        }
                    });
                }
            });
        }

</script>