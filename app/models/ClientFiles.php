<?php

class ClientFiles extends Eloquent {

	protected $table = 'client_files';
	public $timestamps = true;
	protected $softDelete = false;

	public function client()
	{
		return $this->belongsTo('Client', 'client_id');
	}

}