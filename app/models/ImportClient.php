<?php

class ImportClient extends Eloquent {

	protected $table = 'import_client';
	public $timestamps = false;
	protected $softDelete = false;

}