<?php

class PotentialClientType extends Eloquent {

	protected $table = 'potential_client_type';
	public $timestamps = false;

	public function clients()
	{
		return $this->hasMany('Client', 'potential_client_type_id');
	}

}