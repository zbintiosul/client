<?php

class History extends Eloquent {

	protected $table = 'history';
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}