<?php

class Client extends Eloquent {

	protected $table = 'clients';
	public $timestamps = true;
	protected $softDelete = true;

	public function status()
	{
		return $this->belongsTo('StatusClient', 'status_client_id');
	}

	public function policies()
	{
		return $this->hasMany('Policy', 'client_id');
	}

	public function users()
	{
		return $this->belongsToMany('User', 'user_client', 'client_id', 'user_id');
	}
    public function agent()
    {
        return $this->users()->first();
    }

    public function getTimeAgo()
    {
        $minutes_ago = Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at));
        return Carbon\Carbon::now()->subMinutes($minutes_ago)->diffForHumans();
    }

    public function getTimeAgoUpdate()
    {
        $minutes_ago = Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at));
        return Carbon\Carbon::now()->subMinutes($minutes_ago)->diffForHumans();
    }

    public function cars()
    {
        return $this->belongsToMany('Car', 'client_car','client_id','car_id');
    }

    public function notifications()
    {
        return $this->hasMany('Notification', 'client_id');
    }

    public function imports()
    {
        return $this->belongsToMany('Import', 'import_client', 'client_id', 'import_id')->withPivot('FIO')->withPivot('number')->withPivot('srok')->withPivot('brand_model')
                                                                                        ->withPivot('type')->withPivot('bank')->withPivot('sum')->withPivot('year')->withPivot('company')->withPivot('serial')
                                                                                        ->withPivot('serial_number')->withPivot('model');
    }

    public function getBirthDatePolicyAttribute($value)
    {
        return AppHelper::conInvDate($value);
    }

    public function setBirthDatedAttribute($value)
    {
        $this->attributes['birth_date'] = AppHelper::conDate($value);
    }

    public function getDateDriverLicenceAttribute($value)
    {
        return AppHelper::conInvDate($value);
    }

    public function setDateDriverLicenceAttribute($value)
    {
        $this->attributes['date_driver_licence'] = AppHelper::conDate($value);
    }

    public function setDateDriverLicencePolicyAttribute()
    {
        return AppHelper::conInvDate($this->date_driver_licence);
    }

    public function getDateDriverLicencePolicyAttribute()
    {
        $this->attributes['date_driver_licence'] = AppHelper::conDate($this->date_driver_licence);
    }

    public function files()
    {
        return $this->hasMany('ClientFiles', 'client_id');
    }


    public static  function nrNotInterestedClients()
    {
        return Client::where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','3')->count();
    }

    public static function saveImport($values)
    {
        $import = new Import();
        $import->user_id = Auth::user()->id;
        $import->log = 'Cтрок: '.count($values);
        $import->save();
        $clients  = array();
        $clients['new'] = 0;
        $clients['update'] = 0;
        foreach($values as $value)
        {
            if (Client::saveClient($value,$import->id))
                $clients['new'] =  $clients['new']+1;
            else
                $clients['update'] =  $clients['update']+1;
        }
        $import->log = $import->log.'<br> Новые: '.$clients['new'].' <br> Обновление: '.$clients['update'];
        $import->save();
        return $import->id;
    }

    public static function saveClient($value,$import_id)
    {
        if (Client::where('phone','LIKE', '%'.Str::lower(trim($value['2'].'%')))->count()>=1)
        {
            $client = Client::where('phone','LIKE','%'.Str::lower(trim($value['2'].'%')))->first();
            $return = false;
        }else
        {
            $client = new Client();
            $fio = explode(" ", trim($value['1']));
            if (count($fio)>=1)
            $client->lastname = $fio[0];
            if (count($fio)>=2)
            $client->firstname = $fio[1];
            if (count($fio)>=3)
            $client->secondname = $fio[2];
            $client->status_client_id = 0;
            $client->phone = $value['2'];
            $client->save();
            $return = true;
        }

        $client->imports()->attach($import_id,array('FIO'=>$value['1'], 'number'=>trim($value['2']), 'srok'=>$value['3'], 'type'=>$value['4'],
                                                    'bank'=>$value['5'], 'company'=>$value['6'], 'sum'=>$value['7'], 'brand_model'=>$value['8'],
                                                    'model'=>$value['9'], 'year'=>$value['10'], 'serial'=>$value['11'], 'serial_number'=>$value['12']));

        if (AppHelper::has_value($value['8']))
        {
            $nameCar = explode(" ", $value['8']);
            $brand = Null;
            $model = Null;
            if (count($nameCar)>=1)
                $brand = $nameCar[0];
            if (count($nameCar)>=2)
                $model = $nameCar[1];
            //$client->cars()->where('car.brand','=',$nameCar[0])->where('cars.model','=',$nameCar[1])->where('car.year_of_construction','=',$value['11'])->count();
            if ($client->cars()->where('cars.brand','=',$brand)->where('cars.model','=',$model)->where('cars.year_of_construction','=',$value['10'])->count()<=0)
            {
                $car = new Car();
                if (count($nameCar)>=1)
                    $car->brand = $brand;
                if (count($nameCar)>=2)
                    $car->model = $model;
                $car->year_of_construction = $value['10'];
                $car->model_car_id = 0;
                $car->region_id = 0;
                $car->type_consum_id = 0;
                $car->type_car_id = 0;
                $car->type_signaling_id = 0;
                $car->type_gearbox_id = 0;
                $car->type_doc_id = 0;
                $car->save();
                //$client->status_client_id = 0;
                $client->save();
                $client->cars()->attach($car->id);
            }
        }
        return $return;
    }

    public static function arrayToString($data)
    {
        if (is_array($data))
        {
            $data =  implode(" ", $data);
        }
        return $data;
    }
}