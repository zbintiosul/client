<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    public $timestamps = true;
    protected $softDelete = true;
    protected $guarded = array('password');
    private $path;


    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function __construct()
    {
        parent::__construct();
        $this->path = storage_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'users';
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    public function clients()
    {
        return $this->belongsToMany('Client', 'user_client', 'user_id', 'client_id');
    }

    public function salaries()
    {
        return $this->hasMany('SalaryAgent', 'user_id');
    }

    public function calcTimeRegister()
    {
        $dtRegister  = Carbon\Carbon::parse($this->created_at);
        $dtNow = Carbon\Carbon::now();
        return $dtRegister->diffInMonths($dtNow, false);
    }

    public function calcTotalSalesVenit()
    {
        $total = 0;
        $salaries = $this->salaries;
        foreach($salaries as $salary)
            $total += $salary->calcVenit();

        return $total;
    }

    public function calcTotalSales()
    {
        $total = 0;
        $salaries = $this->salaries;
        foreach($salaries as $salary)
            $total += $salary->policy->calcTotalSumInt();

        return $total;
    }

    public function calcMonthTotalSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>=',$dtMonth->startOfMonth()->toDateTimeString())->where('created_at','<=',$dtMonth->endOfMonth()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->policy->calcTotalSumInt();

        return $total;
    }


    public function calcNrMonthTotalSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $salaries = $this->salaries()->where('created_at','>=',$dtMonth->startOfMonth()->toDateTimeString())->where('created_at','<=',$dtMonth->endOfMonth()->toDateTimeString())->count();
        return $salaries;
    }

    public function calcNrTotalSales()
    {

        $salaries = $this->salaries()->count();
        return $salaries;
    }


    public function calcMediumResultMonthSales($datetime)
    {
        $total = 0;
        if($salaries = $this->salaries()->count()>0){
            $total = $this->calcMonthTotalSales($datetime) / $salaries = $this->salaries()->count();
        }

        return $total;
    }


    public function calcNrMonthProcededClients($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $salaries = $this->clients()->where('updated_at','>',$dtMonth->startOfMonth()->toDateTimeString())->where('updated_at','<',$dtMonth->endOfMonth()->toDateTimeString())->count();
        return $salaries;
    }

    public function calcMonthTotalVenitSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>',$dtMonth->startOfMonth()->toDateTimeString())->where('created_at','<',$dtMonth->endOfMonth()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->calcVenit();

        return $total;
    }

    public function calcMonthCountSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        return $this->salaries()->where('created_at','>',$dtMonth->startOfMonth()->toDateTimeString())->where('created_at','<',$dtMonth->endOfMonth()->toDateTimeString())->count();

    }

    public function calcMonthTotalToPay($datetime)
    {
        $bonuses = $this->calcMonthBonuses($datetime);
        if ($this->calcMonthBonuses($datetime)<0){
            $total = $this->calcMonthTotalVenitSales($datetime) + $this->calcMonthBonuses($datetime);
        } else {
            $total = $this->calcMonthTotalVenitSales($datetime) - $this->calcMonthBonuses($datetime);
        }
         return $total;
    }

    public function calcMonthBonuses($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->bonuses()->where('created_at','>',$dtMonth->startOfWeek()->toDateTimeString())->where('created_at','<',$dtMonth->endOfWeek()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->value;

        return $total;

    }

    public function calcWeekTotalSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>',$dtMonth->startOfWeek()->toDateTimeString())->where('created_at','<',$dtMonth->endOfWeek()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->policy->calcTotalSumInt();

        return $total;
    }

    public function calcWeekTotalVenitSales($datetime)
    {
        $dt  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>',$dt->startOfWeek()->toDateTimeString())->where('created_at','<',$dt->endOfWeek()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->calcVenit();

        return $total;
    }

    public function calcWeekCountSales($datetime)
    {
        $dtMonth  = Carbon\Carbon::parse($datetime);
        return $this->salaries()->where('created_at','>',$dtMonth->startOfWeek()->toDateTimeString())->where('created_at','<',$dtMonth->endOfWeek()->toDateTimeString())->count();

    }

    public function calcDayTotalVenitSales($datetime)
    {
        $dt  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>',$dt->startOfDay()->toDateTimeString())->where('created_at','<',$dt->endOfDay()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->calcVenit();

        return $total;
    }


    public function calcDayTotalSales($datetime)
    {
        $dt  = Carbon\Carbon::parse($datetime);
        $total = 0;
        $salaries = $this->salaries()->where('created_at','>',$dt->startOfDay()->toDateTimeString())->where('created_at','<',$dt->endOfDay()->toDateTimeString())->get();
        foreach($salaries as $salary)
            $total += $salary->policy->calcTotalSumInt();

        return $total;
    }

    public function calcDayCountSales($datetime)
    {
        $dt  = Carbon\Carbon::parse($datetime);
        return $this->salaries()->where('created_at','>',$dt->startOfDay()->toDateTimeString())->where('created_at','<',$dt->endOfDay()->toDateTimeString())->count();
    }


    public function notifications()
    {
        return $this->belongsToMany('Notification', 'user_notifications', 'user_id', 'notification_id')->withPivot('seen')->withPivot('sent');
    }

    public function nrNewNote()
    {
       return $this->notifications()->where('appear_time','<=',Carbon\Carbon::now())->wherePivot('seen','=','0')->count();
    }

    public function nrNewClients()
    {
        return $this->clients()->where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','1')->count();
    }

    public function nrInProgressClients()
    {
        return $this->clients()->where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','2')->count();
    }

    public function nrCallBack()
    {
        return $this->clients()->where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','6')->count();
    }


    public function nrCompletedClients()
    {
        return $this->clients()->where('created_at','<=',Carbon\Carbon::now())->where('status_client_id','=','5')->count();
    }

    public function nrCompletedPolicies()
    {
        if (Authority::is_super_admin())
            return SalaryAgent::count();
        else
        return Auth::user()->salaries()->count();
    }

    public function broker_policies()
    {
        return $this->belongsToMany('Policy', 'broker_policy', 'user_id', 'policy_id')->withPivot('approved');
    }

    public function getActiveRoles()
    {
        return $this::with(array('roles' => function($q)
            {
                $q->where('active', '=', '1');
            }))->get();
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }


    public function roles()
    {
        return $this->belongsToMany('Role','user_roles')->withPivot('created_at', 'updated_at');
    }

    public function hasImage()
    {
        if ($this->photo_path!=null)
            return 1;
        else
            return 0;
    }
    public function removeImage()
    {
        try{
            unlink($this->imagePath());
            $this->photo_path = null;
            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function deleteAllRelations()
    {
        $this->roles()->detach();
        $this->clients()->detach();
        $this->salaries()->detach();
        $this->removeImage();
        return true;
    }

    public function imagePath()
    {
        if ($this->hasImage())
            return $this->path.DIRECTORY_SEPARATOR.$this->photo_path;
        else
            return '';
    }

    public function addImage($image)
    {
        $imageName = 'img'.$this->id.'.png';
        $this->photo_path =  $imageName;
        if (!file_exists($this->path)) {
            mkdir($this->path);
        }
        Image::make($image->getRealPath())->grab(250, 250)->save($this->path.DIRECTORY_SEPARATOR.$imageName,70);
        return Image::make($this->imagePath());
    }

    public function bonuses()
    {
        return $this->hasMany('UserBonus');
    }

    public function sessions()
    {
        return $this->hasMany('Sessions');
    }

    public function statusOnline()
    {
        $last_session = $this->sessions()->orderBy('last_activity','desc');
        $status = array();
        if ($last_session->count()<=0)
        {
            $status['code'] = '0';
            $status['text'] = 'не в сети';
        }else
        {
            $last_session = $last_session->first();
            $minutes_ago = Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::createFromTimeStamp($last_session->last_activity));
            if  ($minutes_ago<=5)
            {
                $status['code'] = '1';
                $status['text'] = 'в сети';
            }
            else
            {
                if ($minutes_ago>5 && $minutes_ago<=59)
                {
                    $status['code'] = '2';
                    $status['text'] =  Carbon\Carbon::now()->subMinutes($minutes_ago)->diffForHumans();
                }
                else
                {
                    $status['code'] = '0';
                    $status['text'] = 'не в сети';
                }
            }
        }
        return (object)$status;
    }


    public static function getDefaultImage($width, $height,$id='')
    {
        return '<img data-src="holder.js/'.$width.'x'.$height.'" class="img-rounded"  id="'.$id.'" alt="'.$width.'x'.$height.'" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" style="width: '.$width.'px; height: '.$height.'px;">';
    }

    public static function getTopSalesMonth($datetime)
    {
        $return = array();
        $users = User::all();
        foreach($users as $user)
        {
            $sum = $user->calcMonthTotalSales($datetime);
            if ($sum!=0)
            $return[] = (object) array('total'=>$sum,'user'=>$user,'nr'=>$user->calcMonthCountSales($datetime));
        }
        arsort($return);
       return $return;
    }

    public static function getTopSalesWeek($datetime)
    {
        $return = array();
        $users = User::all();
        foreach($users as $user)
        {
            $sum = $user->calcWeekTotalSales($datetime);
            if ($sum!=0)
                $return[] = (object) array('total'=>$sum,'user'=>$user,'nr'=>$user->calcWeekCountSales($datetime));
        }
        arsort($return);
        return $return;
    }

    public static function getTopSalesDay($datetime)
    {
        $return = array();
        $users = User::all();
        foreach($users as $user)
        {
            $sum = $user->calcDayTotalSales($datetime);
            if ($sum!=0)
                $return[] = (object) array('total'=>$sum,'user'=>$user,'nr'=>$user->calcDayCountSales($datetime));
        }
        arsort($return);
        return $return;
    }

    public function scopeBroker($query)
    {
        return $query->whereHas('roles', function($q)
            {
                $q->whereHas('rights', function($q)
                {
                    $q->where('key','=','is_broker');
                });
            });
    }

    public function scopeNoneBroker($query)
    {
        return $query->whereHas('roles', function($q)
        {
            $q->where('roles.id','<>',"2");
        });
    }

    public function scopeActive($query)
    {
        return $query->where('active','=','1');
    }

    public static function noneBrokerUsers()
    {
        $return = array();
        $users = User::active()->get();
        foreach ($users as $user)
        {
            $add = 1;
            foreach($user->roles as $role)
            {
                foreach($role->rights as $right)
                {
                    if ($right->key=='is_broker')
                    {
                        $add = 0;
                        break;
                    }
                }
            }
            if ($add==1)
            {
                $return[] =  $user;
            }
        }
        return $return;
    }

    public function scopeAgent($query)
    {
        return $query->whereHas('roles', function($q)
        {
            $q->whereHas('rights', function($q)
            {
                $q->where('key','=','is_agent');
            });
        });
    }

    public function has_right($key)
    {
        foreach($this->roles as $role)
        {
            if ($role->active==1)
                foreach($role->rights as $right)
                {
                    if($right->key == $key)
                    {
                        return true;
                    }
                }
        }
        return false;
    }


    public function has_any_right($keys)
    {
        if(!is_array($keys))
        {
            $keys = func_get_args();
        }

        foreach($this->roles as $role)
        {
            foreach($role->rights as $right)
            {
                if(in_array($right->key, $keys))
                {
                    return true;
                }
            }
        }

        return false;
    }
}