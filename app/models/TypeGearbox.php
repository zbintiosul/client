<?php

class TypeGearbox extends Eloquent {

	protected $table = 'type_gearbox';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}