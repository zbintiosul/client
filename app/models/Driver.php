<?php

class Driver extends Eloquent {

	protected $table = 'drivers';
	public $timestamps = true;
	protected $softDelete = false;

	public function gender()
	{
		return $this->belongsTo('Gender', 'gender_id');
	}

	public function mariageStatus()
	{
		return $this->belongsTo('MariageStatus', 'marriage_status_id');
	}

}