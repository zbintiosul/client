<?php

class TypeSignaling extends Eloquent {

	protected $table = 'type_signaling';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}