<?php

class TypeDoc extends Eloquent {

	protected $table = 'type_doc';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

    public function cars()
    {
        return $this->hasMany('Car', 'type_doc_id');
    }
}