<?php

class Userroles extends Eloquent {

	protected $table = 'user_roles';
	public $timestamps = true;
	protected $softDelete = false;

}