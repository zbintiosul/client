<?php

class TypeCar extends Eloquent {

	protected $table = 'type_car';
	public $timestamps = false;
	protected $softDelete = false;

	public function cars()
	{
		return $this->hasMany('Car', 'type_car_id');
	}

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}