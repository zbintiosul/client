<?php

class Sessions extends Eloquent {

	protected $table = 'sessions';
	public $timestamps = false;
	protected $softDelete = false;

	public function user()
	{
		return $this->belongsTo('User');
	}

}