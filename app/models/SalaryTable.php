<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SalaryTable extends Eloquent {

	protected $table = 'salary_table';
	public $timestamps = true;
    protected $softDelete = true;

	public function agents()
	{
		return $this->hasMany('SalaryAgent', 'salary_table_id');
	}

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

    public static function base()
    {
        return SalaryTable::find(0);
    }

    public static function getLevel($id_policy)
    {
        $policy = Policy::find($id_policy);
        $user = User::find($policy->client->agent()->id);
        $userMonthVenit = $user->calcMonthTotalSales(Carbon\Carbon::now());
        $userMonthVenit +=$policy->calcTotalSumInt();
        $salaryLevel = SalaryTable::where('min','<=',$userMonthVenit)->where('max','>=',$userMonthVenit)->first();
        if ($salaryLevel==null){
            $salaryLevel = SalaryTable::where('max','<',$userMonthVenit)->orderBy("max",'desc')->first();
        }
        return $salaryLevel->id;
    }

}