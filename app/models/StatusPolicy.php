<?php

class StatusPolicy extends Eloquent {

	protected $table = 'status_policy';
	public $timestamps = false;
	protected $softDelete = false;

	public function policies()
	{
		return $this->hasMany('Policy', 'status_policy_id');
	}

}