<?php

class Rightsgroup extends Eloquent {

	protected $table = 'rights_group';
	public $timestamps = false;
	protected $softDelete = false;

	public function rights()
	{
		return $this->hasMany('Right', 'rights_group_id');
	}

}