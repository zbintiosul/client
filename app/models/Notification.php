<?php

class Notification extends Eloquent {

	protected $table = 'notifications';
	public $timestamps = true;
	protected $softDelete = false;

	public function type()
	{
		return $this->belongsTo('NotificationType', 'notification_type_id');
	}

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

	public function users()
	{
		return $this->belongsToMany('User', 'user_notifications', 'notification_id', 'user_id')->withPivot('seen')->withPivot('sent');
	}

    public function getTimeAgo()
    {
        $minutes_ago = Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->appear_time));
        return Carbon\Carbon::now()->subMinutes($minutes_ago)->diffForHumans();
    }

    public static function checkAndSendNotifications()
    {
        $noteToSent = Notification::where('appear_time','<=',Carbon\Carbon::now())->where('sent','=','0');
        if ($noteToSent->count()>=1)
        {
            $noteToSent = $noteToSent->get();
            $data = array();
            foreach($noteToSent as $note)
            {

                $noteBody = Notification::where('id','=',$note->id)->first(array('name','text','notification_type_id'));
                $data['text'] = $noteBody;
                $data['users'] = array();
                foreach($note->users as $user)
                {
                    $data['users'][] = $user->username;
                }
                //die(print_r($data['users']));
                Event::fire(NewNotificationsEventHandler::EVENT, array($data));
                $note->sent= 1;
                $note->save();
            }
        }
    }

    public function getIcon()
    {
        switch ($this->type->id) {
            case 1:
                return 'fa-warning';
            case 2:
                return 'fa-check';
            case 3:
                return 'fa-info';
            case 4:
                return 'fa-minus-circle';
        }
    }
}