<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SalaryAgent extends Eloquent {

	protected $table = 'salary_agents';
	public $timestamps = true;
    protected $softDelete = true;

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function policy()
	{
		return $this->belongsTo('Policy', 'policy_id');
	}

	public function salaryTable()
	{
		return $this->belongsTo('SalaryTable', 'salary_table_id');
	}

    public function calcVenit()
    {
        return $this->policy->calcTotalSumInt()*($this->salaryTable->percentage/100);
    }

    public function calcCompanyVenit()
    {
        return $this->policy->calcTotalSumInt()*($this->company_per/100);
    }

    public static function calcTotalCompanyPribili($salaries)
    {
        $total = count($salaries);
        foreach ($salaries as $salary)
        {
            $total += $salary->calcCompanyTotVenit();
        }

        return $total;
    }

    public static function calcTotalKaplate($salaries)
    {
        $total = 0;
        foreach ($salaries as $salary)
        {
            $total += $salary->calcVenit();
        }

        return $total;
    }

    public function calcCompanyTotVenit()
    {
        return $this->calcCompanyVenit()-$this->sum_diff;
    }

    public function calcNew()
    {
        //var_dump($this->policy->calcTotalSumInt() , $this->sum_diff);
        return $this->policy->calcTotalSumInt() - $this->sum_diff;
    }

    public static function calcTotalCompanyVenit($salaries)
    {
        $total = 0;
        foreach ($salaries as $salary)
        {
            $total += $salary->calcCompanyVenit();
        }

        return $total;
    }

    public static function calcTotalCompanyItog($salaries)
    {
        $total = 0;
        foreach ($salaries as $salary)
        {
            $total += $salary->calcCompanyVenit();
        }

        return $total;
    }


}