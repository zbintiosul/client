<?php

class Gender extends Eloquent {

	protected $table = 'genders';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}