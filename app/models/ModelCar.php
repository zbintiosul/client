<?php

class ModelCar extends Eloquent {

	protected $table = 'model_cars';
	public $timestamps = true;
	protected $softDelete = false;

	public function brand()
	{
		return $this->belongsTo('BrandCar', 'brand_car_id');
	}

}