<?php

class BrokerPolicy extends Eloquent {

	protected $table = 'broker_policy';
	public $timestamps = true;
	protected $softDelete = false;

}