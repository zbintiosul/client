<?php

class InsuranceRisk extends Eloquent {

	protected $table = 'insurance_risks';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}