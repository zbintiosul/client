<?php

class Right extends Eloquent {

	protected $table = 'rights';
	public $timestamps = true;
	protected $softDelete = false;

	public function roles()
	{
		return $this->belongsToMany('Role','role_rights','right_id','role_id');
	}

	public function group()
	{
		return $this->belongsTo('Rightsgroup', 'rights_group_id');
	}

}