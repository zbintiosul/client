<?php

class TypePolicy extends Eloquent {

	protected $table = 'type_policy';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}