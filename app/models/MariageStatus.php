<?php

class MariageStatus extends Eloquent {

	protected $table = 'mariage_status';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

    public function drivers()
    {
        return $this->hasMany('Driver', 'mariage_status_id');
    }
}