<?php

class Setting extends Eloquent {

	protected $table = 'settings';
	public $timestamps = true;
	protected $softDelete = false;

    public static function getSettingId($id)
    {
        return Setting::find($id);
    }

    public static function getSettingKey($key)
    {
        return Setting::where('key','=',$key)->first();
    }

    public static function getValKey($key)
    {
        $setting = Setting::where('key','=',$key)->first();
        return $setting->value;
    }

    public static function getValId($id)
    {
        $setting = Setting::where('id','=',$id)->first();
        return $setting->value;
    }

    public static function setValue($key,$value)
    {
        $set = Setting::where('key','=',$key)->first();
        $set->value = $value;
        return $set->save();
    }

}