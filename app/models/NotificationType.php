<?php

class NotificationType extends Eloquent {

	protected $table = 'notification_type';
	public $timestamps = true;
	protected $softDelete = false;

	public function notifications()
	{
		return $this->hasMany('Notification', 'notification_type_id');
	}

}