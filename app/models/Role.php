<?php

class Role extends Eloquent {

	protected $table = 'roles';
	public $timestamps = true;
	protected $softDelete = false;

	public function users()
	{
		return $this->belongsToMany('User');
	}

	public function rights()
	{
		return $this->belongsToMany('Right','role_rights','role_id','right_id');
	}

}