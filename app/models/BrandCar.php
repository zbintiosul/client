<?php

class BrandCar extends Eloquent {

	protected $table = 'brand_cars';
	public $timestamps = true;
	protected $softDelete = false;

	public function models()
	{
		return $this->hasMany('ModelCar', 'brand_car_id');
	}

}