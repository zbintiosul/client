<?php

class Region extends Eloquent {

	protected $table = 'regions';
	public $timestamps = false;
	protected $softDelete = false;

	public function cars()
	{
		return $this->hasMany('Car', 'region_id');
	}

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

}