<?php

class Policy extends Eloquent {

	protected $table = 'policies';
	public $timestamps = true;
	protected $softDelete = true;

	public function status()
	{
		return $this->belongsTo('StatusPolicy', 'status_policy_id');
	}

    public function getDateLastPolicyAttribute($value)
    {
        return AppHelper::conInvDate($value);
    }

    public function setDateLastPolicyAttribute($value)
    {
        $this->attributes['date_last_policy'] = AppHelper::conDate($value);
    }

	public function car()
	{
		return $this->belongsTo('Car', 'car_id');
	}

	public function client()
	{
		return $this->belongsTo('Client', 'client_id');
	}

    public function type()
    {
        return $this->belongsTo('TypePolicy', 'type_policy_id');
    }

    public function drivers()
	{
		return $this->hasMany('Driver','policy_id');
	}

    public function brokers()
    {
        return $this->belongsToMany('User', 'broker_policy', 'policy_id', 'user_id')->withPivot('approved');
    }

	public function typeSumInsured()
	{
		return $this->belongsTo('TypeSumInsured', 'type_sum_insured_id');
	}

	public function insuranceRisk()
	{
		return $this->belongsTo('InsuranceRisk', 'insurance_risk_id');
	}

    public function salaries()
    {
        return $this->hasMany('SalaryAgent', 'policy_id');
    }

    public function calcTotalSum()
    {
        return Locale::number($this->calcTotalSumInt());
    }

    public function calcTotalSumInt()
    {
        return $this->totalSum*$this->coefficient;
    }

    public function lastPolicyYear()
    {
        if (is_null($this->date_last_policy) || $this->date_last_policy=='0000-00-00')
            return '';

        $date  = Carbon\Carbon::createFromTimeStamp($this->date_last_policy);
        return $date->year;
    }

    public function lastPolicyDay()
    {
        if (is_null($this->date_last_policy) || $this->date_last_policy=='0000-00-00')
            return '';

        $date  = Carbon\Carbon::createFromTimeStamp($this->date_last_policy);
        return $date->day;
    }

    public function lastPolicyMonth()
    {
        if (is_null($this->date_last_policy) || $this->date_last_policy=='0000-00-00')
            return '';

        $date  = Carbon\Carbon::createFromTimeStamp($this->date_last_policy);
        return $date->month;
    }

}