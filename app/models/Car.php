<?php

class Car extends Eloquent {

	protected $table = 'cars';
	public $timestamps = true;
	protected $softDelete = true;

	public function policy()
	{
		return $this->belongsTo('Policy', 'policy_id');
	}

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

	public function typeConsum()
	{
		return $this->belongsTo('TypeConsum', 'type_consum_id');
	}

    public function clients()
    {
        return $this->belongsToMany('Clients', 'client_id');
    }

    public function region()
    {
        return $this->belongsTo('Region', 'region_id');
    }

    public function typeCar()
	{
		return $this->belongsTo('TypeCar', 'type_car_id');
	}

    public function modelCar()
    {
        return $this->belongsTo('ModelCar', 'model_car_id');
    }

    public function getDocWhenAttribute($value)
    {
        return AppHelper::conInvDate($value);
    }

    public function setDocWhenAttribute($value)
    {
        $this->attributes['doc_when'] = AppHelper::conDate($value);
    }

    public static function years($nr)
    {
        $return = array();
        $dt =  Carbon\Carbon::now();
        $curent = $dt->year;
        //$return[] = $curent;
        for($i=$curent;$i>=($curent-$nr);$i--)
        {
            $return[] = $i;
        }
        return $return;
    }

    public function typeGearbox()
    {
        return $this->belongsTo('TypeGearbox', 'type_gearbox_id');
    }

    public function typeSignaling()
    {
        return $this->belongsTo('TypeSignaling', 'type_signaling_id');
    }

    public function doc()
    {
        return $this->belongsTo('TypeDoc', 'type_doc_id');
    }

    public function brokers()
    {
        return $this->belongsToMany('User', 'broker_policy', 'policy_id', 'user_id');
    }

}