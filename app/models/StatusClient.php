<?php

class StatusClient extends Eloquent {

	protected $table = 'status_client';
	public $timestamps = false;
	protected $softDelete = false;

	public function clients()
	{
		return $this->hasMany('Client', 'status_client_id');
	}

}