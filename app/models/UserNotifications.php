<?php

class UserNotifications extends Eloquent {

	protected $table = 'user_notifications';
	public $timestamps = true;
	protected $softDelete = false;

}