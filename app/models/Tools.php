<?php

class Tools extends Eloquent {

	protected $table = 'tools';
	public $timestamps = true;
	protected $softDelete = false;

}