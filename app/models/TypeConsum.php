<?php

class TypeConsum extends Eloquent {

	protected $table = 'type_consum';
	public $timestamps = false;
	protected $softDelete = false;

	public function cars()
	{
		return $this->hasMany('Car', 'type_consum_id');
	}

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }

}