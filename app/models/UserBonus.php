<?php

class UserBonus extends Eloquent {

	protected $table = 'user_bonuses';
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

}