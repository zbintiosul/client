<?php

class Import extends Eloquent {

	protected $table = 'imports';
	public $timestamps = true;
	protected $softDelete = false;

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function clients()
	{
		return $this->belongsToMany('Client', 'import_client', 'import_id', 'client_id');
	}

}