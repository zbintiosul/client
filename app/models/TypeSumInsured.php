<?php

class TypeSumInsured extends Eloquent {

	protected $table = 'type_sum_insured';
	public $timestamps = false;
	protected $softDelete = false;

    public function scopeNonzero($query)
    {
        return $query->where('id', '<>', 0);
    }
}