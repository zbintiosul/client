<?php

class Offer extends Eloquent {

	protected $table = 'offers';
	public $timestamps = true;

	public function policy()
	{
		return $this->belongsTo('Policy', 'policy_id');
	}

	public function company()
	{
		return $this->belongsTo('Company', 'company_id');
	}

}