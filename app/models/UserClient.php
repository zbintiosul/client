<?php

class UserClient extends Eloquent {

	protected $table = 'user_client';
	public $timestamps = false;
	protected $softDelete = false;


    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}