<?php

class ClientCar extends Eloquent {

	protected $table = 'client_car';
	public $timestamps = false;
	protected $softDelete = false;

}