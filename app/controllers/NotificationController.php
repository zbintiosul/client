<?php

class NotificationController extends BaseController {

    protected $layout = 'layouts.master';

    public function getIndex()
    {
        $notifications = Auth::user()->notifications()->where('appear_time','<=',Carbon\Carbon::now())->orderBy('appear_time','desc')->paginate(25);
        return View::make('notifications.index')->with('notifications',$notifications);
    }

    public function getAjax()
    {
        return View::make('notifications.ajax');
    }

    public function getNote($id)
    {
        return View::make('notifications.note');
    }

    public function postAddNotification()
    {
        //access rights to method
        if (!Authority::can('add_notice'))
            return Redirect::to(Config::get('app.restricted_url'));

        $note = new Notification();
        $note->name = Input::get('name');
        $note->notification_type_id = Input::get('note_type');
        $note->text = Input::get('text');
        if (Input::has('timeNote')){
            if  (Input::has('noteDate'))
            $date = Input::get('noteDate');
            else
                $date = Carbon\Carbon::now()->toDateString();
            if  (Input::has('noteTime'))
                $time = Input::get('noteTime').':00';
            else
                $time = Carbon\Carbon::now()->toTimeString();
            $note->appear_time = $date.' '.$time;
        }else
            $note->appear_time = Carbon\Carbon::now();
        $note->user_id = Auth::user()->id;
        $note->save();

        $users = User::noneBrokerUsers();
       //die(print_r($users));
        if (Input::has('users')){
            $users = Input::get('users');
            if (is_array($users)){
                foreach($users as $user_id)
                {
                    $user = User::find($user_id);
                    $note->users()->attach($user->id);
                }
            }
            else{
                $user = User::find($users);
                $note->users()->attach($user->id);
            }
        }else{
            foreach($users as $user)
            {
                $note->users()->attach($user->id);
            }
        }
        $note->save();
        Notification::checkAndSendNotifications();
    }

    public function postAddAgentNotification()
    {
        $note = new Notification();
        $note->name = Input::get('name');
        $note->notification_type_id = Input::get('note_type');
        $note->text = Input::get('text');

        $date = AppHelper::conDate(Input::get('noteDate'));
        $time = Input::get('noteTime').':00';
        $note->appear_time = $date.' '.$time;
        $note->client_id = Input::get('client_id');
        $note->user_id = Auth::user()->id;
        $note->save();

        $user = User::find(Auth::user()->id);
        $note->users()->attach($user->id);
        $note->save();
        Notification::checkAndSendNotifications();
    }



    public function postDeleteClientNote($id)
    {
        $note= Notification::find($id);
        $note->delete();
    }

    public function getModalViewNote($id)
    {
        $note = Auth::user()->notifications()->where('notifications.id','=',$id)->first();
        $note->pivot->seen = 1;
        $note->pivot->save();
        return View::make('notifications.viewModal')->with('note', $note);
    }

    public function getReadNote($id)
    {
        $note = Auth::user()->notifications()->where('notifications.id','=',$id)->first();
        $note->pivot->seen = 1;
        $note->pivot->save();
        return 'true';
    }

    public function getNewNote()
    {
        $users = User::all();
        return View::make('notifications.blocks.addNotification')->with('users',$users);
    }
    public function getJsonGetNote()
    {
        return Auth::user()->notifications()->get(array('name'));
    }

}