<?php

class TopController extends BaseController {

    protected $layout = 'layouts.master';

    public function getIndex()
    {
        $month = User::getTopSalesMonth(Carbon\Carbon::now());
        $week = User::getTopSalesWeek(Carbon\Carbon::now());
        $day = User::getTopSalesDay(Carbon\Carbon::now());
        return View::make('top.index')->with('month',$month)->with('day',$day)->with('week',$week)->with('page_body_prop', array("id"=>"login", "class"=>"animated fadeInDown"));
    }
}