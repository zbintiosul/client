<?php

class ClientController extends BaseController {

    protected $layout = 'layouts.master';

    public function getIndex()
    {
        //access rights to method
        if (!Authority::can('view_all_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Client::with(array('users' => function($query)
            {
                $query->withTrashed();

            }))->orderBy('updated_at','desc');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
        }

        if (Input::has('status'))
        {
            $clients = $clients->where('status_client_id','=',Input::get('status'));
        }

        if (Input::has('user'))
        {
            $id_user = Input::get('user');
            $clients =  $clients->whereHas('users', function($q) use ($id_user)
            {
                    $q->where('user_id','=',$id_user);
            });
        }

            if (Input::has('type_pot'))
            {
                $id = Input::get('type_pot');
                $clients =  $clients->where('potential_client_type_id','=',$id);
            }
        if (Input::has('importid'))
        {
            $id_import = Input::get('importid');
            $clients =  $clients->whereHas('imports', function($q) use ($id_import)
            {
                $q->where('imports.id','=',$id_import);
            });
        }

        if (Input::has('import'))
        {
          $import = Import::find(Input::get('import'));
          Session::flash('import',$import);
        }

        $usedUsers = UserClient::distinct()->get(array('user_id'));
        $clients = $clients->paginate(25);

        return View::make('clients.index')->with('clients',$clients)->with('usedUsers',$usedUsers);
    }

    public function getNewClients()
    {
        //access rights to method
        if (!Authority::can('view_new_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Auth::user()->clients()->orderBy('created_at','desc')->where('status_client_id','=','1');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }


        $clients = $clients->paginate(25);
        return View::make('clients.newClients')->with('clients',$clients);
    }

    public function getInProgressClients()
    {
        //access rights to method
        if (!Authority::can('view_in_process_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Auth::user()->clients()->orderBy('updated_at','desc')->where('status_client_id','=','2');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }

        $clients = $clients->paginate(25);
        return View::make('clients.inProgressClients')->with('clients',$clients);
    }

    public function getCallBack()
    {
        //access rights to method
        if (!Authority::can('view_in_process_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Auth::user()->clients()->orderBy('updated_at','desc')->where('status_client_id','=','6');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }

        $clients = $clients->paginate(25);
        return View::make('clients.inProgressClients')->with('clients',$clients);
    }

    public function postCallBackClient()
    {
        $client= Client::find(Input::get('client'));
        $client->status_client_id = 6;
        $client->save();
    }

    public function postMakeInProgress()
    {
        $client= Client::find(Input::get('client'));
        $client->status_client_id = 2;
        $client->save();
    }

    public function getNotInterestedClients()
    {
        //access rights to method
        if (!Authority::can('view_not_interested_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Client::orderBy('updated_at','desc')->where('status_client_id','=','3');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }

        $clients = $clients->paginate(25);
        return View::make('clients.notInterestedClients')->with('clients',$clients);
    }

    public function getCompletedClients()
    {
        //die(print_r(User::noneBroker()->get(array('username'))));
        //access rights to method
        if (!Authority::can('view_completed_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Auth::user()->clients()->orderBy('updated_at','desc')->where('status_client_id','=','5');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }
        $clients = $clients->paginate(25);
        return View::make('clients.completedClients')->with('clients',$clients);
    }

    public function getIncome()
    {
        //access rights to method
        if (!Authority::can('policies_income'))
            return Redirect::to(Config::get('app.restricted_url'));

            $usedUsers = SalaryAgent::with(array('user' => function($query)
                {
                    $query->withTrashed();

                }))->distinct()->get(array('user_id'));
            $salaries = SalaryAgent::with(array('user' => function($query)
                {
                    $query->withTrashed();

                }))->orderBy('created_at','desc');
            if (Input::has('user'))
            {
                $search = Input::get('user');
                $salaries = $salaries->where('user_id','=',$search);
            }

            if (Input::has('date'))
            {
                $search = Input::get('date');
                $date = Carbon\Carbon::createFromFormat("Y-m",$search);
                $start = $date->startOfMonth()->format('Y-m-d H:i:s');
                $end = $date->endOfMonth()->format('Y-m-d H:i:s');
                $salaries = $salaries->where('created_at','>=',$start)->where('created_at','<=',$end);
            }

            $itog = SalaryAgent::calcTotalCompanyItog($salaries->get());
            $pribali = SalaryAgent::calcTotalCompanyPribili($salaries->get());
            $salaries = $salaries->paginate(25);
            //$policies = Policy::orderBy('updated_at','desc')->where('status_policy_id','=','5')->where('id','=',0)->paginate(25);
            return View::make('clients.inCome')->with('salaries',$salaries)->with('usedUsers',$usedUsers)->with('itog',$itog)->with('pribali',$pribali);
    }

    public function getExportIncome()
    {
        //access rights to method
        if (!Authority::can('policies_income'))
            return Redirect::to(Config::get('app.restricted_url'));

        $dateTime = Carbon\Carbon::now()->format('Y-m-d-H-i-s');
        $fileName = "inCome-".$dateTime;
        $salaries = SalaryAgent::with(array('user' => function($query)
            {
                $query->withTrashed();

            }))->orderBy('created_at','desc');

        $data = array();
        if (Input::has('user'))
        {
            $search = Input::get('user');
            $data[] = array('Пользователь:',$search,'','','','','','','');
            $salaries = $salaries->where('user_id','=',$search);
        }

        if (Input::has('date'))
        {
            $search = Input::get('date');
            $data[] = array('Месяц продаж:',$search,'','','','','','','');
            $date = Carbon\Carbon::createFromFormat("Y-m",$search);
            $start = $date->startOfMonth()->format('Y-m-d H:i:s');
            $end = $date->endOfMonth()->format('Y-m-d H:i:s');
            $salaries = $salaries->where('created_at','>=',$start)->where('created_at','<=',$end);
        }


        $salaries = $salaries->get();
        $itog = number_format(SalaryAgent::calcTotalCompanyItog($salaries), 2, '.', '');
        $pribali = number_format(SalaryAgent::calcTotalCompanyPribili($salaries), 2, '.', '');

        $data[] = array('№','ФИО клиента','Марка Модели','Страховая премия','Риск','Комиссион','Скидка','Комиссион с учетом скидки','Дата продажи');
        foreach($salaries as $salary)
        {
            if (is_object($salary->user)){
                if ($salary->user->trashed()){
                    $userName = $salary->user->lastname.' '.$salary->user->firstname.'(уд.)';
                }else {
                    $userName = $salary->user->lastname.' '.$salary->user->firstname;
                }
            }else {
                $userName = 'Физически удален';
            }
            $clientName = '';
            if (!empty($salary->policy->client->id))
            {
                if (empty($salary->policy->client->lastname) && empty($salary->policy->client->firstname))
                    $clientName = $salary->policy->client->id;
                else
                    $clientName = $salary->policy->client->lastname.' '.$salary->policy->client->firstname;
            }
            $calcTotalSum = 0;
            $x = (float)$salary->policy->calcTotalSum();
            if (is_numeric($x)){
                $calcTotalSum = $x;
            }
            $data[] = array(
                $salary->id,
                $clientName,
                $salary->policy->car->modelCar->brand->name.' '.$salary->policy->car->modelCar->name,
                $salary->policy->calcTotalSum(),
                $salary->policy->type->name,
                $salary->company_per,
                $salary->sum_diff,
                number_format($salary->calcCompanyTotVenit(), 2, '.', ''),
                $salary->created_at->format('d-m-Y H:i:s'));
        }

        $data[] = array('Всего:','','','','','','',$pribali,'','');

        Excel::create($fileName, function($excel) use ($dateTime,$data){

            // Set the title
            $excel->setTitle('Доход '.$dateTime);

            // Chain the setters
            $excel->setCreator('Proassist.ru')
                ->setCompany('Proassist.ru');

            // Call them separately
            $excel->setDescription('Доход '.$dateTime);

            $excel->sheet($dateTime, function($sheet) use($data) {

                $sheet->setColumnFormat(array(
                    'D' => '0.00',
                    'F' => '0.00',
                    'G' => '0.00',
                    'H' => '0.00',
                ));

                $sheet->fromArray($data);

            });

        })->export('xls');
    }

    public function postSaveCompanyPer($id)
    {
        if (is_numeric(Input::get('value')))
        {
            $salary = SalaryAgent::find($id);
            $salary->company_per =(double)Input::get('value');
            $salary->save();
        }
    }

    public function postSaveSumDiff($id)
    {
        if (is_numeric(Input::get('value')))
        {
            $salary = SalaryAgent::find($id);
            $salary->sum_diff =(double)Input::get('value');
            $salary->save();
        }
    }

    public function getCompletedPolicies()
    {
        //access rights to method
        if (!Authority::can('view_completed_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        if (Authority::canOnly('is_agent'))
        {
            //$usedUsers = SalaryAgent::distinct()->get('users.username');
            $salaries = Auth::user()->salaries()->orderBy('created_at','desc');
            $kaplate = SalaryAgent::calcTotalKaplate($salaries->get());
            $salaries = $salaries->paginate(25);
            $usedUsers = SalaryAgent::where('user_id','=',Auth::user()->id)->distinct()->get(array('user_id'));
            //$policies = Policy::orderBy('updated_at','desc')->where('status_policy_id','=','5')->where('id','=',0)->paginate(25);

            return View::make('clients.completedPolicies')->with('salaries',$salaries)->with('usedUsers',$usedUsers)->with('kaplate',$kaplate);
        }
        elseif (Authority::is_super_admin())
        {
            $usedUsers = SalaryAgent::with(array('user' => function($query)
                {
                    $query->withTrashed();

                }))->distinct()->get(array('user_id'));
            $salaries = SalaryAgent::with(array('user' => function($query)
                {
                    $query->withTrashed();

                }))->orderBy('created_at','desc');

            if (Input::has('user'))
            {
                $search = Input::get('user');
                $salaries = $salaries->where('user_id','=',$search);
            }

            if (Input::has('date'))
            {
                $search = Input::get('date');
                $date = Carbon\Carbon::createFromFormat("Y-m",$search);
                $start = $date->startOfMonth()->format('Y-m-d H:i:s');
                $end = $date->endOfMonth()->format('Y-m-d H:i:s');
                $salaries = $salaries->where('created_at','>=',$start)->where('created_at','<=',$end);
            }
            $sal = $salaries;
            $kaplate = SalaryAgent::calcTotalKaplate($sal->get());
            $salaries = $salaries->paginate(25);
            //$policies = Policy::orderBy('updated_at','desc')->where('status_policy_id','=','5')->where('id','=',0)->paginate(25);
            return View::make('clients.completedPolicies')->with('salaries',$salaries)->with('usedUsers',$usedUsers)->with('kaplate',$kaplate);
        }
    }

    public function getExportCompletedPolicies()
    {
        //access rights to method
        if (!Authority::can('view_completed_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $dateTime = Carbon\Carbon::now()->format('Y-m-d-H-i-s');
        $fileName = "completed-policies-".$dateTime;
        $salaries = SalaryAgent::with(array('user' => function($query)
        {
            $query->withTrashed();

        }))->orderBy('created_at','desc');

        $data = array();
        if (Input::has('user'))
        {
            $search = Input::get('user');
            $data[] = array('Пользователь:',$search,'','','','','','','');
            $salaries = $salaries->where('user_id','=',$search);
        }

        if (Input::has('date'))
        {
            $search = Input::get('date');
            $data[] = array('Месяц продаж:',$search,'','','','','','','');
            $date = Carbon\Carbon::createFromFormat("Y-m",$search);
            $start = $date->startOfMonth()->format('Y-m-d H:i:s');
            $end = $date->endOfMonth()->format('Y-m-d H:i:s');
            $salaries = $salaries->where('created_at','>=',$start)->where('created_at','<=',$end);
        }


        $salaries = $salaries->get();

        $pribali = number_format(SalaryAgent::calcTotalKaplate($salaries), 2, '.', '');

        $data[] = array('№','Риск', 'ФИО клиента','Страховая премия','Скидка', 'Страховая премия со скидкой', 'Комиссион', 'Комиссион к оплате','Пользователь', 'Фактура ID','Дата продажи');
        foreach($salaries as $salary)
        {
            if (is_object($salary->user)){
                if ($salary->user->trashed()){
                    $userName = $salary->user->lastname.' '.$salary->user->firstname.'(уд.)';
                }else {
                    $userName = $salary->user->lastname.' '.$salary->user->firstname;
                }
            }else {
                $userName = 'Физически удален';
            }
            $clientName = '';
            if (!empty($salary->policy->client->id))
            {
                if (empty($salary->policy->client->lastname) && empty($salary->policy->client->firstname))
                    $clientName = $salary->policy->client->id;
                else
                    $clientName = $salary->policy->client->lastname.' '.$salary->policy->client->firstname;
            }


            $data[] = array(
                $salary->id,
                $salary->policy->type->name,
                $clientName,
                $salary->policy->calcTotalSum(),
                $salary->sum_diff,
                number_format($salary->calcNew(), 2, '.', ''),
                $salary->salaryTable->percentage,
                number_format($salary->calcVenit(), 2, '.', ''),
                $userName,
                $salary->policy->id,
                $salary->created_at->format('d-m-Y H:i:s'));
        }

        $data[] = array('Всего:','','','','','','',$pribali,'','','');

        Excel::create($fileName, function($excel) use ($dateTime,$data){

            // Set the title
            $excel->setTitle('Completed '.$dateTime);

            // Chain the setters
            $excel->setCreator('Proassist.ru')
                ->setCompany('Proassist.ru');

            // Call them separately
            $excel->setDescription('Доход '.$dateTime);

            $excel->sheet($dateTime, function($sheet) use($data) {

                $sheet->setColumnFormat(array(
                    'D' => '0.00',
                    'E' => '0.00',
                    'F' => '0.00',
                    'G' => '0.00',
                    'H' => '0.00',
                ));
                $sheet->fromArray($data);

            });

        })->export('xls');
    }

    public function getModalAddClient()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $users = User::All();
        return View::make('clients.addClient')->with('users', $users);
    }

    public function getModalSendEmailToClient($id)
    {
        $policy = Policy::find($id);
        return View::make('clients.modalSendToEmail')->with('policy', $policy);
    }

    public function getModalImportClients()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        return View::make('clients.importClients');
    }

    public function getModalSelectBroker($id)
    {
        //access rights to method
        if (!Authority::can('approve_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find($id);
        $users = User::broker()->get();
        return View::make('clients.selectBroker')->with('users', $users)->with('policy',$policy);
    }

    public function getModalDistributeClient()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $users = User::agent()->get();
        return View::make('clients.distributeClients')->with('users', $users);
    }

    public function getModalDistributeSelectedClient()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $users = User::agent()->get();
        return View::make('clients.distributeSelectedClients')->with('users', $users);
    }

    public function getModalAddClientFile($id)
    {
        //access rights to method
        if (!Authority::can('add_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find($id);
        return View::make('clients.addClientFile')->with('client',$client);
    }

    public function getModalEditClientFile($id)
    {
        //access rights to method
        if (!Authority::can('edit_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));

        $file = ClientFiles::find($id);
        return View::make('clients.editClientFile')->with('file',$file);
    }

    public function postSendEmailToClient()
    {
        $policy = Policy::find(Input::get('policy_id'));
        $name = 'ProAssist';
        $emailTo = Input::get('email');
        $emailFrom = Auth::user()->email;
        $subject = 'Коммерческое Предложения';
        $token = Crypt::encrypt($policy->id);
        $data = array('policy'=>$name,'policy'=>$policy, 'subject'=>$subject, 'token'=>$token);
        Mail::send('emails.clients.policy', $data, function($message)use($name,$emailTo,$subject,$emailFrom)
        {
            //$message->from($emailFrom,$name);
            $message->to($emailTo)->subject($subject);
        });
    }

    public function postEditClientFile()
    {
        //access rights to method
        if (!Authority::can('edit_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));

        $file = ClientFiles::find(Input::get('file_id'));
        $file->desc = Input::get('desc');
        $file->save();
    }

    public function getCarModels()
    {
        $models1 = ModelCar::where('id','=',0)->get()->toArray();
        $models2 = ModelCar::where('brand_car_id','=',Input::get('brand'))->orderBy('name','asc')->get()->toArray();
        $models = array_merge($models1,$models2);
        return $models;
    }

    public function postEditCarClient()
    {
        //access rights to method
        if (!Authority::can('edit_client_car'))
            return Redirect::to(Config::get('app.restricted_url'));

        $car = Car::find(Input::get('car'));
        //die(print_r($car));
        $client = Client::find(Input::get('client'));
        //$client->status_client_id = 2;
        //$client->save();
        $regions = Region::nonzero()->get();
        $consums = TypeConsum::nonzero()->get();
        $types = TypeCar::nonzero()->get();
        $signalings = TypeSignaling::nonzero()->get();
        $gearboxs = TypeGearbox::nonzero()->get();
        $docs = TypeDoc::nonzero()->get();
        $typesPolicy = TypePolicy::nonzero()->get();
        $models = ModelCar::where('brand_car_id','=',$car->modelCar->brand->id)->get();
        //$cars = $client->cars()->get();
        //$risks = InsuranceRisk::nonzero()->get();
        //$tapeSums = TypeSumInsured::nonzero()->get();
        $brands = BrandCar::orderBy('name','asc')->get();
        return View::make('clients.car.editCar')->with('client',$client)->with('regions',$regions)
            ->with('consums',$consums)->with('types',$types)->with('signalings',$signalings)
            ->with('gearboxs',$gearboxs)->with('docs',$docs)->with('typesPolicy',$typesPolicy)->with('brands',$brands)->with('models',$models)->with('car',$car);
    }

    public function postEditCarClientDo()
    {
        //access rights to method
        if (!Authority::can('edit_client_car'))
            return Redirect::to(Config::get('app.restricted_url'));

        $car = Car::find(Input::get('car_id'));
        $car->model_car_id = Input::get('model_car_id');
        //$car->brand = Input::get('brand');
        //$car->model = Input::get('model');
        $car->year_of_construction = Input::get('year_of_construction');
        $car->motor_volum = Input::get('motor_volum');
        $car->motor_ls = Input::get('motor_ls');
        $car->register_number = Input::get('register_number');
        $car->number = Input::get('number');
        $car->VIN = Input::get('VIN');
        $car->start_explotation = Input::get('start_explotation');
        if (Input::has('region_id'))
            $car->region_id = Input::get('region_id');
        else
            $car->region_id = 0;

        $car->frame_number = Input::get('frame_number');
        $car->pure_mass = Input::get('pure_mass');

        if (Input::get('stand_number')!=1)
        {
            if (Input::has('stand_number_value'))
                $car->stand_number = Input::get('stand_number_value');
        }else
            $car->stand_number = 1;

        $car->allow_max_mass = Input::get('allow_max_mass');
        $car->type_consum_id = Input::get('type_consum_id');
        $car->type_car_id = Input::get('type_car_id');
        $car->mileage = Input::get('mileage');
        $car->type_signaling_id = Input::get('type_signaling_id');
        $car->type_gearbox_id = Input::get('type_gearbox_id');
        if (Input::has('type_doc_id'))
            $car->type_doc_id = Input::get('type_doc_id');
        else
            $car->type_doc_id = 0;
        $car->category = Input::get('category');

        $car->doc_serial = Input::get('doc_serial');
        $car->doc_number = Input::get('doc_number');

        $car->doc_who = Input::get('doc_who');

        $car->setDocWhenAttribute(Input::get('doc_when'));
        //$car->doc_when = Input::get('doc_when');
        $car->save();
    }

    public function postEditPolicyClient()
    {
        //access rights to method
        if (!Authority::can('edit_client_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy'));
        //die(print_r($car));
        $client = Client::find(Input::get('client'));
       // $client->status_client_id = 2;
        //$client->save();
        //$regions = Region::nonzero()->get();
        //$consums = TypeConsum::nonzero()->get();
        $types = TypeCar::nonzero()->get();
        //$signalings = TypeSignaling::nonzero()->get();
        //$gearboxs = TypeGearbox::nonzero()->get();
        $docs = TypeDoc::nonzero()->get();
        $typesPolicy = TypePolicy::nonzero()->get();
        //$models = ModelCar::where('brand_car_id','=',$car->modelCar->brand->id)->get();
        $cars = $client->cars()->get();
        $risks = InsuranceRisk::nonzero()->get();
        $tapeSums = TypeSumInsured::nonzero()->get();
        $genders = Gender::nonzero()->get();
        $mStatuses = MariageStatus::nonzero()->get();
        return View::make('clients.policy.editPolicy')->with('client',$client)->with('cars',$cars)
            ->with('risks',$risks)->with('types',$types)->with('tapeSums',$tapeSums)
            ->with('docs',$docs)->with('typesPolicy',$typesPolicy)->with('policy',$policy)->with('genders',$genders)->with('mStatuses',$mStatuses);
    }

    public function postEditClientPolicyDo()
    {
        //access rights to method
        if (!Authority::can('edit_client_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy_id'));
        //$policy->client_id =  Input::get('client_id');
        //$policy->status_policy_id = 1;
        $policy->type_policy_id = Input::get('type_policy_id');
        $policy->setDateLastPolicyAttribute(Input::get('date_last_policy'));
        $policy->last_policy_company = Input::get('last_policy_company');
        $policy->firstname_manager = Input::get('firstname_manager');
        $policy->lastname_manager = Input::get('lastname_manager');
        $policy->secondname_manager = Input::get('secondname_manager');
        $policy->name = Input::get('name');
        $policy->TO = Input::get('TO');
        $policy->kbm = Input::get('kbm');
        $policy->period_policy = Input::get('period_policy');

        if (Input::has('was_kasko'))
        $policy->was_kasko = Input::get('was_kasko');

        $policy->nr_casco = Input::get('nr_casco');
        $policy->sum_now_car = Input::get('sum_now_car');
        $policy->sum_market_car = Input::get('sum_market_car');
        $policy->used_region = Input::get('used_region');
        if (Input::has('insurance_risk_id'))
            $policy->insurance_risk_id = Input::get('insurance_risk_id');

        $policy->insurance_company = Input::get('insurance_company');

        if (Input::has('in_credit'))
            $policy->in_credit = Input::get('in_credit');
        else
            $policy->in_credit = 0;

        $policy->bank_credit = Input::get('bank_credit');

        if (Input::has('type_sum_insured_id'))
            $policy->type_sum_insured_id = Input::get('type_sum_insured_id');

        if (Input::get('type_policy_id')==3){
            $policy->car_id = 0;
        }else
            $policy->car_id = Input::get('car_id');

        $policy->totalSum = Input::get('totalSum');
        $policy->coefficient = Input::get('coefficient');
        $policy->save();

        $policy->number_drivers = Input::get('number_drivers');
        $policy->save();
        //$policy->drivers()->delete();



        foreach($policy->drivers as $driver)
        {

            if (!Input::has('firstname_driver_edit_'.$driver->id) && !Input::has('lastname_driver_edit_'.$driver->id) && !Input::has('secondname_driver_edit_'.$driver->id))
            {
                $driver->delete();
            }else
            {
                $driver->firstname = Input::get('firstname_driver_edit_'.$driver->id);
                $driver->lastname = Input::get('lastname_driver_edit_'.$driver->id);
                $driver->secondname = Input::get('secondname_driver_edit_'.$driver->id);
                $driver->gender_id = Input::get('gender_id_driver_edit_'.$driver->id);
                $driver->age = Input::get('age_driver_edit_'.$driver->id);
                $driver->experience = Input::get('experience_driver_edit_'.$driver->id);
                $driver->marriage_status_id = Input::get('marriage_status_driver_id_edit_'.$driver->id);
                $driver->save();
            }
        }

        for($i=1;$i<=Input::get('real_nr_drivers');$i++)
        {
            if (Input::has('firstname_driver_editNew_'.$i) || Input::has('lastname_driver_editNew_'.$i) || Input::has('secondname_driver_editNew_'.$i))
            {
                $driver = new Driver();
                $driver->firstname = Input::get('firstname_driver_editNew_'.$i);
                $driver->lastname = Input::get('lastname_driver_editNew_'.$i);
                $driver->secondname = Input::get('secondname_driver_editNew_'.$i);
                $driver->gender_id = Input::get('gender_id_driver_editNew_'.$i);
                $driver->age = Input::get('age_driver_editNew_'.$i);
                $driver->experience = Input::get('experience_driver_editNew_'.$i);
                $driver->marriage_status_id = Input::get('marriage_status_driver_id_editNew_'.$i);
                $driver->policy_id = $policy->id;
                $driver->save();
            }
        }
    }

    public function postDeleteDriver($id)
    {
        if (!Authority::can('edit_client_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $driver = Driver::find($id);
        $driver->delete();
    }

    public function postDistributeClientsSelected()
    {
        $client = Input::get('client_check');
        if (Input::has('users'))
        {
            $toSend = array();
            foreach($client as $cl)
            {
                $clientDB = Client::find($cl);
                if ($clientDB->status_client_id==0)
                    $toSend[] = $clientDB->id;
                else
                if ($clientDB->status_client_id==1 || $clientDB->status_client_id==2)
                {
                    $toSend[] = $clientDB->id;
                    //foreach($clientDB->users as $us)
                    $clientDB->users()->detach();
                    $clientDB->status_client_id=0;
                }
            }
            $data['text'] = '';
            $data['users'] = array();
            $users = Input::get('users');
            $nrClients = count($toSend);
            //$clientsPerUser = (int)$nrClients/count($users);
            //die(print_r(is_array($users)));
            foreach($users as $user_id)
            {
                $user = User::withTrashed()->find($user_id);
                foreach($toSend as $client)
                {
                    $cli = Client::find($client);
                    $user->clients()->attach($cli->id);
                    $cli->status_client_id = 1;
                    $cli->save();
                }
                $data['users'][] = $user->username;
            }
            if($nrClients>=1)
                Event::fire(NewClientsEventHandler::EVENT, array($data));
        }
        return 'true';
    }



    public function postDistributeClientsAll()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        if (Input::has('users'))
        {
            $data['text'] = '';
            $data['users'] = array();
            $users = Input::get('users');
            $nrClients = Client::where('status_client_id','=',0)->count();
            $clientsPerUser = (int)$nrClients/count($users);
            //die(print_r(is_array($users)));
            foreach($users as $user_id)
            {
                 $user = User::find($user_id);
                 $clients = Client::where('status_client_id','=',0)->limit($clientsPerUser)->get();
                 foreach($clients as $client)
                 {
                     $user->clients()->attach($client->id);
                     $client->status_client_id = 1;
                     $client->save();
                 }
                $data['users'][] = $user->username;
            }
            if($nrClients>=1)
            Event::fire(NewClientsEventHandler::EVENT, array($data));
        }
    }

    public function postImportClientsStep1()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $file = Input::file('fileImport');
        $name = 'import'.time();
        $import = array();
        $import = Excel::load($file->getRealPath(), function($reader) use ($import){

            //$results = $reader->get();

            $reader->noHeading();
        },'UTF-8');

        $import = $import->toArray();

        //$import = Excel::load($file->getRealPath())->formatDates(false)->select(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))->toArray();
        $send = array();
        //throw new Exception(print_r($import));
        //sss(print_r($import));
        foreach($import as $data)
        {
           //var_dump($data);

           if (is_array($data))
           {
               $data['1'] = Client::arrayToString($data['1']);
               $data['2'] = Client::arrayToString($data['2']);
               $data['3'] = Client::arrayToString($data['3']);
               $data['4'] = Client::arrayToString($data['4']);
               $data['5'] = Client::arrayToString($data['5']);
               $data['6'] = Client::arrayToString($data['6']);
               $data['7'] = Client::arrayToString($data['7']);
               $data['8'] = Client::arrayToString($data['8']);
               $data['9'] = Client::arrayToString($data['9']);
               $data['10'] = Client::arrayToString($data['10']);
               $data['11'] = Client::arrayToString($data['11']);
               $data['12'] = Client::arrayToString($data['12']);
               $data['13'] = Client::arrayToString($data['13']);

               if ($data['2']!='' && $data['2']!=' ')
                   $send[] = $data;
           }

           //}
          //  if (is_array($data['2']))
              //die(print_r($data));

        }
        Cache::put($name, array_filter($send), 15);
        return $name;
    }

    public function getImportClientsStep2($name)
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));
           // die(print_r(Cache::get($name)));
        if (Cache::has($name))
            $excel = Cache::get($name);
        else
            $excel = '';
            return View::make('clients.importClientsStep2')->with('excel',$excel)->with('importName',$name);
    }

    public function postSaveImport($name)
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        if (Cache::has($name))
        {
            $excel = Cache::get($name);
           // die(print_r($excel));
            $status = Client::saveImport($excel);
            $import = Import::find($status);
            return $import;
        }
        else
        return 'false';
    }

    public function postApproveManager()
    {
        //access rights to method
        if (!Authority::can('approve_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy_id'));
        $policy->status_policy_id = 4;
        $policy->save();
        $policy->brokers()->attach(Input::get('user'),array('approved'=>'0'));
        $policy->save();

        if (Input::has('send_email'))
        {
            $name = 'ProAssist';
            $user = User::find(Input::get('user'));
            $emailTo = $user->email;
            $emailFrom = Auth::user()->email;
            $subject = 'Новая Заявка';
            $token = Crypt::encrypt($policy->id);
            $data = array('policy'=>$name,'policy'=>$policy, 'subject'=>$subject, 'token'=>$token);
            Mail::send('emails.broker.policy', $data, function($message)use($name,$emailTo,$subject,$emailFrom)
            {
                //$message->from($emailFrom,$name);
                $message->to($emailTo)->subject($subject);
            });
        }

        if (Input::has('send_note'))
        {
            $note = new Notification();
            $note->name = 'Новая Заявка';
            $note->notification_type_id = 2;
            $note->text = "<a href='".action('BrokerController@getViewPolicy')."/".$policy->id."' class='btn btn-sm btn-success ajaxa'>Просмотр</a>";
            $note->appear_time = Carbon\Carbon::now();
            $note->client_id = 0;
            $note->user_id = Auth::user()->id;
            $note->save();

            $user = User::find(Input::get('user'));
            $note->users()->attach($user->id);
            $note->save();
            Notification::checkAndSendNotifications();
        }
    }

    public function postDeclineManager()
    {
        //access rights to method
        if (!Authority::can('approve_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy'));
        $policy->status_policy_id = 6;
        $policy->save();
        $client = Client::find($policy->client_id);
        $client->status_client_id = 2;
        $client->save();
    }

    public function getJsonClients($status)
    {
        if ($status!=-1)
            return Client::where('status_client_id','=',$status)->get(array('phone','firstname','lastname','secondname'));
        else
            return Client::get(array('phone','firstname','lastname','secondname'));
    }

    public function postCheckClient()
    {
         return Client::where('phone','=',Input::get('phone'))->count();
    }

    public function postAddClient()
    {
        //access rights to method
        if (!Authority::can('add_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = new Client();
        $client->phone = Input::get('phone');
        $client->firstname = Input::get('firstname');
        $client->lastname = Input::get('lastname');
        $client->secondname = Input::get('secondname');
        if (Input::has('user'))
        $client->status_client_id = 1;

        $client->save();
        $data['text'] = $client;
        $data['users'] = array();
        if (Input::has('user'))
        {
            $client->users()->attach(Input::get('user'));

            $user = User::find(Input::get('user'));
            $data['users'][] = $user->username;
            Event::fire(NewClientsEventHandler::EVENT, array($data));
        }
    }

    public function postAddClientFile()
    {
        //access rights to method
        if (!Authority::can('add_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find(Input::get('client_id'));
        $file = Input::file('client_file');
        $fileBD = new ClientFiles();
        $fileBD->desc = Input::get('desc');
        $fileBD->client_id = $client->id;
        $fileBD->save();
        $original_name = $file->getClientOriginalName();
        $fileName = 'file_'.$client->id.'_'.$fileBD->id.'_'.$original_name;
        $fileBD->original_name = $original_name;
        $fileBD->name = $fileName;
        $fileBD->save();
        $file->move(Config::get('paths.clients_files'), $fileName);
    }

    public function getDownload($id)
    {
        //access rights to method
        if (!Authority::can('download_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));

        $file = ClientFiles::find($id);
        $pathToFile = Config::get('paths.clients_files').DIRECTORY_SEPARATOR.$file->name;
        return Response::download($pathToFile, $file->original_name);
    }

    public function postDeleteFile($id)
    {
        //access rights to method
        if (!Authority::can('delete_client_files'))
            return Redirect::to(Config::get('app.restricted_url'));


        $file = ClientFiles::find($id);
        $filePath = Config::get('paths.clients_files').DIRECTORY_SEPARATOR.$file->name;
        if (file_exists($filePath))
            unlink($filePath);
        $file->delete();
    }


    public function getNewAjax()
    {
        return View::make('clients.newAjax');
    }

    public function getNotInterestedAjax()
    {
        return View::make('clients.notInterestedAjax');
    }

    public function getInProgress($id)
    {
        //access rights to method
        if (!Authority::can('procesing_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find($id);
        if ($client->status_client_id==1)
        $client->status_client_id = 2;

        $client->save();
        $regions = Region::nonzero()->get();
        $consums = TypeConsum::nonzero()->get();
        $types = TypeCar::nonzero()->get();
        $signalings = TypeSignaling::nonzero()->get();
        $gearboxs = TypeGearbox::nonzero()->get();
        $docs = TypeDoc::nonzero()->get();
        $typesPolicy = TypePolicy::nonzero()->get();
        $cars = $client->cars()->get();
        $risks = InsuranceRisk::nonzero()->get();
        $tapeSums = TypeSumInsured::nonzero()->get();
        $potential_client_types = PotentialClientType::all();
        $brands = BrandCar::orderBy('name','asc')->get();
        return View::make('clients.inProgress')->with('client',$client)->with('regions',$regions)
            ->with('consums',$consums)->with('types',$types)->with('signalings',$signalings)
            ->with('gearboxs',$gearboxs)->with('docs',$docs)->with('typesPolicy',$typesPolicy)
            ->with('cars',$cars)->with('risks',$risks)
            ->with('tapeSums',$tapeSums)->with('brands',$brands)->with('potential_client_types',$potential_client_types);
    }

    public function getViewClient($id)
    {
        //access rights to method
        if (!Authority::can('edit_client'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find($id);
        $regions = Region::nonzero()->get();
        $consums = TypeConsum::nonzero()->get();
        $types = TypeCar::nonzero()->get();
        $signalings = TypeSignaling::nonzero()->get();
        $gearboxs = TypeGearbox::nonzero()->get();
        $docs = TypeDoc::nonzero()->get();
        $typesPolicy = TypePolicy::nonzero()->get();
        $cars = $client->cars()->get();
        $risks = InsuranceRisk::nonzero()->get();
        $tapeSums = TypeSumInsured::nonzero()->get();
        $brands = BrandCar::orderBy('name','asc')->get();
        return View::make('clients.inProgress')->with('client',$client)->with('regions',$regions)
            ->with('consums',$consums)->with('types',$types)->with('signalings',$signalings)
            ->with('gearboxs',$gearboxs)->with('docs',$docs)->with('typesPolicy',$typesPolicy)
            ->with('cars',$cars)->with('risks',$risks)
            ->with('tapeSums',$tapeSums)->with('brands',$brands);
    }

    public function getNotInterested($id)
    {
        //access rights to method
        if (!Authority::can('view_not_interested_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::where('id','=',$id)->where('status_client_id','=',3)->first();
        return View::make('clients.notInterested')->with('client',$client);
    }

    public function postSendToNotInterested()
    {
        //access rights to method
        if (!Authority::can('make_not_interested_clients'))
            return Redirect::to(Config::get('app.restricted_url'));


        $client = Client::find(Input::get('client'));
        $client->status_client_id = 3;
        $client->save();
        $client->users()->detach(Auth::user()->id);
       // $client->users()->attach(Input::get('user'));

        $data['text'] = $client;
        $data['users'] = array();
        foreach(User::all() as $user)
        $data['users'][] = $user->username;
        //Event::fire(NotInterestedClientsEventHandler::EVENT, array($data));
    }

    public function getCheck()
    {
        Event::fire(TopAgentsEventHandler::EVENT, array('success'));
        //access rights to method
        if (!Authority::can('view_new_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Auth::user()->clients()->orderBy('created_at','desc')->where('status_client_id','=','1');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }


        $clients = $clients->paginate(25);
        return View::make('clients.newClients')->with('clients',$clients);
    }

    public function postAddToMeNotInterested()
    {
        //access rights to method
        if (!Authority::can('get_not_interested_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find(Input::get('client'));
        $client->status_client_id = 2;
        $client->save();
        $client->users()->attach(Auth::user()->id);
    }

    public function postSaveClient()
    {
        //access rights to method
        if (!Authority::can('edit_client'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find(Input::get('client_id'));
        $client->firstname = Input::get('firstname');
        $client->lastname = Input::get('lastname');
        $client->secondname = Input::get('secondname');
        $client->setBirthDatedAttribute(Input::get('birth_date'));
        //$client->birth_date = Input::get('birth_date');
        $client->setDateDriverLicenceAttribute(Input::get('date_driver_licence'));
        //$client->date_driver_licence = Input::get('date_driver_licence');
        $client->serial_driver_licence = Input::get('serial_driver_licence');
        if (Input::has("potential_client_type_id")) {
            $client->potential_client_type_id = Input::get("potential_client_type_id");
        }
        $client->serial_driver_licence = Input::get('serial_driver_licence');
        $client->serial_bulletin = Input::get('serial_bulletin');
        $client->address = Input::get('address');
        $client->email = Input::get('email');
        $client->time_contact = Input::get('time_contact');

        $client->comments = Input::get('comments');
        $client->save();
    }


    public function postAddCarClient()
    {
        //access rights to method
        if (!Authority::can('add_client_car'))
            return Redirect::to(Config::get('app.restricted_url'));

        $car = new Car();
        $car->model_car_id = Input::get('model_car_id');
        //$car->brand = Input::get('brand');
        //$car->model = Input::get('model');
        $car->year_of_construction = Input::get('year_of_construction');
        $car->motor_volum = Input::get('motor_volum');
        $car->motor_ls = Input::get('motor_ls');
        $car->register_number = Input::get('register_number');
        $car->number = Input::get('number');
        $car->VIN = Input::get('VIN');
        if (Input::has('region_id'))
        $car->region_id = Input::get('region_id');
        else
        $car->region_id = 0;
        $car->frame_number = Input::get('frame_number');
        $car->pure_mass = Input::get('pure_mass');
        $car->start_explotation = Input::get('start_explotation');

        if (Input::get('stand_number')!=1)
        {
            if (Input::has('stand_number_value'))
                $car->stand_number = Input::get('stand_number_value');
        }else
            $car->stand_number = 1;

        $car->allow_max_mass = Input::get('allow_max_mass');
        $car->type_consum_id = Input::get('type_consum_id');
        $car->type_car_id = Input::get('type_car_id');
        $car->mileage = Input::get('mileage');
        $car->type_signaling_id = Input::get('type_signaling_id');
        $car->type_gearbox_id = Input::get('type_gearbox_id');


        if (Input::has('type_doc_id'))
            $car->type_doc_id = Input::get('type_doc_id');
        else
            $car->type_doc_id = 0;
        $car->category = Input::get('category');

        $car->doc_serial = Input::get('doc_serial');
        $car->doc_number = Input::get('doc_number');

        $car->doc_who = Input::get('doc_who');

        $car->setDocWhenAttribute(Input::get('doc_when'));
        //$car->doc_when = Input::get('doc_when');
        $car->save();
        $client = Client::find(Input::get('client_id'));
        $client->cars()->attach($car->id);
        $client->save();
    }

    public function postDeleteClient($id)
    {
        //access rights to method
        if (!Authority::can('delete_client'))
            return Redirect::to(Config::get('app.restricted_url'));

        $client = Client::find($id);
        if ($client->status->id==0 || $client->status->id==1)
        $client->forceDelete();
        else
            $client->delete();
    }

    public function postDeleteCar($id)
    {
        //access rights to method
        if (!Authority::can('delete_client_car'))
            return Redirect::to(Config::get('app.restricted_url'));

        $car = Car::find($id);
        if (Policy::where('car_id','=',$car->id)->withTrashed()->count()<=0)
        {
            $car->forceDelete();
        }else
        {
            $car->delete();
        }
    }
    public function postDeleteSelectedClient()
    {
        //access rights to method
        if (!Authority::can('delete_client_car'))
            return Redirect::to(Config::get('app.restricted_url'));

        $clients = Input::get('client_check');
        foreach($clients as $cl)
        {
            $client = Client::find($cl);
            if ($client->status->id==0 || $client->status->id==1)
                $client->forceDelete();
            else
                $client->delete();
        }
    }

    public function postDeletePolicy($id)
    {
        //access rights to method
        if (!Authority::can('delete_client_policy'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find($id);
        $policy->delete();
    }

    public function postAddClientPolicy()
    {
        //access rights to method
        if (!Authority::can('add_client_policy'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = new Policy();
        $policy->client_id =  Input::get('client_id');
        $policy->status_policy_id = 1;
        $policy->date_created = Carbon\Carbon::now()->toDateString();
        $policy->type_policy_id = Input::get('type_policy_id');
        $policy->setDateLastPolicyAttribute(Input::get('date_last_policy'));
       // $policy->date_last_policy = Input::get('date_last_policy');
        $policy->last_policy_company = Input::get('last_policy_company');
        $policy->name =  Input::get('name');
        $policy->firstname_manager = Input::get('firstname_manager');
        $policy->lastname_manager = Input::get('lastname_manager');
        $policy->secondname_manager = Input::get('secondname_manager');
        $policy->TO = Input::get('TO');
        $policy->kbm = Input::get('kbm');
        $policy->period_policy = Input::get('period_policy');

        if (Input::has('was_kasko'))
        $policy->was_kasko = Input::get('was_kasko');

        $policy->nr_casco = Input::get('nr_casco');
        $policy->sum_now_car = Input::get('sum_now_car');
        $policy->sum_market_car = Input::get('sum_market_car');

        if (Input::get('type_policy_id')==3){
            $policy->car_id = 0;
        }else
            $policy->car_id = Input::get('car_id');

        if (Input::has('insurance_risk_id'))
        $policy->insurance_risk_id = Input::get('insurance_risk_id');

        $policy->insurance_company = Input::get('insurance_company');
        $policy->used_region = Input::get('used_region');

        if (Input::has('in_credit'))
        $policy->in_credit = Input::get('in_credit');
        else
            $policy->in_credit = 0;

        $policy->bank_credit = Input::get('bank_credit');

        if (Input::has('type_sum_insured_id'))
        $policy->type_sum_insured_id = Input::get('type_sum_insured_id');


        $policy->number_drivers = Input::get('number_drivers');
        $policy->totalSum = Input::get('totalSum');
        $policy->coefficient = Input::get('coefficient');
        $policy->save();
        $client = Client::find($policy->client_id);
        $client->status_client_id = 2;
        $client->save();


        for($i=1;$i<=Input::get('number_drivers');$i++)
        {
            $driver = new Driver();
            $driver->firstname = Input::get('firstname_driver_'.$i);
            $driver->lastname = Input::get('lastname_driver_'.$i);
            $driver->secondname = Input::get('secondname_driver_'.$i);
            $driver->gender_id = Input::get('gender_id_driver_'.$i);
            $driver->age = Input::get('age_driver_'.$i);
            $driver->experience = Input::get('experience_driver_'.$i);
            $driver->marriage_status_id = Input::get('marriage_status_driver_id_'.$i);
            $driver->policy_id = $policy->id;
            $driver->save();

            //$policy->drivers()->attach($driver->id);
            //$policy->save();
        }

    }

    public function postSendToApproval()
    {
        //access rights to method
        if (!Authority::can('is_agent'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy'));
        $policy->status_policy_id = 2;
        $policy->save();
       // $client = Client::find(Input::get('policy'));
    }

    public function getClientToApproval()
    {
        //access rights to method
        if (!Authority::can('approve_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policies = Policy::where('status_policy_id','=','2')->paginate(25);
        return View::make('clients.toApproval')->with('policies',$policies);
    }

    public function getClientsSentToBroker()
    {
        //access rights to method
        if (!Authority::can('approve_clients'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policies = Policy::where('status_policy_id','=','4')->paginate(25);
        return View::make('clients.sentToBroker')->with('policies',$policies);
    }

    public function getViewPolicy($id)
    {
        //access rights to method
        if (!Authority::can('view_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find($id);
        return View::make('clients.policy.viewPolicy')->with('policy',$policy);
    }


    public function postGetDrivers($nr)
    {
        $genders = Gender::nonzero()->get();
        $mStatuses = MariageStatus::nonzero()->get();
        return View::make('clients.policy.driverInfo')->with('nr',$nr)->with('genders',$genders)->with('mStatuses',$mStatuses)->with('new','1');
    }

    public function postGetDriver($i)
    {
        $genders = Gender::nonzero()->get();
        $mStatuses = MariageStatus::nonzero()->get();
        return View::make('clients.policy.driverOneInfo')->with('i',$i)->with('genders',$genders)->with('mStatuses',$mStatuses)->with('new','0');
    }

    public function getInProgressAjax()
    {
        return View::make('clients.inProgressAjax');
    }
    public function postChangeColor()
    {
        $client = Client::find(Input::get('client'));
        $client->color = Input::get('color');
        $client->timestamps = false;
        $client->save();
        return 'true';
    }
}
