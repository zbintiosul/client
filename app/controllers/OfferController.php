<?php

class OfferController extends BaseController {

    protected $layout = 'layouts.master';

    public function getOffers($policy_id)
    {
        $policy = Policy::find($policy_id);
        $offers = Offer::where('policy_id','=',$policy_id)->paginate(25);
        return View::make('offers.index')->with('offers', $offers)->with('policy',$policy);
    }

    public function getModalAddOffer($policy_id)
    {
        $policy = Policy::find($policy_id);
        return View::make('offers.addOffer')->with('policy',$policy);
    }

    public function getModalEditOffer($id)
    {
        $offer = Offer::find($id);
        return View::make('offers.editOffer')->with('offer',$offer);
    }

    public function postAddOffer()
    {
        $offer = new Offer();
        $offer->policy_id = Input::get('policy_id');
        $offer->terms = Input::get('terms');
        $offer->additional_services = Input::get('additional_services');
        $offer->baner = Input::get('baner');
        $offer->company_id = Input::get('company_id');
        $offer->strahavaia_suma = Input::get('strahavaia_suma');
        $offer->strahavaia_premia = Input::get('strahavaia_premia');
        $offer->fransiza = Input::get('fransiza');
        $offer->strahavaia_suma_grajdanscaia = Input::get('strahavaia_suma_grajdanscaia');
        $offer->strahavaia_premia_grajdanscaia = Input::get('strahavaia_premia_grajdanscaia');
        $offer->strahavaia_suma_dobrovolina = Input::get('strahavaia_suma_dobrovolina');
        $offer->strahavaia_premia_dobrovolina = Input::get('strahavaia_premia_dobrovolina');
        $offer->comments = Input::get('comments');
        $offer->stantii = Input::get('stantii');
        $offer->signature = Input::get('signature');
        $offer->save();
    }

    public function postEditOffer()
    {
        $offer = Offer::find(Input::get('offer_id'));
        $offer->terms = Input::get('terms');
        $offer->additional_services = Input::get('additional_services');
        $offer->company_id = Input::get('company_id');
        $offer->baner = Input::get('baner');
        $offer->strahavaia_suma = Input::get('strahavaia_suma');
        $offer->strahavaia_premia = Input::get('strahavaia_premia');
        $offer->fransiza = Input::get('fransiza');
        $offer->strahavaia_suma_grajdanscaia = Input::get('strahavaia_suma_grajdanscaia');
        $offer->strahavaia_premia_grajdanscaia = Input::get('strahavaia_premia_grajdanscaia');
        $offer->strahavaia_suma_dobrovolina = Input::get('strahavaia_suma_dobrovolina');
        $offer->strahavaia_premia_dobrovolina = Input::get('strahavaia_premia_dobrovolina');
        $offer->comments = Input::get('comments');
        $offer->stantii = Input::get('stantii');
        $offer->signature = Input::get('signature');
        $offer->save();
    }

    public function postDeleteOffer($id)
    {
        $offer = Offer::find($id);
        $offer->delete();
    }

    public function getModalSendEmailToClient($id)
    {
        $offer = Offer::find($id);
        return View::make('offers.modalSendToEmail')->with('offer', $offer);
    }

    public function getOffer($id){
        $offer = Offer::find($id);
        return View::make('offers.offer')->with('offer', $offer);
    }

    public function getViewOffer($id){
        $offer = Offer::find($id);
        return View::make('offers.viewOffer')->with('offer', $offer);
    }

    public function postSendEmailToClient(){

        $offer = Offer::find(Input::get('offer_id'));
        $name = $offer->company->name;
        $emailTo = Input::get('email');
        $emailFrom = Auth::user()->email;
        $subject = 'Коммерческое Предложение Страхования '.$offer->policy->type->name;
        $data = array('policy'=>$name,'offer'=>$offer, 'subject'=>$subject);
        Mail::send('offers.offer', $data, function($message)use($name,$emailTo,$subject,$emailFrom)
        {
            //$message->from($emailFrom,$name);
            $message->to($emailTo)->subject($subject);
        });
        $offer->sent_times = $offer->sent_times + 1;
        $offer->save();
    }
}