<?php

class CompanyController extends BaseController {

    protected $layout = 'layouts.master';

    public function getIndex()
    {
        $companies = Company::paginate(25);
        return View::make('companies.index')->with('companies', $companies);
    }

    public function getModalAddCompany()
    {
        return View::make('companies.addCompany');
    }

    public function getModalEditCompany($id)
    {
        $company = Company::find($id);
        return View::make('companies.editCompany')->with('company',$company);
    }

    public function postAddCompany()
    {
        $company = new Company();
        $company->name = Input::get('name');
        $company->url = Input::get('url');
        $company->logo = Input::get('logo');
        $company->save();
    }

    public function postEditCompany()
    {
        $company = Company::find(Input::get('company_id'));
        $company->name = Input::get('name');
        $company->url = Input::get('url');
        $company->logo = Input::get('logo');
        $company->save();
    }

    public function postDeleteCompany($id)
    {
        $company = Company::find($id);
        $company->delete();
    }
}