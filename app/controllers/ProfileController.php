<?php

class ProfileController extends BaseController {

    protected $layout = 'layouts.master';

    public function getIndex()
    {
        $users = User::All();
        return View::make('profile.index')->with('users',$users);
    }

    public function postForcePassword()
    {
        //access to method
        if (!Auth::user()->force_login)
            Redirect::to(Config::get('app.restricted_url'));
        $user = User::find(Auth::user()->id);
        $user->password = Hash::make(Input::get('password'));
        $user->force_login = 0;
        $user->save();
        return Redirect::to('/');
    }

    public function getNrNewNote()
    {
        return Auth::user()->nrNewNote();
    }

    public function getNrNewClients()
    {
        return Auth::user()->nrNewClients();
    }

    public function getNrInProgressClients()
    {
        return Auth::user()->nrInProgressClients();
    }

    public function getNrNotInterestedClients()
    {
        return Client::nrNotInterestedClients();
    }

    public function getSearch()
    {
        $clients = Auth::user()->clients()->orderBy('updated_at','desc');

        if (Input::has('search'))
        {
            $search = Input::get('search');
            $clients = $clients->where(function($query) use ($search)
            {
                $query->where('firstname','LIKE','%'.$search.'%')->orwhere('lastname','LIKE','%'.$search.'%')->orwhere('secondname','LIKE','%'.$search.'%')->orwhere('phone','LIKE','%'.$search.'%');
            });
        }

        $clients = $clients->paginate(25);
        return View::make('profile.search')->with('clients',$clients);
    }

    public function getModalTools()
    {
        //access rights to method
        if (!Authority::can('show_tools'))
            return Redirect::to(Config::get('app.restricted_url'));

        return View::make('layouts.blocks.tools');
    }
}