<?php

class SettingController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    protected $layout = 'layouts.master';
    public function getUsers()
    {
        //access rights to method
        if (!Authority::can('user_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        return View::make('settings.users');
    }

    public function postUsers()
    {
        //access rights to method
        if (!Authority::can('user_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        Setting::setValue('users_per_page',$users_per_page = Input::get('users_per_page'));
    }

    public function postRoles()
    {
        //access rights to method
        if (!Authority::can('user_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        Setting::setValue('roles_per_page',$users_per_page = Input::get('roles_per_page'));
    }

    public function getSalary()
    {
        //access rights to method
        if (!Authority::can('salary_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        $salary_table = SalaryTable::nonzero()->get();
        return View::make('settings.salary')->with('salary_table',$salary_table);
    }

    public function postSalary()
    {
        $baseSalary = SalaryTable::find(0);
        $baseSalary->min = Input::get('base');
        $baseSalary->save();

        foreach(SalaryTable::nonzero()->get() as $sal)
        {
            if (Input::has('rank_'.$sal->id) || Input::has('min_'.$sal->id) || Input::has('max_'.$sal->id))
            {
                $sal->rank =Input::get('rank_'.$sal->id);
                $sal->min =Input::get('min_'.$sal->id);
                $sal->max =Input::get('max_'.$sal->id);
                $sal->percentage =Input::get('percentage_'.$sal->id);
                $sal->save();
            }
        }

        for($i=1;$i<=Input::get('nr_new_levels');$i++)
        {
            if (Input::has('rank_new_'.$i) || Input::has('min_new_'.$i) || Input::has('max_new_'.$i))
            {
                $salary = new SalaryTable();
                $salary->rank =Input::get('rank_new_'.$i);
                $salary->min =Input::get('min_new_'.$i);
                $salary->max =Input::get('max_new_'.$i);
                $salary->percentage =Input::get('percentage_new_'.$i);
                $salary->save();
            }
        }
    }

    public function getOther()
    {
        //access rights to method
        if (!Authority::can('user_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        return View::make('settings.other');
    }

    public function postGetLevel($i)
    {
        return View::make('settings.salaryOneLevel')->with('i',$i);
    }

    public function postDeleteLevel($id)
    {
        $level = SalaryTable::find($id);
        $level->delete();
    }

    public function getTools()
    {
        //access rights to method
        if (!Authority::can('view_tools'))
            return Redirect::to(Config::get('app.restricted_url'));

        $tools = Tools::paginate(25);
        return View::make('settings.tools')->with('tools',$tools);
    }

    public function getModalAddTool()
    {
        //access rights to method
        if (!Authority::can('add_tools'))
            return Redirect::to(Config::get('app.restricted_url'));

        return View::make('settings.addTool');
    }

    public function postAddTool()
    {
        //access rights to method
        if (!Authority::can('add_tools'))
            return Redirect::to(Config::get('app.restricted_url'));

        ///die(print_r(Input::get('content_tool')));
        $tool = new Tools();
        if (Input::has('content_tool'))
        {
            $tool->text = Input::get('content_tool');
            $tool->save();
        }

        if (Input::hasFile('tool_image'))
        {
            $tool->save();
            $image = Input::file('tool_image');
            $imageName = 'img'.$tool->id.'.png';
            $path =  Config::get('paths.tools_img');
            if (!file_exists($path)) {
                mkdir($path);
            }
            Image::make($image->getRealPath())->save($path.DIRECTORY_SEPARATOR.$imageName);
            $tool->img_path = $imageName;
            $tool->save();
        }

    }

    public function postDeleteTool($id)
    {
        //access rights to method
        if (!Authority::can('delete_tools'))
            return Redirect::to(Config::get('app.restricted_url'));

        $tool = Tools::find($id);
        if ($tool->img_path!=null && file_exists(Config::get('paths.tools_img').DIRECTORY_SEPARATOR.$tool->img_path))
        unlink(Config::get('paths.tools_img').DIRECTORY_SEPARATOR.$tool->img_path);
        $tool->delete();
    }

    public function getHistory()
    {
        //access rights to method
        if (!Authority::can('user_settings'))
            return Redirect::to(Config::get('app.restricted_url'));

        $history = History::orderBy('created_at','desc');

        if (Input::has('user'))
        {
            $id_user = Input::get('user');
            $history =  $history->where('user_id','=',$id_user);
        }

        if (Input::has('from'))
        {
            $from = Carbon\Carbon::createFromFormat('d-m-Y H:i:s', Input::get('from').' 00:00:00');
            $history =  $history->where('created_at','>=',$from->toDateTimeString());
        }

        if (Input::has('too'))
        {
            $to = Carbon\Carbon::createFromFormat('d-m-Y H:i:s', Input::get('too').' 23:59:59');
            $history =  $history->where('created_at','<=',$to->toDateTimeString());
        }

        $minDate = $history->first();
        if ($minDate!=null)
        {
            $dt = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $minDate->created_at);
        }else{
            $m = History::orderBy('created_at','asc')->first();
            if ($m==null){
                $dt = Carbon\Carbon::now();
            } else {
                $dt = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $m->created_at);
            }

        }

        $minDate = $dt->format('d-m-Y');
        $history = $history->paginate(50);
        $usedUsers = History::distinct()->get(array('user_id'));

        return View::make('settings.history')->with('history',$history)->with('usedUsers',$usedUsers)->with('minDate',$minDate);
    }
}