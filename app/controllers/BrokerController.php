<?php

class BrokerController extends BaseController {

    protected $layout = 'layouts.master';

    public function getInsuranceForApproval()
    {
        //access rights to method
        if (!Authority::can('is_broker'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policies = Auth::user()->broker_policies()->wherePivot('approved','0')->orderBy('updated_at','desc')->paginate(25);
        return View::make('broker.index')->with('policies',$policies);
    }

    public function getInsurance($id)
    {
        //access rights to method
        if (!Authority::can('is_broker'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find($id);
        return View::make('broker.allPolicy')->with('policy',$policy);
    }

    public function getAllInsurances()
    {
        //access rights to method
        if (!Authority::can('is_broker'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policies = Auth::user()->broker_policies()->orderBy('updated_at','desc')->paginate(25);
        return View::make('broker.allPolicy')->with('policies',$policies);;
    }

    public function getViewPolicy($id)
    {
        //access rights to method
        if (!Authority::can('view_policies'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find($id);
        return View::make('broker.viewPolicy')->with('policy',$policy);
    }

    public function postDeclineBroker()
    {
        //access rights to method
        if (!Authority::can('approve_insurance_broker'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy'));
        $policy->status_policy_id = 7;
        $policy->save();
        $policy->brokers()->detach(Auth::user()->id);
        $client = Client::find($policy->client_id);
        $client->status_client_id = 2;
        $client->save();
    }

    public function postApproveBroker()
    {
        //access rights to method
        if (!Authority::can('approve_insurance_broker'))
            return Redirect::to(Config::get('app.restricted_url'));

        $policy = Policy::find(Input::get('policy'));
        $policy->status_policy_id = 5;
        $policy->save();
        $policy->brokers()->detach(Auth::user()->id);
        $policy->save();
        $policy->brokers()->attach(Auth::user()->id, array('approved'=>'1'));
        $policy->save();

        $client = Client::find($policy->client_id);
        $noCompleted = 0;
        foreach ($client->policies as $policy)
        {
            if ($policy->status_policy_id<>5)
            {
                $noCompleted = 1;
            }
        }

        if (!$noCompleted)
        {
            $client->status_client_id = 5;
            $client->save();
        }

        $levelID = SalaryTable::getLevel($policy->id);
        $salaryAgent = new SalaryAgent();
        $salaryAgent->policy_id = Input::get('policy');
        $salaryAgent->user_id = $client->agent()->id;
        $salaryAgent->salary_table_id = $levelID;
        $salaryAgent->save();

        $dtMonth  = Carbon\Carbon::now();
        $minMonth =  $dtMonth->startOfMonth()->toDateTimeString();
        $maxMonth =  $dtMonth->endOfMonth()->toDateTimeString();
        $salaryAgent->user->salaries()->where('created_at','<',$maxMonth)->where('created_at','>=',$minMonth)->update(array('salary_table_id' => $levelID));

        $name = 'ProAssist';
        $emailTo = 'sales@proassist.ru';
        $emailFrom = Auth::user()->email;
        $subject = 'Новая продажа';
        $data = array('policy'=>$name,'policy'=>$policy, 'subject'=>$subject);
        Mail::send('emails.clients.newSale', $data, function($message)use($name,$emailTo,$subject,$emailFrom)
        {
            //$message->from($emailFrom,$name);
            $message->to($emailTo)->subject($subject);
        });
        Event::fire(TopAgentsEventHandler::EVENT, array('success'));
    }

}