<?php

class AccountController extends BaseController {

    protected $layout = 'layouts.master';
    public function getLogin()
    {
        return View::make('account.login')->with('page_body_prop', array("id"=>"login", "class"=>"animated fadeInDown"));
    }

    public function postLogin()
    {
        $login = Input::get('username');
        $credentials = array(
            'username' => $login,
            'password' => Input::get('password'),
            'active' => '1'
        );
       $user = User::where('username','=',$login)->first();
       $session=null;
       if ($user!==null) {
           $session = Sessions::where('user_id','=',$user->id)->orderBy('last_activity','desc')->first();
       }
       
       if(Auth::attempt($credentials))
       {
            if ($session!=null){
                $last_acitivity = Carbon\Carbon::createFromTimeStamp($session->last_activity);
                $history = new History();
                $history->text = "Last activity ".$last_acitivity->format('d-m-Y H:i:s');
                $history->user_id = Auth::user()->id;
                $history->save();
            }

            
            $history = new History();
            $history->text = "User login";
            $history->user_id = Auth::user()->id;
            $history->save();
            // we are now logged in, go to home
            return Redirect::intended();
       }
        else
        {
            // auth failure! lets go back to the login
            return Redirect::back()
                ->with('login_errors', true);
            // pass any error notification you want
            // i like to do it this way
        }
    }

    public function getLogout()
    {
        $history = new History();
        $history->text = "User logout";
        $history->user_id = Auth::user()->id;
        $history->save();
        Auth::logout();
        //Session::forget('last_login');
        return Redirect::to('/');
    }
/*
    public function getRegister()
    {
        $regions = Regions::withLang();
        $operators = Mobileoperators::all();
        $this->layout->content = View::make('front.account.register')->with('regions', $regions)->with('operators',$operators);
    }

    public function postRegister()
    {
        $rules = array(
            'username' => 'required|min:4|unique:users|alpha_dash',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'rules' => 'required',

        );

        if (Input::has('checkbox_moreinfo'))
        {
            $rules = array_add($rules, 'firstname', 'min:3');
            $rules = array_add($rules, 'lastname', 'min:3');
            $rules = array_add($rules, 'region', 'required');
            $rules = array_add($rules, 'phone', 'alpha_num|min:5');
        }

        if (Input::has('checkbox_pj'))
        {
            $rules = array_add($rules, 'pjuridic', 'required|min:3');
            $rules = array_add($rules, 'function', 'required|min:3');
        }
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }else
        {
            $user = new User();
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->active = 0;
            if (Input::has('checkbox_pj'))
            {
                $user->user_type_id = 2;
            }else
                $user->user_type_id = 1;
            $user->save();

            if (Input::has('checkbox_moreinfo'))
            {
                $userInfo = new Userinfo();
                $userInfo->firstname = Input::get('firstname');
                $userInfo->lastname = Input::get('lastname');
                $userInfo->secondname = Input::get('secondname');
                $userInfo->user_id = $user->id;
                $userInfo->region_id =  Input::get('region');
                $userInfo->save();
                if (Input::has('phone'))
                {
                    $phone = new UserPhone();
                    $phone->phone = Input::get('phone');
                    $phone->active = 1;
                    $phone->user_id = $user->id;
                    if (Input::get('operator')==0)
                    {
                        $phone->phone_type_id = 2;
                        $phone->mobile_operator_id = 4;
                    }else
                    {
                        $phone->phone_type_id = 1;
                        $phone->mobile_operator_id = Input::get('operator');
                    }
                    $phone->save();
                }

            }
            if (Input::has('checkbox_pj'))
            {
                $company = new Usercompany();
                $company->name = Input::get('pjuridic');
                $company->function = Input::get('function');
                $company->save();
            }

           // Crypt::setMode('cbc');
           // Crypt::setCipher('blowfish');
            $token =  Crypt::encrypt($user->id);
            $data = array('username'=>$user->username,'token'=>$token);

            Mail::send('emails.registerconfirm', $data, function($message) use ($user)
            {
                $message->to($user->email, '')->subject('['.Config::get('app.site_name').'] '.Lang::get('account.register'));
            });
        }
        return Redirect::to(action('AccountController@getRegister'))->with('success',Lang::get('account.success'));
    }

    public function getEmailconfirm($id)
    {
        $errors = array();
        $message = '';
        //Crypt::setMode('cbc');
        //Crypt::setCipher('blowfish');
        $id = Crypt::decrypt($id);
        $user = User::find($id);
        if ($user->count()>0)
        {
            if ($user->active==0)
            {
                $user->active = 1;
                $user->save();
                //Auth::loginUsingId($user->id);
                $message = Lang::get('account.register_succ1');
                return Redirect::to(action('AccountController@getLogin'))->with('reset',$message);
            }else
            {
                $errors[] = Lang::get('account.register_err1');
            }
        }else
        {
            $errors[] = Lang::get('account.register_err2');
        }
        return Redirect::to(action('AccountController@getRegister'))->with('errors',$errors);
    }

    public function getEnter()
    {
        $regions = Regions::withLang();
        $operators = Mobileoperators::all();
        $this->layout->content = View::make('front.account.enter')->with('regions', $regions)->with('operators',$operators);
    }

    public function getIsLogged()
    {
        if (Auth::check())
            return 'true';
        else
            return 'false';
    }

    public function getLockedScreen($id)
    {
        //Session::regenerate();
        //$data = Session::all();
        //die(print_r($data));
        //$user = null;
        //if (Session::has('last_login'))
       // {
          //  z
            $last_login = Crypt::decrypt($id); //Session::get('last_login');
            $user = User::find($last_login);
        //}
        return View::make('admin.home.lock')->with('user',$user);
    }

    public function postLockedScreen()
    {

        $user = null;
       // if (Session::has('last_login'))
        //{

            $last_login = Crypt::decrypt(Input::get('user_token'));
            $user = User::find($last_login);
            if (Auth::attempt(array('username' => $user->username, 'password' => Input::get('password'), 'active' => 1)))
            return 'true';
        else
        return 'false';
    }

    public function getEmailChangeConfirm($email,$crypt)
    {
        try
        {
            $id_user = Crypt::decrypt($crypt);
        }catch (Exception $e)
        {
            if (Auth::check())
            {
                return Redirect::to(action('ProfileController@getInfo'))->with('error',Lang::get('account.error_email_change'));
            }else
            {
                return Redirect::to(action('AccountController@getLogin'))->with('login_errors',Lang::get('account.error_email_change'));
            }
        }

        $exist = User::where('id','=',$id_user)->count();
        if ($exist)
        {
            $user = User::find($id_user);
            $user->email = $email;
            $user->save();
            if (Auth::check())
            {
                return Redirect::to(action('ProfileController@getInfo'))->with('success',Lang::get('account.success_email_change'));
            }else
            {
                return Redirect::to(action('AccountController@getLogin'))->with('reset',Lang::get('account.success_email_change'));
            }
        }else
        {
            if (Auth::check())
            {
                return Redirect::to(action('ProfileController@getInfo'))->with('error',Lang::get('account.error_email_change'));
            }else
            {
                return Redirect::to(action('AccountController@getLogin'))->with('login_errors',Lang::get('account.error_email_change'));
            }
        }
    }*/
}