<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    protected $layout = 'layouts.master';
    public function getIndex()
    {
        $this->layout->with('pageName', 'index');
        $this->layout->content;
    }

    public function getPolicy($id)
    {
        $id = Crypt::decrypt($id);
        $policy = Policy::find($id);

        //Session::put('extern','1');
        return View::make('home.viewPolicy')->with('policy',$policy)->with('is_broker','0');
    }

    public function getPolicyBroker($id)
    {
        $id = Crypt::decrypt($id);
        $policy = Policy::find($id);

        //Session::put('extern','1');
        return View::make('home.viewPolicy')->with('policy',$policy)->with('is_broker','1');
    }

}