<?php

class UserManagementController extends BaseController
{

    public function getUser($id)
    {
        //access rights to method

        if (Auth::user()->id == $id || Authority::can('users_money_info'))
        {
            $user = User::withTrashed()->find($id);

            $monthRegistered = $user->calcTimeRegister();

            return View::make('userManagement.viewUser')->with('user', $user);
        } else {
            return Redirect::to(Config::get('app.restricted_url'));
        }
    }

    public function postAddBonus()
    {
        $user_id = Input::get('user_id');

        $bonus = new UserBonus();
        $bonus->reason = Input::get('text');
        $bonus->value = Input::get('bonus');
        $bonus->user_id = $user_id;
        $bonus->save();

        $note = new Notification();
        $note->name = "Bonus";
        $note->notification_type_id = 0;
        $note->text = Input::get('text').' '.Input::get('bonus');
        $note->appear_time = Carbon\Carbon::now();
        $note->user_id = Auth::user()->id;
        $note->save();

        $note->users()->attach($user_id);
        $note->save();
        try{
            Notification::checkAndSendNotifications();
        } catch (\Exception $ex)
        {

        }

    }

    public function getExportUserInfo($id)
    {
        $dateTime = Carbon\Carbon::now()->format('Y-m-d-H-i-s');
        $fileName = "user-".$id."-".$dateTime;
        $user = User::find($id);

        $totalMonth = $user->calcTimeRegister();

        $data = array();
        $data[] = array('Месяц','Всего продано','Стаж агента','Продажи','Клиенты','Маржа','Средний результат','Комиссион','Бонусы','Всего к оплате');
        for ($i=0; $i<$totalMonth; $i++)
        {
            $nrProdaji = $user->calcMonthCountSales(Carbon\Carbon::now()->subMonths($i));
            $nrProcededClients = $user->calcNrMonthProcededClients(Carbon\Carbon::now()->subMonths($i));
            if ($nrProcededClients==0)
                $marja = 0;
            else
                $marja = ($nrProdaji / $nrProcededClients) * 100;
            $data[] = array(
                Carbon\Carbon::now()->subMonths($i)->format("M Y"),
                Locale::number($user->calcMonthTotalSales(Carbon\Carbon::now()->subMonths($i))),
                Locale::number($totalMonth),
                Locale::number($nrProdaji),
                Locale::number($nrProcededClients),
                Locale::number($marja),
                Locale::number($user->calcMediumResultMonthSales(Carbon\Carbon::now()->subMonths($i))),
                Locale::number($user->calcMonthTotalVenitSales(Carbon\Carbon::now()->subMonths($i))),
                Locale::number($user->calcMonthBonuses(Carbon\Carbon::now()->subMonths($i))),
                Locale::number($user->calcMonthTotalToPay(Carbon\Carbon::now()->subMonths($i)))
            );
        }


        Excel::create($fileName, function($excel) use ($dateTime,$data){

            // Set the title
            $excel->setTitle('User '.$dateTime);

            // Chain the setters
            $excel->setCreator('Proassist.ru')
                ->setCompany('Proassist.ru');

            // Call them separately
            $excel->setDescription('User '.$dateTime);

            $excel->sheet($dateTime, function($sheet) use($data) {
                $sheet->setColumnFormat(array(
                    'B' => '0.00',
                    'C' => '0',
                    'D' => '0',
                    'E' => '0',
                    'F' => '0.00',
                    'G' => '0.00',
                    'H' => '0.00',
                    'I' => '0.00',
                    'J' => '0.00',
                ));
                $sheet->fromArray($data);

            });

        })->export('xls');
    }

    //protected $layout = 'admin.layouts.master';
    public function getUserManagement()
    {
        //access rights to method
        if (!Authority::can('view_users'))
            return Redirect::to(Config::get('app.restricted_url'));
        //$this->layout->with('pagename', 'index');
        return View::make('userManagement.index');
    }

    public function getIndex()
    {
        //access rights to method
        if (!Authority::can('view_users'))
            return Redirect::to(Config::get('app.restricted_url'));
        //$this->layout->with('pagename', 'index');
        $users = User::orderBy('deleted_at','asc')->orderBy('id','desc')->withTrashed();

        if (Input::has('search'))
        {
            $search =  Input::get('search');
            $users = $users->where('username','LIKE', '%'.$search.'%');
        }
        $totalUsers = $users->count();
        $users = $users->paginate(Setting::getValKey('users_per_page'));

        return View::make('userManagement.index')->with('users', $users)->with('totalUsers', $totalUsers);
    }

    public function getRoles()
    {
        //access rights to method
        if (!Authority::can('view_roles'))
            return Redirect::to(Config::get('app.restricted_url'));
        $roles = Role::orderBy('id', 'desc');
        if (Input::has('search'))
        {
            $search =  Input::get('search');
            $roles = $roles->where('name','LIKE', '%'.$search.'%');
        }
        $roles = $roles->paginate(Setting::getValKey('roles_per_page'));

        return View::make('userManagement.roles')->with('roles', $roles);
    }

    public function getModalAddRole()
    {
        //access rights to method
        if (!Authority::can('add_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $rights = Rightsgroup::All();
        return View::make('userManagement.addRole')->with('rights', $rights);
    }

    public function postAddRole()
    {
        //access rights to method
        if (!Authority::can('add_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = new Role();
        $role->name = Input::get('name');
        $role->active = Input::get('active');
        $role->save();

        $rights = Right::all();
        foreach ($rights as $right) {
            if (Input::has('right_' . $right->id)) {
                $role->rights()->attach($right->id);
            }
        }

    }

    public function getEditRole($id)
    {
        //access rights to method
        if (!Authority::can('edit_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find($id);
        $rightsGroups = Rightsgroup::All();
        return View::make('userManagement.editRole')->with('role', $role)->with('rightsGroups',$rightsGroups);
    }

    public function postEditRoleData()
    {
        //access rights to method
        if (!Authority::can('edit_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find(Input::get('id_role'));
        $role->name = Input::get('name');
        $role->save();
    }

    public function postEditRoleRights()
    {
        //access rights to method
        if (!Authority::can('edit_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find(Input::get('id_role'));
        $rights = Right::all();
        foreach ($rights as $right) {
            if (Input::has('right_' . $right->id)) {
                if ($role->rights()->where('right_id','=',$right->id)->count()<=0)
                {
                    $role->rights()->attach($right->id);
                }
            }else{
                if ($role->rights()->where('right_id','=',$right->id)->count()>=1)
                {
                    $role->rights()->detach($right->id);
                }
            }
        }
    }

    public function postActivateRole($id)
    {
        //access rights to method
        if (!Authority::can('edit_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find($id);
        if ($role->active == 0)
            $role->active = 1;
        else
            $role->active = 0;
        $role->save();
        return $role->active;
    }

    public function postDeleteRole($id)
    {
        //access rights to method
        if (!Authority::can('delete_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find($id);
        $role->delete();
    }


    public function getModalAddUser()
    {
        //access rights to method
        if (!Authority::can('add_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $roles = Role::All();
        return View::make('userManagement.addUser')->with('roles', $roles);
    }

    public function postUploadImageUser()
    {
        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $id = Input::get('user_id');

        if ($id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find($id);
        //die(print_r($cat->hasImage()));
        if ($user->hasImage())
            $user->removeImage();
        $img = $user->addImage(Input::file('user_image'));
        $user->save();
        return 'true';
    }

    public function postRemoveImageUser($id)
    {
        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find($id);
        if ($id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        if ($user->hasImage())
            $user->removeImage();
        $user->save();
        return 'true';
    }

    public function postAddUser()
    {
        //access rights to method
        if (!Authority::can('add_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = new User();
        $user->username = Input::get('username');
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->secondname = Input::get('secondname');
        $user->work_phone = Input::get('work_phone');
        $user->home_phone = Input::get('home_phone');
        $user->email = Input::get('email');
        $user->active = Input::get('active');
        $user->password = Hash::make(Input::get('password'));
        if (Input::has('force_login'))
            $user->force_login = Input::get('force_login');
        else
            $user->force_login = Input::get('force_login');
        $user->save();
        $roles = Right::all();
        foreach ($roles as $role) {
            if (Input::has('role_' . $role->id)) {
                $user->roles()->attach($role->id);
            }
        }
    }

    public function postEditPassword()
    {

        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find(Input::get('id_user'));
        if ($user->id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user->password = Hash::make(Input::get('password'));
        $user->save();
    }

    public function getRights()
    {
        //access rights to method
        if (!Authority::can('view_rights'))
            return Redirect::to(Config::get('app.restricted_url'));

        $rights = Rightsgroup::All();
        return View::make('userManagement.rights')->with('rights', $rights);
    }

    public function postRestoreUser($id)
    {
        //access rights to method
        if (!Authority::can('delete_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        User::onlyTrashed()->where('id', $id)->restore();
    }

    public function getRole($id)
    {
        //access rights to method
        if (!Authority::can('view_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $role = Role::find($id);
        return View::make('userManagement.role')->with('role', $role);
    }

    public function getEditUser($id)
    {
        //access rights to method
        if (!Authority::can('edit_role'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find($id);

        if ($user->id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        $roles = Role::All();
        return View::make('userManagement.editUser')->with('user', $user)->with('roles',$roles);
    }

    public function postEditUserRoles()
    {
        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find(Input::get('id_user'));
        $roles = Role::all();
        foreach ($roles as $role) {
            if (Input::has('role_' . $role->id)) {
                if ($user->roles()->where('role_id','=',$role->id)->count()<=0)
                {
                    $user->roles()->attach($role->id);
                }
            }else{
                if ($user->roles()->where('role_id','=',$role->id)->count()>=1)
                {
                    $user->roles()->detach($role->id);
                }
            }
        }
    }

    public function postDeleteUser($id)
    {
        //access rights to method
        if (!Authority::can('delete_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user = User::find($id);

        if ($user->id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        $physic = Input::get('physic');
        if ($physic)
        {
            $user->deleteAllRelations();
            $user->forceDelete();
        }
        else
            $user->delete();
    }

    public function postEditUserData()
    {
        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));

        $id = Input::get('id_user');
        $user = User::find($id);

        if ($user->id==Config::get('app.super_admin') && Auth::user()->id!=Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->secondname = Input::get('secondname');
        $user->work_phone = Input::get('work_phone');
        $user->home_phone = Input::get('home_phone');
        $user->email = Input::get('email');
        $user->save();
    }

    public function postActivateUser($id)
    {
        //access rights to method
        if (!Authority::can('edit_user'))
            return Redirect::to(Config::get('app.restricted_url'));


        $user = User::find($id);

        if ($user->id==Config::get('app.super_admin'))
            return Redirect::to(Config::get('app.restricted_url'));

        if ($user->active == 0)
            $user->active = 1;
        else
            $user->active = 0;
        $user->save();
        return $user->active;
    }

    public function postCheckUsername()
    {
        $username = Input::get('username');
        if (Input::has('user'))
            return User::where('username','=',$username)->where('id','<>',Input::get('user'))->count();
        else
            return User::where('username','=',$username)->count();
    }

    public function postCheckEmail()
    {
        $email = Input::get('email');
        if (Input::has('user'))
            return User::where('email','=',$email)->where('id','<>',Input::get('user'))->count();
        else
            return User::where('email','=',$email)->count();
    }

    public function getJsonUsers()
    {
        return User::get(array('username'));
    }

    public function getJsonRoles()
    {
        return Role::get(array('name'));
    }

}