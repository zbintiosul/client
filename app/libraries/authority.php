<?php

class Authority {

    /**
     * Check if user is active and has access for specified right.
     *
     * @return bool
     */
    public static function can($right)
    {
        if (Authority::is_active())
        {
            if (Authority::has_access($right))
            {
                return true;
            }
        }
        return false;
    }

    public static function canOnly($right)
    {
        if (Authority::is_active())
        {
            if (Auth::user()->has_right($right))
            {
                return true;
            }
        }
        return false;
    }

    public static function can_any($rights)
    {
        if (Authority::is_active())
        {
            if (Authority::has_any_access($rights))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if user has access for specified right in active role.
     *
     * @return bool
     */
    public static function has_access($right)
    {
        $user = User::find(Auth::user()->id);
        if (Authority::is_super_admin() || $user->has_right($right))
        {
            return true;
        }else return false;
    }

    public static function has_any_access($rights)
    {
        $user = User::find(Auth::user()->id);
        if (Authority::is_super_admin() || $user->has_any_right($rights))
        {
            return true;
        }else return false;
    }

    public static function is_super_admin()
    {
        if (Auth::user()->id==Config::get('app.super_admin'))
            return true;
        else
            return false;

    }
    /**
     * Check if user is active and is not deleted.
     *
     * @return bool
     */
    public static function is_active()
    {
        if (Auth::check())
            $user = User::find(Auth::user()->id);
        else return false;

        if ($user->active==0 || $user->trashed())
        {
            return false;
        }else return true;
    }

    /*
    public static function is_active_by_username($name)
    {
        $user =  User::where('username','=', $name)->first();

        if ($user!=null && $user->active==1 && $user->id_in_active_role()==false)
        {
            return true;
        }else return false;
    }*/
}