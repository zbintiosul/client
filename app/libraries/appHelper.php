<?php

class AppHelper {

    public static function has_value($value)
    {
        if (!empty($value) && trim($value)!=' ')
            return true;
        else
            return false;
    }

    public static function getDay($date)
    {
        try{
            if (is_null($date) || $date==='0000-00-00')
                return '';

            $dt  = Carbon\Carbon::parse($date);
            $day = $dt->day;
            if (strlen($day)==1)
                $day = '0'.$day;

            return $day;
        }catch (Exception $ex){
            return '';
        }
    }

    public static function getMonth($date)
    {
        try{
            if (is_null($date) || $date==='0000-00-00')
                return '';

            $dt  = Carbon\Carbon::parse($date);
            $month = $dt->month;
            if (strlen($month)==1)
                $month = '0'.$month;

            return $month;
        }catch (Exception $ex){
            return '';
        }
    }

    public static function getYear($date)
    {
        try{
            if (is_null($date) || $date==='0000-00-00')
                return '';
            $dt  = Carbon\Carbon::parse($date);
            return $dt->year;
        }catch (Exception $ex){
            return '';
        }
    }

    public static function conDate($dateString)
    {
        try{
        if (is_null($dateString) || $dateString=='00-00-0000' || $dateString=='' || empty($dateString))
            return '';

            $dt = Carbon\Carbon::createFromFormat('d-m-Y', $dateString);
            return $dt->toDateString();
        }catch (Exception $ex){
            return '';
        }

    }

    public static function conInvDate($dateString)
    {
        $return = '';
        //die(print_r($dateString));
        if(strtotime($dateString) === false)
            return '';

        $dt = Carbon\Carbon::parse($dateString);
        $return = $dt->format('d-m-Y');
        //die(print_r($dt->format('d-m-Y')));
        return  $return;
    }

}