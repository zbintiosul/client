<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'success' => 'Успешно!',
    'successText' => 'Успешное завершение действий.',

	'error'     => 'Ошибка!',
    'errorText'     => 'Во время выполнения задачи произошла ошибка, повторите попытку позже или сообщите об ошибке администратору.',

);