<?php

return array(

//ribbon breadcrumbs config
//array("Display Name" => "URL");
    'breadcrumbs' => array(
        "Home" => Config::get('app.url')
    ),

    /*navigation array config

    ex:
    "dashboard" => array(
        "title" => "Display Title",
        "url" => "http://yoururl.com",
        "icon" => "fa-home"
        "label_htm" => "<span>Add your custom label/badge html here</span>",
        "sub" => array() //contains array of sub items with the same format as the parent
    )
    */
    'page_nav' => array(
        "dashboard" => array(
            "title" => "Мой профиль",
            "url" => action('ProfileController@getIndex'),
            "icon" => "fa-home",
            "access" => null
        ),
        "customers" => array(
            "title" => "Клиенты",
            "icon" => "fa-phone",
            "sub" => array(
                "all_customers" => array(
                    "title" => "Все",
                    "url" => action('ClientController@getIndex'),
                    'access' => 'view_all_clients'
                ),
                "new_customers" => array(
                    "title" => "Новые",
                    "url" => action('ClientController@getNewClients'),
                    "label_htm" => '<span id="newClientsBadge" class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrNewClients().'</span>',
                    'access' => 'view_new_clients'
                ),
                "customers_in_process" => array(
                    "title" => "В процессе",
                    "url" => action('ClientController@getInProgressClients'),
                    "label_htm" => '<span id="inProgressBadge" class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrInProgressClients().'</span>',
                    'access' => 'view_in_process_clients'
                ),
                "callback" => array(
                    "title" => "Перезвонить",
                    "url" => action('ClientController@getCallBack'),
                    "label_htm" => '<span id="inCallBack" class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrCallBack().'</span>',
                    'access' => 'view_in_process_clients'
                ),
                "customers_not_interested" => array(
                    "title" => "Незаинтересованные",
                    "url" => action('ClientController@getNotInterestedClients'),
                    "label_htm" => '<span id="notInterestedBadge" class="badge pull-right inbox-badge bg-color-greenLight">'.Client::nrNotInterestedClients().'</span>',
                    'access' => 'view_not_interested_clients',
                    'li_id' => 'customers_not_interested'
                ),
                "customers_completed" => array(
                    "title" => "Завершенные",
                    "url" => action('ClientController@getCompletedClients'),
                    "label_htm" => '<span  class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrCompletedClients().'</span>',
                    'access' => 'view_completed_clients'
                )
            ),
            "access_any" => array('view_all_clients', 'view_new_clients', 'view_in_process_clients','view_not_interested_clients','view_completed_clients')
        ),
        "policies" => array(
            "title" => "Страховки",
            "icon" => "fa-file-text",
            "sub" => array(
                "polices_to_approve" => array(
                    "title" => "Ждут подтверждения",
                    "url" => action('ClientController@getClientToApproval'),
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-greenLight">'.Policy::where('status_policy_id','=','2')->count().'</span>',
                    'access' => 'approve_clients'
                ),
                "policies_to_sent" => array(
                    "title" => "Отправленые брокеру",
                    "url" => action('ClientController@getClientsSentToBroker'),
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-greenLight">'.Policy::where('status_policy_id','=','4')->count().'</span>',
                    'access' => 'view_sent_to_broker_policies'
                ),
                "policies_completed" => array(
                    "title" => "Завершенные",
                    "url" => action('ClientController@getCompletedPolicies'),
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrCompletedPolicies().'</span>',
                    'access' => 'view_completed_policies'
                ),
                "policies_income" => array(
                    "title" => "Доход",
                    "url" => action('ClientController@getIncome'),
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->nrCompletedPolicies().'</span>',
                    'access' => 'view_income_policies'
                )
            ),
            "access_any" => array('approve_clients','view_sent_to_broker_policies','view_completed_policies')
        ),
        "notifications" => array(
            "title" => "Уведомления",
            "icon" => "fa-bell",
            "sub" => array(
                "new_notice" => array(
                    "title" => "Новое уведомление",
                    "url" => action('NotificationController@getNewNote'),
                    'access' => 'add_notice'
                ),
                "all_notifications" => array(
                    "title" => "Все уведомления",
                    "url" => action('NotificationController@getIndex')
                )
            )
        ),
        "usersmanagement" => array(
            "title" => "Пользователи",
            "icon" => "fa-group",
            "sub" => array(
                "users" => array(
                    "title" => "Все",
                    "url" => action('UserManagementController@getIndex'),
                    "access" => 'view_users'
                ),
                "roles" => array(
                    "title" => "Роли",
                    "url" => action('UserManagementController@getRoles'),
                    "access" => 'view_roles'
                ),
                "rights" => array(
                    "title" => "Права",
                    "url" => action('UserManagementController@getRights'),
                    "access" => 'view_rights'
                )
            ),
            "access_any" => array('view_users', 'view_roles', 'view_rights')
        ),
        "settings" => array(
            "title" => "Настройки",
            "icon" => "fa-cogs",
            "sub" => array(
                "users" => array(
                    "title" => "Пользователи",
                    "url" => action('SettingController@getUsers'),
                    "access" => 'user_settings'
                ),
                "tools" => array(
                    "title" => "Инструменты",
                    "url" => action('SettingController@getTools'),
                    "access" => 'view_tools'
                ),
                "salary" => array(
                    "title" => "Зарплата",
                    "url" => action('SettingController@getSalary'),
                    "access" => 'other_settings'
                ),
                "companies" => array(
                    "title" => "Фирмы",
                    "url" => action('CompanyController@getIndex'),
                    "access" => 'companies'
                ),
                "history" => array(
                    "title" => "History",
                    "url" => action('SettingController@getHistory'),
                    "access" => 'companies'
                ),
                "other" => array(
                    "title" => "Другие",
                    "url" => action('SettingController@getOther'),
                    "access" => 'other_settings'
                )
            ),
            "access_any" => array('user_settings', 'other_settings','view_tools')
        ),
        "broker" => array(
            "title" => "Брокер",
            "icon" => "fa-suitcase",
            "sub" => array(
                "insurances_for_approval" => array(
                    "title" => "Страховки для утверждения",
                    "url" => action('BrokerController@getInsuranceForApproval'),
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-greenLight">'.Auth::user()->broker_policies()->wherePivot('approved','0')->count().'</span>',
                    "access" => 'is_broker'
                ),
                "all_insurances" => array(
                    "title" => "Все мои страховки",
                    "url" => action('BrokerController@getAllInsurances')
                ),
            ),
            "access_any" => array('is_broker')
        )

        /*"graphs" => array(
            "title" => "Graphs",
            "icon" => "fa-bar-chart-o",
            "sub" => array(
                "flot" => array(
                    "title" => "Flot Chart",
                    "url" => "ajax/flot.php"
                ),
                "morris" => array(
                    "title" => "Morris Charts",
                    "url" => "ajax/morris.php"
                ),
                "inline" => array(
                    "title" => "Inline Charts",
                    "url" => "ajax/inline-charts.php"
                )
            )
        ),
        "tables" => array(
            "title" => "Tables",
            "icon" => "fa-table",
            "sub" => array(
                "normal" => array(
                    "title" => "Normal Tables",
                    "url" => "ajax/table.php"
                ),
                "data" => array(
                    "title" => "Data Tables",
                    "url" => "ajax/datatables.php"
                )
            )
        ),
        "forms" => array(
            "title" => "Forms",
            "icon" => "fa-pencil-square-o",
            "sub" => array(
                "smart_elements" => array(
                    "title" => "Smart Form Elements",
                    "url" => "ajax/form-elements.php"
                ),
                "smart_layout" => array(
                    "title" => "Smart Form Layouts",
                    "url" => "ajax/form-templates.php"
                ),
                "smart_validation" => array(
                    "title" => "Smart Form Validation",
                    "url" => "ajax/validation.php"
                ),
                "bootstrap_forms" => array(
                    "title" => "Bootstrap Form Elements",
                    "url" => "ajax/bootstrap-forms.php"
                ),
                "form_plugins" => array(
                    "title" => "Form Plugins",
                    "url" => "ajax/plugins.php"
                ),
                "wizards" => array(
                    "title" => "Wizards",
                    "url" => "ajax/wizard.php"
                ),
                "bootstrap_editors" => array(
                    "title" => "Bootstrap Editors",
                    "url" => "ajax/other-editors.php"
                ),
                "dropzone" => array(
                    "title" => "Dropzone",
                    "url" => "ajax/dropzone.php",
                    "label_htm" => '<span class="badge pull-right inbox-badge bg-color-yellow">new</span>'
                )
            )
        ),
        "ui_elements" => array(
            "title" => "UI Elements",
            "icon" => "fa-desktop",
            "sub" => array(
                "general" => array(
                    "title" => "General Elements",
                    "url" => "ajax/general-elements.php"
                ),
                "buttons" => array(
                    "title" => "Buttons",
                    "url" => "ajax/buttons.php"
                ),
                "icons" => array(
                    "title" => "Icons",
                    "sub" => array(
                        "fa" => array(
                            "title" => "Font Awesome",
                            "icon" => "fa-plane",
                            "url" => "fa.php"
                        ),
                        "glyph" => array(
                            "title" => "Glyph Icons",
                            "icon" => "fa-plane",
                            "url" => "glyph.php"
                        )
                    )
                ),
                "grid" => array(
                    "title" => "Grid",
                    "url" => "ajax/grid.php"
                ),
                "tree_view" => array(
                    "title" => "Tree View",
                    "url" => "ajax/treeview.php"
                ),
                "nestable_lists" => array(
                    "title" => "Nestable Lists",
                    "url" => "ajax/nestable-list.php"
                ),
                "jquery_ui" => array(
                    "title" => "jQuery UI",
                    "url" => "ajax/jqui.php"
                )
            )
        ),
        "nav6" => array(
            "title" => "6 Level Navigation",
            "icon" => "fa-folder-open",
            "sub" => array(
                "second_lvl" => array(
                    "title" => "2nd Level",
                    "icon" => "fa-folder-open",
                    "sub" => array(
                        "third_lvl" => array(
                            "title" => "3rd Level",
                            "icon" => "fa-folder-open",
                            "sub" => array(
                                "file" => array(
                                    "title" => "File",
                                    "icon" => "fa-file-text"
                                ),
                                "fourth_lvl" => array(
                                    "title" => "4th Level",
                                    "icon" => "fa-folder-open",
                                    "sub" => array(
                                        "file" => array(
                                            "title" => "File",
                                            "icon" => "fa-file-text"
                                        ),
                                        "fifth_lvl" => array(
                                            "title" => "5th Level",
                                            "icon" => "fa-folder-open",
                                            "sub" => array(
                                                "file" => array(
                                                    "title" => "File",
                                                    "icon" => "fa-file-text"
                                                ),
                                                "file" => array(
                                                    "title" => "File",
                                                    "icon" => "fa-file-text"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                "folder" => array(
                    "title" => "Folder",
                    "icon" => "fa-folder-open",
                    "sub" => array(
                        "third_lvl" => array(
                            "title" => "3rd Level",
                            "icon" => "fa-folder-open",
                            "sub" => array(
                                "file1" => array(
                                    "title" => "File",
                                    "icon" => "fa-file-text"
                                ),
                                "file2" => array(
                                    "title" => "File",
                                    "icon" => "fa-file-text"
                                )
                            )
                        )
                    )
                )
            )
        ),
        "cal" => array(
            "title" => "Calendar",
            "url" => "ajax/calendar.php",
            "icon" => "fa-calendar",
            "icon_badge" => "3"
        ),
        "widgets" => array(
            "title" => "Widgets",
            "url" => "ajax/widgets.php",
            "icon" => "fa-list-alt"
        ),
        "gallery" => array(
            "title" => "Gallery",
            "url" => "ajax/gallery.php",
            "icon" => "fa-picture-o"
        ),
        "gmap_skins" => array(
            "title" => "Google Map Skins",
            "url" => "ajax/gmap-xml.php",
            "icon" => "fa-map-marker",
            "label_htm" => '<span class="badge bg-color-greenLight pull-right inbox-badge">9</span>'
        ),
        "misc" => array(
            "title" => "Miscellaneous",
            "icon" => "fa-windows",
            "sub" => array(
                "typo" => array(
                    "title" => "Typography",
                    "url" => "ajax/typography.php"
                ),
                "pricing_tables" => array(
                    "title" => "Pricing Tables",
                    "url" => "ajax/pricing-table.php"
                ),
                "invoice" => array(
                    "title" => "Invoice",
                    "url" => "ajax/invoice.php"
                ),
                "login" => array(
                    "title" => "Login",
                    "url" => Config::get('app.url')."/login.php",
                    "ajax" => false
                ),
                "register" => array(
                    "title" => "Register",
                    "url" => Config::get('app.url')."/register.php",
                    "ajax" => false
                ),
                "lock" => array(
                    "title" => "Lock Screen",
                    "url" => Config::get('app.url')."/lock.php",
                    "ajax" => false
                ),
                "err_404" => array(
                    "title" => "Error 404",
                    "url" => "ajax/error404.php"
                ),
                "err_500" => array(
                    "title" => "Error 500",
                    "url" => "ajax/error500.php"
                ),
                "blank" => array(
                    "title" => "Blank Page",
                    "icon" => "fa-file",
                    "url" => "ajax/blank_.php"
                ),
                "email_template" => array(
                    "title" => "Email Template",
                    "url" => "ajax/email-template.php"
                ),
                "search" => array(
                    "title" => "Search Page",
                    "url" => "ajax/search.php"
                ),
                "ck_editor" => array(
                    "title" => "CK Editor",
                    "url" => "ajax/ckeditor.php"
                ),
            )
        ),
        "others" => array(
            "title" => "Other Pages",
            "icon" => "fa-file",
            "sub" => array(
                "forum" => array(
                    "title" => "Forum Layout",
                    "url" => "ajax/forum.php"
                ),
                "profile" => array(
                    "title" => "Profile",
                    "url" => "ajax/profile.php"
                ),
                "timeline" => array(
                    "title" => "Timeline",
                    "url" => "ajax/timeline.php"
                )
            )
        )*/
    ),
);