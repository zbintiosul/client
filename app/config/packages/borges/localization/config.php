<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    // Default language
    'locale'           => 'ru',

    // Use default language if the translation line doesn't exist
    'useDefault'       => true,

    // Languages that are allowed in routes
    'availableLocales' => array('ru', 'ro','en'),

    /*
    |--------------------------------------------------------------------------
    | Application use system Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    // Use user language 
    // Set to false, to use Session Language
    'useUserLanguage'       => false,
    // 'useUserLanguage'       => function () { 
    //     if(Auth::check()) {
    //         return Auth::user()->language;
    //     }
    //     return false;
    // },

    // Take the browser language if it's not defined in the route? 
    // If false, system will take app.php locale attribute 
    'useSessionLanguage'    => false,

    // Take the browser language if it's not defined in the route? 
    // If false, system will take app.php locale attribute 
    'useBrowserLanguage'    => false,

    /*
    |--------------------------------------------------------------------------
    | Application Locales Definitions
    |--------------------------------------------------------------------------
    | http://www.i18nguy.com/unicode/language-identifiers.html
    */
    'locales' => array(
        'en' => array(
            'name' => 'English',
            'iso_code' => 'en',
            'language_code' => 'en',
            'date_format' => '%m/%e/%Y',
            'time_format' => '%l:%M %p',
            'datetime_format' => '%m/%e/%Y, %l:%M %p',
            'date_longformat' => '%A, %B %e, %Y',
            'time_longformat' => '%l:%M:%S %p',
            'datetime_longformat' => '%A, %B %e, %Y at %l:%M %p',
            'dec_point' => '.',
            'thousands_sep' => ' '),
        'ro' => array(
            'name' => 'Română',
            'iso_code' => 'ro',
            'language_code' => 'ro',
            'date_format' => '%dd-%MMM-%yy',
            'time_format' => '%HH:%mm',
            'datetime_format' => '%dd-%MMM-%yy, %HH:%mm',
            'date_longformat' => '%dddd, %MMMM %d, %yyyy',
            'time_longformat' => '%HH:%mm:%ss',
            'datetime_longformat' => '%dddd, %MMMM %d, %yyyy, %HH:%mm:%ss',
            'dec_point' => '.',
            'thousands_sep' => ' '),
        'ru' => array(
            'name' => 'Pусский',
            'iso_code' => 'ru',
            'language_code' => 'ru',
            'date_format' => '%m/%e/%Y',
            'time_format' => '%l:%M %p',
            'datetime_format' => '%m/%e/%Y, %l:%M %p',
            'date_longformat' => '%A, %B %e, %Y',
            'time_longformat' => '%l:%M:%S %p',
            'datetime_longformat' => '%A, %B %e, %Y at %l:%M %p',
            'dec_point' => '.',
            'thousands_sep' => ' '),
        'en-US' => array(
            'name' => 'English (United States)',
            'inherit' => 'en',
            'iso_code' => 'en',
            'language_code' => 'en-US'),

    )
);
