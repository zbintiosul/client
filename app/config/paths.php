<?php

return array(

    'tools_img' => storage_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'tools',

    'users_img' => storage_path().DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'users',

    'clients_files' => storage_path().DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'clients',

);
