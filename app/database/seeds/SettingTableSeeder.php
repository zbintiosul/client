<?php

class SettingTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('settings')->delete();

		// users_per_page
		Setting::create(array(
				'name' => 'Пользователи на странице',
				'description' => 'Сколько пользователей будут на одну страницу',
				'key' => 'users_per_page',
				'value' => 25
			));

		// roles_per_page
		Setting::create(array(
				'name' => 'Роли на странице',
				'description' => 'Сколько ролей будут на одну страницу',
				'key' => 'roles_per_page',
				'value' => 25
			));
	}
}