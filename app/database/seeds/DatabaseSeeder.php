<?php

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Eloquent::unguard();

		//$this->call('UserTableSeeder');
		//$this->command->info('User table seeded!');

		//$this->call('RightsgroupTableSeeder');
		//$this->command->info('Rightsgroup table seeded!');

		//$this->call('RightTableSeeder');
		//$this->command->info('Right table seeded!');

        //$this->call('RightTableSeeder2');
        //$this->command->info('Right table seeded 2!');

        //$this->call('SettingTableSeeder');
		//$this->command->info('Setting table seeded!');
		
		//$this->call('NotificationTypeTableSeeder');
		//$this->command->info('NotificationType table seeded!');

//		$this->call('StatusClientTableSeeder');
//		$this->command->info('StatusClient table seeded!');
//
//		$this->call('StatusPolicyTableSeeder');
//		$this->command->info('StatusPolicy table seeded!');
//
//		$this->call('TypeConsumTableSeeder');
//		$this->command->info('TypeConsum table seeded!'); PotentialClientTypeTableSeeder

        $this->call('PotentialClientTypeTableSeeder');
		$this->command->info('PotentialClientTypeTableSeeder table seeded!');
	}
}