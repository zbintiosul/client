<?php

class NotificationTypeTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('notification_type')->delete();

		// warning
		NotificationType::create(array(
				'name' => 'Предупреждение',
				'description' => 'Предупреждение'
			));

		// success
		NotificationType::create(array(
				'name' => 'Успех',
				'description' => 'Успех'
			));

		// info
		NotificationType::create(array(
				'name' => 'Инфо',
				'description' => 'Инфо'
			));

		// error
		NotificationType::create(array(
				'name' => 'Ошибка',
				'description' => 'Ошибка'
			));

		// default
		NotificationType::create(array(
				'name' => 'По умолчанию',
				'description' => 'По умолчанию'
			));
	}
}