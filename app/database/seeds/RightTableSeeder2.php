<?php

class RightTableSeeder2 extends Seeder {

	public function run()
	{
		//DB::table('rights')->delete();

		// view users
		Right::create(array(
				'name' => 'Настройки пользователи',
				'desc' => 'Настройки пользователи',
				'rights_group_id' => 1,
				'key' => 'user_settings'
			));

        Right::create(array(
            'name' => 'Настройки другие ',
            'desc' => 'Настройки другие ',
            'rights_group_id' => 1,
            'key' => 'other_settings'
        ));

        Right::create(array(
            'name' => 'Настройки другие ',
            'desc' => 'Настройки другие ',
            'rights_group_id' => 1,
            'key' => 'users_money_info'
        ));

	}
}