<?php

class TypeConsumTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('type_consum')->delete();

		// bezin
		TypeConsum::create(array(
				'name' => 'Бензин'
			));

		// diesel
		TypeConsum::create(array(
				'name' => 'Дизтопливо'
			));

		// gaz benzina
		TypeConsum::create(array(
				'name' => 'Газ-бензин'
			));
	}
}