<?php

class StatusClientTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('status_client')->delete();

		// new
		StatusClient::create(array(
				'name' => 'Новый',
				'desc' => 'Новый'
			));

		// in progreass
		StatusClient::create(array(
				'name' => 'В процессе',
				'desc' => 'В процессе'
			));

		// uninterested
		StatusClient::create(array(
				'name' => 'Незаинтересованный',
				'desc' => 'Незаинтересованный'
			));

		// sent
		StatusClient::create(array(
				'name' => 'Отправлен',
				'desc' => 'Отправлен агенту'
			));
	}
}