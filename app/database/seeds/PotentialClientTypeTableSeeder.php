<?php

class PotentialClientTypeTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('potential_client_type')->delete();

		// Prospect
		PotentialClientType::create(array(
				'name' => 'Проспект'
			));

		// bezperspectivnii
		PotentialClientType::create(array(
				'name' => 'Бесперспективный'
			));
	}
}