<?php

class RightTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('rights')->delete();

		// view users
		Right::create(array(
				'name' => 'Просмотр пользователи',
				'desc' => 'Просмотр пользователи',
				'rights_group_id' => 1,
				'key' => 'view_users'
			));

		// add user
		Right::create(array(
				'name' => 'Добавить пользователь',
				'desc' => 'Добавить пользователь',
				'rights_group_id' => 1,
				'key' => 'add_user'
			));

		// edit user
		Right::create(array(
				'key' => 'edit_user',
				'name' => 'Редактировать пользователи',
				'desc' => 'Редактировать пользователи',
				'rights_group_id' => 1
			));

		// delete user
		Right::create(array(
				'key' => 'delete_user',
				'name' => 'Удалять пользователи',
				'desc' => 'Удалять пользователи',
				'rights_group_id' => 1
			));

		// view roles
		Right::create(array(
				'key' => 'view_roles',
				'name' => 'Просмотр роли',
				'desc' => 'Просмотр роли',
				'rights_group_id' => 1
			));

		// add role
		Right::create(array(
				'key' => 'add_role',
				'name' => 'Добавить роли',
				'desc' => 'Добавить роли',
				'rights_group_id' => 1
			));

		// edit role
		Right::create(array(
				'key' => 'edit_role',
				'name' => 'Редактировать роли',
				'desc' => 'Редактировать роли',
				'rights_group_id' => 1
			));

		// delete role
		Right::create(array(
				'key' => 'delete_role',
				'name' => 'Удалять роли',
				'desc' => 'Удалять роли',
				'rights_group_id' => 1
			));

		// view rights
		Right::create(array(
				'key' => 'view_rights',
				'name' => 'Просмотр права',
				'desc' => 'Просмотр права',
				'rights_group_id' => 1
			));
	}
}