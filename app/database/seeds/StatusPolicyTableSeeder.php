<?php

class StatusPolicyTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('status_policy')->delete();

		// new
		StatusPolicy::create(array(
				'name' => 'Новое',
				'desc' => 'Новое'
			));

		// sent for approval
		StatusPolicy::create(array(
				'name' => 'Отправлена к Менеджеру',
				'desc' => 'Отправлена для утверждение к Менеджеру'
			));

		// approved
		StatusPolicy::create(array(
				'name' => 'Утверждена Менеджером',
				'desc' => 'Утверждена Менеджером'
			));

		// sent to broker
		StatusPolicy::create(array(
				'name' => 'Отправлена к брокеру',
				'desc' => 'Отправлена к брокеру'
			));

		// approved broker
		StatusPolicy::create(array(
				'name' => 'Утверждена брокером',
				'desc' => 'Утверждена брокером'
			));
	}
}