<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys18 extends Migration {

	public function up()
	{
		Schema::table('clients', function(Blueprint $table) {
			$table->integer('potential_client_type_id')->unsigned()->nullable();
		});
		
		Schema::table('clients', function(Blueprint $table) {
			$table->foreign('potential_client_type_id')->references('id')->on('potential_client_type')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('clients', function(Blueprint $table) {
			$table->dropForeign('clients_potential_client_type_id_foreign');
		});
	}
}