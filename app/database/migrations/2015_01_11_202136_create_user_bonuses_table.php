<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserBonusesTable extends Migration {

	public function up()
	{
		Schema::create('user_bonuses', function(Blueprint $table) {
			$table->increments('id');
			$table->string('reason')->nullable();
			$table->integer('value');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('user_bonuses');
	}
}