<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateImportClientTable extends Migration {

	public function up()
	{
		Schema::table('import_client', function(Blueprint $table) {
			$table->string('FIO')->nullable();
			$table->string('number')->nullable();
			$table->string('srok')->nullable();
			$table->string('brand_model')->nullable();
			$table->string('type')->nullable();
			$table->string('bank')->nullable();
			$table->string('sum')->nullable();
			$table->string('year')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('import_client');
	}
}