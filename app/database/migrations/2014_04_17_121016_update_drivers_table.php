<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateDriversTable extends Migration {

	public function up()
	{
		Schema::table('drivers', function(Blueprint $table) {
			$table->integer('experience')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('drivers');
	}
}