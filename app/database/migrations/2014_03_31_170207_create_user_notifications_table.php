<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('user_notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->integer('notification_id')->unsigned();
			$table->boolean('seen')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('user_notifications');
	}
}