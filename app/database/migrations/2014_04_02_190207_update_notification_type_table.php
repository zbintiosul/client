<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateNotificationTypeTable extends Migration {

	public function up()
	{
		Schema::table('notification_type', function(Blueprint $table) {
			$table->string('class')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('notification_type');
	}
}