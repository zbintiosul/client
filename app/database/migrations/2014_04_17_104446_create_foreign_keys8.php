<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys8 extends Migration {

	public function up()
	{

		/*Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_policy_id_foreign');
		});

		Schema::table('cars', function(Blueprint $table) {
			$table->dropColumn('policy_id');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->integer('car_id')->unsigned();
		});*/

		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('car_id')->references('id')->on('cars')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}
	public function down()
	{
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_car_id_foreign');
		});
		
	}
}