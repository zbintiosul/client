<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatusClientTable extends Migration {

	public function up()
	{
		Schema::create('status_client', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->string('desc');
		});
	}

	public function down()
	{
		Schema::drop('status_client');
	}
}