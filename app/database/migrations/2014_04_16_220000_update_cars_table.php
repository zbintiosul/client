<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateCarsTable extends Migration {

	public function up()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->string('brand')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('cars');
	}
}