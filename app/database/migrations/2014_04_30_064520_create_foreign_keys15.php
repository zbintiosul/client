<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys15 extends Migration {

	public function up()
	{
		Schema::table('client_files', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('client_files', function(Blueprint $table) {
			$table->dropForeign('client_files_client_id_foreign');
		});
	}
}