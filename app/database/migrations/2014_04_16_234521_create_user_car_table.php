<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCarTable extends Migration {

	public function up()
	{
		Schema::create('client_car', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->integer('car_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('user_car');
	}
}