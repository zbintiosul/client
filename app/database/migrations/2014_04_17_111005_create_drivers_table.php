<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriversTable extends Migration {

	public function up()
	{
		Schema::create('drivers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('firstname')->nullable();
			$table->string('lastname')->nullable();
			$table->tinyInteger('age')->nullable();
			$table->integer('policy_id')->unsigned();
			$table->integer('gender_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('drivers');
	}
}