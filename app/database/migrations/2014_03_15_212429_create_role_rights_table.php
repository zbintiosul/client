<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleRightsTable extends Migration {

	public function up()
	{
		Schema::create('role_rights', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('role_id')->unsigned();
			$table->integer('right_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('role_rights');
	}
}