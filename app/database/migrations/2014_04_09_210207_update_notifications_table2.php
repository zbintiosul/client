<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateNotificationsTable2 extends Migration {

	public function up()
	{
		Schema::table('notifications', function(Blueprint $table) {
			$table->boolean('sent')->nullable()->default(0);
		});

	}

	public function down()
	{
		//Schema::drop('notifications');
	}
}