<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMariageStatusTable extends Migration {

	public function up()
	{
		Schema::create('mariage_status', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('mariage_status');
	}
}