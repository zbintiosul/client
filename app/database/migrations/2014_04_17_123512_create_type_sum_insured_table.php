<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTypeSumInsuredTable extends Migration {

	public function up()
	{
		Schema::create('type_sum_insured', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('type_sum_insured');
	}
}