<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTypeCarTable extends Migration {

	public function up()
	{
		Schema::create('type_car', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 150);
		});
	}

	public function down()
	{
		Schema::drop('type_car');
	}
}