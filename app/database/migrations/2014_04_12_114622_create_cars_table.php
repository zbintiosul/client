<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarsTable extends Migration {

	public function up()
	{
		Schema::create('cars', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->float('motor_volum')->nullable();
			$table->string('motor_ls')->nullable();
			$table->string('rigister_region')->nullable();
			$table->string('KBM')->nullable();
			$table->string('number')->nullable();
			$table->string('model')->nullable();
			$table->string('register_number', 100)->nullable();
			$table->string('VIN')->nullable();
			$table->string('category', 150)->nullable();
			$table->string('frame_number')->nullable();
			$table->string('stand_number')->nullable();
			$table->date('year_of_contruction')->nullable();
			$table->integer('type_consum_id')->unsigned();
			$table->string('type')->nullable();
			$table->string('tire_brand')->nullable();
			$table->float('mileage')->nullable();
			$table->integer('policy_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cars');
	}
}