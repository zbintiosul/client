<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoliciesTable extends Migration {

	public function up()
	{
		Schema::create('policies', function(Blueprint $table) {
			$table->timestamps();
			$table->increments('id');
			$table->softDeletes();
			$table->integer('client_id')->unsigned();
			$table->date('date_last_policy')->nullable();
			$table->string('last_policy_company')->nullable();
			$table->float('price_last_policy')->nullable();
			$table->tinyInteger('nr_drivers')->nullable();
			$table->tinyInteger('age')->nullable();
			$table->tinyInteger('licence_age');
			$table->timestamp('date_created')->nullable();
			$table->integer('status_policy_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('policies');
	}
}