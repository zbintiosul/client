<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys12 extends Migration {

	public function up()
	{
		/*Schema::table('broker_policy', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});*/
		Schema::table('broker_policy', function(Blueprint $table) {
			$table->foreign('policy_id')->references('id')->on('policies')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('broker_policy', function(Blueprint $table) {
			$table->dropForeign('broker_policy_user_id_foreign');
		});
		Schema::table('broker_policy', function(Blueprint $table) {
			$table->dropForeign('broker_policy_policy_id_foreign');
		});
	}
}