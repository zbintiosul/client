<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys10 extends Migration {

	public function up()
	{

		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('type_sum_insured_id')->references('id')->on('type_sum_insured')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('insurance_risk_id')->references('id')->on('insurance_risks')
						->onDelete('no action')
						->onUpdate('no action');
		});
		
		Schema::table('drivers', function(Blueprint $table) {
			$table->foreign('mariage_status_id')->references('id')->on('mariage_status')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_type_sum_insured_id_foreign');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_insurance_risk_id_foreign');
		});
		Schema::table('drivers', function(Blueprint $table) {
			$table->dropForeign('drivers_mariage_status_id_foreign');
		});
	}
}