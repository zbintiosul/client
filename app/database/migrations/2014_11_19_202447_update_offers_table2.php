<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateOffersTable2 extends Migration {

	public function up()
	{
        Schema::table('offers', function(Blueprint $table) {
            $table->text('stantii')->nullable();
        });
	}

	public function down()
	{
        Schema::table('offers', function(Blueprint $table) {
            $table->dropColumn('stantii');
        });
	}
}