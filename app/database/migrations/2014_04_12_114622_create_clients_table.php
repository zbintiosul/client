<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('firstname', 150)->nullable();
			$table->string('lastname', 150)->nullable();
			$table->string('secondname', 150)->nullable();
			$table->date('birth_date')->nullable();
			$table->date('date_driver_licence')->nullable();
			$table->string('serial_driver_licence', 100)->nullable();
			$table->string('serial_bulletin', 50)->nullable();
			$table->string('address')->nullable();
			$table->string('phone', 50)->nullable();
			$table->string('time_contact')->nullable();
			$table->boolean('is_driver')->nullable();
			$table->boolean('is_client')->nullable();
			$table->integer('status_client_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}