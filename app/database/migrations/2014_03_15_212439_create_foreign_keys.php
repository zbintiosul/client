<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('rights', function(Blueprint $table) {
			$table->foreign('rights_group_id')->references('id')->on('rights_group')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('role_rights', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('role_rights', function(Blueprint $table) {
			$table->foreign('right_id')->references('id')->on('rights')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('rights', function(Blueprint $table) {
			$table->dropForeign('rights_rights_group_id_foreign');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->dropForeign('user_roles_role_id_foreign');
		});
		Schema::table('user_roles', function(Blueprint $table) {
			$table->dropForeign('user_roles_user_id_foreign');
		});
		Schema::table('role_rights', function(Blueprint $table) {
			$table->dropForeign('role_rights_role_id_foreign');
		});
		Schema::table('role_rights', function(Blueprint $table) {
			$table->dropForeign('role_rights_right_id_foreign');
		});
	}
}