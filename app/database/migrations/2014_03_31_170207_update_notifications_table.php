<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateNotificationsTable extends Migration {

	public function up()
	{
		Schema::table('notifications', function(Blueprint $table) {
			//$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});

	}

	public function down()
	{
		//Schema::drop('notifications');
		Schema::table('notifications', function(Blueprint $table) {
			$table->dropForeign('notifications_user_id_foreign');
		});
	}
}