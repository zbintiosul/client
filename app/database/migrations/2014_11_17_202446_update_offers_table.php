<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateOffersTable extends Migration {

	public function up()
	{
        Schema::table('offers', function(Blueprint $table) {
            //$table->dropColumn('to');
            $table->integer('sent_times')->unsigned();
            $table->double('strahavaia_suma', 15, 2)->nullable()->default(0);
            $table->double('strahavaia_premia', 15, 2)->nullable()->default(0);
            $table->double('fransiza', 15, 2)->nullable()->default(0);
            $table->double('strahavaia_suma_grajdanscaia', 15, 2)->nullable()->default(0);
            $table->double('strahavaia_premia_grajdanscaia', 15, 2)->nullable()->default(0);
            $table->double('strahavaia_suma_dobrovolina', 15, 2)->nullable()->default(0);
            $table->double('strahavaia_premia_dobrovolina', 15, 2)->nullable()->default(0);
            $table->text('comments')->nullable();
            $table->text('signature')->nullable();
        });
	}

	public function down()
	{
        Schema::table('offers', function(Blueprint $table) {
            $table->string('to')->nullable();
            $table->dropColumn('sent_times');
            $table->dropColumn('strahavaia_suma');
            $table->dropColumn('strahavaia_premia');
            $table->dropColumn('fransiza');
            $table->dropColumn('strahavaia_suma_grajdanscaia');
            $table->dropColumn('strahavaia_premia_grajdanscaia');
            $table->dropColumn('strahavaia_suma_dobrovolina');
            $table->dropColumn('strahavaia_premia_dobrovolina');
            $table->dropColumn('comments');
            $table->dropColumn('signature');
        });
	}
}