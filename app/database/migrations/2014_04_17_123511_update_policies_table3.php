<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdatePoliciesTable3 extends Migration {

	public function up()
	{
		Schema::table('policies', function(Blueprint $table) {
			$table->string('firstname_manager')->nullable();
			$table->string('lastname_manager')->nullable();
			$table->string('secondname_manager')->nullable();
			$table->string('nr_casco')->nullable();
			$table->float('sum_now_car')->nullable();
			$table->float('sum_market_car')->nullable();
			$table->boolean('in_credit')->nullable();
			$table->string('bank_credit')->nullable();
			$table->integer('type_sum_insured_id')->unsigned();
			$table->integer('insurance_risk_id')->unsigned();
		});
	}

	public function down()
	{
		//Schema::drop('policies');
	}
}