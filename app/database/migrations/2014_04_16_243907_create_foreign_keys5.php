<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys5 extends Migration {

	public function up()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->integer('region_id')->unsigned();
			//$table->dropColumn('rigister_region');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('region_id')->references('id')->on('regions')
						->onDelete('no action')
						->onUpdate('no action');
		});
		
	}

	public function down()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_region_id_foreign');
		});
	}
}