<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys3 extends Migration {

	public function up()
	{
		Schema::table('clients', function(Blueprint $table) {
			$table->foreign('status_client_id')->references('id')->on('status_client')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('type_consum_id')->references('id')->on('type_consum')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('policy_id')->references('id')->on('policies')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('status_policy_id')->references('id')->on('status_policy')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('user_client', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('user_client', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('clients', function(Blueprint $table) {
			$table->dropForeign('clients_status_client_id_foreign');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_type_consum_id_foreign');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_policy_id_foreign');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_client_id_foreign');
		});
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_status_policy_id_foreign');
		});
		Schema::table('user_client', function(Blueprint $table) {
			$table->dropForeign('user_client_client_id_foreign');
		});
		Schema::table('user_client', function(Blueprint $table) {
			$table->dropForeign('user_client_user_id_foreign');
		});
	}
}