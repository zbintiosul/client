<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdatePoliciesTable extends Migration {

	public function up()
	{
		Schema::table('policies', function(Blueprint $table) {
			$table->integer('type_policy_id')->unsigned();
		});
	}

	public function down()
	{
		//Schema::drop('policies');
	}
}