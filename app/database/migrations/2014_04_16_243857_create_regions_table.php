<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegionsTable extends Migration {

	public function up()
	{
		Schema::create('regions', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('values')->nullable();
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->integer('region_id')->unsigned();
			$table->dropColumn('rigister_region');
		});
	}

	public function down()
	{
		Schema::drop('regions');
	}
}