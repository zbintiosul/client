<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('notification_type_id')->unsigned();
			$table->string('name')->nullable();
			$table->text('text');
			$table->timestamp('appear_time')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}