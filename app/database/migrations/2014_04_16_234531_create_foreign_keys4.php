<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys4 extends Migration {

	public function up()
	{
		Schema::table('user_car', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('user_car', function(Blueprint $table) {
			$table->foreign('car_id')->references('id')->on('cars')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{

		Schema::table('user_car', function(Blueprint $table) {
			$table->dropForeign('user_car_client_id_foreign');
		});
		Schema::table('user_car', function(Blueprint $table) {
			$table->dropForeign('user_car_car_id_foreign');
		});
	}
}