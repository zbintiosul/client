<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys7 extends Migration {

	public function up()
	{

		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('type_gearbox_id')->references('id')->on('type_gearbox')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('type_signaling_id')->references('id')->on('type_signaling')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('type_doc_id')->references('id')->on('type_doc')
						->onDelete('no action')
						->onUpdate('no action');
		});

		Schema::table('policies', function(Blueprint $table) {
			$table->foreign('type_policy_id')->references('id')->on('type_policy')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}
	public function down()
	{

		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_type_gearbox_id_foreign');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_type_signaling_id_foreign');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_type_doc_id_foreign');
		});
		
		Schema::table('policies', function(Blueprint $table) {
			$table->dropForeign('policies_type_policy_id_foreign');
		});
		
	}
}