<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('username')->unique();
			$table->string('email')->unique();
			$table->string('password');
			$table->string('firstname')->nullable();
			$table->string('lastname')->nullable();
			$table->string('secondname')->nullable();
			$table->boolean('active')->default(0);
			$table->string('home_phone');
			$table->string('work_phone');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}