<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateToolsTable extends Migration {

	public function up()
	{
		Schema::create('tools', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('img_path')->nullable();
			$table->text('text')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('tools');
	}
}