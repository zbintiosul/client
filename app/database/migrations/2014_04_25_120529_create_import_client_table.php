<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImportClientTable extends Migration {

	public function up()
	{
		Schema::create('import_client', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->integer('import_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('import_client');
	}
}