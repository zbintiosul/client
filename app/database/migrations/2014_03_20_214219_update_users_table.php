<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUsersTable extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->boolean('force_login')->default(0);
			$table->timestamp('paid_start_time')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('users');
	}
}