<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys14 extends Migration {

	public function up()
	{
		Schema::table('imports', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('import_client', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('import_client', function(Blueprint $table) {
			$table->foreign('import_id')->references('id')->on('imports')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('imports', function(Blueprint $table) {
			$table->dropForeign('imports_user_id_foreign');
		});
		Schema::table('import_client', function(Blueprint $table) {
			$table->dropForeign('import_client_client_id_foreign');
		});
		Schema::table('import_client', function(Blueprint $table) {
			$table->dropForeign('import_client_import_id_foreign');
		});
	}
}