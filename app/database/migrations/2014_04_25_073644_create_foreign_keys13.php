<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys13 extends Migration {

	public function up()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('model_car_id')->references('id')->on('model_cars')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('model_cars', function(Blueprint $table) {
			$table->foreign('brand_car_id')->references('id')->on('brand_cars')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_model_car_id_foreign');
		});
		
		Schema::table('model_cars', function(Blueprint $table) {
			$table->dropForeign('model_cars_brand_car_id_foreign');
		});
	}
}