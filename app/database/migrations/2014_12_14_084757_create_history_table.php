<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoryTable extends Migration {

	public function up()
	{
		Schema::create('history', function(Blueprint $table) {
            $table->engine = 'MyISAM';
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('text')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('history');
	}
}