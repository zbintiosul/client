<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePotentialClientTypeTable extends Migration {

	public function up()
	{
		Schema::create('potential_client_type', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('potential_client_type');
	}
}