<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateCarsTable2 extends Migration {

	public function up()
	{
		Schema::table('cars', function(Blueprint $table) {
			$table->integer('type_gearbox_id')->unsigned();
			$table->integer('type_signaling_id')->unsigned();
			$table->integer('type_doc_id')->unsigned();
			$table->string('doc_serial', 50)->nullable();
			$table->string('doc_number', 50)->nullable();
			$table->string('doc_who')->nullable();
			$table->date('doc_when')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('cars');
	}
}