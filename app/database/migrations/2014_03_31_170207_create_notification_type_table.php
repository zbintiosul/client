<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationTypeTable extends Migration {

	public function up()
	{
		Schema::create('notification_type', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->text('description');
		});
	}

	public function down()
	{
		Schema::drop('notification_type');
	}
}