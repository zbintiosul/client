<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUserNotificationsTable extends Migration {

	public function up()
	{
		Schema::table('user_notifications', function(Blueprint $table) {
			$table->boolean('sent')->nullable()->default(0);
		});
	}

	public function down()
	{
		//Schema::drop('user_notifications');
	}
}