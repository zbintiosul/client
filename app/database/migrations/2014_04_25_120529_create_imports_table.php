<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImportsTable extends Migration {

	public function up()
	{
		Schema::create('imports', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->text('log')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('imports');
	}
}