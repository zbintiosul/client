<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys17 extends Migration {

	public function up()
	{
		Schema::table('offers', function(Blueprint $table) {
			$table->foreign('policy_id')->references('id')->on('policies')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('offers', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{

		Schema::table('offers', function(Blueprint $table) {
			$table->dropForeign('offers_policy_id_foreign');
		});
		Schema::table('offers', function(Blueprint $table) {
			$table->dropForeign('offers_company_id_foreign');
		});
	}
}