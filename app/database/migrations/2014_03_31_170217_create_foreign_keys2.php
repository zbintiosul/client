<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys2 extends Migration {

	public function up()
	{
		Schema::table('notifications', function(Blueprint $table) {
			$table->foreign('notification_type_id')->references('id')->on('notification_type')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('user_notifications', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('user_notifications', function(Blueprint $table) {
			$table->foreign('notification_id')->references('id')->on('notifications')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('notifications', function(Blueprint $table) {
			$table->dropForeign('notifications_notification_type_id_foreign');
		});
		Schema::table('user_notifications', function(Blueprint $table) {
			$table->dropForeign('user_notifications_user_id_foreign');
		});
		Schema::table('user_notifications', function(Blueprint $table) {
			$table->dropForeign('user_notifications_notification_id_foreign');
		});
	}
}