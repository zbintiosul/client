<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUsersTable2 extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->string('photo_path')->nullable();
		});
	}

	public function down()
	{
		//Schema::drop('users');
	}
}