<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys19 extends Migration {

	public function up()
	{
		Schema::table('user_bonuses', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('user_bonuses', function(Blueprint $table) {
			$table->dropForeign('user_bonuses_user_id_foreign');
		});
	}
}