<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModelCarsTable extends Migration {

	public function up()
	{
		Schema::create('model_cars', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->integer('brand_car_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('model_cars');
	}
}