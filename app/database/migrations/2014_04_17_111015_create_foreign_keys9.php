<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys9 extends Migration {

	public function up()
	{
		Schema::table('drivers', function(Blueprint $table) {
			$table->foreign('policy_id')->references('id')->on('policies')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('drivers', function(Blueprint $table) {
			$table->foreign('gender_id')->references('id')->on('genders')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('drivers', function(Blueprint $table) {
			$table->dropForeign('drivers_policy_id_foreign');
		});
		Schema::table('drivers', function(Blueprint $table) {
			$table->dropForeign('drivers_gender_id_foreign');
		});
	}
}