<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys6 extends Migration {

	public function up()
	{
		
		Schema::table('cars', function(Blueprint $table) {
			$table->integer('type_car_id')->unsigned();
			//$table->dropColumn('rigister_region');
		});
		Schema::table('cars', function(Blueprint $table) {
			$table->foreign('type_car_id')->references('id')->on('type_car')
						->onDelete('no action')
						->onUpdate('no action');
		});
		
	}

	public function down()
	{
	
		Schema::table('cars', function(Blueprint $table) {
			$table->dropForeign('cars_type_car_id_foreign');
		});
		
	}
}