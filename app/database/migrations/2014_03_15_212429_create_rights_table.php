<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRightsTable extends Migration {

	public function up()
	{
		Schema::create('rights', function(Blueprint $table) {
			$table->increments('id');
			$table->string('key');
			$table->string('name', 100);
			$table->string('desc', 100);
			$table->integer('rights_group_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('rights');
	}
}