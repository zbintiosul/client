<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrokerPolicyTable extends Migration {

	public function up()
	{
		Schema::create('broker_policy', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->integer('policy_id')->unsigned();
			$table->boolean('approved')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('broker_policy');
	}
}