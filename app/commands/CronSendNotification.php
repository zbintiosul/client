<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CronSendNotification extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cron:send-notification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'send new notifications to users at time';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        Notification::checkAndSendNotifications();
        $this->info('New notifications sent.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */


}
