<?php
/**
 * Created by PhpStorm.
 * User: MIhaâ
 * Date: 4/8/14
 * Time: 10:58 AM
 */

class NewNotificationsEventHandler {

    CONST EVENT = 'notification.update';
    CONST CHANNEL = 'notification.update';

    public function handle($data)
    {
        $redis = Redis::connection();
       // $redis->get('user','Admin');
        //redisClient.get('user:' + 'Admin', function (err, socketId) {
          //  client(socketId).emit(channel, message.text);
        //});
        //$redis->set('name', 'Admin');
        //die(print_r($data['users']));

        if (count($data['users'])>=1)
        foreach($data['users'] as $user)
        $redis->publish(self::CHANNEL.'.'.$user, $data['text']);
    }
}