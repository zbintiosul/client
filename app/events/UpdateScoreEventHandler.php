<?php
/**
 * Created by PhpStorm.
 * User: MIhaâ
 * Date: 4/8/14
 * Time: 10:58 AM
 */

class UpdateScoreEventHandler {

    CONST EVENT = 'score.update';
    CONST CHANNEL = 'score.update';

    public function handle($data)
    {
        $redis = Redis::connection();
        $redis->set('name', 'Admin');
        $redis->publish(self::CHANNEL, $data);
    }
}