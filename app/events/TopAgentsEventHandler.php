<?php
/**
 * Created by PhpStorm.
 * User: MIhaâ
 * Date: 4/8/14
 * Time: 10:58 AM
 */

class TopAgentsEventHandler {

    CONST EVENT = 'topAgents.update';
    CONST CHANNEL = 'topAgents.update';

    public function handle($data)
    {
        $redis = Redis::connection();
        $redis->publish(self::CHANNEL, $data);
    }
}