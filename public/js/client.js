
function loading(action)
{
    if(action=='show')
    {
        $('body').append('<div id="loadingDiv" style="position:fixed; z-index: 10000; width: 100%; top:0; height:100%;  background-color: #FFFFFF; opacity: 0.6; filter: alpha(opacity = 60);"><div style="height: 100%; width: 80px; margin: auto; background: url( \'/img/loading.gif\' ); background-repeat: no-repeat; background-position: center center;"></div></div>');
        $('#loadingDiv').show();
    }
    else
    {   $('#loadingDiv').remove();
        $('#loadingDiv').hide();
    }
}


function loadingLocal(action, container)
{
    if(action=='show')
    {
        $(container).append('<div id="loadingDiv" style="position:absolute; z-index: 10000; width: 100%; top:0; height:100%;  background-color: #FFFFFF; opacity: 0.6; filter: alpha(opacity = 60);"><div style="height: 100%; width: 80px; margin: auto; background: url( \'/img/loading.gif\' ); background-repeat: no-repeat; background-position: center center;"></div></div>');
        $('#loadingDiv').show();
    }
    else
    {   $('#loadingDiv').remove();
        $('#loadingDiv').hide();
    }
}

function getCurentUrl()
{
    var urlCurent = document.URL;
    return urlCurent.split("#")[1];
}
/*
* LOAD SCRIPTS
* Usage:
* Define function = myPrettyCode ()...
    * loadScript("js/my_lovely_script.js", myPrettyCode);
*/
var jsArray = {};

function loadScript(scriptName, callback) {

    if (!jsArray[scriptName]) {
        jsArray[scriptName] = true;

        // adding the script tag to the head as suggested before
        var body = document.getElementsByTagName('body')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = scriptName;

        // then bind the event to the callback function
        // there are several events for cross browser compatibility
        //script.onreadystatechange = callback;
        script.onload = callback;

        // fire the loading
        body.appendChild(script);

    } else if (callback) {// changed else to else if(callback)
        //console.log("JS file already added!");
        //execute function
        callback();
    }

}



function  ajaxLoadPart(part,url,id,callback)
{
    loadingLocal('show', '#'+part);
    //History.pushState(null, null, url);
    //var State = History.getState(); // Note: We are using History.getState() instead of event.state
    //alert(State.url);
    $.ajax({
        url:url,
        async: false,
        success:function(msg){
            $('#'+part).html($($.parseHTML(msg)).find('#'+part).html());
            loadingLocal('hide', '#'+part);
            //$('#'+part).tab('show');
            //docReady();
            if (id!=null)
            {
                location.hash = id;
            }
            //alert('d');
            if (callback!=null)
            {
                callback();
            }

        }
    });
    //callback();
}

function getModal(url,id_modal)
{
    if (!!$(id_modal))
        $(id_modal).remove();
    loading('show');
    $.ajax({
        type: "GET",
        url: url
    }).done(function(msg) {
        loading('hide');
        $('body').append(msg);
        $(id_modal).modal('show')
        })
      .fail(function() {
            $.smallBox({
                title : "Ошибка!",
                content : "Во время выполнения задачи произошла ошибка, повторите попытку позже или сообщите об ошибке администратору!",
                color : "red",
                iconSmall : "fa fa-times bounce animated",
                timeout : 4000
            });
            loading('hide');
         });
}

function getTools(url,id_modal)
{
    if (!$(id_modal))
        $(id_modal).remove();
    loading('show');
    $.ajax({
        type: "GET",
        url: url
    }).done(function(msg) {
        loading('hide');
        $('body').append(msg);
        $(id_modal).show('slow')
    })
        .fail(function() {
            $.smallBox({
                title : "Ошибка!",
                content : "Во время выполнения задачи произошла ошибка, повторите попытку позже или сообщите об ошибке администратору!",
                color : "red",
                iconSmall : "fa fa-times bounce animated",
                timeout : 4000
            });
            loading('hide');
        });
}

function checkAll(elements)
{
    if ($('#checkAll').prop('checked'))
    {
        $(elements).prop('checked','checked');
    }else
    {
        $(elements).prop('checked','');
    }
}

   /* function updateClock ()
    {
      var currentTime = new Date ( );

      var currentHours = currentTime.getHours ( );
      var currentMinutes = currentTime.getMinutes ( );
      var currentSeconds = currentTime.getSeconds ( );

      // Pad the minutes and seconds with leading zeros, if required
      currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
      currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

      // Choose either "AM" or "PM" as appropriate
      var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

      // Convert the hours component to 12-hour format if needed
      currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

      // Convert an hours component of "0" to "12"
      currentHours = ( currentHours == 0 ) ? 12 : currentHours;

      // Compose the string for display
      var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

      // Update the time display
      document.getElementById("clock").firstChild.nodeValue = currentTimeString;
    }*/

$( document ).ready(function() {

    if( $('#clock').length )         // use this if you are using id to check
    {
        setInterval(function updateClock() {
            var monthNames = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];
            var weekNames = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"];
            var currentTime = new Date();
            var currentWeekDay = weekNames[currentTime.getDay()];
            var currentDay = currentTime.getDate();
            var currentMonth = monthNames[currentTime.getMonth()];
            var currentYear = currentTime.getFullYear();
            var currentHours = currentTime.getHours();
            var currentMinutes = currentTime.getMinutes();
            var currentSeconds = currentTime.getSeconds();
            currentHours = (currentHours < 10 ? "0" : "") + currentHours;
            currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
            currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;
           // var timeOfDay = (currentHours < 12) ? "AM" : "PM";
           // currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;
           // currentHours = (currentHours == 0) ? 12 : currentHours;

            var md = calcTime('+4');
            var mcurrentHours = md.getHours();
            var mcurrentMinutes = md.getMinutes();
            var mcurrentSeconds = md.getSeconds();
            mcurrentHours = (mcurrentHours < 10 ? "0" : "") + mcurrentHours;
            mcurrentMinutes = (mcurrentMinutes < 10 ? "0" : "") + mcurrentMinutes;
            mcurrentSeconds = (mcurrentSeconds < 10 ? "0" : "") + mcurrentSeconds;

            var currentTimeString = currentWeekDay + ", " + currentDay +" "+currentMonth + " "+ currentYear + ", " + currentHours + ":" + currentMinutes + ":" + currentSeconds +" MSK:" + mcurrentHours + ":" + mcurrentMinutes + ":" + mcurrentSeconds;
            document.getElementById("clock").firstChild.nodeValue = currentTimeString;
            delete currentTime;
        }, 1000);
            // it exists
    }


    setInterval(function updateClock() {

    }, 50000);
});

function calcTime(offset) {
    // create Date object for current location
    var d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*offset));

    // return time as a string
    return nd;
}

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Пожалуйста, проверьте ваш ввод."
);

// add the rule here
$.validator.addMethod(
    "valueNotEquals",
    function(value, element, arg){
    return arg != value;
    },
    "Значение не должна быть равна аргументу."
);

$.validator.addMethod(
    "australianDate",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
    },
    "Пожалуйста, проверьте ваш ввод, dd-mm-yyyy."
);

$.validator.addMethod(
    "isTime",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\:\d\d?$/);
    },
    "Пожалуйста, проверьте ваш ввод, hh:mm."
);

function Popup(data)
{
    var mywindow = window.open('', 'Заявка', 'width=600');
    mywindow.document.write('<html><head><title>Заявка на страхование</title>');
    mywindow.document.write('<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/css/smartadmin-production_unminified.css" type="text/css" />');
    mywindow.document.write('<link rel="stylesheet" href="/css/smartadmin-skins.css" type="text/css" />');

    mywindow.document.write('<link rel="stylesheet" href="/css/client_style.css" type="text/css" />');
    mywindow.document.write('</head><body style="padding: 5px;" id="printBody">');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.focus();
    mywindow.print();
    /*if (navigator.appName == 'Microsoft Internet Explorer')
    {

    }
    else
    {

        //if(mywindow.opener && !mywindow.opener.closed)
        //{mywindow.close();}
    }*/
    return true;
}

$(document).ready(function() {

    // NewClients
    // ajax drop
    $('#newClients').click(function(e) {
        var $this = $(this);

        //if ($this.find('.badge').hasClass('bg-color-red')) {
           // $this.find('.badge').removeClassPrefix('bg-color-');
            //$this.find('.badge').text("0");
            // console.log("Ajax call for activity")
        //}

        if (!$this.next('.ajax-dropdown').is(':visible')) {
            $this.next('.ajax-dropdown').fadeIn(150);
            $this.addClass('active');
        } else {
            $this.next('.ajax-dropdown').fadeOut(150);
            $this.removeClass('active')
        }

        var mytest = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');
        //console.log(mytest)

        e.preventDefault();
    });


    $('input[name="newClients"]').change(function() {
        if($(this).is(':checked')){
            //alert($(this).val())
            var $this = $(this);

            url = $this.attr('id');
            container = $('.ajax-new-clients');

            loadURL(url, container);
            $('#footerNewClients').show();
        } else {
            $('.ajax-new-clients').html('<div class="alert alert-transparent">'
                +'<h4>Нажмите кнопку, чтобы показывать клиентов</h4>'
                +'Это пустое сообщение страницы помогает вам защитить вашу частную жизнь.'
                +'</div>'
                +'<i class="fa fa-lock fa-4x fa-border"></i>');
            $('#footerNewClients').hide();
        }
    });

    $('#refreshNewClients').click(function() {
        //alert($(this).val())
        var $this = $('input[name="newClients"]');

        url = $this.attr('id');
        container = $('.ajax-new-clients');

        loadURL(url, container);
    });

    // NOTIFICATION IS PRESENT

    function new_clients_check() {
        $this = $('#newClients > .badge');

        if (parseInt($this.text()) > 0) {
            $this.addClass("bg-color-red bounceIn animated")
        }
    }

    new_clients_check();
    // NewClients END

    // InProgress
    // ajax drop
    $('#inProgress').click(function(e) {
        var $this = $(this);

        //if ($this.find('.badge').hasClass('bg-color-red')) {
        // $this.find('.badge').removeClassPrefix('bg-color-');
        //$this.find('.badge').text("0");
        // console.log("Ajax call for activity")
        //}

        if (!$this.next('.ajax-dropdown').is(':visible')) {
            $this.next('.ajax-dropdown').fadeIn(150);
            $this.addClass('active');
        } else {
            $this.next('.ajax-dropdown').fadeOut(150);
            $this.removeClass('active')
        }

        var mytest = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');
        //console.log(mytest)

        e.preventDefault();
    });


    $('input[name="inProgress"]').change(function() {
        if($(this).is(':checked')){
            //alert($(this).val())
            var $this = $(this);

            url = $this.attr('id');
            container = $('.ajax-in-progress');

            loadURL(url, container);
            $('#footerInProgress').show();
        } else {
            $('.ajax-in-progress').html('<div class="alert alert-transparent">'
                +'<h4>Нажмите кнопку, чтобы показывать клиентов</h4>'
                +'Это пустое сообщение страницы помогает вам защитить вашу частную жизнь.'
                +'</div>'
                +'<i class="fa fa-lock fa-4x fa-border"></i>');
            $('#footerInProgress').hide();
        }
    });

    $('#refreshInProgress').click(function() {
        //alert($(this).val())
        var $this = $('input[name="inProgress"]');

        url = $this.attr('id');
        container = $('.ajax-in-progress');

        loadURL(url, container);
    });

    // NOTIFICATION IS PRESENT

    function in_progress_check() {
        $this = $('#inProgress > .badge');

        if (parseInt($this.text()) > 0) {
            $this.addClass("bg-color-red bounceIn animated")
        }
    }

    in_progress_check();
    // Inprogress END

    // Not Interested
    // ajax drop
    $('#notInterested').click(function(e) {
        var $this = $(this);

        //if ($this.find('.badge').hasClass('bg-color-red')) {
        // $this.find('.badge').removeClassPrefix('bg-color-');
        //$this.find('.badge').text("0");
        // console.log("Ajax call for activity")
        //}

        if (!$this.next('.ajax-dropdown').is(':visible')) {
            $this.next('.ajax-dropdown').fadeIn(150);
            $this.addClass('active');
        } else {
            $this.next('.ajax-dropdown').fadeOut(150);
            $this.removeClass('active')
        }

        var mytest = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');
        //console.log(mytest)

        e.preventDefault();
    });


    $('input[name="notInterested"]').change(function() {
        if($(this).is(':checked')){
            //alert($(this).val())
            var $this = $(this);

            url = $this.attr('id');
            container = $('.ajax-not-interested');

            loadURL(url, container);
            $('#footerNotInterested').show();
        } else {
            $('.ajax-not-interested').html('<div class="alert alert-transparent">'
                +'<h4>Нажмите кнопку, чтобы показывать клиентов</h4>'
                +'Это пустое сообщение страницы помогает вам защитить вашу частную жизнь.'
                +'</div>'
                +'<i class="fa fa-lock fa-4x fa-border"></i>');
            $('#footerNotInterested').hide();
        }
    });

    $('#refreshNotInterested').click(function() {
        //alert($(this).val())
        var $this = $('input[name="notInterested"]');

        url = $this.attr('id');
        container = $('.ajax-not-interested');

        loadURL(url, container);
    });


    // NOTIFICATION IS PRESENT

    function not_interested_check() {
        $this = $('#notInterested > .badge');

        if (parseInt($this.text()) > 0) {
            $this.addClass("bg-color-red bounceIn animated")
        }
    }

    not_interested_check();
    // Not Interested END
});

function runTypeAhead(id,repos)
{
    $(id).typeahead(null, {
        name: 'repos',
        source: repos.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
                '<p class="repo-name">{{username}}</p>',
                '<p class="repo-description">{{email}}</p>'
            ].join(''))
        }
    });
}

function URLInjectionHelper (Url,key,value)
{   var newurl = '';
    if (key!='page')
    {        if (Url.indexOf('page')>0)
    {
        st = 'page=';
        ind1 = Url.lastIndexOf(st);
        to1 = Url.indexOf('&',ind1);
        if (to1==-1)
        {
            ss = Url.substring(ind1,Url.length);
        }
        else
        {
            ss = Url.substring(ind1,to1);
        }
        Url = Url.replace(ss,'page=1');
        //alert(ss);
    }
    }
    var search = String.fromCharCode(63);
    //alert(Url.search(search));
    if (Url.indexOf(search)>0)
    {    //alert(1);
        if (Url.indexOf(key)>0)
        {  str = key+'=';
            ind = Url.lastIndexOf(str);
            to = Url.indexOf('&',ind);
            if (to==-1)
            {
                urlval = Url.substring(ind,Url.length);
            }
            else
            {
                urlval = Url.substring(ind,to);
            }
            //alert(str);
            //alert(ind+' '+to)
            //alert(urlval);
            //alert(Url);
            newurl = Url.replace(urlval,key+'='+value);

        } else
        {
            //alert('3');
            newurl = Url+'&'+key+'='+value;
        }
    }else
    {
        //alert('2');
        newurl = Url+'?'+key+'='+value;
    }
    return  newurl;//window.location.href = newurl;
}
